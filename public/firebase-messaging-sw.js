importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('../firebase-messaging-sw.js')
        .then(function (registration) {
            console.log('Registration successful, scope is:', registration.scope);
        })
        .catch(function (err) {
            console.log('Service worker registration failed, error:', err);
        });
}

firebase.initializeApp({
    apiKey: "AIzaSyDUjoj2bXCmfIBGOL14fmPY3222kOnA1dc",
    authDomain: "doctoroh-536e0.firebaseapp.com",
    databaseURL: "https://doctoroh-536e0.firebaseio.com",
    projectId: "doctoroh-536e0",
    storageBucket: "doctoroh-536e0.appspot.com",
    messagingSenderId: "346671816292",
    appId: "1:346671816292:web:64ead678d81d72ef29c971",
    measurementId: "G-E5MGEZ370S"
});

const initMessaging = firebase.messaging();