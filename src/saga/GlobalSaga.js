import {
    ActionListCity, ActionYieldListCity
} from "../actions/ActionType";
import {ModelListProvince} from "../models/ModelCommon";
import {put, call, takeLatest} from "redux-saga/effects";

function* City() {
    try {
        const fetchCity = yield () => ModelListProvince().then(resultFt => {
            return resultFt;
        }).catch(error => { return error; });
        const resCity = yield call(fetchCity);
        if (resCity) {
            if (resCity.message || resCity.description) {
                yield put({
                    type: ActionYieldListCity,
                    data: ""
                });
            } else {
                const dataCity = resCity.data.one_health_msg;
                yield put({
                    type: ActionYieldListCity,
                    data: dataCity
                });
            }
        } else {
            yield put({
                type: ActionYieldListCity,
                data: ""
            });
        }
    } catch(error) {
        yield put({
            type: ActionYieldListCity,
            data: ""
        });
    }
}

export function* WatchGlobal() {
    yield takeLatest(ActionListCity, City);
}