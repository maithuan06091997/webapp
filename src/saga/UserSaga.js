import {
  ActionSignIn,
  ActionSignInSuccess,
  ActionSignInFailed,
  ActionRegister,
  ActionRecoverPassword,
} from "../actions/ActionType";
import md5 from "md5";
import {
  ModelSignIn,
  ModelRegister,
  ModelRecoverPassword,
} from "../models/ModelUser";
import { msg_text } from "../constants/Message";
import { put, call, takeLatest } from "redux-saga/effects";

function* SignIn(action) {
  try {
    const params = action.data;
    const reqParamFt = {
      username: params.phone,
      password: md5(params.password),
    };
    const fetchSignIn = yield () =>
      ModelSignIn(reqParamFt)
        .then((resultFt) => {
          return resultFt;
        })
        .catch((error) => {
          return error;
        });
    const resSignIn = yield call(fetchSignIn);
    if (resSignIn) {
      if (resSignIn.message || resSignIn.description) {
        yield put({
          type: ActionSignInFailed,
          message: resSignIn.message
            ? resSignIn.message
            : resSignIn.description,
        });
      } else {
        const dataUser = resSignIn.data.one_health_msg;
        yield put({
          type: ActionSignInSuccess,
          data: dataUser,
        });
      }
    } else {
      yield put({
        type: ActionSignInFailed,
        message: msg_text.disconnect,
      });
    }
  } catch (error) {
    yield put({
      type: ActionSignInFailed,
      message: error.message ? error.message : error.name,
    });
  }
}

function* RecoverPassword(action) {
  try {
    const params = action.data;
    const reqParamFt = {
      id: params.code,
      phone: params.phone,
      password: md5(params.password),
    };
    const fetchNewPass = yield () =>
      ModelRecoverPassword(reqParamFt)
        .then((resultFt) => {
          return resultFt;
        })
        .catch((error) => {
          return error;
        });
    const resNewPass = yield call(fetchNewPass);
    if (resNewPass) {
      if (resNewPass.message || resNewPass.description) {
        yield put({
          type: ActionSignInFailed,
          message: resNewPass.message
            ? resNewPass.message
            : resNewPass.description,
        });
      } else {
        const dataUser = resNewPass.data.one_health_msg;
        yield put({
          type: ActionSignInSuccess,
          data: dataUser,
        });
      }
    } else {
      yield put({
        type: ActionSignInFailed,
        message: msg_text.disconnect,
      });
    }
  } catch (error) {
    yield put({
      type: ActionSignInFailed,
      message: error.message ? error.message : error.name,
    });
  }
}

function* Register(action) {
  try {
    const params = action.data;
    const reqParamFt = {
      id: params.code,
      phone: params.phone,
      password: md5(params.password),
    };
    const fetchNewPass = yield () =>
      ModelRegister(reqParamFt)
        .then((resultFt) => {
          return resultFt;
        })
        .catch((error) => {
          return error;
        });
    const resNewPass = yield call(fetchNewPass);
    if (resNewPass) {
      if (resNewPass.message || resNewPass.description) {
        yield put({
          type: ActionSignInFailed,
          message: resNewPass.message
            ? resNewPass.message
            : resNewPass.description,
        });
      } else {
        const dataUser = resNewPass.data.one_health_msg;
        yield put({
          type: ActionSignInSuccess,
          data: dataUser,
        });
      }
    } else {
      yield put({
        type: ActionSignInFailed,
        message: msg_text.disconnect,
      });
    }
  } catch (error) {
    yield put({
      type: ActionSignInFailed,
      message: error.message ? error.message : error.name,
    });
  }
}

export function* WatchUser() {
  yield takeLatest(ActionSignIn, SignIn);
  yield takeLatest(ActionRegister, Register);
  yield takeLatest(ActionRecoverPassword, RecoverPassword);
}
