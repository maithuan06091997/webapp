import {
    ActionBalanceUser, ActionYieldBalanceUser
} from "../actions/ActionType";
import {ModelBalanceAccount} from "../models/ModelUser";
import {put, call, takeLatest} from "redux-saga/effects";

function* BalanceUser() {
    try {
        const storageUser = JSON.parse(localStorage.getItem("user"));
        const reqParamFt = {
            user: {
                id: storageUser.personal._id
            }
        };
        const fetchBalance = yield () => ModelBalanceAccount(reqParamFt).then(resultFt => {
            return resultFt;
        }).catch(error => { return error; });
        const resBalance = yield call(fetchBalance);
        if (resBalance) {
            if (resBalance.message || resBalance.description) {
                yield put({
                    type: ActionYieldBalanceUser,
                    data: resBalance
                });
            } else {
                const dataBalance = resBalance.data.one_health_msg[0];
                yield put({
                    type: ActionYieldBalanceUser,
                    data: dataBalance
                });
            }
        }  else {
            yield put({
                type: ActionYieldBalanceUser,
                data: ""
            });
        }
    } catch(error) {
        yield put({
            type: ActionYieldBalanceUser,
            data: ""
        });
    }
}

export function* WatchBalance() {
    yield takeLatest(ActionBalanceUser, BalanceUser);
}