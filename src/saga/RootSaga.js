import {all} from "redux-saga/effects";
import {WatchUser} from "./UserSaga";
import {WatchNotification} from "./NotificationSaga";
import {WatchBalance} from "./BalanceSaga";
import {WatchGlobal} from "./GlobalSaga";

export default function* rootSaga(){
    yield all([
        WatchUser(),
        WatchNotification(),
        WatchBalance(),
        WatchGlobal()
    ]);
}
