import {
    ActionBadgeNotification, ActionYieldNotification
} from "../actions/ActionType";
import {ModelGetBadgeNotification} from "../models/ModelNotification";
import {put, call, takeLatest} from "redux-saga/effects";

function* Notification() {
    try {
        const fetchNotify = yield () => ModelGetBadgeNotification(null).then(resultFt => {
            return resultFt;
        }).catch(error => { return error; });
        const resNotify = yield call(fetchNotify);
        if (resNotify) {
            if (resNotify.message || resNotify.description) {
                yield put({
                    type: ActionYieldNotification,
                    data: ""
                });
            } else {
                const dataNotify = resNotify.data.one_health_msg;
                yield put({
                    type: ActionYieldNotification,
                    data: dataNotify
                });
            }
        }  else {
            yield put({
                type: ActionYieldNotification,
                data: ""
            });
        }
    } catch(error) {
        yield put({
            type: ActionYieldNotification,
            data: ""
        });
    }
}

export function* WatchNotification() {
    yield takeLatest(ActionBadgeNotification, Notification);
}