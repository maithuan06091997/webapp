import Emitter from './Emitter';
import { setting } from '../../api/Config';

// const STUN = {
//   urls: 'stun:stun.l.google.com:19302'
// };

var TURN = {
  urls: setting.turnUrl,
  credential: setting.turnCredential,
  username: setting.turnUsername
};

const PC_CONFIG = {
  iceServers: [TURN]
};

class PeerConnection extends Emitter {
  constructor(socket, socketId, localStream) {
    super();
    this.pc = new RTCPeerConnection(PC_CONFIG);

    this.pc.onicecandidate = (event) => {
      if (event.candidate) {
        socket.emit('send-from-client', { 'to': socketId, 'candidate': event.candidate });
      }
    }

    // this.pc.onaddstream = (event) => {
    //   console.log("onaddstream", event);
    //   this.emit('peerStream', event.stream);
    // }

    this.pc.ontrack = (event) => {
      console.log("ontrack", event.streams);
      if (event.streams && event.streams[0]) {
        this.emit('peerStream', event.streams[0]);
      }
    }

    this.pc.oniceconnectionstatechange = (event) => {
      this.emit("changeConnectionStatus", event.target.iceConnectionState);
    };

    // this.pc.onnegotiationneeded = () => {
    //   this.createOffer()
    // }

    localStream.getTracks().forEach((track) => {
      this.pc.addTrack(track, localStream);
    });
    // this.pc.addStream(localStream);
    this.socketId = socketId;
    this.socket = socket;
  }

  start() {
    return this;
  }

  /**
   * Stop the call
   * @param {Boolean} isStarter
   */
  stop(isStarter) {
    if (isStarter) {
      this.socket.emit('leave', { to: this.socketId });
    }
    this.pc.close();
    this.pc = null;
    this.off();
    return this;
  }

  createOffer() {
    this.pc.createOffer()
      .then(this.getDescription.bind(this))
      .catch((err) => console.log(err));
    return this;
  }

  createAnswer() {
    this.pc.createAnswer()
      .then(this.getDescription.bind(this))
      .catch((err) => console.log(err));
    return this;
  }

  getDescription(desc) {
    this.pc.setLocalDescription(desc);
    this.socket.emit('send-from-client', { 'to': this.socketId, 'sdp': desc });
    return this;
  }

  /**
   * @param {Object} sdp - Session description
   */
  setRemoteDescription(sdp) {
    const rtcSdp = new RTCSessionDescription(sdp);
    this.pc.setRemoteDescription(rtcSdp);
    return this;
  }

  /**
   * @param {Object} candidate - ICE Candidate
   */
  addIceCandidate(candidate) {
    if (candidate) {
      const iceCandidate = new RTCIceCandidate(candidate);
      this.pc.addIceCandidate(iceCandidate);
    }
    return this;
  }
}

export default PeerConnection;
