import {
  dataGender,
  dataRelationship,
  dataStatusAppointment,
  dataStatusVideoCall,
  dataTypePayment,
} from "../constants/Data";
import moment from "moment";
const crypto = require("crypto");
const uniqid = require("uniqid");
const key = Buffer.from(
  "e7b7316b33f73fd1c41fcbff9ab09cb0c83b8c91040f0832e8ef454d1099bba4",
  "hex"
);
const iv = Buffer.from("b52db8bbb062d6224c3ed44eb9726e60", "hex");

export function encryptParam(q) {
  const cipher = crypto.createCipheriv("aes-256-cbc", key, iv);
  let encrypted = cipher.update(q);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return encrypted.toString("hex");
}

export function decryptParam(q) {
  let plaintext;
  let decrypted;
  if (q) {
    try {
      const encryptedText = Buffer.from(q, "hex");
      const decipher = crypto.createDecipheriv("aes-256-cbc", key, iv);
      decrypted = decipher.update(encryptedText);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
      plaintext = decrypted.toString();
    } catch (e) {
      plaintext = false;
    }
  } else {
    plaintext = false;
  }
  return plaintext;
}

export function generatorKey() {
  const time = Math.round(new Date().getTime() / 1000);
  return uniqid.process(time + "_", "_") + uniqid();
}

export function removeAccents(str) {
  return str
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/đ/g, "d")
    .replace(/Đ/g, "D");
}

export function ucFirst(str) {
  if (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }
  return null;
}

export function checkSpecialChar(str) {
  const regex = /[`!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?~]/;
  return regex.test(str);
}

export function formatDate(str, max) {
  if (str.charAt(0) !== "0" || str === "00") {
    let num = parseInt(str);
    if (isNaN(num) || num <= 0 || num > max) num = "";
    str =
      num > parseInt(max.toString().charAt(0)) && num.toString().length === 1
        ? "0" + num
        : num.toString();
  }
  return str;
}

export function formatISODatetime(str) {
  if (str) {
    const _str = str.replace(/ /g, "");
    const [date, month, year] = _str.split("/");
    const datetime = year + "-" + month + "-" + date;
    return moment(datetime).utc(true).toISOString();
  }
  return null;
}

export function formatDatetimeToString(str) {
  if (str) {
    return moment(str).format("DD / MM / yyyy");
  }
  return "";
}

export function isValidPhone(number) {
  const regex = /^0+/;
  let phone;
  if (regex.test(number)) {
    phone = number;
  } else {
    phone = "0" + number;
  }
  return phone.toString().substring(0, 11);
}

export function isValidDate(day, month, year) {
  const _day = parseInt(day);
  const _month = parseInt(month);
  const _year = parseInt(year);
  const isLeap = _year % 4 === 0 && (_year % 100 !== 0 || _year % 400 === 0);
  const tmp = [31, isLeap ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][
    _month - 1
  ];
  return _day <= tmp;
}

export function yieldBirthdayFollowHis(str) {
  let strBD = str ? str : "";
  let newBD = "";
  if (strBD && strBD.length === 10) {
    let arrTemp = strBD.split(/[-/]+/); // split '-' or '/'
    if (arrTemp.length === 3) {
      // hong_duc, drkhoa : 1955-05-22
      if (arrTemp[2] === "00") {
        newBD = "01";
      } else {
        newBD = arrTemp[2];
      }
      if (arrTemp[1] === "00") {
        newBD += "/01";
      } else {
        newBD += "/" + arrTemp[1];
      }
      newBD += "/" + arrTemp[0];
    } else {
      // nhi_dong : 28/06/2018
      newBD = strBD;
    }
  } else if (strBD && strBD.length === 4) {
    // ung buou: 1979
    newBD = strBD;
  }
  return newBD;
}

export function yieldValueDate(str) {
  let input = str;
  if (/\D\/$/.test(input)) {
    input = input.substr(0, input.length - 3);
  }
  const values = input.split("/").map(function (v) {
    return v.replace(/\D/g, "");
  });
  if (values[0]) {
    if (values[0].length === 1 || values[0].length === 3) {
      const tmp = values[0].substr(0, 2);
      values[0] = formatDate(tmp, 31);
      values[1] = "";
      values[2] = "";
    } else {
      values[0] = formatDate(values[0], 31);
    }
  }
  if (values[1]) {
    if (values[1].length === 1 || values[1].length === 3) {
      const tmp = values[1].substr(0, 2);
      values[1] = formatDate(tmp, 12);
      values[2] = "";
    } else {
      values[1] = formatDate(values[1], 12);
    }
  }
  const output = values.map(function (v, i) {
    return v.length === 2 && i < 2 ? v + " / " : v;
  });
  return output.join("").substr(0, 14);
}

export function yieldCoverPhone(str) {
  let result = "";
  if (str) {
    const threeTop = str.slice(0, 3);
    const threeBot = str.slice(-3);
    result = threeTop + "****" + threeBot;
  }
  return result;
}

export function yieldAge(str) {
  const b = moment(str);
  let yearOld;
  if (b.isValid()) {
    yearOld = Math.floor(
      moment(new Date()).diff(moment(str, "YYYY/MM/DD"), "years", true)
    );
    if (yearOld === 0) {
      yearOld = Math.floor(
        moment(new Date()).diff(moment(str, "YYYY/MM/DD"), "months", true)
      );
      return yearOld + " tháng";
    }
    return yearOld;
  }
  return "";
}

export function yieldGraduation(year) {
  if (year && year > 0) {
    const today = new Date();
    return today.getFullYear() - year;
  }
  return "";
}

export function yieldFullInfoUser(personal) {
  const first_name = personal.first_name ? personal.first_name : "";
  const last_name = personal.last_name ? personal.last_name : "";
  const phone = personal.username;
  if (first_name && last_name) {
    const tmpLastName = last_name.split(" ");
    return {
      full_name: last_name + " " + first_name,
      last_name: tmpLastName[0],
      first_name: first_name,
      phone: phone,
    };
  } else {
    return {
      phone: phone,
    };
  }
}

export function yieldLocation(obj) {
  let full_text = "";
  if (obj) {
    if (obj.address) {
      full_text += obj.address + ", ";
    }
    if (obj.ward && obj.ward.vi_name) {
      full_text += obj.ward.vi_name + ", ";
    }
    if (obj.district && obj.district.vi_name) {
      full_text += obj.district.vi_name + ", ";
    }
    if (obj.province && obj.province.vi_name) {
      full_text += obj.province.vi_name;
    } else {
      full_text += "...";
    }
    return full_text;
  }
}

export function yieldDatetimeSchedule(start = null, end = null) {
  let datetime;
  if (end) {
    const fulldate = moment(start).format("DD/MM/YYYY");
    const _start = moment(start).format("HH:mm");
    const _end = moment(end).format("HH:mm");
    datetime = `${_start} - ${_end}, ${fulldate}`;
  } else {
    datetime = moment(start).format("HH:mm - DD/MM/YYYY");
  }
  return datetime;
}

export function yieldDatetime(iso, type = "") {
  if (type === "date") {
    return moment(iso).format("DD/MM/YYYY");
  } else {
    return moment(iso).format("HH:mm - DD/MM/YYYY");
  }
}

export function yieldDatetimeNowUTC() {
  let tempDate = new Date();
  return new Date(
    tempDate.getFullYear(),
    tempDate.getMonth(),
    tempDate.getDate(),
    0,
    0,
    0
  );
}

export function yieldScheduleInfo(obj) {
  const type = obj.type;
  const time = obj.time;
  const date = obj.date;
  const startTime = obj.start_time;
  const endTime = obj.end_time;
  let subName = obj.title;
  let address = obj.address;
  let datetime;

  if (date && time) {
    datetime = `${time} - ${moment(date).format("DD/MM/YYYY")}`;
  }

  if (startTime && endTime) {
    const fulldate = moment(startTime).format("DD/MM/YYYY");
    const start = moment(startTime).format("HH:mm");
    const end = moment(endTime).format("HH:mm");
    datetime = `${start} - ${end}, ${fulldate}`;
  }

  if (type === "appt_hospital") {
    subName = obj.hospital_name;
  }

  if (type === "call_video_schedule") {
    subName = obj.doctor.last_name + " " + obj.doctor.first_name;
    address = obj.title;
  }

  return {
    subName: subName,
    address: address,
    datetime: datetime,
  };
}

export function yieldCardUsed(element) {
  const type = element.cardType;
  let data;
  switch (type) {
    case "visa_card":
      data = {
        bankName: "Visa",
        logo: "/wallet/logo_visa.png",
      };
      break;
    case "master_card":
      data = {
        bankName: "Mastercard",
        logo: "/wallet/logo_master.png",
      };
      break;
    case "japan_credit_card":
      data = {
        bankName: "JCB",
        logo: "/wallet/logo_jcb.png",
      };
      break;
    case "american_eagle_card":
      data = {
        bankName: "American Express",
        logo: "/wallet/logo_amex.png",
      };
      break;
    case "unionpay_credit_card":
      data = {
        bankName: "UnionPay",
        logo: "/wallet/logo_unionpay.png",
      };
      break;
    default:
      data = {
        bankName: element.bankName,
        logo: "/wallet/logo_atm.png",
      };
  }
  return data;
}

export function yieldIconSpecialist(code) {
  switch (code) {
    case "001": // khoa noi tong quat
      return "stomach.png";
    case "002": // khoa mat
      return "eye.png";
    case "003": // khoa ngoai tong quat
      return "medical.png";
    case "004": // khoa nhi
      return "baby.png";
    case "005": // khoa ung buou
      return "bacteria.png";
    case "006": // khoa than kinh
      return "brain.png";
    case "007": // khoa chuan doan anh chuc nang
      return "records.png";
    case "008": // khoa tham my
      return "beauty.png";
    case "009": // khoa vat ly tri lieu
      return "exercise.png";
    case "010": // khoa dot quy
      return "heart.png";
    case "011": // khoa phau thuat gay me
      return "scalpel.png";
    case "012": // khoa san
      return "pregnancy.png";
    case "013": // khoa dong y
      return "mortar.png";
    case "014": // khoa tai mui hong
      return "ear.png";
    case "015": // khoa rang ham mat
      return "tooth.png";
    case "016": // khoa duoc
      return "pharmacy.png";
    case "017": // khoa cap cuu
      return "ambulance.png";
    case "018": // khoa kham benh
      return "stethoscope.png";
    case "019": // khoa chan thuong chinh hinh
      return "bone.png";
    case "020": // khoa xet nghiem
      return "test-tubes.png";
    case "021": // khoa da lieu
      return "hair.png";
    case "022": // khoa loc than
      return "kidney.png";
    case "023": // khoa phong benh
      return "patient.png";
    case "024": // khoa tam ly
      return "brain3.png";
    case "11111": // khoa tam than
      return "brain2.png";
    case "1505351994129": // Bac si nha toi
      return "droh.png";
    default:
      return "droh.png";
  }
}

export function findKeyNameHospital(str) {
  let key;
  switch (str) {
    case "hong_duc":
      key = "his_profile";
      break;
    case "ung_buou":
      key = "ung_buou";
      break;
    case "nhi_dong":
      key = "nhi_dong";
      break;
    case "drkhoa":
      key = "drkhoa";
      break;
    default:
      key = "";
  }
  return key;
}

export function findObjectPatientCodeHospital(str, code) {
  let obj;
  switch (str) {
    case "hong_duc":
      obj = { his_profile: { patient_code: code } };
      break;
    case "ung_buou":
      obj = { ung_buou: { patient_code: code } };
      break;
    case "nhi_dong":
      obj = { nhi_dong: { patient_code: code } };
      break;
    case "drkhoa":
      obj = { drkhoa: { patient_code: code } };
      break;
    default:
      obj = "";
  }
  return obj;
}

export function findNameHospital(str) {
  let name;
  switch (str) {
    case "hong_duc":
      name = "Hồng Đức";
      break;
    case "ung_buou":
      name = "Ung Bướu";
      break;
    case "nhi_dong":
      name = "Nhi Đồng 2";
      break;
    case "drkhoa":
      name = "Dr.Khoa";
      break;
    default:
      name = "";
  }
  return name;
}

export function findDataOfStep(form, num) {
  const index = form.findIndex((el) => {
    return el.step === num;
  });
  let result = "";
  if (index > -1) {
    result = form[index].data;
  }
  return result;
}

export function findRelationship(target, key) {
  const data = dataRelationship();
  const index = data.findIndex((el) => {
    return el.id === target || el.value === target || el.text === target;
  });
  let result = "";
  if (index > -1) {
    if (key) {
      result = data[index][key];
    }
  }
  return result;
}

export function findGender(target, key) {
  const data = dataGender();
  const index = data.findIndex((el) => {
    return el.id === target || el.value === target || el.text === target;
  });
  let result = "";
  if (index > -1) {
    if (key) {
      result = data[index][key];
    }
  }
  return result;
}

export function findStatusAppointment(target, key) {
  const data = dataStatusAppointment();
  const index = data.findIndex((el) => {
    return el.id === target || el.value === target || el.text === target;
  });
  let result = "";
  if (index > -1) {
    if (key) {
      result = data[index][key];
    }
  }
  return result;
}

export function findStatusVideoCall(target, key) {
  const data = dataStatusVideoCall();
  const index = data.findIndex((el) => {
    return el.id === target || el.value === target || el.text === target;
  });
  let result = "";
  if (index > -1) {
    if (key) {
      result = data[index][key];
    }
  }
  return result;
}

export function findTypePayment(target, key) {
  const data = dataTypePayment();
  const index = data.findIndex((el) => {
    return el.id === target || el.value === target || el.text === target;
  });
  let result = "";
  if (index > -1) {
    if (key) {
      result = data[index][key];
    }
  }
  return result;
}

export function numberToMoney(number) {
  if (!number) {
    return 0;
  }

  let array = [];
  let result = "";
  let count = 0;

  let flag1 = false;
  if (number < 0) {
    number = -number;
    flag1 = true;
  }

  let numberString = number.toString();
  if (numberString.length < 3) {
    let result = numberString;
    if (flag1) result = "-" + result;
    return result;
  }

  for (let i = numberString.length - 1; i >= 0; i--) {
    count += 1;
    if (numberString[i] === "." || numberString[i] === ",") {
      array.push(",");
      count = 0;
    } else {
      array.push(numberString[i]);
    }
    if (count === 3 && i >= 1) {
      array.push(",");
      count = 0;
    }
  }

  for (let i = array.length - 1; i >= 0; i--) {
    result += array[i];
  }

  if (flag1) result = "-" + result;

  return result;
}

export function moneyToNumber(money) {
  try {
    if (money) {
      let moneyString = money
        .replaceAll(",", "")
        .replaceAll("đ", "")
        .replaceAll(".", "")
        .replaceAll(" ", "");
      let number = Number(moneyString);
      if (isNaN(number)) {
        return 0;
      }
      return number;
    } else {
      return money;
    }
  } catch (error) {
    return money;
  }
}

export function isoDateToString(isoDate, format) {
  let temp = new Date(isoDate);
  return moment(temp).format(format);
}

export const renderStatus = (status) => {
  let statusText = "";
  let statusColor = "";
  switch (status) {
    case 1:
      statusText = "Chờ xác nhận";
      statusColor = "color-green";
      break;
    case 2:
      statusText = "Xác nhận";
      statusColor = "color-sky";
      break;
    case 3:
      statusText = "Hủy từ người dùng";
      statusColor = "color-red";
      break;
    case 4:
      statusText = "Hủy từ hệ thống";
      statusColor = "color-red";
      break;
    case 5:
      statusText = "Hoàn thành";
      statusColor = "color-purpil";
      break;
    default:
      break;
  }
  return <span className={statusColor}>{statusText} </span>;
};

export function getFullName(data) {
  if (data !== null && data !== "") {
    let full_name =
      (data.last_name ? data.last_name + " " : "") +
      (data.first_name ? data.first_name : "");
    return full_name.trim();
  } else {
    return "";
  }
}
function padding(input) {
  return (input > 9 ? input : "0" + input) + "";
}

export function getCountDateToNow(dateString) {
  if (dateString != null) {
    let date = new Date(dateString);
    var dateNow = new Date();
    var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds

    var diff = Math.round(
      Math.abs((dateNow.getTime() - date.getTime()) / oneDay)
    );

    if (diff === 0) {
      return padding(date.getHours()) + ":" + padding(date.getMinutes());
    } else if (diff < 30) {
      return parseInt(diff) + " ngày";
    } else if ((diff) => 30 && diff < 365) {
      return parseInt(diff / 30) + " tháng";
    } else {
      return parseInt(diff / 365) + " năm";
    }
  }
  return null;
}

export function getFullName_2(first_name, last_name) {
  let patientName =
    (last_name ? last_name + " " : "") + (first_name ? first_name : "");
  return patientName.trim();
}

export function secondToMinute(second) {
  let min = second / 60;
  let sec = second % 60;
  return min + ":" + sec;
}

export function getAge(birthday) {
  let today = new Date();
  let birthDate = new Date(birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  if (age <= 0) {
    let month = today.getUTCMonth() - birthDate.getUTCMonth();
    if (month <= 0) {
      let day = today.getUTCDate() - birthDate.getUTCDate();
      return day + " ngày tuổi";
    } else {
      return month + " tháng tuổi";
    }
  } else {
    return age + " tuổi";
  }
}

export function getSexName(value) {
  let name = "Nữ";
  if (value && (value === 1 || value === "male")) {
    name = "Nam";
  }
  return name;
}

export function getSocialAvatar(url) {
  let uriAvatar = url;
  if (uriAvatar && uriAvatar !== "") {
    let n = uriAvatar.includes("profile");
    if (n === false) {
      uriAvatar = "";
    }
  }
  return uriAvatar;
}

export function formatTimeSocial(date) {
  let currentDate = new Date();
  let compareDate = new Date(date);
  if (currentDate.getFullYear() === compareDate.getFullYear()) {
    if (
      currentDate.getMonth() === compareDate.getMonth() &&
      currentDate.getDate() === compareDate.getDate()
    ) {
      moment.updateLocale("en", {
        relativeTime: {
          future: "trong %s",
          past: "%s trước",
          s: "vài giây",
          ss: "%d giây",
          m: "một phút",
          mm: "%d phút",
          h: "một giờ",
          hh: "%d giờ",
          d: "một ngày",
          dd: "%d ngày",
          M: "một tháng",
          MM: "%d tháng",
          y: "một năm",
          yy: "%d năm",
        },
      });
      moment.locale("en");
      return moment(date).fromNow();
    } else {
      return moment(compareDate).format("HH:mm DD/MM/YYYY");
    }
  } else {
    return moment(compareDate).format("HH:mm DD/MM/YYYY");
  }
}

export function formatTimePeriod(date) {
  moment.updateLocale("en", {
    relativeTime: {
      future: "trong %s",
      past: "%s trước",
      s: "vài giây",
      ss: "%d giây",
      m: "một phút",
      mm: "%d phút",
      h: "một giờ",
      hh: "%d giờ",
      d: "một ngày",
      dd: "%d ngày",
      M: "một tháng",
      MM: "%d tháng",
      y: "một năm",
      yy: "%d năm",
    },
  });
  moment.locale("en");
  return moment(date).fromNow();
}

export function getDayOfWeek(date) {
  const strData = ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"];
  return strData[date.getDay()];
}

export function getStringDateFromDate(date, dateFormat) {
  let strDate = moment(date).format(dateFormat);
  return strDate;
}

export function getDayOfWeekVN(date) {
  const strData = {
    Monday: "Thứ 2",
    Tuesday: "Thứ 3",
    Wednesday: "Thứ 4",
    Thursday: "Thứ 5",
    Friday: "Thứ 6",
    Saturday: "Thứ 7",
  };
  return strData[date];
}

export function convertHourMinToSec(str) {
  const a = str.split(":");
  if (!a[2]) {
    a[2] = 0;
  }
  return +a[0] * 60 * 60 + +a[1] * 60 + +a[2];
}

export function convertTime(time, formatType = "hh:mm dd/MM/YYYY") {
  if (!time) return "";
  var date = new Date(parseInt(time));
  var strDate = date.getDate();
  var numMonth = date.getMonth() + 1;
  var strMonth = numMonth < 10 ? "0" + numMonth : numMonth;
  var strYear = date.getFullYear();
  var strHours = date.getHours();
  strHours = strHours > 9 ? strHours : "0" + strHours;
  var strMinutes = date.getMinutes();
  strMinutes = strMinutes > 9 ? strMinutes : "0" + strMinutes;
  if (formatType === "hh:mm dd/MM/YYYY")
    return (
      strHours +
      ":" +
      strMinutes +
      " " +
      strDate +
      "/" +
      strMonth +
      "/" +
      strYear
    );
  else if (formatType === "dd/MM/YYYY") {
    return strDate + "/" + strMonth + "/" + strYear;
  } else if (formatType === "dd/MM/YYYY hh:mm") {
    return (
      strDate +
      "/" +
      strMonth +
      "/" +
      strYear +
      " " +
      strHours +
      ":" +
      strMinutes
    );
  } else if (formatType === "dd/MM/YYYY - hh:mm") {
    return (
      strDate +
      "/" +
      strMonth +
      "/" +
      strYear +
      " - " +
      strHours +
      ":" +
      strMinutes
    );
  } else if (formatType === "hh:mm") {
    return strHours + ":" + strMinutes;
  } else if (formatType === "YYYYMMDDHHMM") {
    return strYear + strMonth + strDate + strHours + strMinutes;
  } else {
    return "";
  }
}

export function formatTimeCall(time) {
  if (time) {
    const minutes = parseInt(time / 60);
    const seconds = time % 60;
    return `${minutes.toLocaleString(undefined, {
      minimumIntegerDigits: 2,
    })}:${seconds.toLocaleString(undefined, { minimumIntegerDigits: 2 })}`;
  }
  return "00:00";
}

export function calculateAge(str) {
  const b = moment(str);
  if (b.isValid()) {
    return Math.floor(
      moment(new Date()).diff(moment(str, "YYYY/MM/DD"), "years", true)
    );
  }
  return "";
}

export function hasVietnamese(text) {
  var re =
    /[ĂăÂâÊêÔôƠơƯưÀàẰằẦầÈèỀềÌìÒòỒồỜờÙùỪừỲỳẢảẲẳẨẩẺẻỂểỈỉỎỏỔổỞởỦủỬửỶỷÃãẴẵẪẫẼẽỄễĨĩÕõỖỗỠỡŨũỮữỸỹÁáẮắẤấÉéẾếÍíÓóỐốỚớÚúỨứÝýẠạẶặẬậẸẹỆệỊịỌọỘộỢợỤụỰựỴỵĐđ]/i;
  return re.test(text);
}

export function formatNumberToMoney(number, defaultNum, predicate) {
  predicate = !predicate ? "" : "" + predicate;
  if (
    number === 0 ||
    number === "" ||
    number === null ||
    number === "undefined" ||
    isNaN(number) === true ||
    number === "0" ||
    number === "00" ||
    number === "000"
  )
    return "0" + predicate;

  var array = [];
  var result = "";
  var count = 0;

  if (!number) {
    return defaultNum ? defaultNum : "" + predicate;
  }

  let flag1 = false;
  if (number < 0) {
    number = -number;
    flag1 = true;
  }

  var numberString = number.toString();
  if (numberString.length < 3) {
    let result = numberString + predicate;
    if (flag1) result = "-" + result;
    return result;
  }

  for (let i = numberString.length - 1; i >= 0; i--) {
    count += 1;
    if (numberString[i] === "." || numberString[i] === ",") {
      array.push(",");
      count = 0;
    } else {
      array.push(numberString[i]);
    }
    if (count === 3 && i >= 1) {
      array.push(",");
      count = 0;
    }
  }

  for (let i = array.length - 1; i >= 0; i--) {
    result += array[i];
  }

  if (flag1) result = "-" + result;

  return result + predicate;
}
