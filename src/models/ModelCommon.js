import {setting} from "../api/Config";
import {FetchGetHeader, FetchPostHeader} from "../api/Fetch";
import {msg_text} from "../constants/Message";

export const mdlCommon= {
    check_insurrance: setting.api+"/other_hos/schedule_appointments/check-insurrance",
    firebase_token: setting.api+"/firebase-token/create-device-id",
    list_medifast: setting.api+"/medifast",
    list_province: setting.api+"/provinces",
    list_district: setting.api+"/districts/find/province",
    list_ward: setting.api+"/wards/find/district"
};

export const ModelCheckInsurrance = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlCommon.check_insurrance, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListMedifast = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlCommon.list_medifast, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListProvince = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlCommon.list_province, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelListDistrict = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlCommon.list_district, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelListWard = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlCommon.list_ward, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelFirebaseToken = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlCommon.firebase_token, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};
