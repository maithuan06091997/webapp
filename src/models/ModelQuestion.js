import { setting } from "../api/Config";
import { FetchGetHeader, FetchPostHeader } from "../api/Fetch";
import { msg_text } from "../constants/Message";

export const mdlQuestion = {
    get_questions: setting.api + "/question-answer/find?sort_field=answer_time&type=patient&answered=true",
    get_my_questions: setting.api + "/question-answer/find?",
    get_specialities: setting.api + "/specialities",
    get_question_details: setting.api + "/question-answer/",
    create_question: setting.api + "/question-answer",
    save_question_image: setting.api + "/images",
    like: setting.api + "/like",
};

export const ModelGetQuestions = (userId, page, speciality, params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(`${mdlQuestion.get_questions}&user=${userId}&page=${page}${speciality ? "&speciality=" + speciality : ""}`, { one_health_msg: params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelGetMyQuestions = (page, params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(`${mdlQuestion.get_my_questions}page=${page}`, { one_health_msg: params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelGetSpecialities = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlQuestion.get_specialities, { "showAll": "1" }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelGetQuestionDetails = (userId, questionId) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlQuestion.get_question_details + questionId, { user: userId, suggest: true }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelCreateQuestion = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlQuestion.create_question, { one_health_msg: params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelSaveQuestionImage = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlQuestion.save_question_image, { one_health_msg: params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
}

export const ModelLikeQuestion = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlQuestion.like, { one_health_msg: params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
}