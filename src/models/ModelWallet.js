import { setting } from "../api/Config";
import { msg_text } from "../constants/Message";
import { FetchGetHeader, FetchPostHeader } from "../api/Fetch";

export const mdlWallet = {
    list_card_used: setting.api_payment + "/onepay/v2/cards",
    submit_oh: setting.api + "/oh-card/cashIn",
    submit_onepay: setting.api_payment + "/onepay/v2/pay"
};

export const ModelGetListCardUsed = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlWallet.list_card_used, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelSubmitOH = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlWallet.submit_oh, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelSubmitOnePay = (params) => {
    let endpoint = mdlWallet.submit_onepay;
    if (params.verifyToken) {
        endpoint += '?verify_token=' + params.verifyToken;
    }
    return new Promise((resolve, reject) => {
        FetchPostHeader(endpoint, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};
