import {setting} from "../api/Config";
import {msg_text} from "../constants/Message";
import {FetchGetHeader, FetchPostHeader, FetchPutHeader} from "../api/Fetch";

export const mdlExamination = {
    list_hospital: setting.api+"/other_hos/sources",
    list_examination_schedule: setting.api+"/schedules/summary-list",
    list_history_examination: setting.api+"/other_hos/schedule_appointments",
    detail_examination_schedule: setting.api+"/other_hos/schedule_appointments",
    cancel_examination_schedule: setting.api+"/other_hos/schedule_appointments",
    list_special: setting.api+"/other_hos/exam_areas",
    group_special: setting.api+"/other_hos/specialities",
    exam_type: setting.api+"/other_hos/exam_types",
    list_doctor: setting.api+"/other_hos/doctors",
    list_time: setting.api+"/other_hos/times",
    check_examination_register: setting.api+"/other_hos/schedule_appointments/check-register",
    payment_wallet_examination: setting.api+"/other_hos/schedule_appointments",
    payment_debit_examination: setting.api_payment+"/onepay-order",
};

export const ModelListHospital = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_hospital, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListHistoryExamination = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_history_examination, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListExaminationSchedule = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_examination_schedule, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelDetailExaminationSchedule = (id) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.detail_examination_schedule+"/"+id, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelCancelExaminationSchedule = (id) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlExamination.cancel_examination_schedule+"/"+id+"/cancel-status", null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListSpecial = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_special, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelGroupSpecial = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.group_special, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelExamType = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.exam_type, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListDoctor = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_doctor, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelListTime = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.list_time, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelCheckExaminationRegister = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlExamination.check_examination_register, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelPaymentWalletExamination = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlExamination.payment_wallet_examination, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelPaymentDebitExamination = (params, pay_type, verify_token = "") => {
    const verifyToken = verify_token ? "?verify_token="+verify_token : "";
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlExamination.payment_debit_examination+"/"+pay_type+"/pay"+verifyToken, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};