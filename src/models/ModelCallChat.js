import { setting } from "../api/Config";
import _ from "lodash";
import { msg_text } from "../constants/Message";
import { FetchGetHeader, FetchPostHeader, FetchPutHeader } from "../api/Fetch";

export const mdlCallChat = {
  list_special: setting.api + "/video-call-schedule/speciality",
  list_doctor: setting.api + "/users/doctor",
  list_doctor_online: setting.api + "/online/get-doctor-online",
  list_schedule: setting.api + "/video-call-schedule/doctor",
  list_rating: setting.api + "/rate/user",
  list_history_call: setting.api + "/video-call-schedule/exam",
  detail_video_call_schedule: setting.api + "/video-call-schedule/exam",
  detail_doctor: setting.api + "/users",
  cancel_video_call_schedule: setting.api + "/video-call-schedule/exam",
  check_video_price: setting.api + "/video-call-schedule/exam/check-price",
  rate_video_call: setting.api + "/rate/phone",
  payment_wallet_video_call: setting.api + "/video-call-schedule/exam",
  payment_wallet_video_call1: setting.api + "/order",
  payment_wallet_record: setting.api + "/take_blood/order",
  payment_debit_video_call: setting.api_payment + "/onepay-order",
};

export const ModelListSpecial = () => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlCallChat.list_special, null, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelListDoctor = (params) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlCallChat.list_doctor, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelListDoctorOnline = () => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(mdlCallChat.list_doctor_online, null, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error && _.isEmpty(data.data.error) === false) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelListSchedule = (params, doctor_id) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(
      mdlCallChat.list_schedule + "/" + doctor_id + "/free",
      params,
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelListRating = (params, doctor_id) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(
      mdlCallChat.list_rating + "/" + doctor_id,
      params,
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelListHistoryCall = (params) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlCallChat.list_history_call, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelDetailDoctor = (params, doctor_id) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(
      mdlCallChat.detail_doctor + "/" + doctor_id,
      params,
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelDetailVideoCallSchedule = (id) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(
      mdlCallChat.detail_video_call_schedule + "/" + id,
      null,
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelCancelVideoCallSchedule = (id) => {
  return new Promise((resolve, reject) => {
    FetchPutHeader(
      mdlCallChat.cancel_video_call_schedule + "/" + id,
      { one_health_msg: { status: "cancel" } },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelCheckVideoPrice = (params) => {
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlCallChat.check_video_price, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelRateVideoCall = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlCallChat.rate_video_call,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelPaymentWalletVideoCall = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlCallChat.payment_wallet_video_call,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelPaymentWalletVideoCall1 = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlCallChat.payment_wallet_video_call1,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelPaymentWalletRecord = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlCallChat.payment_wallet_record,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelPaymentDebitVideoCall = (params, pay_type, verify_token) => {
  const verifyToken = verify_token ? "?verify_token=" + verify_token : "";
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlCallChat.payment_debit_video_call +
        "/" +
        pay_type +
        "/pay" +
        verifyToken,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error && _.isEmpty(data.data.error) === false) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};
