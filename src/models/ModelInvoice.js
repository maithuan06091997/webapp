import { setting } from "../api/Config";
import { msg_text } from "../constants/Message";
import { FetchGetHeader } from "../api/Fetch";

export const mdlInvoice = {
    list_invoice: setting.api + "/invoices/users",
    detail_invoice: setting.api + "/invoices/"
};

export const ModelGetListInvoice = (id, params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlInvoice.list_invoice+"/"+id, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelGetDetailInvoice = (params) => {
    let endpoint = mdlInvoice.detail_invoice + params.invoice;
    return new Promise((resolve, reject) => {
        FetchGetHeader(endpoint, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

