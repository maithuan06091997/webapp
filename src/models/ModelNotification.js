import { setting } from "../api/Config";
import { msg_text } from "../constants/Message";
import { FetchGetHeader, FetchPutHeader } from "../api/Fetch";

export const mdlNotification = {
    list_notification: setting.api_social + "/api/notification/list",
    read_all: setting.api_social + "/api/notification/readAll",
    clicked: setting.api_social + "/api/notification/isClick/",
    badge: setting.api_social + "/api/notification/count_click"
};

export const ModelGetBadgeNotification = (params) => {
    let endpoint = mdlNotification.badge;
    return new Promise((resolve, reject) => {
        FetchGetHeader(endpoint, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelGetListNotification = (params) => {
    let endpoint = mdlNotification.list_notification;
    return new Promise((resolve, reject) => {
        FetchGetHeader(endpoint, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelPutReadAll = (params) => {
    let endpoint = mdlNotification.read_all;
    return new Promise((resolve, reject) => {
        FetchPutHeader(endpoint, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};

export const ModelPutClicked = (params) => {
    let endpoint = mdlNotification.clicked + params.notification;
    return new Promise((resolve, reject) => {
        FetchPutHeader(endpoint, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        });
    });
};


