import {setting} from "../api/Config";
import {msg_text} from "../constants/Message";
import {FetchGetHeader, FetchPostHeader, FetchPutHeader} from "../api/Fetch";

export const mdlUser = {
    user_exist: setting.api+"/users/check-exist",
    terms: setting.api+"/terms/submit",
    sign_in: setting.api+"/users/login/patient",
    balance_account: setting.api+"/accounts/get-balance",
    new_password: setting.api+"/users/patient",
    change_password: setting.api+"/users/password",
    update_info: setting.api+"/users",
    update_avatar: setting.api+"/images",
    verify_otp: setting.api+"/verify/check",
    verify_recover_otp: setting.api+"/verify/check?type=reset",
    recover_password: setting.api+"/users/patient/reset-password",
    request_recover_password: setting.api+"/verify/reset-password",
    request_otp: setting.api+"/verify",
    request_otp_recover_password: setting.api+"/verify/reset-password"
};

export const ModelSignIn = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.sign_in, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelRegister = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.new_password, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelRecoverPassword = (params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlUser.recover_password, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelCheckUserExist = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlUser.user_exist, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelBalanceAccount = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.balance_account, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelAgreeTerms = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.terms, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelVerifyOTP = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.verify_otp, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelVerifyRecoverOTP = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.verify_recover_otp, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelRequestRecoverPassword = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.request_recover_password, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelRequestOTP = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.request_otp, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelRequestOTPRecoverPassword = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.request_otp_recover_password, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelUpdateAvatar = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlUser.update_avatar, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelUpdateUserInfo = (id, params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlUser.update_info+"/"+id, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};

export const ModelChangePass = (id, params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlUser.change_password+"/"+id, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        })
    });
};