import {setting} from "../api/Config";
import {msg_text} from "../constants/Message";
import {FetchGetHeader, FetchPutHeader} from "../api/Fetch";

export const mdlPromotion = {
    point_promotion: setting.api+"/api/pointAccount/balance",
    point_history: setting.api+"/api/pointAccount/history",
    store_promotion: setting.api+"/api/voucher/mobile/summary",
    search_promotion: setting.api+"/api/promotion/search",
    search_promotion_order_type: setting.api+"/api/promotion/v2/search",
    check_price_promotion: setting.api+"/api/promotion/v2/get-price",
    filter_promotion: setting.api+"/api/voucher/mobile/list",
    owner_gift: setting.api+"/api/exchange/owner_gifts",
    apply_promotion: setting.api+"/api/exchange/apply/promotion",
    apply_voucher: setting.api+"/api/exchange/apply/voucher",
    detail_voucher: setting.api+"/api/voucher/mobile/detail",
    exchange_voucher: setting.api+"/api/exchange",
    list_available: setting.api+"/api/promotion/v2/list-available",
};

export const ModelPointPromotion = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.point_promotion, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelPointHistory = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.point_history, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelStorePromotion = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.store_promotion, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelSearchPromotion = (code) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.search_promotion+"/"+code, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelOwnerGift = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.owner_gift, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelApplyPromotion = (params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlPromotion.apply_promotion, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelApplyVoucher = (id) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.apply_voucher+"/"+id, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelExchangeVoucher = (id) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlPromotion.exchange_voucher+"/"+id, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelDetailVoucher = (id, point) => {
    let params;
    if (point) {
        params = {
            point: point
        }
    }
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.detail_voucher+"/"+id, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelFilterPromotion = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.filter_promotion, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelListAvailable = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.list_available, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelSearchPromotionOrderType = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.search_promotion_order_type, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};

export const ModelCheckPricePromotion = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlPromotion.check_price_promotion, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({message: msg_text.not_found, status: 400});
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({message: msg_text.disconnect, status: 400});
            }
        });
    });
};