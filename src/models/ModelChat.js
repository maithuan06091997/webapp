import { setting } from "../api/Config";
import { msg_text } from "../constants/Message";
import {
  FetchGetHeader,
  FetchPostHeader,
  FetchDeleteHeader,
  // authPost,
  // authGet,
} from "../api/Fetch";

export const mdlChat = {
  list_history: setting.api + "/messenger/history",
  search_user_history: setting.api_social + "/api/profile/tag_name",
  check_update: setting.api + "/rooms/check-update",
  close_chat: setting.api + "/rooms/close",
  send_message: setting.ohDomainChat + "/new-message",
  find_user: setting.api + "/users/username/",
  delete_room: setting.api + "/messenger/room",
  list_message: setting.api + "/messenger/room",
};

// export async function getListHistory(postData) {
//   const response = await authPost(mdlChat.list_history, {
//     one_health_msg: postData,
//   });
//   return response.data;
// }

// export async function ModelSearchUser(params) {
//   const response = await authGet(mdlChat.search_user_history, params);
//   return response.data;
// }

export const ModelSearchUser = (params) => {
  // param url: ?keyword=Pull&limit=50&page=1&userType=3
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlChat.search_user_history, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelFindUser = (params) => {
  // param url: userID
  return new Promise((resolve, reject) => {
    FetchGetHeader(mdlChat.find_user, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelCheckUpdate = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlChat.check_update,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelCloseChat = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(mdlChat.close_chat, { one_health_msg: params }, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelGetListConversations = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlChat.list_history,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelGetListMessages = (page, params) => {
  let endpoint = mdlChat.list_message;
  if (page) {
    endpoint += "?page=" + page;
  }
  return new Promise((resolve, reject) => {
    FetchPostHeader(endpoint, { one_health_msg: params }, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};

export const ModelSendMessage = (params) => {
  return new Promise((resolve, reject) => {
    FetchPostHeader(
      mdlChat.send_message,
      { one_health_msg: params },
      (data) => {
        if (data && data.data) {
          if (data.status === 404) {
            reject({ message: msg_text.not_found, status: 400 });
          } else if (data.data.error) {
            let resError = data.data.error;
            if (!resError.message) {
              if (resError.description) {
                resError.message = resError.description;
              } else {
                resError.message = msg_text.connect_server_error;
              }
            }
            reject(resError);
          } else {
            resolve(data);
          }
        } else {
          reject({ message: msg_text.disconnect, status: 400 });
        }
      }
    );
  });
};

export const ModelDeleteRoom = (params) => {
  return new Promise((resolve, reject) => {
    FetchDeleteHeader(mdlChat.delete_room, params, (data) => {
      if (data && data.data) {
        if (data.status === 404) {
          reject({ message: msg_text.not_found, status: 400 });
        } else if (data.data.error) {
          let resError = data.data.error;
          if (!resError.message) {
            if (resError.description) {
              resError.message = resError.description;
            } else {
              resError.message = msg_text.connect_server_error;
            }
          }
          reject(resError);
        } else {
          resolve(data);
        }
      } else {
        reject({ message: msg_text.disconnect, status: 400 });
      }
    });
  });
};
