import {setting} from "../api/Config";
import {msg_text} from "../constants/Message";
import {FetchGetHeader, FetchPostHeader, FetchPutHeader, FetchDeleteHeader} from "../api/Fetch";

export const mdlProfile = {
    list_profile: setting.api+"/profiles",
    new_profile: setting.api+"/profiles",
    update_profile: setting.api+"/profiles",
    delete_profile: setting.api+"/profiles",
    find_patient_code: setting.api+"/profiles/check-info",
    update_patient_code: setting.api+"/profiles",
};

export const ModelListProfile = () => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlProfile.list_profile, null, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelNewProfile = (params) => {
    return new Promise((resolve, reject) => {
        FetchPostHeader(mdlProfile.new_profile, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelUpdateProfile = (id, params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlProfile.update_profile+"/"+id, { "one_health_msg": params }, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelDeleteProfile = (id) => {
    return new Promise((resolve, reject) => {
        FetchDeleteHeader(mdlProfile.delete_profile+"/"+id, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelFindPatientCode = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlProfile.find_patient_code, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelFindPatientInfo = (params) => {
    return new Promise((resolve, reject) => {
        FetchGetHeader(mdlProfile.find_patient_code, params, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};

export const ModelUpdatePatientCode = (id, params) => {
    return new Promise((resolve, reject) => {
        FetchPutHeader(mdlProfile.update_patient_code+"/"+id, {"one_health_msg": params}, data => {
            if (data && data.data) {
                if (data.status === 404) {
                    reject({ message: msg_text.not_found, status: 400 });
                } else if (data.data.error) {
                    let resError = data.data.error;
                    if (!resError.message) {
                        if (resError.description) {
                            resError.message = resError.description;
                        } else {
                            resError.message = msg_text.connect_server_error;
                        }
                    }
                    reject(resError);
                } else {
                    resolve(data);
                }
            } else {
                reject({ message: msg_text.disconnect, status: 400 });
            }
        })
    });
};