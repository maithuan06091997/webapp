import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyDUjoj2bXCmfIBGOL14fmPY3222kOnA1dc",
    authDomain: "doctoroh-536e0.firebaseapp.com",
    databaseURL: "https://doctoroh-536e0.firebaseio.com",
    projectId: "doctoroh-536e0",
    storageBucket: "doctoroh-536e0.appspot.com",
    messagingSenderId: "346671816292",
    appId: "1:346671816292:web:64ead678d81d72ef29c971",
    measurementId: "G-E5MGEZ370S"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
if (process.env.REACT_APP_ENV === "pro") {
    firebase.analytics();
}

export default firebase;