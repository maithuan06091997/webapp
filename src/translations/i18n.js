import i18n from "i18next";
import {en} from "./en";
import {vi} from "./vi";

i18n.init({
    // We init with resources
    lng: "VI",
    resources: {
        EN: {
            translations: en
        },
        VI: {
            translations: vi
        }
    },
    fallbackLng: "VI",
    debug: false,

    // Have a common namespace used around the full app
    ns: ["translations"],
    defaultNS: "translations",

    keySeparator: false, // We use content as keys

    interpolation: {
        escapeValue: false, // Not needed for react!!
        formatSeparator: ","
    },

    react: {
        wait: true
    }
});

export default i18n;