export const vi = {
    "phone or password invalid": "Mật khẩu không hợp lệ",
    "Phone invalid": "Số điện thoại không hợp lệ",
    "Password is invalid": "Mật khẩu không hợp lệ",
    "new password must difference old password": "Mật khẩu mới phải khác mật khẩu cũ",
    "Token invalid or expire": "Token đã hết hạn",
    "Card not found or wrong pin": "Thông tin của card không đúng",
    "Card was used.": "Card đã sử dụng",
    "promitionCode no exits": "Mã ưu đãi không tồn tại",
    "Can not use this code": "Không thể sử dụng mã khuyến mãi này",
};