export const en = {
    "phone or password invalid": "Invalid password",
    "Phone invalid": "Invalid phone ",
    "Password is invalid": "Invalid password",
    "new password must difference old password": "New password must difference old password",
    "Token invalid or expire": "Token invalid or expire",
    "Card not found or wrong pin": "Card not found or wrong pin",
    "Card was used.": "Card was used",
    "promitionCode no exits": "Coupon code does not exist",
    "Can not use this code": "Can not use this coupon code",
};
