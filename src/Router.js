import React, { lazy } from "react";
import { Switch, BrowserRouter as Router } from "react-router-dom";
import { url } from "./constants/Path";
import Role from "./Role";
import Layouts from "./layouts";
import GoogleAnalytics from "./components/GoogleAnalytics";

// const SignIn = lazy(() => import("./pages/Authorization/SignIn"));
// const Register = lazy(() => import("./pages/Authorization/Register"));
const VerifyPhone = lazy(() => import("./pages/Authorization/VerifyPhone"));
const UpdatePass = lazy(() => import("./pages/Authorization/UpdatePass"));
const ConfirmPass = lazy(() => import("./pages/Authorization/ConfirmPass"));
// const Home = lazy(() => import("./pages/Home"));
// const AccountDetailPersonal = lazy(() =>
//   import("./pages/MyAccount/Personal/Detail")
// );
// const AccountEditPersonal = lazy(() =>
//   import("./pages/MyAccount/Personal/Edit")
// );
// const AccountChangePassword = lazy(() =>
//   import("./pages/MyAccount/ChangePassword")
// );
const AccountListRelativeProfile = lazy(() =>
  import("./pages/MyAccount/RelativeProfile/List")
);
const AccountCreateRelativeProfile = lazy(() =>
  import("./pages/MyAccount/RelativeProfile/Create")
);
const AccountEditRelativeProfile = lazy(() =>
  import("./pages/MyAccount/RelativeProfile/Edit")
);
// const AccountListHistoryCall = lazy(() =>
//   import("./pages/MyAccount/HistoryCall/List")
// );
// const AccountDetailHistoryCall = lazy(() =>
//   import("./pages/MyAccount/HistoryCall/Detail")
// );
// const AccountListHistoryDrug = lazy(() =>
//   import("./pages/MyAccount/HistoryDrug/List")
// );
// const AccountListHistoryDrugDetails = lazy(() =>
//   import("./pages/MyAccount/HistoryDrug/Detail")
// );
// const AccountListAppointmentSchedule = lazy(() =>
//   import("./pages/MyAccount/AppointmentSchedule/List")
// );
// const AccountDetailAppointmentSchedule = lazy(() =>
//   import("./pages/MyAccount/AppointmentSchedule/Detail")
// );
// const AccountListHistoryExamination = lazy(() =>
//   import("./pages/MyAccount/HistoryExamination/List")
// );
// const AccountListHistory = lazy(() => import("./pages/MyAccount/History"));
// const AccountDetailHistoryExamination = lazy(() =>
//   import("./pages/MyAccount/HistoryExamination/Detail")
// );
// const AccountListInvoice = lazy(() => import("./pages/MyAccount/Invoice/List"));
// const AccountDetailInvoice = lazy(() =>
//   import("./pages/MyAccount/Invoice/Detail")
// );
// const AccountPromotion = lazy(() => import("./pages/MyAccount/Promotion"));
// const AccountNotification = lazy(() =>
//   import("./pages/MyAccount/Notification")
// );
// const AccountWalletRecharge = lazy(() =>
//   import("./pages/MyAccount/Wallet/Recharge")
// );
// const AccountWalletConfirmation = lazy(() =>
//   import("./pages/MyAccount/Wallet/Confirmation")
// );
// const ExaminationStepOne = lazy(() =>
//   import("./pages/Service/Examination/StepOne")
// );
// const ExaminationStepTwo = lazy(() =>
//   import("./pages/Service/Examination/StepTwo")
// );
// const ExaminationStepThree = lazy(() =>
//   import("./pages/Service/Examination/StepThree")
// );
// const ExaminationStepFour = lazy(() =>
//   import("./pages/Service/Examination/StepFour")
// );
// const ExaminationStepFive = lazy(() =>
//   import("./pages/Service/Examination/StepFive")
// );
// const CallChatStepOne = lazy(() => import("./pages/Service/CallChat/StepOne"));
// const CallChatStepTwo = lazy(() => import("./pages/Service/CallChat/StepTwo"));
// const CallChatStepThree = lazy(() =>
//   import("./pages/Service/CallChat/StepThree")
// );
// const CallChatStepFour = lazy(() =>
//   import("./pages/Service/CallChat/StepFour")
// );
// const CallChatStepFive = lazy(() =>
//   import("./pages/Service/CallChat/StepFive")
// );

// const DrHera = lazy(() => import("./pages/Service/DrHera"));

// const PayChat = lazy(() => import("./pages/Service/CallChat/PayChat"));
const HealthServices = lazy(() => import("./pages/Service/HealthServices"));
const HealthServicesProfilePage = lazy(() =>
  import("./pages/Service/HealthServices/HealthServicesProfilePage")
);
const HealthServicesPackage = lazy(() =>
  import("./pages/Service/HealthServices/PackageHealthServices")
);
const HealthServicesPayment = lazy(() =>
  import("./pages/Service/HealthServices/HealthServicesPayment")
);
// const DrugStoreOnline = lazy(() => import("./pages/Service/DrugStoreOnline"));
// const DrugDeliveryAddress = lazy(() =>
//   import("./pages/Service/DrugStoreOnline/DrugDeliveryAddress")
// );
// const DrugConfirmOrder = lazy(() =>
//   import("./pages/Service/DrugStoreOnline/ConfirmOrder")
// );

// const HomeTest = lazy(() => import("./pages/Service/HomeTest"));
// const ScheduleTest = lazy(() =>
//   import("./pages/Service/HomeTest/ScheduleTest")
// );
// const RecordsTest = lazy(() => import("./pages/Service/HomeTest/RecordsTest"));
// const RecordsPayment = lazy(() =>
//   import("./pages/Service/HomeTest/RecordsPayment")
// );
// const AccountListHistoryRecord = lazy(() =>
//   import("./pages/MyAccount/HistoryRecord/List")
// );
// const AccountDetailsHistoryRecord = lazy(() =>
//   import("./pages/MyAccount/HistoryRecord/Detail")
// );

const HealthServicesHistory = lazy(() =>
  import("./pages/MyAccount/HistoryServices")
);
const HealthServicesHistoryDetails = lazy(() =>
  import("./pages/MyAccount/HistoryServices/HistoryServicesDetails")
);

// const DetailParaclinical = lazy(() =>
//   import("./pages/MyAccount/HistoryServices/DetailParaclinical")
// );
const ServiceConfirmation = lazy(() => import("./pages/Service/Confirmation"));
// const ScreenVideoCall = lazy(() => import("./pages/ScreenVideoCall"));
const NotFound = lazy(() => import("./pages/NotFound"));
// const AccountChat = lazy(() => import("./pages/Chat"));
// const ListQuestions = lazy(() => import("./pages/Service/Question/List"));
// const CreateQuestion = lazy(() => import("./pages/Service/Question/Create"));
// const MyQuestions = lazy(() => import("./pages/Service/Question/MyQuestions"));
// const SocialNetwork = lazy(() => import("./pages/SocialNetwork"));
// const SocialPersonal = lazy(() =>
//   import("./pages/SocialNetwork/SocialPersonal")
// );
// const CarePay = lazy(() => import("./pages/CarePay"));

const CRouter = () => {
  return (
    <Router>
      <GoogleAnalytics />

      <Switch>
        {/* <Role
          path={url.name.index}
          exact
          component={SignIn}
          layout={Layouts.OtherLayout}
        />
        <Role
          path={url.name.register}
          exact
          component={Register}
          layout={Layouts.OtherLayout}
        /> */}
        <Role
          path={url.name.verify}
          exact
          component={VerifyPhone}
          layout={Layouts.OtherLayout}
        />
        <Role
          path={url.name.update_pass}
          exact
          component={UpdatePass}
          layout={Layouts.OtherLayout}
        />
        <Role
          path={url.name.confirm_pass}
          exact
          component={ConfirmPass}
          layout={Layouts.OtherLayout}
        />
        {/* <Role
          path={url.name.screen_video_call}
          exact
          auth={true}
          component={ScreenVideoCall}
          layout={Layouts.VideoCallLayout}
        /> */}
        {/* <Role
          path={url.name.home}
          exact
          auth={true}
          component={Home}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_detail_personal}
          exact
          auth={true}
          component={AccountDetailPersonal}
          layout={Layouts.MainLayout}
        /> */}
        {/* <Role
          path={url.name.account_edit_personal}
          exact
          auth={true}
          component={AccountEditPersonal}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_list_appointment_schedule}
          exact
          auth={true}
          component={AccountListAppointmentSchedule}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_detail_appointment_schedule}
          exact
          auth={true}
          component={AccountDetailAppointmentSchedule}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_change_password}
          exact
          auth={true}
          component={AccountChangePassword}
          layout={Layouts.MainLayout}
        /> */}
        <Role
          path={url.name.account_list_relative_profile}
          exact
          auth={true}
          component={AccountListRelativeProfile}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_create_relative_profile}
          exact
          auth={true}
          component={AccountCreateRelativeProfile}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_edit_relative_profile}
          exact
          auth={true}
          component={AccountEditRelativeProfile}
          layout={Layouts.MainLayout}
        />
        {/* <Role
          path={url.name.account_list_history_call}
          exact
          auth={true}
          component={AccountListHistoryCall}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_detail_history_call}
          exact
          auth={true}
          component={AccountDetailHistoryCall}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_drug_history}
          exact
          auth={true}
          component={AccountListHistoryDrug}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.service_drug_history_details}/:drugId`}
          exact
          auth={true}
          component={AccountListHistoryDrugDetails}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_list_history_examination}
          exact
          auth={true}
          component={AccountListHistoryExamination}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_list_history}
          exact
          auth={true}
          component={AccountListHistory}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_detail_history_examination}
          exact
          auth={true}
          component={AccountDetailHistoryExamination}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_promotion}
          exact
          auth={true}
          component={AccountPromotion}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_list_invoice}
          exact
          auth={true}
          component={AccountListInvoice}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_detail_invoice}
          exact
          auth={true}
          component={AccountDetailInvoice}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_notification}
          exact
          auth={true}
          component={AccountNotification}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_wallet}
          exact
          auth={true}
          component={AccountWalletRecharge}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_wallet_confirmation}
          exact
          auth={true}
          component={AccountWalletConfirmation}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.examination_1}
          exact
          auth={true}
          component={ExaminationStepOne}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.examination_2}
          exact
          auth={true}
          component={ExaminationStepTwo}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.examination_3}
          exact
          auth={true}
          component={ExaminationStepThree}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.examination_4}
          exact
          auth={true}
          component={ExaminationStepFour}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.examination_5}
          exact
          auth={true}
          component={ExaminationStepFive}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.call_chat_1}
          exact
          auth={true}
          component={CallChatStepOne}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.call_chat_2}
          exact
          auth={true}
          component={CallChatStepTwo}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.call_chat_3}
          exact
          auth={true}
          component={CallChatStepThree}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.call_chat_4}
          exact
          auth={true}
          component={CallChatStepFour}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.call_chat_5}
          exact
          auth={true}
          component={CallChatStepFive}
          layout={Layouts.MainLayout}
        /> */}
        {/* <Role
          path={url.name.dr_hera}
          exact
          auth={true}
          component={DrHera}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.pay_chat}
          exact
          auth={true}
          component={PayChat}
          layout={Layouts.MainLayout}
        /> */}
        <Role
          path={url.name.service_health}
          exact
          auth={true}
          component={HealthServices}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_health_profile}
          exact
          auth={true}
          component={HealthServicesProfilePage}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_health_package}
          exact
          auth={true}
          component={HealthServicesPackage}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_health_payment}
          exact
          auth={true}
          component={HealthServicesPayment}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_health_history}
          exact
          auth={true}
          component={HealthServicesHistory}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.service_health_history_details}/:orderId`}
          exact
          auth={true}
          component={HealthServicesHistoryDetails}
          layout={Layouts.MainLayout}
        />
        {/* <Role
          path={`${url.name.paraclinical_history_details}/:serviceId`}
          exact
          auth={true}
          component={DetailParaclinical}
          layout={Layouts.MainLayout}
        /> */}
        {/* <Role
          path={url.name.service_drug_store}
          exact
          auth={true}
          component={DrugStoreOnline}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_drug_delivery_address}
          exact
          auth={true}
          component={DrugDeliveryAddress}
          layout={Layouts.MainLayout}
        />

        <Role
          path={url.name.service_drug_confirm}
          exact
          auth={true}
          component={DrugConfirmOrder}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_home_test}
          exact
          auth={true}
          component={HomeTest}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_schedule_test}
          exact
          auth={true}
          component={ScheduleTest}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_records_test}
          exact
          auth={true}
          component={RecordsTest}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.service_home_test_payment}
          exact
          auth={true}
          component={RecordsPayment}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.account_list_home_test}
          exact
          auth={true}
          component={AccountListHistoryRecord}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.account_details_home_test}/:recordId`}
          exact
          auth={true}
          component={AccountDetailsHistoryRecord}
          layout={Layouts.MainLayout}
        /> */}
        <Role
          path={url.name.service_confirmation}
          exact
          auth={true}
          component={ServiceConfirmation}
          layout={Layouts.MainLayout}
        />
        {/* <Role
          path={url.name.chat}
          exact
          auth={true}
          component={AccountChat}
          layout={Layouts.ChatLayout}
        />
        <Role
          path={url.name.list_questions}
          exact
          auth={true}
          component={ListQuestions}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.list_questions}/:questionId`}
          exact
          auth={true}
          component={ListQuestions}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.create_question}
          exact
          auth={true}
          component={CreateQuestion}
          layout={Layouts.MainLayout}
        />
        <Role
          path={url.name.my_questions}
          exact
          auth={true}
          component={MyQuestions}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.my_questions}/:questionId`}
          exact
          auth={true}
          component={MyQuestions}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.social_network}`}
          exact
          auth={true}
          component={SocialNetwork}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.social_personal}/:personalId`}
          exact
          auth={true}
          component={SocialPersonal}
          layout={Layouts.MainLayout}
        />
        <Role
          path={`${url.name.care_pay}`}
          exact
          auth={true}
          component={CarePay}
          layout={Layouts.MainLayout}
        /> */}
        <Role
          path="*"
          exact
          component={NotFound}
          layout={Layouts.NotFoundLayout}
        />
      </Switch>
    </Router>
  );
};

export default CRouter;
