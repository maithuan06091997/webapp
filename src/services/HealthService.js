import { setting } from "../api/Config";
import AxiosUtil, { authGet, authPost, authPut } from "../api/Fetch";

export const mdlChat = {
  healthServices: setting.ohDomainChat1 + "/json/groupServiceIcon.json",
  text: setting.api + "/video-call-schedule/speciality",
  healthServicesByCategory: setting.api + "/healthService",
  healthServicesOrder: setting.api + "/order",
  detailsHealthServices: setting.api + "/healthService",
  priceChooseService: setting.api + "/order/price_register",
  cancelHealthServices: setting.api + "/order/patient",
  listParaclinical: setting.api + "/paraclinical/find",
  paraclinical: setting.api + "/paraclinical",
  verifyMb: setting.api + "/mb-bank/verify-token",
  serviceTransaction: setting.api + "/mb-bank/create-service-transaction",
};

const notAuthHeader = {
  headers: {
    "Accept-Language": "vi",
  },
};

export async function getListHealthServices() {
  const response = await AxiosUtil.get(mdlChat.healthServices, notAuthHeader);
  return response.data;
}

export async function getListHealthServicesByCategory(params, token) {
  const response = await AxiosUtil.get(mdlChat.healthServicesByCategory, {
    params,
    headers: {
      "Accept-Language": "vi",
      oh_token: token,
      platform: "webapp",
    },
  });
  return response.data;
}

export async function getInfoMb(token, params) {
  const response = await AxiosUtil.get(
    `${mdlChat.verifyMb}/${token}`,
    notAuthHeader
  );
  return response.data;
}

export async function getListHealthServicesOrder(params) {
  const response = await authGet(mdlChat.healthServicesOrder, params);
  return response.data;
}

export async function getDetailsHealthServices(id, params, token) {
  const response = await AxiosUtil.get(
    `${mdlChat.detailsHealthServices}/${id}`,
    {
      params,
      headers: {
        "Accept-Language": "vi",
        oh_token: token,
        platform: "webapp",
      },
    }
  );
  return response.data;
}

export async function getDetailsOrderHealthServices(id, params) {
  const response = await authGet(
    `${mdlChat.healthServicesOrder}/${id}`,
    params
  );
  return response.data;
}

export async function getDetailsParaclinical(id, params) {
  const response = await authGet(`${mdlChat.paraclinical}/${id}`, params);
  return response.data;
}

export async function getDetailsTakeBlood(id, params) {
  const response = await authGet(
    `${mdlChat.healthServicesOrder}/${id}`,
    params
  );
  return response.data;
}

export async function getPriceChooseService(params) {
  const response = await authGet(mdlChat.priceChooseService, params);
  return response.data;
}

export async function cancelHealthServices(orderId, postData) {
  const response = await authPut(
    `${mdlChat.cancelHealthServices}/${orderId}`,
    postData
  );
  return response.data;
}

export async function getListParaclinical(orderId, postData) {
  const response = await authPost(
    `${mdlChat.listParaclinical}?order=${orderId}`,
    postData
  );
  return response.data;
}

export async function createServiceTransaction(postData) {
  const response = await authPost(mdlChat.serviceTransaction, postData);
  return response.data;
}
