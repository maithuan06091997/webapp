import { setting } from "../api/Config";
import { authGet, authPost, authPut } from "../api/Fetch";

export const mdlChat = {
  take_blood: setting.api + "/take_blood/service",
  take_blood_time: setting.api + "/take_blood/times",
  profiles: setting.api + "/profiles",
  priceTestService: setting.api + "/take_blood/order/check-register",
  record_history: setting.api + "/order",
  record_history_details: setting.api + "/take_blood/order",
};

export async function getListHomeTest() {
  const response = await authGet(`${mdlChat.take_blood}`);
  return response.data;
}

export async function getHomeTestDetail(id) {
  const response = await authGet(`${mdlChat.take_blood}/${id}`);
  return response.data;
}

export async function getListScheduleTest(params) {
  const response = await authGet(`${mdlChat.take_blood_time}`, params);
  return response.data;
}

export async function getListProfiles(params) {
  const response = await authGet(`${mdlChat.profiles}`, params);
  return response.data;
}

export async function getPriceHomeTestService(params) {
  const response = await authPost(mdlChat.priceTestService, params);
  return response.data;
}

export async function getListHistoryRecord(reqParamFt) {
  const response = await authGet(`${mdlChat.record_history}`, reqParamFt);
  return response.data;
}

export async function getDetailsHistoryRecord(id, reqParamFt) {
  const response = await authGet(`${mdlChat.record_history}/${id}`, reqParamFt);
  return response.data;
}

export async function patchCancelRecord(id) {
  const response = await authPut(
    `${mdlChat.record_history_details}/${id}?order_status=cancel&cancel_by=user`
  );
  return response.data;
}
