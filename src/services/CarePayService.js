import { setting } from "../api/Config";
import { authGet, authPost } from "../api/Fetch";

export const mdlChat = {
  mbInfo: setting.api_carepay + "/mb-bank/ewallet/info",
  confirm_otp: setting.api_carepay + "/mb-bank/payment/pay-confirm",
  mb_order: setting.api_carepay + "/mb-bank/payment/pay-order",
  history_payment: setting.api_carepay + "/mb-bank/other/list-history-cash",
};

export async function getMbBank(params) {
  const response = await authGet(mdlChat.mbInfo, params);
  return response.data;
}

export async function confirmOtp(postData) {
  const response = await authPost(mdlChat.confirm_otp, postData);
  return response.data;
}

export async function postMbOrder(postData) {
  const response = await authPost(mdlChat.mb_order, postData);
  return response.data;
}

export async function getHistoryPayment(params) {
  const response = await authGet(mdlChat.history_payment, params);
  return response.data;
}
