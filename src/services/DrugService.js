import { setting } from "../api/Config";
import { authGet, authPatch, authPost } from "../api/Fetch";

export const mdlChat = {
  findProprietary: setting.api + "/drugs/find-proprietary-name",
  findOriginal: setting.api + "/drugs/find-original-name",
  provinces: setting.api + "/provinces",
  district: setting.api + "/districts/find/province",
  ward: setting.api + "/wards/find/district",
  drug_history: setting.api + "/api/prescription/list",
  drug_history_details: setting.api + "/api/prescription",
  drug_confirm: setting.api + "/accounts/get-balance",
};

export async function getListNameDrug(postData) {
  const response = await authPost(`${mdlChat.findProprietary}`, postData);
  return response.data;
}

export async function getListOriginalDrug(postData) {
  const response = await authPost(`${mdlChat.findOriginal}`, postData);
  return response.data;
}

export async function getListProvinces() {
  const response = await authGet(`${mdlChat.provinces}`);
  return response.data;
}

export async function getListDistrict(postData) {
  const response = await authPost(`${mdlChat.district}`, postData);
  return response.data;
}

export async function getListWard(postData) {
  const response = await authPost(`${mdlChat.ward}`, postData);
  return response.data;
}

export async function postDrugOrder(url, postData) {
  const response = await authPost(`${url}`, postData);
  return response.data;
}

export async function getListHistoryDrug(reqParamFt) {
  const response = await authGet(`${mdlChat.drug_history}`, reqParamFt);
  return response.data;
}

export async function getDetailsHistoryDrug(id, params) {
  const response = await authGet(
    `${mdlChat.drug_history_details}/${id}`,
    params
  );
  return response.data;
}

export async function postConfirmDrug(postData) {
  const response = await authPost(mdlChat.drug_confirm, postData);
  return response.data;
}

export async function patchCancelDrug(id, postData) {
  const response = await authPatch(
    `${mdlChat.drug_history_details}/${id}`,
    postData
  );
  return response.data;
}
