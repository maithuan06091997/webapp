import { setting } from "../api/Config";
import { authDelete, authGet, authPost, authPut } from "../api/Fetch";

export const mdlChat = {
  follow: setting.api_social + "/api/post/list",
  suggest: setting.api_social + "/api/contact/suggests",
  like: setting.api_social + "/api/post/like_unlike",
  post: setting.api_social + "/api/post",
  follow_unfollow: setting.api_social + "/api/profile",
  comment: setting.api_social + "/api",
  likeComment: setting.api_social + "/api/comment/like_unlike",
  likes: setting.api_social + "/api/profile/detail",
  new_post: setting.api_social + "/api/post/post-social",
};

export async function getListFollow(params) {
  const response = await authGet(`${mdlChat.follow}`, params);
  return response.data;
}

export async function getListSuggest(params) {
  const response = await authGet(`${mdlChat.suggest}`, params);
  return response.data;
}

export async function putLikePost(id, data) {
  const response = await authPut(`${mdlChat.like}/${id}`, data);
  return response.data;
}

export async function deletePost(id, data) {
  const response = await authDelete(`${mdlChat.post}/${id}`, data);
  return response.data;
}

export async function postFollow(id, data) {
  const response = await authPost(
    `${mdlChat.follow_unfollow}/${id}/follow_unfollow`,
    data
  );
  return response.data;
}

export async function getComment(data) {
  const response = await authPost(`${mdlChat.comment}`, data);
  return response.data;
}

export async function putLikeComment(id, data) {
  const response = await authPut(`${mdlChat.likeComment}/${id}`, data);
  return response.data;
}

export async function getListLike(postId, params) {
  const response = await authGet(`${mdlChat.likes}/likes/${postId}`, params);
  return response.data;
}

export async function getListToFollow(listType, creatorId, params) {
  const response = await authGet(
    `${mdlChat.likes}/${listType}/${creatorId}`,
    params
  );
  return response.data;
}

export async function getNewPost(url, params) {
  const response = await authPost(`${mdlChat.new_post}${url}`, params);
  return response.data;
}
