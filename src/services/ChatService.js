import { setting } from "../api/Config";
import { authGet, authPost } from "../api/Fetch";

export const mdlChat = {
  price: setting.api + "/invoices/checkCallChat",
  payChat: setting.api + "/accounts/check-balance",
};

export async function getPriceChat(params) {
  const response = await authGet(`${mdlChat.price}`, params);
  return response.data;
}

export async function postPayChat(postData) {
  const response = await authPost(`${mdlChat.payChat}`, postData);
  return response.data;
}
