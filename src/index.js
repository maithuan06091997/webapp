import React, { Suspense, useEffect } from "react";
import ReactDOM from "react-dom";
import * as serviceWorker from "./serviceWorker";
import CRouter from "./Router";

import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import Reducers from "./reducers";

import createSagaMiddleware from "redux-saga";
import RootSaga from "./saga/RootSaga";

import { I18nextProvider } from "react-i18next";
import i18n from "./translations/i18n";

import "react-notifications/lib/notifications.css";
import "antd/dist/antd.css";
import "bootstrap/scss/bootstrap.scss";
import "./index.scss";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(Reducers, applyMiddleware(sagaMiddleware));

// if (!localStorage.getItem("user")) {
//   localStorage.setItem("user", JSON.stringify(store.getState().user));
// }

if (!localStorage.getItem("balance")) {
  localStorage.setItem("balance", JSON.stringify(store.getState().balance));
}

if (!localStorage.getItem("ck_firebase")) {
  localStorage.setItem(
    "ck_firebase",
    JSON.stringify(store.getState().ck_firebase)
  );
}

if (!localStorage.getItem("browser")) {
  localStorage.setItem("browser", JSON.stringify(store.getState().browser));
}

if (!localStorage.getItem("notify")) {
  localStorage.setItem("notify", JSON.stringify(store.getState().notify));
}

if (!localStorage.getItem("language")) {
  localStorage.setItem("language", JSON.stringify(store.getState().language));
}

if (!sessionStorage.getItem("confirm")) {
  sessionStorage.setItem("confirm", JSON.stringify(store.getState().confirm));
}

if (!sessionStorage.getItem("verify")) {
  sessionStorage.setItem("verify", JSON.stringify(store.getState().verify));
}

if (!sessionStorage.getItem("appointment")) {
  sessionStorage.setItem(
    "appointment",
    JSON.stringify(store.getState().appointment)
  );
}

if (!sessionStorage.getItem("category_service")) {
  sessionStorage.setItem(
    "category_service",
    JSON.stringify(store.getState().category_service)
  );
}

if (!sessionStorage.getItem("drug_store")) {
  sessionStorage.setItem(
    "drug_store",
    JSON.stringify(store.getState().drug_store)
  );
}

if (!sessionStorage.getItem("home_test")) {
  sessionStorage.setItem(
    "home_test",
    JSON.stringify(store.getState().home_test)
  );
}

if (!sessionStorage.getItem("call_chat")) {
  sessionStorage.setItem(
    "call_chat",
    JSON.stringify(store.getState().call_chat)
  );
}

store.subscribe(() => {
  try {
    // localStorage.setItem("user", JSON.stringify(store.getState().user));
    localStorage.setItem("balance", JSON.stringify(store.getState().balance));
    localStorage.setItem(
      "ck_firebase",
      JSON.stringify(store.getState().ck_firebase)
    );
    localStorage.setItem("browser", JSON.stringify(store.getState().browser));
    localStorage.setItem("notify", JSON.stringify(store.getState().notify));
    localStorage.setItem("language", JSON.stringify(store.getState().language));
    sessionStorage.setItem("confirm", JSON.stringify(store.getState().confirm));
    sessionStorage.setItem("verify", JSON.stringify(store.getState().verify));
    sessionStorage.setItem(
      "appointment",
      JSON.stringify(store.getState().appointment)
    );
    sessionStorage.setItem(
      "category_service",
      JSON.stringify(store.getState().category_service)
    );
    sessionStorage.setItem(
      "drug_store",
      JSON.stringify(store.getState().drug_store)
    );
    sessionStorage.setItem(
      "home_test",
      JSON.stringify(store.getState().home_test)
    );
    sessionStorage.setItem(
      "call_chat",
      JSON.stringify(store.getState().call_chat)
    );
  } catch (e) {
    console.log(e);
  }
});

sagaMiddleware.run(RootSaga);

ReactDOM.render(
  <Provider store={store}>
    <I18nextProvider i18n={i18n}>
      <Suspense fallback="">
        <CRouter />
      </Suspense>
    </I18nextProvider>
  </Provider>,
  document.getElementById("root")
);

serviceWorker.register();
