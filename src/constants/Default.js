export const hotline = 19001851;
export const google_play =
  "https://play.google.com/store/apps/details?id=com.onehealth.DoctorOHApp&hl=en&gl=US";
export const app_store = "https://apps.apple.com/us/app/droh/id1264916712";
export const hong_duc = { name: "Hồng Đức", key: "hong_duc" };
export const nhi_dong = { name: "Nhi Đồng 2", key: "nhi_dong" };
export const ung_buou = { name: "Ung Bướu", key: "ung_buou" };
export const drkhoa = { name: "DrKhoa", key: "drkhoa" };

export const nameOH = {
  hong_duc: "Hồng Đức",
  nhi_dong: "Nhi Đồng 2",
  ung_buou: "Ung Bướu",
  drkhoa: "DrKhoa",
};
