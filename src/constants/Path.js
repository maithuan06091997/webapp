const HOME_PAGE = process.env.REACT_APP_HOME_PAGE;

export const public_url = {
  img: HOME_PAGE + "images",
  media: HOME_PAGE + "media",
};

export const url = {
  name: {
    index: HOME_PAGE + "",
    not_found: HOME_PAGE + "not-found",
    register: HOME_PAGE + "register",
    verify: HOME_PAGE + "verify",
    update_pass: HOME_PAGE + "update-pass",
    confirm_pass: HOME_PAGE + "confirm-pass",
    // home: HOME_PAGE + "home",
    // account_detail_personal: HOME_PAGE + "my-account/personal/detail",
    // account_edit_personal: HOME_PAGE + "my-account/personal/edit",
    // account_change_password: HOME_PAGE + "my-account/change-password",
    // account_list_appointment_schedule:
    //   HOME_PAGE + "my-account/appointment-schedule",
    // account_detail_appointment_schedule:
    //   HOME_PAGE + "my-account/appointment-schedule/detail",
    account_list_relative_profile: HOME_PAGE + "relative-profile",
    account_create_relative_profile: HOME_PAGE + "relative-profile/create",
    account_edit_relative_profile: HOME_PAGE + "relative-profile/edit",
    // account_promotion: HOME_PAGE + "my-account/promotion",
    // account_list_invoice: HOME_PAGE + "my-account/invoice",
    // account_list_history_call: HOME_PAGE + "my-account/history-call",
    // account_detail_history_call: HOME_PAGE + "my-account/history-call/detail",
    // account_list_history_examination:
    //   HOME_PAGE + "my-account/history-examination",
    // account_list_history: HOME_PAGE + "my-account/history",
    // account_detail_history_examination:
    //   HOME_PAGE + "my-account/history-examination/detail",
    // account_detail_invoice: HOME_PAGE + "my-account/invoice/detail",
    // account_notification: HOME_PAGE + "my-account/notification",
    // account_wallet: HOME_PAGE + "my-account/wallet",
    // account_wallet_confirmation: HOME_PAGE + "my-account/wallet/confirmation",
    // examination_1: HOME_PAGE + "service/examination/step-1",
    // examination_2: HOME_PAGE + "service/examination/step-2",
    // examination_3: HOME_PAGE + "service/examination/step-3",
    // examination_4: HOME_PAGE + "service/examination/step-4",
    // examination_5: HOME_PAGE + "service/examination/step-5",
    // call_chat_1: HOME_PAGE + "service/call-chat/step-1",
    // call_chat_2: HOME_PAGE + "service/call-chat/step-2",
    // call_chat_3: HOME_PAGE + "service/call-chat/step-3",
    // call_chat_4: HOME_PAGE + "service/call-chat/step-4",
    // call_chat_5: HOME_PAGE + "service/call-chat/step-5",
    // pay_chat: HOME_PAGE + "service/pay-chat",
    // dr_hera: HOME_PAGE + "service/dr-hera",

    service_health: HOME_PAGE + "",
    service_health_by_category: HOME_PAGE + "category",
    service_health_profile: HOME_PAGE + "profile",
    service_health_package: HOME_PAGE + "package",
    service_health_payment: HOME_PAGE + "payment",

    // service_drug_store: HOME_PAGE + "service/drugstore/order",
    // service_drug_delivery_address: HOME_PAGE + "service/drugstore/address",
    // service_drug_confirm: HOME_PAGE + "service/drugstore/confirm",

    // service_home_test: HOME_PAGE + "service/home-test",
    // service_schedule_test: HOME_PAGE + "service/home-schedule",
    // service_records_test: HOME_PAGE + "service/home-records",
    // service_home_test_payment: HOME_PAGE + "service/home-test/payment",
    // account_list_home_test: HOME_PAGE + "my-account/home-test",
    // account_details_home_test: HOME_PAGE + "my-account/home-test/details",

    service_health_history: HOME_PAGE + "history-services",
    service_health_history_details: HOME_PAGE + "history-services-details",
    // paraclinical_history_details:
    //   HOME_PAGE + "my-account/history-paraclinical-details",

    // service_drug_history: HOME_PAGE + "my-account/history-drug",
    // service_drug_history_details: HOME_PAGE + "my-account/history-drug-details",

    service_confirmation: HOME_PAGE + "confirmation",
    // list_questions: HOME_PAGE + "service/questions",
    // create_question: HOME_PAGE + "service/create-question",
    // my_questions: HOME_PAGE + "service/my-questions",
    // screen_video_call: HOME_PAGE + "screen/video-call",
    // chat: HOME_PAGE + "chat",
    // social_network: HOME_PAGE + "social-network",
    // social_personal: HOME_PAGE + "social-personal",
    // care_pay: HOME_PAGE + "care_pay",
  },
};
