export const dataRelationship = () => {
  return [
    { id: 15, value: "other", text: "Khác", label: "Khác" },
    { id: 0, value: "owner", text: "Tôi", label: "Tôi" },
    { id: 1, value: "father", text: "Bố", label: "Bố" },
    { id: 2, value: "mother", text: "Mẹ", label: "Mẹ" },
    { id: 3, value: "husband", text: "Chồng", label: "Chồng" },
    { id: 4, value: "wife", text: "Vợ", label: "Vợ" },
    { id: 5, value: "son", text: "Con trai", label: "Con trai" },
    { id: 6, value: "daughter", text: "Con gái", label: "Con gái" },
    { id: 7, value: "brother", text: "Anh", label: "Anh" },
    { id: 8, value: "sister", text: "Chị", label: "Chị" },
    { id: 9, value: "younger_brother", text: "Em trai", label: "Em trai" },
    { id: 10, value: "younger_sister", text: "Em gái", label: "Em gái" },
    { id: 11, value: "grandfather", text: "Ông", label: "Ông" },
    { id: 12, value: "grandmother", text: "Bà", label: "Bà" },
    { id: 13, value: "uncle", text: "Chú/Bác", label: "Chú/Bác" },
    { id: 14, value: "aunt", text: "Cô/Dì", label: "Cô/Dì" },
  ];
};

export const dataGender = () => {
  return [
    { id: 1, value: "male", text: "Nam", label: "Nam" },
    { id: 2, value: "female", text: "Nữ", label: "Nữ" },
    { id: 3, value: "all", text: "Tất cả", label: "Tất cả" },
  ];
};

export const dataStatusAppointment = () => {
  return [
    { id: 99, value: "all", label: "Tất cả", text: "Tất cả", color: "" },
    {
      id: 1,
      value: "draft",
      label: "Chờ đăng ký",
      text: "Chờ đăng ký",
      color: "badge-primary",
    },
    {
      id: 2,
      value: "paid",
      label: "Đã thanh toán",
      text: "Đã thanh toán",
      color: "badge-success",
    },
    {
      id: 3,
      value: "registed",
      label: "Đã đăng ký",
      text: "Đã đăng ký",
      color: "badge-success",
    },
    {
      id: 4,
      value: "done",
      label: "Đã hoàn thành",
      text: "Đã hoàn thành",
      color: "badge-success",
    },
    {
      id: 5,
      value: "fail",
      label: "Đăng ký thất bại",
      text: "Đăng ký thất bại",
      color: "badge-danger",
    },
    {
      id: 6,
      value: "cancel",
      label: "Đã hủy",
      text: "Đã hủy",
      color: "badge-danger",
    },
    {
      id: 7,
      value: "expire",
      label: "Đã hết hạn",
      text: "Đã hết hạn",
      color: "badge-danger",
    },
    {
      id: 8,
      value: "refund",
      label: "Đã hoàn tiền",
      text: "Đã hoàn tiền",
      color: "badge-info",
    },
  ];
};

export const dataStatusVideoCall = () => {
  return [
    { id: 1, value: "new", text: "Mới tạo", color: "badge-primary" },
    { id: 2, value: "called", text: "Đã gọi", color: "badge-success" },
    { id: 3, value: "expired", text: "Đã hết hạn", color: "badge-warning" },
    { id: 4, value: "cancel", text: "Đã hủy", color: "badge-danger" },
  ];
};

export const dataTypePayment = () => {
  return [
    { id: 1, value: "all", text: "Áp dụng trả trước và trả sau" },
    { id: 2, value: "prepay", text: "Chỉ áp dụng trả trước" },
    { id: 3, value: "postpaid", text: "Chỉ áp dụng trả sau" },
  ];
};

export const paymentServices = () => {
  return [
    { id: 0, value: 1, text: "Thanh toán trả sau" },
    { id: 1, value: 2, text: "Thanh toán trả trước" },
  ];
};

export const dataSortPriority = () => {
  return [
    { value: "newest", label: "Mới nhất" },
    { value: "popular", label: "Phổ biến" },
    { value: "lowest", label: "Số điểm từ thấp đến cao" },
    { value: "highest", label: "Số điểm từ cao đến thấp" },
  ];
};

export const dataSortDoctor = () => {
  return [
    { value: "all", label: "Tất cả" },
    { value: "online", label: "Bác sĩ online" },
    { value: "male", label: "Bác sĩ nam" },
    { value: "female", label: "Bác sĩ nữ" },
  ];
};

export const dataSortHospital = () => {
  return [
    { value: "all", label: "Tất cả" },
    { value: "hong_duc", label: "Hồng Đức" },
    { value: "nhi_dong", label: "Nhi Đồng 2" },
    { value: "ung_buou", label: "Ung Bướu" },
    { value: "drkhoa", label: "DrKhoa" },
  ];
};
