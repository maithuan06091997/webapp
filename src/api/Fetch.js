import Axios from "axios";
import { getUA } from "react-device-detect";
// import { decryptParam } from "../helpers";

const headers = () => {
  const storageUser = JSON.parse(localStorage.getItem("user"));
  const token = storageUser.token;
  return {
    "Content-Type": "application/json",
    deviceId: getUA,
    oh_token: token,
    platform: "webapp", //webdroh
  };
};
const AxiosUtil = Axios.create({});

AxiosUtil.interceptors.response.use(
  function (response) {
    // alertSuccess(response.data);
    return response;
  },
  function (error) {
    if (Axios.isCancel(error)) {
      throw error;
    }
    // alertError(error);
    if (error.response) {
      if (error.response.status === 401) {
        // history.push("/home");
        // clearAuth();
      }
    }
    return Promise.reject(error);
  }
);

export async function authGet(url, params = {}, tokens) {
  return await AxiosUtil.get(url, {
    params,
    headers: headers(),
    cancelToken: tokens?.token,
  });
}
export async function authPut(url, params = {}) {
  return await AxiosUtil.put(url, params, { headers: headers() });
}

export async function authPatch(url, params = {}) {
  return await AxiosUtil.patch(url, params, { headers: headers() });
}

export async function authPost(url, params = {}) {
  return await AxiosUtil.post(url, params, { headers: headers() });
}

export async function authDelete(url) {
  return await AxiosUtil.delete(url, { headers: headers() });
}

export const FetchGetHeader = (url, params = null, result) => {
  if (params) {
    Axios.get(url, { headers: headers(), params })
      .then((res) => {
        result(res);
      })
      .catch((error) => {
        result(error.response);
      });
  } else {
    Axios.get(url, { headers: headers() })
      .then((res) => {
        result(res);
      })
      .catch((error) => {
        result(error.response);
      });
  }
};

export const FetchPostHeader = (url, params, result) => {
  Axios.post(url, params, { headers: headers() })
    .then((res) => {
      result(res);
    })
    .catch((error) => {
      result(error.response);
    });
};

export const FetchPutHeader = (url, params, result) => {
  Axios.put(url, params, { headers: headers() })
    .then((res) => {
      result(res);
    })
    .catch((error) => {
      result(error.response);
    });
};

export const FetchDeleteHeader = (url, params, result) => {
  Axios.delete(url, {
    data: { one_health_msg: params },
    headers: headers(),
  })
    .then((res) => {
      result(res);
    })
    .catch((error) => {
      result(error.response);
    });
};

export default AxiosUtil;
