const ohDomain = process.env.REACT_APP_API_OH_DOMAIN;
const ohDomainPayment = process.env.REACT_APP_API_OH_DOMAIN_PAYMENT;
const ohDomainCarePay = process.env.REACT_APP_API_OH_DOMAIN_CAREPAY;
const ohDomainSocialNetwork = process.env.REACT_APP_API_OH_DOMAIN_SOCIAL;
const ohDomainVideoCall = process.env.REACT_APP_VIDEO_CALL_DOMAIN;
const ohDomainChat = process.env.REACT_APP_CHAT_DOMAIN;
const ohDomainChat1 = process.env.REACT_APP_CHAT_DOMAIN1;
const turnUrl = process.env.REACT_APP_TURN_URL;
const turnCredential = process.env.REACT_APP_TURN_CREDENTIAL;
const turnUsername = process.env.REACT_APP_TURN_USERNAME;
const googleKeyApi = process.env.REACT_APP_GOOGLE_KEY_API;

export const setting = {
  api: ohDomain,
  api_payment: ohDomainPayment,
  api_carepay: ohDomainCarePay,
  api_social: ohDomainSocialNetwork,
  ohDomainVideoCall,
  ohDomainChat,
  ohDomainChat1,
  turnUrl,
  turnCredential,
  turnUsername,
  googleKeyApi: googleKeyApi,
};
