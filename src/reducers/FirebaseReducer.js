import {
    ActionFirebaseTokenActive
} from "../actions/ActionType";

let DEFAULT = {
    token: false,
    name: ""
};

if (localStorage.getItem("ck_firebase")){
    DEFAULT = JSON.parse(localStorage.getItem("ck_firebase"));
}

const FirebaseReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionFirebaseTokenActive:
            return {
                ...state, ...{
                    token: action.data.token,
                    name: action.data.name
                }
            };
        default:
            return state;
    }
}

export default FirebaseReducer;