import {
    ActionBadgeNotification, ActionYieldNotification
} from "../actions/ActionType";

let DEFAULT = "";

if (localStorage.getItem("notify")){
    DEFAULT = JSON.parse(localStorage.getItem("notify"));
}

const NotificationReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionBadgeNotification:
            return state;
        case ActionYieldNotification:
            return action.data;
        default:
            return state;
    }
}

export default NotificationReducer;