import {
    ActionConfirmation
} from "../actions/ActionType";

let DEFAULT = {
    status: false,
    type: ""
};

if (sessionStorage.getItem("confirm")){
    DEFAULT = JSON.parse(sessionStorage.getItem("confirm"));
}

const ConfirmationReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionConfirmation:
            if (action.data) {
                return {...state, ...action.data};
            }
            return {...state, ...{status: false, type: "", gateway: ""}};
        default:
            return state;
    }
}

export default ConfirmationReducer;