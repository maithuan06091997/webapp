import { ActionDrugStore } from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("drug_store")) {
  DEFAULT = JSON.parse(sessionStorage.getItem("drug_store"));
}
const DrugStoreReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ActionDrugStore:
      const dataExamination = { form: action.data };
      return { ...state, ...dataExamination };
    default:
      return state;
  }
};

export default DrugStoreReducer;
