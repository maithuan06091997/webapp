import // ActionSignIn,
// ActionSignOut,
// ActionSignInSuccess,
// ActionSignInFailed,
// ActionChangePassword,
// ActionRegister,
// ActionUpdateUser,
// ActionUpdateAvatarUser,
// ActionCleanStoreUser,
// ActionTokenExpired,
"../actions/ActionType";
// import { encryptParam } from "../helpers";
import _ from "lodash";

let DEFAULT = {
  personal: "",
  avatar: "",
  auth: false,
  update: false,
  type: "",
  message: "",
};

if (localStorage.getItem("user")) {
  DEFAULT = JSON.parse(localStorage.getItem("user"));
}

const UserReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    // case ActionSignIn:
    //   return state;

    // case ActionRegister:
    //   return state;

    // case ActionUpdateAvatarUser:
    //   const dataAvatarUser = action.data;
    //   return { ...state, avatar: dataAvatarUser };

    // case ActionUpdateUser:
    //   const dataUpdateUser = action.data;
    //   const tmpUpdateUser = {
    //     birthday: dataUpdateUser.birthday,
    //     first_name: dataUpdateUser.first_name,
    //     last_name: dataUpdateUser.last_name,
    //     email: dataUpdateUser.email,
    //     address: dataUpdateUser.address,
    //     province: dataUpdateUser.province,
    //     district: dataUpdateUser.district,
    //     ward: dataUpdateUser.ward,
    //     sex: dataUpdateUser.sex,
    //     modified_time: dataUpdateUser.modified_time,
    //   };
    //   const dataUpdatePersonal = { ...state.personal, ...tmpUpdateUser };
    //   return { ...state, personal: dataUpdatePersonal };

    // case ActionSignInSuccess:
    //   const avatar = action.data.avatar;
    //   let dataNew = _.omit(action.data, "secret");
    //   dataNew = _.omit(action.data, "avatar");
    //   return {
    //     ...state,
    //     personal: { ...dataNew, token: encryptParam(dataNew.token) },
    //     avatar: avatar,
    //     auth: true,
    //     type: "",
    //     message: "",
    //   };

    // case ActionSignInFailed:
    //   return {
    //     ...state,
    //     personal: "",
    //     auth: false,
    //     type: "",
    //     message: action.message,
    //   };

    // case ActionSignOut:
    //   return { ...state, personal: "", auth: false, type: "", message: "" };

    // case ActionTokenExpired:
    //   let expired = "";
    //   if (action.data) {
    //     expired =
    //       action.data.code && action.data.code === "007" ? "token_expired" : "";
    //     return {
    //       ...state,
    //       personal: "",
    //       auth: false,
    //       type: expired,
    //       message: "",
    //     };
    //   }
    //   return { ...state };

    // case ActionChangePassword:
    //   return {
    //     ...state,
    //     personal: "",
    //     auth: false,
    //     update: action.data,
    //     message: "",
    //   };

    // case ActionCleanStoreUser:
    //   return { ...state, personal: "", auth: false, type: "", message: "" };

    default:
      return state;
  }
};

export default UserReducer;
