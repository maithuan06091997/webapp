import { ActionHomeTest } from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("home_test")) {
  DEFAULT = JSON.parse(sessionStorage.getItem("home_test"));
}
const HomeTestReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ActionHomeTest:
      const dataExamination = { form: action.data };
      return { ...state, ...dataExamination };
    default:
      return state;
  }
};

export default HomeTestReducer;
