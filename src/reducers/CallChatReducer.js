import { ActionCallChat } from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("call_chat")) {
  DEFAULT = JSON.parse(sessionStorage.getItem("call_chat"));
}

const CallChatReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ActionCallChat:
      const dataCallChat = { form: action.data };
      return { ...state, ...dataCallChat };
    default:
      return state;
  }
};

export default CallChatReducer;
