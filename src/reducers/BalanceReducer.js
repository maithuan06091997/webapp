import {
    ActionBalanceUser, ActionYieldBalanceUser, ActionCleanBalanceUser, ActionUpdatePointPromotion
} from "../actions/ActionType";

let DEFAULT = {
    point: 0,
    vnd: 0,
    expire: 0
};

if (localStorage.getItem("balance")){
    DEFAULT = JSON.parse(localStorage.getItem("balance"));
}

const BalanceReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionBalanceUser:
            return state;
        case ActionUpdatePointPromotion:
            const dataPointPromotion = {point: action.data.balance};
            return {...state, ...dataPointPromotion};
        case ActionYieldBalanceUser:
            let balance = 0;
            let point = 0;
            let expire = 0;
            if (action.data) {
                if (action.data.code === "007") {
                    expire = action.data.code;
                } else {
                    const dataBalance = action.data;
                    balance = dataBalance.balance;
                    point = dataBalance.point;
                }
            }
            const dataPointBalance = {point: point, vnd: balance, expire: expire};
            return {...state, ...dataPointBalance};
        case ActionCleanBalanceUser:
            return {...state, ...{point: 0, vnd: 0, expire: 0}};
        default:
            return state;
    }
}

export default BalanceReducer;