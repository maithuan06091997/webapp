import {
    ActionLanguageActive
} from "../actions/ActionType";

let DEFAULT = "VI";

if (localStorage.getItem("language")){
    DEFAULT = JSON.parse(localStorage.getItem("language"));
}

const LanguageReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionLanguageActive:
            return action.data;
        default:
            return state;
    }
}

export default LanguageReducer;