import {
    ActionVerifyPhone
} from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("verify")){
    DEFAULT = JSON.parse(sessionStorage.getItem("verify"));
}

const VerifyReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionVerifyPhone:
            let dataVerifyPhone = "";
            if (action.data) {
                dataVerifyPhone = action.data;
            }
            return dataVerifyPhone;
        default:
            return state;
    }
}

export default VerifyReducer;