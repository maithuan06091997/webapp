import {
    ActionBrowserNotifyActive, ActionBrowserCameraActive
} from "../actions/ActionType";

let DEFAULT = "";

if (localStorage.getItem("browser")){
    DEFAULT = JSON.parse(localStorage.getItem("browser"));
}

const BrowserReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionBrowserNotifyActive:
            return {...state, ...action.data};
        case ActionBrowserCameraActive:
            return {...state, ...action.data};
        default:
            return state;
    }
}

export default BrowserReducer;