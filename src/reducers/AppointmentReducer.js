import {
    ActionAppointment
} from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("appointment")){
    DEFAULT = JSON.parse(sessionStorage.getItem("appointment"));
}

const AppointmentReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionAppointment:
            const dataExamination = {form: action.data};
            return {...state, ...dataExamination};
        default:
            return state;
    }
}

export default AppointmentReducer;