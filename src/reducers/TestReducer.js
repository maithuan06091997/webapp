import { ActionTest } from "../actions/ActionType";

let DEFAULT = "";

const TestReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ActionTest:
      const dataCallChat = { form: action.data };
      return { ...state, ...dataCallChat };
    default:
      return state;
  }
};

export default TestReducer;
