import { ActionCategoryService } from "../actions/ActionType";

let DEFAULT = "";

if (sessionStorage.getItem("category_service")) {
  DEFAULT = JSON.parse(sessionStorage.getItem("category_service"));
}
const CategoryServiceReducer = (state = DEFAULT, action) => {
  switch (action.type) {
    case ActionCategoryService:
      const dataExamination = { form: action.data };
      return { ...state, ...dataExamination };
    default:
      return state;
  }
};

export default CategoryServiceReducer;
