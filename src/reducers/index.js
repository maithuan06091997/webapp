// import UserReducer from "./UserReducer";
import LanguageReducer from "./LanguageReducer";
import VerifyReducer from "./VerifyReducer";
import AppointmentReducer from "./AppointmentReducer";
import CallChatReducer from "./CallChatReducer";
import CategoryServiceReducer from "./CategoryServiceReducer";
import DrugStoreReducer from "./DrugStoreReducer";
import HomeTestReducer from "./HomeTestReducer";
import GlobalReducer from "./GlobalReducer";
import ConfirmationReducer from "./ConfirmationReducer";
import NotificationReducer from "./NotificationReducer";
import FirebaseReducer from "./FirebaseReducer";
import BrowserReducer from "./BrowserReducer";
import BalanceReducer from "./BalanceReducer";
import TestReducer from "./TestReducer";
import { combineReducers } from "redux";

const reducer = combineReducers({
  // user: UserReducer,
  language: LanguageReducer,
  verify: VerifyReducer,
  appointment: AppointmentReducer,
  category_service: CategoryServiceReducer,
  drug_store: DrugStoreReducer,
  home_test: HomeTestReducer,
  call_chat: CallChatReducer,
  global: GlobalReducer,
  confirm: ConfirmationReducer,
  notify: NotificationReducer,
  ck_firebase: FirebaseReducer,
  browser: BrowserReducer,
  balance: BalanceReducer,
  test: TestReducer,
});

export default reducer;
