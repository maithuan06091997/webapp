import {
    ActionListCity, ActionYieldListCity, ActionFindProfile
} from "../actions/ActionType";

const DEFAULT = "";

const GlobalReducer = (state = DEFAULT, action) => {
    switch (action.type) {
        case ActionListCity:
            return state;
        case ActionYieldListCity:
            const city = {city: action.data};
            return {...state, ...city};
        case ActionFindProfile:
            const profile = {find_profile: action.data};
            return {...state, ...profile};
        default:
            return state;
    }
}

export default GlobalReducer;