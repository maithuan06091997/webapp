import React, {Fragment} from "react";
import {Skeleton} from "antd";
import "./style.scss";

const BoxSkeleton = (props) => {
    //*** Declare props ***//
    const {skeleton, data, full, panelCol, rows, length} = props;

    function yieldSkeleton() {
        const list = new Array(length).fill(1);
        let yieldSkeleton;
        if (full) {
            yieldSkeleton = list.map((item, index) => {
                return (
                    <div key={index} className="col-xl-12 col-lg-12 col-md-12 wrap-skeleton-col">
                        <div className="wrap-skeleton-item">
                            <Skeleton active paragraph={{rows: rows}}/>
                        </div>
                    </div>
                );
            });
        } else {
            if (panelCol === 3) {
                yieldSkeleton = list.map((item, index) => {
                    return (
                        <div key={index} className="col-xl-4 col-lg-4 col-md-4 wrap-skeleton-col">
                            <div className="wrap-skeleton-item">
                                <Skeleton active paragraph={{rows: rows}}/>
                            </div>
                        </div>
                    );
                });
            } else {
                yieldSkeleton = list.map((item, index) => {
                    return (
                        <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-skeleton-col">
                            <div className="wrap-skeleton-item">
                                <Skeleton active paragraph={{rows: rows}}/>
                            </div>
                        </div>
                    );
                });
            }
        }
        return (
            <Fragment>{yieldSkeleton}</Fragment>
        );
    }

    return (
        <Fragment>
            {skeleton ? yieldSkeleton() : data}
        </Fragment>
    );
};

export default BoxSkeleton;
