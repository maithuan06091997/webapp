import React from "react";
import { AiFillPlusCircle, AiOutlineArrowLeft } from "react-icons/ai";
import { Link, withRouter } from "react-router-dom";
import { public_url, url } from "../../constants/Path";
// import { history } from "../../helpers/history";
import "./style.scss";
function CardServices(props) {
  const {
    header,
    description,
    step,
    children,
    createProfile,
    hiddenButtonBack,
    history,
    className,
  } = props;

  const onPressCreateNew = () => {
    history.push(url.name.account_create_relative_profile);
  };

  const back = () => {
    history.goBack();
  };

  return (
    <div className={`card card-custom ${className ? className : ""}`}>
      <div className="card-header d-flex">
        <h3>
          {!hiddenButtonBack && (
            <span className="back-button" onClick={back}>
              <AiOutlineArrowLeft />
            </span>
          )}{" "}
          {header}
        </h3>
        <Link title="Lịch sử dịch vụ" to={url.name.service_health_history}>
          <img
            className="img-fluid"
            src={public_url.img + "/icons/icon_schedule.png"}
            alt=""
            width={30}
          />
        </Link>
        {createProfile && (
          <div className="wrap-event-action">
            <div className="box-button-event">
              <div onClick={onPressCreateNew} className="ant-btn btn-accept">
                <AiFillPlusCircle /> <span>Tạo mới hồ sơ</span>
              </div>
            </div>
          </div>
        )}
      </div>
      <div className="card-body-top">
        <div className="descript">
          <p dangerouslySetInnerHTML={{ __html: description }}></p>
        </div>
        <div className="step">
          <div className="step-text">{step}</div>
        </div>
      </div>
      <div className="card-body">{children}</div>
    </div>
  );
}

export default withRouter(CardServices);
