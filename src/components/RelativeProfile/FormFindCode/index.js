import React, {Fragment, useEffect, useState} from "react";
import _ from "lodash";
import {Input, Button, Form, Empty} from "antd";
import {FaPhoneAlt, FaUserAlt, FaTransgender} from "react-icons/fa";
import {BsChevronRight} from "react-icons/bs";
import BoxSkeleton from "../../BoxSkeleton";
import {ModelFindPatientCode} from "../../../models/ModelProfile";
import ModalConfirmProfile from "../ModalConfirmProfile";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../helpers";

const FormFindCode = (props) => {
    const [skeleton, setSkeleton] = useState(false);
    const [listPatientCode, setListPatientCode] = useState([]);
    const [detailPatientCode, setDetailPatientCode] = useState(undefined);
    const [profileName, setProfileName] = useState(undefined);
    //*** Declare props ***//
    const {
        _TokenExpired, modalFindProfile, onCloseModalFindProfile,
        hospitalName, hospitalKey
    } = props;

    const [form] = Form.useForm();

    //*** Handle when open modal ***//
    useEffect(() => {
        if (modalFindProfile) {
            form.resetFields();
            setListPatientCode([]);
        }
    }, [hospitalKey, modalFindProfile, form]);

    const onFinish = values => {
        const reqParamFt = {
            patient_code: values.itemCode,
            source: hospitalKey
        };
        setSkeleton(true);
        ModelFindPatientCode(reqParamFt).then(resultFt => {
            const dataListPatientCode = resultFt.data.one_health_msg;
            setListPatientCode(dataListPatientCode);
            setSkeleton(false);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                console.log("FormFindCode - FindPatientCode");
                setSkeleton(false);
            }
        });
    };

    const onConfirmProfile = item => {
        setProfileName(item.name);
        setDetailPatientCode(item);
    };

    const onCloseEventConfirmProfile = flag => {
        setProfileName(undefined);
        setDetailPatientCode(undefined);
        if (flag === "confirm") {
            onCloseModalFindProfile();
        }
    };

    function yieldListPatientCode() {
        if (listPatientCode.length > 0) {
            return _.map(listPatientCode, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-main">
                                <div className="wrap-line">
                                    <FaUserAlt/> <span>{item.name}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaTransgender/> <span>{func.findGender(item.sex, "text")}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaPhoneAlt/> <span>{func.yieldCoverPhone(item.phone)}</span>
                                </div>
                            </div>
                            <div onClick={onConfirmProfile.bind(this, item)} className="wrap-event-action">
                                <BsChevronRight/>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Thông tin bệnh nhân trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <h4>Bệnh viện {hospitalName}</h4>
            <p className="text-muted wrap-description">
                Quét QRcode/Barcode hoặc nhập mã số bệnh nhân in trên toa thuốc, phiếu chỉ định,
                phiếu trả kết quả cận lâm sàng để tìm hồ sơ
            </p>
            <Form onFinish={onFinish}
                  form={form}
                  size="large"
                  className="form-find-code"
                  name="form-find-code">
                <Input.Group className="text-center" compact>
                    <Form.Item className="text-left"
                               name="itemCode"
                               rules={[{validateTrigger: 'onBlur', required: true, message: "Vui lòng nhập mã"}]}>
                        <Input placeholder="Ví dụ: 123456"/>
                    </Form.Item>
                    {" "}
                    <Form.Item>
                        <Button className="btn-accept" htmlType="submit"><span>Tìm mã số</span></Button>
                    </Form.Item>
                </Input.Group>
            </Form>
            <hr/>
            <div className="wrap-panel-content">
                <div className="row">
                    <BoxSkeleton skeleton={skeleton}
                                 full={false}
                                 length={8}
                                 rows={2}
                                 data={yieldListPatientCode()}/>
                </div>
            </div>
            <ModalConfirmProfile detailPatientCode={detailPatientCode}
                                 profileName={profileName}
                                 hospitalKey={hospitalKey}
                                 onCloseEventConfirmProfile={onCloseEventConfirmProfile}/>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(FormFindCode));
