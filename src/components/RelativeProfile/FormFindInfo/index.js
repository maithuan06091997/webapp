import React, {Fragment, useEffect, useState} from "react";
import _ from "lodash";
import {Input, Button, Form, Empty} from "antd";
import {FaPhoneAlt, FaTransgender, FaUserAlt} from "react-icons/fa";
import {BsChevronRight} from "react-icons/bs";
import {dataGender} from "../../../constants/Data";
import BoxSkeleton from "../../BoxSkeleton";
import BoxInputBirthday from "../../BoxInputBirthday";
import BoxInputFullName from "../../BoxInputFullName";
import BoxSelect from "../../BoxSelect";
import {ModelFindPatientInfo} from "../../../models/ModelProfile";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../helpers";
import ModalConfirmProfile from "../ModalConfirmProfile";

const FormFindInfo = (props) => {
    const [skeleton, setSkeleton] = useState(false);
    const [fields, setFields] = useState([]);
    const [listPatientCode, setListPatientCode] = useState([]);
    const [detailPatientCode, setDetailPatientCode] = useState(undefined);
    const [profileName, setProfileName] = useState(undefined);
    //*** Declare props ***//
    const {
        _TokenExpired, modalFindProfile, onCloseModalFindProfile,
        hospitalName, hospitalKey, global
    } = props;

    const [form] = Form.useForm();

    //*** Handle when open modal ***//
    useEffect(() => {
        if (modalFindProfile) {
            form.resetFields();
            setListPatientCode([]);
        }
    }, [hospitalKey, modalFindProfile, form]);

    const onFinish = values => {
        const reqParamFt = {
            name: values.itemFullName.trim(),
            source: hospitalKey,
            birthday:  func.formatISODatetime(values.itemBirthday),
            sex: func.findGender(values.itemGender.value, "value"),
            province_id: values.itemCity,
            identity_num: values.itemCMND
        };
        setSkeleton(true);
        ModelFindPatientInfo(reqParamFt).then(resultFt => {
            const dataListPatientCode = resultFt.data.one_health_msg;
            setListPatientCode(dataListPatientCode);
            setSkeleton(false);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                console.log("FormFindInfo - FindPatientInfo");
                setSkeleton(false);
            }
        })
    };

    const onInputBirthday = (e) => {
        const value = func.yieldValueDate(e.target.value);
        setFields([{name: ['itemBirthday'], value: value}]);
    };

    const onConfirmProfile = item => {
        setProfileName(item.name);
        setDetailPatientCode(item);
    };

    const onCloseEventConfirmProfile = flag => {
        setProfileName(undefined);
        setDetailPatientCode(undefined);
        if (flag === "confirm") {
            onCloseModalFindProfile();
        }
    };

    function yieldListPatientCode() {
        if (listPatientCode.length > 0) {
            return _.map(listPatientCode, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-main">
                                <div className="wrap-line">
                                    <FaUserAlt/> <span>{item.name}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaTransgender/> <span>{func.findGender(item.sex, "text")}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaPhoneAlt/> <span>{func.yieldCoverPhone(item.phone)}</span>
                                </div>
                            </div>
                            <div onClick={onConfirmProfile.bind(this, item)} className="wrap-event-action">
                                <BsChevronRight/>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Thông tin bệnh nhân trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <h4>Bệnh viện {hospitalName}</h4>
            <p className="text-muted wrap-description">
                Vui lòng nhập đúng thông tin bên dưới để tìm hồ sơ
            </p>
            <Form onFinish={onFinish}
                  form={form}
                  fields={fields}
                  size="large"
                  className="form-find-info"
                  name="form-find-info">
                <Input.Group compact>
                    <BoxInputFullName label="Họ và tên"
                                      required={true}
                                      name="itemFullName"
                                      placeholder="Nhập họ và tên"/>
                    {" "}
                    <Form.Item label="Giới tính"
                               name="itemGender"
                               rules={[{required: true, message: "Vui lòng chọn giới tính"}]}>
                        <BoxSelect placeholder="Chọn giới tính"
                                   data={dataGender(this)}/>
                    </Form.Item>

                </Input.Group>
                {" "}
                <Input.Group compact>
                    <BoxInputBirthday label="Ngày sinh (dd/mm/yyyy)"
                                      name="itemBirthday"
                                      onChange={onInputBirthday}
                                      placeholder="__ / __ / ____"/>
                    {" "}
                    <Form.Item label="Chứng minh nhân dân"
                               name="itemCMND">
                        <Input/>
                    </Form.Item>
                    {" "}
                    <Form.Item label="Tỉnh/Thành"
                               name="itemCity">
                        <BoxSelect placeholder="Chọn tỉnh/thành"
                                   data={global.city}/>
                    </Form.Item>
                </Input.Group>
                {" "}
                <Form.Item>
                    <Button className="w-100 mt-2 btn-accept" htmlType="submit">
                        <span>Tìm hồ sơ</span>
                    </Button>
                </Form.Item>
            </Form>
            <hr/>
            <div className="wrap-panel-content">
                <div className="row">
                    <BoxSkeleton skeleton={skeleton}
                                 full={false}
                                 length={8}
                                 rows={2}
                                 data={yieldListPatientCode()}/>
                </div>
            </div>
            <ModalConfirmProfile detailPatientCode={detailPatientCode}
                                 profileName={profileName}
                                 hospitalKey={hospitalKey}
                                 onCloseEventConfirmProfile={onCloseEventConfirmProfile}/>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(FormFindInfo));
