import React, {Fragment, useEffect, useState} from "react";
import {Form, Button, Modal} from "antd";
import BoxInputPhone from "../../BoxInputPhone";
import {_FindProfile} from "../../../actions";
import {withRouter} from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

const ModalConfirmProfile = (props) => {
    const [message, setMessage] = useState(undefined);
    const [modalConfirmProfile, setModalConfirmProfile] = useState(false);
    //*** Declare props ***//
    const {
        _FindProfile, onCloseEventConfirmProfile, detailPatientCode,
        profileName, hospitalKey
    } = props;

    const [form] = Form.useForm();

    //*** Handle when open modal ***//
    useEffect(() => {
        if (detailPatientCode) {
            form.resetFields();
            setModalConfirmProfile(true);
            setMessage(undefined);
        }
    }, [detailPatientCode, form]);

    const onCloseModalConfirmProfile = flag => {
        setModalConfirmProfile(false);
        if (flag === "confirm") {
            onCloseEventConfirmProfile(flag);
        } else {
            onCloseEventConfirmProfile();
        }
    };

    const onFinish = values => {
        setMessage(undefined);
        const phone = func.isValidPhone(values.itemPhone);
        if (phone === detailPatientCode.phone) {
            _FindProfile({...detailPatientCode, ...{hospital: hospitalKey}});
            onCloseModalConfirmProfile("confirm");
        } else {
            setMessage("Số điện thoại xác nhận không đúng");
        }
    };

    let yieldMessage;
    if (message) {
        yieldMessage = (<div className="alert alert-danger" role="alert">{message}</div>);
    }

    return (
        <Fragment>
            <Modal title="Thông báo"
                   className="block-modal-confirm-profile"
                   closable={true}
                   maskClosable={false}
                   footer={null}
                   visible={modalConfirmProfile}
                   onCancel={onCloseModalConfirmProfile}
                   centered>
                <p>Vì lí do bảo mật thông tin, bạn vui lòng xác nhận số điện thoại của hồ sơ "{profileName}"</p>
                <Form onFinish={onFinish}
                      form={form}
                      size="large"
                      className="form-confirm-profile"
                      name="form-confirm-profile">
                    {yieldMessage}
                    {" "}
                    <BoxInputPhone required={true}
                                   name="itemPhone"
                                   placeholder="Xác nhận số điện thoại"/>
                    {" "}
                    <Form.Item>
                        <Button className="btn-save w-100" htmlType="submit"><span>Xác nhận</span></Button>
                    </Form.Item>
                </Form>
            </Modal>
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_FindProfile}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(ModalConfirmProfile));
