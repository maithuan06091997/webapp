import React, {Fragment, useState} from "react";
import {Collapse, Input, Button, Tabs, Modal} from "antd";
import FormFindCode from "../FormFindCode";
import FormFindInfo from "../FormFindInfo";
import {VscSearch} from "react-icons/vsc";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./style.scss";

const FindProfile = (props) => {
    const [modalFindProfile, setModalFindProfile] = useState(false);
    const [hospitalName, setHospitalName] = useState("");
    const [hospitalKey, setHospitalKey] = useState("");
    const {Panel} = Collapse;
    const {TabPane} = Tabs;

    const onOpenFindProfile = (key, name) => {
        setHospitalName(name);
        setHospitalKey(key);
        setModalFindProfile(true);
    };

    const onCloseModalFindProfile = () => {
        setModalFindProfile(false);
    };

    return (
        <Fragment>
            <Collapse className="collapse-custom block-collapse-find-profile" defaultActiveKey={['1']}>
                <Panel header="Tìm hồ sơ theo mã bệnh nhân" key="1">
                    <Input.Group compact>
                        <Button onClick={onOpenFindProfile.bind(this, "hong_duc", "Hồng Đức")}
                                icon={<VscSearch size="1.2em" className="mr-2"/>}
                                type="primary"><span>Mã hồ sơ Hồng Đức</span></Button>
                        {" "}
                        <Button onClick={onOpenFindProfile.bind(this, "drkhoa", "Dr.Khoa")}
                                icon={<VscSearch size="1.2em" className="mr-2"/>}
                                type="primary"><span>Mã hồ sơ Dr.Khoa</span></Button>
                    </Input.Group>
                    <Input.Group compact>
                        <Button onClick={onOpenFindProfile.bind(this, "nhi_dong", "Nhi Đồng 2")}
                                icon={<VscSearch size="1.2em" className="mr-2"/>}
                                type="primary"><span>Mã hồ sơ Nhi Đồng 2</span></Button>
                        {" "}
                        <Button onClick={onOpenFindProfile.bind(this, "ung_buou", "Ung Bướu")}
                                icon={<VscSearch size="1.2em" className="mr-2"/>}
                                type="primary"><span>Mã hồ sơ Ung Bướu</span></Button>
                    </Input.Group>
                </Panel>
            </Collapse>
            <Modal title="Tìm hồ sơ"
                   className="block-modal-find-profile ant-modal-custom"
                   closable={true}
                   maskClosable={false}
                   width={800}
                   visible={modalFindProfile}
                   footer={null}
                   onCancel={onCloseModalFindProfile}>
                <Tabs defaultActiveKey="1">
                    <TabPane className="block-tabpane-find-code" tab="Mã số bệnh nhân" key="1">
                        <FormFindCode {...props}
                                      onCloseModalFindProfile={onCloseModalFindProfile}
                                      modalFindProfile={modalFindProfile}
                                      hospitalName={hospitalName}
                                      hospitalKey={hospitalKey}/>
                    </TabPane>
                    {" "}
                    <TabPane className="block-tabpane-find-info scrollbar-1" tab="Thông tin bệnh nhân" key="2">
                        <FormFindInfo {...props}
                                      onCloseModalFindProfile={onCloseModalFindProfile}
                                      modalFindProfile={modalFindProfile}
                                      hospitalName={hospitalName}
                                      hospitalKey={hospitalKey}/>
                    </TabPane>
                </Tabs>
            </Modal>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(FindProfile));
