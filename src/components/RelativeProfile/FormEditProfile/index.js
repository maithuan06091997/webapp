import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import i18next from "i18next";
import { Button, Form, Input, Modal } from "antd";
import { msg_text } from "../../../constants/Message";
import {
  hong_duc,
  nhi_dong,
  ung_buou,
  drkhoa,
} from "../../../constants/Default";
import { dataGender, dataRelationship } from "../../../constants/Data";
import BoxSelect from "../../BoxSelect";
import BoxInputPhone from "../../BoxInputPhone";
import BoxInputBirthday from "../../BoxInputBirthday";
import BoxInputFullName from "../../BoxInputFullName";
import FindProfile from "../FindProfile/FindProfile";
import { ModelUpdateUserInfo } from "../../../models/ModelUser";
import { ModelUpdateProfile } from "../../../models/ModelProfile";
import {
  ModelCheckInsurrance,
  ModelListDistrict,
  ModelListWard,
} from "../../../models/ModelCommon";
import Loading from "../../Loading";
import { _ListCity, _UpdateUser } from "../../../actions";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";
import { url } from "../../../constants/Path";

const FormEditProfile = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [isOwner, setIsOwner] = useState(false);
  const [listDistrict, setListDistrict] = useState([]);
  const [triggerDistrict, setTriggerDistrict] = useState([]);
  const [listPatientCode, setListPatientCode] = useState([]);
  const [listWard, setListWard] = useState([]);
  const [triggerWard, setTriggerWard] = useState([]);
  const [fields, setFields] = useState([]);
  const [valueRelationship, setValueRelationship] = useState(undefined);
  const [valueFullName, setValueFullName] = useState(undefined);
  const [valueBirthday, setValueBirthday] = useState(undefined);

  //*** Declare props ***//
  const { _TokenExpired, _ListCity, _UpdateUser, global, history, location } =
    props;

  const [form] = Form.useForm();
  const [modal, contextHolder] = Modal.useModal();
  const user = JSON.parse(localStorage.getItem("user"));

  const handleListDistrict = useCallback((id, trigger = false) => {
    if (id) {
      const reqParamFt = {
        data: {
          province: {
            id: id,
          },
        },
      };
      ModelListDistrict(reqParamFt)
        .then((resultFt) => {
          if (!isRendered.current) {
            const dataListDistrict = resultFt.data.one_health_msg;
            setListDistrict(dataListDistrict);
            if (trigger) {
              setTriggerDistrict(dataListDistrict);
            }
          }
          return null;
        })
        .catch((error) => {
          if (error.code !== "007") {
            console.log("FormEditProfile - ListDistrict");
          }
        });
    }
    return () => {
      isRendered.current = true;
    };
  }, []);

  const handleListWard = useCallback((id, trigger = false) => {
    if (id) {
      const reqParamFt = {
        data: {
          district: {
            id: id,
          },
        },
      };
      ModelListWard(reqParamFt)
        .then((resultFt) => {
          if (!isRendered.current) {
            const dataListWard = resultFt.data.one_health_msg;
            setListWard(dataListWard);
            if (trigger) {
              setTriggerWard(dataListWard);
            }
          }
          return null;
        })
        .catch((error) => {
          if (error.code !== "007") {
            console.log("FormEditProfile - ListWard");
          }
        });
    }
    return () => {
      isRendered.current = true;
    };
  }, []);

  useEffect(() => {
    _ListCity();
  }, [_ListCity]);

  useEffect(() => {
    const detail = location.state.detail_profile;
    if (detail) {
      const fullName = detail.name ? detail.name : detail.names[0];
      const birthday = detail.birthday
        ? func.formatDatetimeToString(detail.birthday)
        : "";
      const phone = detail.phone ? detail.phone : detail.phones[0];
      const cmnd = detail.identity_num ? detail.identity_num : "";
      const bhyt = detail.num_insurance ? detail.num_insurance : "";
      const address = detail.location.address ? detail.location.address : "";
      const father_name = detail.father_name ? detail.father_name : "";
      const father_phone = detail.father_phone ? detail.father_phone : "";
      const mother_name = detail.mother_name ? detail.mother_name : "";
      const mother_phone = detail.mother_phone ? detail.mother_phone : "";

      let tmpPatient = [];
      const his_profile_code = detail.his_profile.patient_code
        ? detail.his_profile.patient_code
        : "";
      const nhi_dong_code = detail.nhi_dong.patient_code
        ? detail.nhi_dong.patient_code
        : "";
      const ung_buou_code = detail.ung_buou.patient_code
        ? detail.ung_buou.patient_code
        : "";
      const drkhoa_code = detail.drkhoa.patient_code
        ? detail.drkhoa.patient_code
        : "";
      if (his_profile_code) {
        tmpPatient.push({
          hospital_key: hong_duc.key,
          hospital_name: hong_duc.name,
          patient_code: his_profile_code,
        });
      }
      if (nhi_dong_code) {
        tmpPatient.push({
          hospital_key: nhi_dong.key,
          hospital_name: nhi_dong.name,
          patient_code: nhi_dong_code,
        });
      }
      if (ung_buou_code) {
        tmpPatient.push({
          hospital_key: ung_buou.key,
          hospital_name: ung_buou.name,
          patient_code: ung_buou_code,
        });
      }
      if (drkhoa_code) {
        tmpPatient.push({
          hospital_key: drkhoa.key,
          hospital_name: drkhoa.name,
          patient_code: drkhoa_code,
        });
      }

      const gender_value = func.findGender(detail.sex, "value");
      const gender_name = func.findGender(detail.sex, "text");
      const gender =
        gender_value && gender_name
          ? { value: gender_value, label: gender_name }
          : "";

      const relationship_value = func.findRelationship(
        detail.relationship,
        "value"
      );
      const relationship_name = func.findRelationship(
        detail.relationship,
        "text"
      );
      const relationship =
        relationship_value && relationship_name
          ? { value: relationship_value, label: relationship_name }
          : "";

      const city_id =
        global.city && detail.location.province
          ? detail.location.province.province_id
          : "";
      const city_name =
        global.city && detail.location.province
          ? detail.location.province.vi_name
          : "";
      const city =
        city_id && city_name ? { value: city_id, label: city_name } : "";

      const district_id =
        global.city && detail.location.district
          ? detail.location.district.district_id
          : "";
      const district_name =
        global.city && detail.location.district
          ? detail.location.district.vi_name
          : "";
      const district =
        district_id && district_name
          ? { value: district_id, label: district_name }
          : "";

      const ward_id =
        global.city && detail.location.ward ? detail.location.ward.ward_id : "";
      const ward_name =
        global.city && detail.location.ward ? detail.location.ward.vi_name : "";
      const ward =
        ward_id && ward_name ? { value: ward_id, label: ward_name } : "";

      setIsOwner(relationship_value === "owner");
      setValueRelationship(relationship_value);
      setValueFullName(fullName);
      setValueBirthday(birthday);
      setListPatientCode(tmpPatient);
      setFields([
        { name: ["itemFullName"], value: fullName },
        { name: ["itemBirthday"], value: birthday },
        { name: ["itemPhone"], value: phone },
        { name: ["itemRelationship"], value: relationship },
        { name: ["itemGender"], value: gender },
        { name: ["itemCMND"], value: cmnd },
        { name: ["itemBHYT"], value: bhyt },
        { name: ["itemAddress"], value: address },
        { name: ["itemCity"], value: city },
        { name: ["itemDistrict"], value: district },
        { name: ["itemWard"], value: ward },
        { name: ["itemFullNameDad"], value: father_name },
        { name: ["itemPhoneDad"], value: father_phone },
        { name: ["itemFullNameMom"], value: mother_name },
        { name: ["itemPhoneMom"], value: mother_phone },
      ]);

      handleListDistrict(city_id);
      handleListWard(district_id);
    }
  }, [location, global.city, handleListDistrict, handleListWard]);

  useEffect(() => {
    const detail = global.find_profile;
    if (detail) {
      const oldProfile = location.state.detail_profile;
      const fullName = detail.name;
      const address = detail.address ? detail.address : "";
      const birthday = detail.birthday
        ? func.yieldBirthdayFollowHis(detail.birthday)
        : "";
      const patient_code = detail.patient_code ? detail.patient_code : "";
      const phone = detail.phone ? detail.phone : "";
      const hospital_name = detail.hospital
        ? func.findNameHospital(detail.hospital)
        : "";

      if (listPatientCode.length > 0) {
        const indexPatientCode = listPatientCode.findIndex((el) => {
          return el.hospital_name === hospital_name;
        });
        if (indexPatientCode > -1) {
          listPatientCode.forEach(function (el) {
            if (el.hospital_name === hospital_name) {
              el.patient_code = patient_code;
            }
          });
        } else {
          listPatientCode.push({
            hospital_key: detail.hospital,
            hospital_name: hospital_name,
            patient_code: patient_code,
          });
        }
      } else {
        listPatientCode.push({
          hospital_key: detail.hospital,
          hospital_name: hospital_name,
          patient_code: patient_code,
        });
      }

      const gender_value = func.findGender(detail.sex, "value");
      const gender_name = func.findGender(detail.sex, "text");
      const gender =
        gender_value && gender_name
          ? { value: gender_value, label: gender_name }
          : "";

      const city_id = global.city && detail.province ? detail.province : "";
      const indexCity = global.city.findIndex((el) => {
        return el.province_id === city_id;
      });
      const city_name =
        global.city && global.city[indexCity]
          ? global.city[indexCity].vi_name
          : "";
      const city =
        city_id && city_name ? { value: city_id, label: city_name } : "";

      const district_id = global.city && detail.district ? detail.district : "";
      let district = "";
      if (triggerDistrict.length > 0) {
        const indexDistrict = triggerDistrict.findIndex((el) => {
          return el.district_id === district_id;
        });
        const district_name = triggerDistrict[indexDistrict]
          ? triggerDistrict[indexDistrict].vi_name
          : "";
        district =
          district_id && district_name
            ? { value: district_id, label: district_name }
            : "";
      } else {
        handleListDistrict(city_id, true);
      }

      const ward_id = global.city && detail.ward ? detail.ward : "";
      let ward = "";
      if (triggerWard.length > 0) {
        const indexWard = triggerWard.findIndex((el) => {
          return el.ward_id === ward_id;
        });
        const ward_name = triggerWard[indexWard]
          ? triggerWard[indexWard].vi_name
          : "";
        ward = ward_id && ward_name ? { value: ward_id, label: ward_name } : "";
      } else {
        handleListWard(district_id, true);
      }

      const tmpFields = [
        { name: ["itemFullName"], value: fullName },
        { name: ["itemBirthday"], value: birthday },
        { name: ["itemGender"], value: gender },
        { name: ["itemAddress"], value: address },
        { name: ["itemCity"], value: city },
        { name: ["itemDistrict"], value: district },
        { name: ["itemWard"], value: ward },
      ];

      const relationship = func.findRelationship(
        oldProfile.relationship,
        "value"
      );
      if (relationship !== "owner") {
        tmpFields.push({ name: ["itemPhone"], value: phone });
      }

      setValueFullName(fullName);
      setValueBirthday(birthday);
      setFields(tmpFields);
    }
  }, [
    global.find_profile,
    global.city,
    location.state.detail_profile,
    handleListDistrict,
    handleListWard,
    triggerDistrict,
    triggerWard,
    listPatientCode,
  ]);

  const validatorInsurrance = (rule, value) => {
    if (value && valueBirthday && valueFullName) {
      const reqParamFt = {
        name: valueFullName,
        birthday: func.formatISODatetime(valueBirthday),
        num_insurrance: value,
      };
      setLoading(true);
      return ModelCheckInsurrance(reqParamFt)
        .then(() => {
          setLoading(false);
          return Promise.resolve();
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            setLoading(false);
            return Promise.reject(i18next.t(msg));
          }
        });
    } else {
      return Promise.resolve();
    }
  };

  const onFinish = (values) => {
    const detail = location.state.detail_profile;
    const userID = user.personal._id;
    const id = detail._id;
    const myself = detail.relationship;
    const returnURL = url.name.service_health_profile;
    let reqParamFt = {
      user: userID,
      sex: func.findGender(values.itemGender.value, "id"),
      birthday: func.formatISODatetime(values.itemBirthday),
      name: values.itemFullName ? values.itemFullName.trim() : "",
      num_insurance: values.itemBHYT ? values.itemBHYT.trim() : "",
      identity_num: values.itemCMND ? values.itemCMND.trim() : "",
      phone: values.itemPhone ? func.isValidPhone(values.itemPhone) : "",
      address: values.itemAddress ? values.itemAddress.trim() : "",
      province: values.itemCity.value,
      district: values.itemDistrict.value,
      ward: values.itemWard.value,
      father_name: values.itemFullNameDad ? values.itemFullNameDad.trim() : "",
      father_phone: values.itemPhoneDad
        ? func.isValidPhone(values.itemPhoneDad)
        : "",
      mother_name: values.itemFullNameMom ? values.itemFullNameMom.trim() : "",
      mother_phone: values.itemPhoneMom
        ? func.isValidPhone(values.itemPhoneMom)
        : "",
    };

    if (listPatientCode.length > 0) {
      listPatientCode.forEach(function (el) {
        const tmpObjHospital = func.findObjectPatientCodeHospital(
          el.hospital_key,
          el.patient_code
        );
        reqParamFt = { ...reqParamFt, ...tmpObjHospital };
      });
    }

    if (myself !== "owner") {
      const tmpObjRelationship = {
        relationship: func.findRelationship(
          values.itemRelationship.value,
          "value"
        ),
      };
      reqParamFt = { ...reqParamFt, ...tmpObjRelationship };
    }

    setLoading(true);
    ModelUpdateProfile(id, reqParamFt)
      .then(() => {
        if (myself === "owner") {
          const reqParamSd = {
            user: {
              name: values.itemFullName ? values.itemFullName.trim() : "",
              sex: func.findGender(values.itemGender.value, "id"),
              birthday: func.formatISODatetime(values.itemBirthday),
              province: values.itemCity.value,
              district: values.itemDistrict.value,
              ward: values.itemWard.value,
              address: values.itemAddress ? values.itemAddress.trim() : "",
            },
          };
          return ModelUpdateUserInfo(userID, reqParamSd);
        }
      })
      .then((resultSd) => {
        if (resultSd) {
          const dataUpdateUser = resultSd.data.one_health_msg;
          _UpdateUser(dataUpdateUser);
        }
        const pushState = {
          status: "success",
          message: msg_text.create_profile_success,
        };
        history.push({
          pathname: returnURL,
          state: pushState,
        });
      })
      .catch((error) => {
        if (error.code === "007") {
          _TokenExpired(error);
        } else {
          const msg = error.message ? error.message : error.description;
          modal.warning({
            title: "Thông báo",
            okText: "Đóng",
            centered: true,
            content: i18next.t(msg),
          });
          setLoading(false);
        }
      });
  };

  const onAcceptImportInfo = () => {
    const detail = user.personal;
    const fullName = detail.last_name + " " + detail.first_name;
    const gender = detail.sex ? func.findGender(detail.sex, "value") : "";
    const address = detail.address ? detail.address : "";
    const phone = detail.username ? detail.username : "";
    const city_id =
      global.city && detail.province ? detail.province.province_id : "";
    const city_name =
      global.city && detail.province ? detail.province.vi_name : "";
    const city =
      city_id && city_name ? { value: city_id, label: city_name } : "";

    const district_id =
      global.city && detail.district ? detail.district.district_id : "";
    const district_name =
      global.city && detail.district ? detail.district.vi_name : "";
    const district =
      district_id && district_name
        ? { value: district_id, label: district_name }
        : "";

    const ward_id = global.city && detail.ward ? detail.ward.ward_id : "";
    const ward_name = global.city && detail.ward ? detail.ward.vi_name : "";
    const ward =
      ward_id && ward_name ? { value: ward_id, label: ward_name } : "";

    handleListDistrict(city_id);
    handleListWard(district_id);

    if (gender === "male") {
      setFields([
        { name: ["itemFullNameDad"], value: fullName },
        { name: ["itemPhoneDad"], value: phone },
        { name: ["itemAddress"], value: address },
        { name: ["itemCity"], value: city },
        { name: ["itemDistrict"], value: district },
        { name: ["itemWard"], value: ward },
      ]);
    } else if (gender === "female") {
      setFields([
        { name: ["itemFullNameMom"], value: fullName },
        { name: ["itemPhoneMom"], value: phone },
        { name: ["itemAddress"], value: address },
        { name: ["itemCity"], value: city },
        { name: ["itemDistrict"], value: district },
        { name: ["itemWard"], value: ward },
      ]);
    }
  };

  const onInputBirthday = (e) => {
    const value = func.yieldValueDate(e.target.value);
    setFields([{ name: ["itemBirthday"], value: value }]);
    setValueBirthday(value);
  };

  const onInputFullName = (e) => {
    const value = e.target.value.trim();
    if (value) {
      setValueFullName(func.removeAccents(value));
    }
  };

  const onSelectCity = (values) => {
    setFields([
      { name: ["itemDistrict"], value: "" },
      { name: ["itemWard"], value: "" },
    ]);
    setListWard([]);
    handleListDistrict(values.value);
  };

  const onSelectDistrict = (values) => {
    setFields([{ name: ["itemWard"], value: "" }]);
    handleListWard(values.value);
  };

  const onSelectRelationship = (values) => {
    const _value = func.findRelationship(values.value, "value");
    setValueRelationship(_value);
    if (_value === "son" || _value === "daughter") {
      modal.confirm({
        title: "Thông báo",
        okText: "Đồng ý",
        cancelText: "Huỷ",
        onOk: onAcceptImportInfo,
        centered: true,
        content:
          "Bạn có muốn thêm thông tin (họ tên, số điện thoại, địa chỉ) từ hồ sơ của bạn vào hồ sơ này?",
      });
    }
  };

  function yieldOtherInput() {
    if (valueRelationship === "son" || valueRelationship === "daughter") {
      return (
        <Fragment>
          <Input.Group compact>
            <BoxInputFullName
              label="Họ tên bố"
              required={false}
              dependencies={["itemFullNameMom"]}
              validator={[
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (getFieldValue("itemFullNameMom") || value) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject("Vui lòng nhập họ tên bố hoặc mẹ");
                    }
                  },
                }),
              ]}
              placeholder="Vui lòng nhập họ tên bố hoặc mẹ"
              name="itemFullNameDad"
            />{" "}
            <BoxInputPhone
              label="Số điện thoại của bố"
              required={false}
              dependencies={["itemPhoneMom"]}
              validator={[
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (getFieldValue("itemPhoneMom") || value) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        "Vui lòng nhập số điện thoại bố hoặc mẹ"
                      );
                    }
                  },
                }),
              ]}
              placeholder="Vui lòng nhập số điện thoại bố hoặc mẹ"
              name="itemPhoneDad"
            />
          </Input.Group>{" "}
          <Input.Group compact>
            <BoxInputFullName
              label="Họ tên mẹ"
              required={false}
              dependencies={["itemFullNameDad"]}
              validator={[
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (getFieldValue("itemFullNameDad") || value) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject("Vui lòng nhập họ tên bố hoặc mẹ");
                    }
                  },
                }),
              ]}
              placeholder="Vui lòng nhập họ tên bố hoặc mẹ"
              name="itemFullNameMom"
            />{" "}
            <BoxInputPhone
              label="Số điện thoại của mẹ"
              required={false}
              dependencies={["itemPhoneDad"]}
              validator={[
                ({ getFieldValue }) => ({
                  validator(rule, value) {
                    if (getFieldValue("itemPhoneDad") || value) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        "Vui lòng nhập số điện thoại bố hoặc mẹ"
                      );
                    }
                  },
                }),
              ]}
              placeholder="Vui lòng nhập số điện thoại bố hoặc mẹ"
              name="itemPhoneMom"
            />
          </Input.Group>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  function yieldInfoPatientCode() {
    if (listPatientCode.length > 0) {
      const list = _.map(listPatientCode, (item, index) => {
        return (
          <div key={index} className="wrap-info-patient-code">
            <p>
              Mã hồ sơ - {item.hospital_name}: <span>{item.patient_code}</span>
            </p>
          </div>
        );
      });
      return <div className="wrap-panel-patient-code">{list}</div>;
    }
    return null;
  }

  return (
    <Fragment>
      <FindProfile {...props} />
      {yieldInfoPatientCode()}
      <Form
        onFinish={onFinish}
        form={form}
        fields={fields}
        size="large"
        className="form-edit-profile"
        name="form-edit-profile"
      >
        <Input.Group compact>
          <Form.Item
            label="Bảo hiểm y tế"
            name="itemBHYT"
            rules={[
              { validateTrigger: "onBlur", validator: validatorInsurrance },
            ]}
          >
            <Input placeholder="Mã bảo hiểm y tế" />
          </Form.Item>{" "}
          <Form.Item
            label="Mối quan hệ"
            name="itemRelationship"
            rules={[{ required: true, message: "Vui lòng chọn mối quan hệ" }]}
          >
            <BoxSelect
              placeholder="Chọn mối quan hệ"
              placement={"auto"}
              isDisabled={isOwner}
              onChange={onSelectRelationship}
              data={
                isOwner
                  ? dataRelationship(this)
                  : _.omitBy(dataRelationship(this), { value: "owner" })
              }
            />
          </Form.Item>
        </Input.Group>{" "}
        <Input.Group compact>
          <BoxInputFullName
            label="Họ và tên"
            required={true}
            name="itemFullName"
            onChange={onInputFullName}
            placeholder="Nhập họ và tên"
          />{" "}
          <BoxInputBirthday
            label="Ngày sinh (dd/mm/yyyy)"
            name="itemBirthday"
            onChange={onInputBirthday}
            placeholder="__ / __ / ____"
          />
        </Input.Group>{" "}
        <Input.Group compact>
          <Form.Item
            label="Giới tính"
            name="itemGender"
            rules={[{ required: true, message: "Vui lòng chọn giới tính" }]}
          >
            <BoxSelect
              showSearch
              placeholder="Chọn giới tính"
              data={dataGender(this)}
            />
          </Form.Item>{" "}
          <BoxInputPhone
            label="Số điện thoại"
            disabled={isOwner}
            required={
              valueRelationship === "son" || valueRelationship === "daughter"
                ? undefined
                : true
            }
            name="itemPhone"
            placeholder={
              valueRelationship === "son" || valueRelationship === "daughter"
                ? ""
                : "Nhập số điện thoại"
            }
          />
        </Input.Group>{" "}
        {yieldOtherInput()}{" "}
        <Input.Group compact>
          <Form.Item label="Chứng minh nhân dân" name="itemCMND">
            <Input />
          </Form.Item>{" "}
          <Form.Item
            label="Số nhà, đường, thôn, ấp,..."
            name="itemAddress"
            rules={[
              { required: true, message: "Vui lòng nhập số nhà" },
              { whitespace: true, message: "Vui lòng nhập số nhà" },
            ]}
          >
            <Input placeholder="Nhập số nhà, đường, thôn, ấp,..." />
          </Form.Item>
        </Input.Group>{" "}
        <Input.Group compact>
          <Form.Item
            label="Tỉnh/Thành"
            name="itemCity"
            rules={[{ required: true, message: "Vui lòng chọn tỉnh/thành" }]}
          >
            <BoxSelect
              placeholder="Chọn tỉnh/thành"
              onChange={onSelectCity}
              data={global.city}
            />
          </Form.Item>{" "}
          <Form.Item
            label="Quận/Huyện"
            name="itemDistrict"
            rules={[{ required: true, message: "Vui lòng chọn quận/huyện" }]}
          >
            <BoxSelect
              placeholder="Chọn quận/huyện"
              onChange={onSelectDistrict}
              data={listDistrict}
            />
          </Form.Item>{" "}
          <Form.Item
            label="Phường/Xã"
            name="itemWard"
            rules={[{ required: true, message: "Vui lòng chọn phường/xã" }]}
          >
            <BoxSelect placeholder="Chọn phường/xã" data={listWard} />
          </Form.Item>
        </Input.Group>{" "}
        <Form.Item className="wrap-form-action">
          <hr />
          <div className="box-button-event">
            <Button loading={loading} className="btn-accept" htmlType="submit">
              <span>Lưu hồ sơ</span>
            </Button>
          </div>
        </Form.Item>
      </Form>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _ListCity, _UpdateUser }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(FormEditProfile)
);
