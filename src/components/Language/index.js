import React, {Component, Fragment} from "react";
import i18next from "i18next";
import {Select} from "antd";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {_LanguageActive} from "../../actions";

class Language extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    async componentDidMount() {
        const {i18n, language} = await this.props;
        i18n.changeLanguage(language);
    }

    onSelectLanguage = (val) => {
        const {i18n} = this.props;
        i18n.changeLanguage(val);
        this.props._LanguageActive(val);
    };

    render() {
        const {language} = this.props;
        const {Option} = Select;
        return (
            <Fragment>
                <div className="language">
                    <Select
                        onChange={this.onSelectLanguage}
                        defaultValue={language}
                        style={{width: 120}}>
                        <Option value="VI">{i18next.t("Vietnamese")}</Option>
                        <Option value="EN">{i18next.t("English")}</Option>
                    </Select>
                </div>
            </Fragment>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_LanguageActive}, dispatch);
};

export default connect(null, mapDispatchToProps, null, {forwardRef: true})(Language);
