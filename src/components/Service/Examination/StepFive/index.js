import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import moment from "moment";
import { FaRegUserCircle, FaRegListAlt } from "react-icons/fa";
import { nhi_dong } from "../../../../constants/Default";
import Loading from "../../../Loading";
import FormPayment from "../../FormPayment";
import { ModelCheckExaminationRegister } from "../../../../models/ModelExamination";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const StepFive = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [dataPayment, setDataPayment] = useState(undefined);
  //*** Declare props ***//
  const { appointment } = props;
  const form = appointment && appointment.form ? appointment.form : [];
  const formOne = func.findDataOfStep(form, 1);
  const formTwo = func.findDataOfStep(form, 2);
  const formThree = func.findDataOfStep(form, 3);
  const formFour = func.findDataOfStep(form, 4);
  const handleCheckAppointmentRegister = useCallback(() => {
    const reqParamFt = {
      source: formOne.key,
      profile: formTwo._id,
      appoint_date: formFour.date,
      time: formFour.time,
      num_insurrance: formFour.insurance,
      exam_area: formThree._id,
      exam_type: formFour.examType,
      payment: formFour.paymentType,
    };
    setLoading(true);
    ModelCheckExaminationRegister(reqParamFt)
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataAppointment = resultFt.data.one_health_msg;
          const tmpDataPayment = {
            type: "APPOINTMENT",
            source: formOne.key,
            amount: dataAppointment.payment,
            discount: dataAppointment.discount,
            price: dataAppointment.price,
            notice: dataAppointment.message,
            promotion: dataAppointment.promotion
              ? dataAppointment.promotion
              : "",
            exchangeId: dataAppointment.promotion
              ? dataAppointment.promotion.exchangeId
              : "",
            profile_id: formTwo._id,
            reason: formFour.symptom,
            appoint_date: formFour.date,
            time: formFour.time,
            payment: formFour.paymentType,
            exam_type: formFour.examType,
            exam_area: formThree._id,
            num_insurrance: formFour.insurance,
            doctor: formFour.doctor ? formFour.doctor.value : "",
          };
          setDataPayment(tmpDataPayment);
          setLoading(false);
        }
        return null;
      })
      .catch((error) => {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("Examination - StepFive - CheckExaminationRegister");
            setLoading(false);
          }
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, [formOne, formTwo, formThree, formFour]);

  useEffect(() => {
    if (!isRendered.current) {
      handleCheckAppointmentRegister();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleCheckAppointmentRegister]);

  const handleRecalculationAmount = (flag, data) => {
    if (flag === "changed") {
      const tmpDataPayment = {
        type: "APPOINTMENT",
        source: formOne.key,
        amount: data.payment,
        discount: data.discount,
        price: data.price,
        notice: data.message,
        promotion: data.promotion ? data.promotion : "",
        exchangeId: data.promotion ? data.promotion.exchangeId : "",
        profile_id: formTwo._id,
        reason: formFour.symptom,
        appoint_date: formFour.date,
        time: formFour.time,
        payment: formFour.paymentType,
        exam_type: formFour.examType,
        exam_area: formThree._id,
        num_insurrance: formFour.insurance,
        doctor: formFour.doctor ? formFour.doctor.value : "",
      };
      setDataPayment(tmpDataPayment);
    }
  };

  function yieldProfileInfo() {
    if (formTwo) {
      const name = formTwo.name ? formTwo.name : formTwo.names[0];
      const gender = func.findGender(formTwo.sex, "text");
      const age = func.yieldAge(formTwo.birthday);
      const relationship = func.findRelationship(formTwo.relationship, "text");
      const phone = formTwo.phone ? formTwo.phone : formTwo.phones[0];
      const location = func.yieldLocation(formTwo.location);
      const keyHospital = func.findKeyNameHospital(formOne.key);
      let yieldInsurance;
      if (formFour.insurance) {
        yieldInsurance = (
          <div className="wrap-line">
            <p className="label">Mã BHYT</p>
            <p className="value">{formFour.insurance}</p>
          </div>
        );
      }
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Họ và tên</p>
            <p className="value">{name}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Giới tính</p>
            <p className="value">{gender}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Tuổi</p>
            <p className="value">{age}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Mối quan hệ</p>
            <p className="value">{relationship}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Số điện thoại</p>
            <p className="value">{phone}</p>
          </div>
          <div className="wrap-line address">
            <p className="label">Địa chỉ</p>
            <p className="value">{location}</p>
          </div>
          {yieldInsurance}
          <div className="wrap-line">
            <p className="label">Mã hồ sơ</p>
            <p className="value">
              <span className="code">{formTwo[keyHospital].patient_code}</span>
            </p>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  function yieldExamScheduleInfo() {
    if (formOne && formThree && formFour) {
      const nameHospital = formOne.name;
      const nameSpecialist = formThree.name;
      const nameExamType = formFour.nameExamType;
      const namePaymentType = formFour.namePaymentType
        ? formFour.namePaymentType
        : "-";
      const date = moment(formFour.date).format("DD/MM/YYYY");
      const time = formFour.time ? formFour.time : "-";
      let yieldDoctor;
      if (formFour.doctor) {
        yieldDoctor = (
          <div className="wrap-line">
            <p className="label">Bác sĩ khám</p>
            <p className="value">{formFour.doctor.label}</p>
          </div>
        );
      }
      let yieldSpecExam, yieldAreaExam;
      if (formOne.key === nhi_dong.key) {
        yieldSpecExam = (
          <div className="wrap-line">
            <p className="label">Phân khu</p>
            <p className="value">{formThree.group_special.name}</p>
          </div>
        );
        yieldAreaExam = (
          <div className="wrap-line">
            <p className="label">Chi tiết khám</p>
            <p className="value">{formThree.name}</p>
          </div>
        );
      } else {
        yieldSpecExam = (
          <div className="wrap-line">
            <p className="label">Chuyên khoa</p>
            <p className="value">{nameSpecialist}</p>
          </div>
        );
        yieldAreaExam = (
          <div className="wrap-line">
            <p className="label">Vùng khám</p>
            <p className="value">{nameSpecialist}</p>
          </div>
        );
      }
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Bệnh viện</p>
            <p className="value">{nameHospital}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Loại khám</p>
            <p className="value">{nameExamType}</p>
          </div>
          {yieldSpecExam}
          {yieldAreaExam}
          {yieldDoctor}
          <div className="wrap-line">
            <p className="label">Giờ khám</p>
            <p className="value">{time}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Ngày khám</p>
            <p className="value">{date}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Loại thanh toán</p>
            <p className="value">{namePaymentType}</p>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Xác nhận thông tin & Thanh toán</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>Quý khách vui lòng kiểm tra lại các thông tin đặt khám</p>
          </div>
          <div className="step">
            <div className="step-text">Thông tin & Thanh toán</div>
          </div>
        </div>
        <div className="card-body">
          <div className="wrap-content block-confirm-info-examination">
            <div className="row">
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item wrap-profile-info">
                  <div className="wrap-title">
                    <h4>
                      <FaRegUserCircle /> Thông tin bệnh nhân
                    </h4>
                  </div>
                  <div className="wrap-main">{yieldProfileInfo()}</div>
                  <div className="wrap-title">
                    <h4>
                      <FaRegListAlt /> Thông tin lịch khám
                    </h4>
                  </div>
                  <div className="wrap-main">{yieldExamScheduleInfo()}</div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item wrap-payment-info">
                  <FormPayment
                    {...props}
                    handleRecalculationAmount={handleRecalculationAmount}
                    dataPayment={dataPayment}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default connect(null, null, null, { forwardRef: true })(StepFive);
