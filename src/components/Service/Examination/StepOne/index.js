import React, { Fragment, useEffect, useRef, useState } from "react";
import _ from "lodash";
import { Modal, Empty } from "antd";
import BoxSkeleton from "../../../BoxSkeleton";
import { url } from "../../../../constants/Path";
import { ModelListHospital } from "../../../../models/ModelExamination";
import { _Appointment } from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./style.scss";

const StepOne = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHospital, setListHospital] = useState([]);
  const [detailHospital, setDetailHospital] = useState(undefined);
  const [modalDetailHospital, setModalDetailHospital] = useState(false);
  //*** Declare props ***//
  const { _Appointment, history } = props;

  useEffect(() => {
    setSkeleton(true);
    ModelListHospital()
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataListHospital = resultFt.data.one_health_msg.list;
          setListHospital(dataListHospital);
          setSkeleton(false);
        }
        return null;
      })
      .catch((error) => {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("Examination - StepOne - ListHospital");
            setSkeleton(false);
          }
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, []);

  const onPressNext = (item) => {
    const params = [
      {
        step: 1,
        data: item,
      },
    ];
    _Appointment(params);
    history.push(url.name.examination_2);
  };

  const onReadDetail = (item) => {
    setModalDetailHospital(true);
    setDetailHospital(item);
  };

  const onCloseModalDetailHospital = () => {
    setModalDetailHospital(false);
  };

  function yieldListHospital() {
    if (listHospital.length > 0) {
      return _.map(listHospital, (item, index) => {
        return (
          <div
            key={index}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-top">
                <img className="img-fluid" src={item.image} alt="" />
              </div>
              <div className="wrap-main">
                <h4 className="title">{item.name}</h4>
                <p className="address text-muted">{item.address}</p>
              </div>
              <div className="wrap-event-action">
                <div className="box-button-event">
                  <div
                    onClick={onReadDetail.bind(this, item)}
                    className="ant-btn ant-btn-lg read-more"
                  >
                    <span>Xem chi tiết</span>
                  </div>
                  <div
                    onClick={onPressNext.bind(this, item)}
                    className="ant-btn ant-btn-lg btn-booking booking"
                  >
                    <span>Đặt khám</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Danh sách bệnh viện trống"
          />
        );
      }
    }
  }

  function yieldDetailHospital() {
    if (detailHospital) {
      return (
        <Fragment>
          <div className="wrap-top">
            <img src={detailHospital.image} alt={detailHospital.name} />
          </div>
          <div className="wrap-main">
            <div className="title mb-2">
              <h4>{detailHospital.name}</h4>
              <p className="text-muted">{detailHospital.address}</p>
              <div
                onClick={onPressNext.bind(this, detailHospital)}
                className="ant-btn btn-booking ant-btn-lg booking w-100"
              >
                <span>Đặt khám</span>
              </div>
            </div>
            <div className="content">
              <iframe
                src={detailHospital.guide}
                title="Hướng dẫn các bước thực hiện khám chữa bệnh"
                width="100%"
              />
            </div>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Đặt lịch khám/Medifast</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>
              Quý khách vui lòng xem thông tin và chọn bệnh viện cần đặt khám.
            </p>
          </div>
          <div className="step">
            <div className="step-text">Chọn bệnh viện</div>
          </div>
        </div>
        <div className="card-body">
          <div className="wrap-content block-list-hospital-examination">
            <div className="wrap-panel-content">
              <div className="row">
                <BoxSkeleton
                  skeleton={skeleton}
                  full={false}
                  length={4}
                  rows={7}
                  data={yieldListHospital()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        className="block-modal-detail-hospital-examination ant-modal-custom"
        width={600}
        closable={true}
        footer={null}
        visible={modalDetailHospital}
        onCancel={onCloseModalDetailHospital}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">{yieldDetailHospital()}</div>
        </div>
      </Modal>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _Appointment }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  StepOne
);
