import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import moment from "moment";
import {url} from "../../../../constants/Path";
import {nhi_dong, ung_buou} from "../../../../constants/Default";
import {MdSchedule, MdPayment, MdLabelOutline} from "react-icons/md";
import {AiOutlineRight} from "react-icons/ai";
import {GiHospitalCross} from "react-icons/gi";
import {Input, Modal} from "antd";
import Select from "react-select";
import Loading from "../../../Loading";
import {ModelExamType, ModelListDoctor, ModelListTime} from "../../../../models/ModelExamination";
import {ModelCheckInsurrance} from "../../../../models/ModelCommon";
import {_Appointment} from "../../../../actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const EXAM_BHYT = "bhyt";

//*** Handle component exam type ***//
const ExamType = (props) => {
    const {label, value, selected, onClick} = props;
    const onPress = () => {onClick(value, label);};
    const isActive  = selected === value;
    const className = isActive ? 'active item' : 'item';
    return (
        <Fragment>
            <span className={className} onClick={onPress}>{label}</span>
        </Fragment>
    );
};

//*** Handle component date ***//
const DateFrame = (props) => {
    const {labelText_2, labelText_1, value, selected, onClick} = props;
    const onPress = () => {onClick(value);};
    const isActive  = new Date(selected).getTime() === new Date(value).getTime();
    const className = isActive ? 'active item' : 'item';
    return (
        <Fragment>
            <div className={className} onClick={onPress}>
                <p>{labelText_2}</p>
                <p>{labelText_1}</p>
            </div>
        </Fragment>
    );
};

//*** Handle component time ***//
const TimeFrame = (props) => {
    const {label, value, selected, onClick} = props;
    const onPress = () => {onClick(value);};
    const isActive  = selected === value;
    const className = isActive ? 'active item' : 'item';
    return (
        <Fragment>
            <div className={className} onClick={onPress}><p>{label}</p></div>
        </Fragment>
    );
};

//*** Handle component payment type ***//
const PaymentType = (props) => {
    const {label, value, selected, onClick} = props;
    const onPress = () => {onClick(value, label);};
    const isActive  = selected === value;
    const className = isActive ? 'active item' : 'item';
    return (
        <Fragment>
            <span className={className} onClick={onPress}>{label}</span>
        </Fragment>
    );
};

const StepFour = (props) => {
    const isRendered = useRef(false);
    const [loading, setLoading] = useState(false);
    const [listExamType, setListExamType] = useState([]);
    const [listTime, setListTime] = useState([]);
    const [listDoctor, setListDoctor] = useState([]);
    const [listPaymentType, setListPaymentType] = useState([]);
    const [nameExamType, setNameExamType] = useState(undefined);
    const [valueExamType, setValueExamType] = useState(undefined);
    const [valueInsurance, setValueInsurance] = useState(undefined);
    const [valueDate, setValueDate] = useState(undefined);
    const [valueTime, setValueTime] = useState(undefined);
    const [selectDoctor, setSelectDoctor] = useState(undefined);
    const [valuePaymentType, setValuePaymentType] = useState(undefined);
    const [valueSymptom, setValueSymptom] = useState(undefined);
    const [namePaymentType, setNamePaymentType] = useState(undefined);
    //*** Declare props ***//
    const {_TokenExpired, _Appointment, appointment, history} = props;

    const [modal, contextHolder] = Modal.useModal();
    const form = appointment && appointment.form ? appointment.form : [];
    const formOne = func.findDataOfStep(form, 1);
    const formTwo = func.findDataOfStep(form, 2);
    const formThree = func.findDataOfStep(form, 3);
    const formFour = func.findDataOfStep(form, 4);

    //*** Handle data doctor **//
    const handleListDoctor = useCallback(params => {
        setLoading(true);
        ModelListDoctor(params).then(resultFt => {
            if (!isRendered.current) {
                const dataListDoctor = resultFt.data.one_health_msg.list;
                let tmpListDoctor = [];
                if (dataListDoctor.length > 0) {
                    _.map(dataListDoctor, (item) => {
                        tmpListDoctor.push({value: item._id, label: item.name});
                        setListDoctor(tmpListDoctor);
                    });
                }
            }
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("Examination - StepFour - ListDoctor");
                } else {
                    _TokenExpired(error);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, [_TokenExpired]);

    //*** Handle data time **//
    const handleListTime = useCallback(params => {
        setLoading(true);
        ModelListTime(params).then(resultFt => {
            if (!isRendered.current) {
                const dataListTime = resultFt.data.one_health_msg;
                setListTime(dataListTime);
                setLoading(false);
            }
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("Examination - StepFour - ListTime");
                    setLoading(false);
                } else {
                    _TokenExpired(error);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, [_TokenExpired]);

    useEffect(() => {
        if (!formFour && formOne.exam_type) {
            const tempDate = func.yieldDatetimeNowUTC();
            const reqParamFt = {
                source: formOne.key
            };
            setValueDate(tempDate);
            setLoading(true);
            ModelExamType(reqParamFt).then(resultFt => {
                if (!isRendered.current) {
                    const dataListExamType = resultFt.data.one_health_msg.list;
                    setValueExamType(dataListExamType[0].type);
                    setNameExamType(dataListExamType[0].name);
                    setListExamType(dataListExamType);
                    setLoading(false);
                    const reqParamSd = {
                        source: formOne.key,
                        exam_area: formThree._id,
                        exam_type: dataListExamType[0].type,
                        appoint_date: moment(tempDate).utc(true).toISOString()
                    };
                    if (formOne.key === ung_buou.key && formThree.enable_time) {
                        handleListTime(reqParamSd);
                    }
                    if (formOne.key !== ung_buou.key && formOne.enable_time) {
                        handleListTime(reqParamSd);
                    }
                    if (formOne.doctor) {
                        const reqParamThr = {
                            source: formOne.key,
                            limit: 500,
                            page: 1,
                            exam_area: formThree._id,
                            exam_type: dataListExamType[0].type,
                            appoint_date: moment(tempDate).utc(true).toISOString()
                        };
                        handleListDoctor(reqParamThr);
                    }
                    if (formOne.show_pay_in_hos) {
                        setListPaymentType(formOne.payment);
                        setValuePaymentType(formOne.payment[0].value);
                        setNamePaymentType(formOne.payment[0].name);
                    }
                }
                return null;
            }).catch(error => {
                if (!isRendered.current) {
                    if (error.code !== "007") {
                        console.log("Examination - StepFour - ExamType");
                        setLoading(false);
                    }
                }
            });
        }
        return () => {
            isRendered.current = true;
        };
    }, [formOne, formFour, formThree, handleListTime, handleListDoctor]);

    useEffect(() => {
        if (formFour) {
            const tempDate = formFour.date;
            const reqParamFt = {
                source: formOne.key
            };
            setValueDate(tempDate);
            setValueTime(formFour.time);
            setLoading(true);
            ModelExamType(reqParamFt).then(resultFt => {
                if (!isRendered.current) {
                    const dataListExamType = resultFt.data.one_health_msg.list;
                    setValueExamType(formFour.examType);
                    setNameExamType(formFour.nameExamType);
                    setValueInsurance(formFour.insurance);
                    setValueSymptom(formFour.symptom);
                    setListExamType(dataListExamType);
                    setLoading(false);
                    const reqParamSd = {
                        source: formOne.key,
                        exam_area: formThree._id,
                        exam_type: formFour.examType,
                        appoint_date: moment(tempDate).utc(true).toISOString()
                    };
                    if (formOne.key === ung_buou.key && formThree.enable_time) {
                        handleListTime(reqParamSd);
                    }
                    if (formOne.key !== ung_buou.key && formOne.enable_time) {
                        handleListTime(reqParamSd);
                    }
                    if (formOne.doctor) {
                        const reqParamThr = {
                            source: formOne.key,
                            limit: 500,
                            page: 1,
                            exam_area: formThree._id,
                            exam_type: formFour.type,
                            appoint_date: moment(tempDate).utc(true).toISOString()
                        };
                        handleListDoctor(reqParamThr);
                        setSelectDoctor(formFour.doctor);
                    }
                    if (formOne.show_pay_in_hos) {
                        setListPaymentType(formOne.payment);
                        setValuePaymentType(formFour.paymentType);
                        setNamePaymentType(formFour.namePaymentType);
                    }
                }
                return null;
            }).catch(error => {
                if (!isRendered.current) {
                    if (error.code !== "007") {
                        console.log("Examination - StepFour - ExamType");
                        setLoading(false);
                    }
                }
            });
        }
        return () => {
            isRendered.current = true;
        };
    }, [formOne, formFour, formThree, handleListTime, handleListDoctor]);

    const onPressExamType = (key, label)  => {
        setValueExamType(key);
        setNameExamType(label);
        setValueTime(undefined);
        setValueInsurance(undefined);
        setSelectDoctor("");
        if (key === EXAM_BHYT) {
            setValueInsurance(formTwo.num_insurance);
        }
        const reqParamFt = {
            source: formOne.key,
            exam_area: formThree._id,
            exam_type: key,
            appoint_date: moment(valueDate).utc(true).toISOString()
        };
        if (formOne.key === ung_buou.key && formThree.enable_time) {
            handleListTime(reqParamFt);
        }
        if (formOne.key !== ung_buou.key && formOne.enable_time) {
            handleListTime(reqParamFt);
        }
        if (formOne.doctor) {
            const reqParamSd = {
                source: formOne.key,
                limit: 500,
                page: 1,
                exam_area: formThree._id,
                exam_type: key,
                appoint_date: moment(valueDate).utc(true).toISOString()
            };
            handleListDoctor(reqParamSd);
        }
    };

    const onInputInsurance = e => {
        setValueInsurance(e.target.value);
    };

    const onInputSymptom = e => {
        setValueSymptom(e.target.value);
    };

    const onPressDate = key => {
        setValueDate(key);
        setValueTime(undefined);
        const reqParamSd = {
            source: formOne.key,
            exam_area: formThree._id,
            exam_type: valueExamType,
            appoint_date: moment(key).utc(true).toISOString()
        };
        if (formOne.key === ung_buou.key && (formOne.enable_time || formThree.enable_time)) {
            setLoading(true);
            handleListTime(reqParamSd);
        }
        if (formOne.key !== ung_buou.key && (formOne.enable_time || formThree.enable_time)) {
            setLoading(true);
            handleListTime(reqParamSd);
        }
    };

    const onPressTime = key => {
        setValueTime(key);
    };

    const onPressPaymentType = (key, label) => {
        setValuePaymentType(key);
        setNamePaymentType(label);
    }

    const onSelectDoctor = values => {
        setSelectDoctor(values);
    };

    const onPressNext = () => {
        if (valueExamType === EXAM_BHYT) {
            if (!valueInsurance.trim()) {
                modal.info({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: "Bạn phải nhập mã bảo hiểm y tế"
                });
                return false;
            }
        }
        if (formOne.key !== ung_buou.key) {
            if (!valueTime) {
                modal.info({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: "Vui lòng chọn giờ khám"
                });
                return false;
            }
        }
        const reqParamFt = {
            source: formOne.key,
            profile: formTwo._id,
            exam_area: formThree._id,
            exam_type: valueExamType,
            appoint_date: valueDate,
            time: valueTime,
            num_insurrance: valueInsurance ? valueInsurance.trim() : ""
        }
        setLoading(true);
        ModelCheckInsurrance(reqParamFt).then(() => {
            const s4 = form.findIndex(key => {return key.step === 4});
            const data = {
                symptom: valueSymptom,
                examType : valueExamType,
                nameExamType : nameExamType,
                paymentType : valuePaymentType,
                namePaymentType : namePaymentType,
                date : valueDate,
                time : valueTime,
                insurance : valueInsurance ? valueInsurance.trim() : "",
                doctor : selectDoctor
            }
            const params = {
                step: 4,
                data: data
            };
            if (s4 === -1) {
                form.push(params);
            } else {
                form[s4] = params;
            }
            _Appointment(form);
            history.push(url.name.examination_5);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description;
                modal.info({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        })
    };

    function yieldListExamType() {
        if (formOne.exam_type && listExamType.length > 0) {
            const mapListExamType = _.map(listExamType, (item, index) => {
                return (
                    <ExamType key={index} onClick={onPressExamType}
                              value={item.type}
                              selected={valueExamType}
                              label={item.name}/>
                );
            });
            return (
                <div className="wrap-panel-item">
                    <div className="wrap-top">
                        <h4><MdLabelOutline/> Chọn loại khám</h4>
                    </div>
                    <div className="wrap-main">{mapListExamType}</div>
                </div>
            );
        } else {
            return null;
        }
    }

    function yieldInsurance() {
        if (valueExamType === EXAM_BHYT) {
            return (
                <div className="wrap-panel-item">
                    <div className="wrap-top">
                        <h4><MdLabelOutline/> Nhập bảo hiểm y tế</h4>
                    </div>
                    <div className="wrap-main">
                        <Input placeholder="Nhập bảo hiểm y tế"
                               value={valueInsurance}
                               onChange={onInputInsurance}
                               size="large"/>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

    function yieldListDay() {
        let listDate = [];
        for (let i = 0; i < 30; i++) {
            let temp = new Date();
            temp = new Date(temp.getFullYear(), temp.getMonth(), temp.getDate(), 0, 0, 0);
            temp.setDate(temp.getDate() + i);
            const dayOfWeek = ((i === 0) ? 'Hôm nay' : func.getDayOfWeek(temp));
            const dateText = moment(temp).format('DD/MM');
            const newItem = {
                date: temp,
                dayOfWeek: dayOfWeek,
                dateText: dateText
            };
            listDate.push(newItem);
        }
        return _.map(listDate, (item, index) => {
            return (
                <DateFrame key={index} onClick={onPressDate}
                           value={item.date}
                           selected={valueDate}
                           labelText_2={item.dayOfWeek}
                           labelText_1={item.dateText}/>
            );
        });
    }

    function yieldListTime() {
        if (formOne.key === ung_buou.key && !formThree.enable_time) {
            return null;
        }
        if (formOne.key !== ung_buou.key && !formOne.enable_time) {
            return null
        }
        if (listTime.length > 0) {
            const mapListTime = _.map(listTime, (item, index) => {
                return (
                    <TimeFrame key={index} onClick={onPressTime}
                               value={item}
                               selected={valueTime}
                               label={item}/>
                );
            });
            return (
                <div className="wrap-datetime">
                    <h4 className="text-muted">Giờ khám</h4>
                    <div className="content">{mapListTime}</div>
                </div>
            );
        } else {
            if (!loading) {
                return (
                    <div className="wrap-datetime">
                        <h4 className="text-muted">Giờ khám</h4>
                        <div className="content">
                            <div className="item-empty">
                                Các khung giờ trong ngày {moment(valueDate).format('DD/MM/YYYY')} đã được đặt hết. Bạn vui
                                lòng chọn ngày khác.
                            </div>
                        </div>
                    </div>
                );
            }
        }
    }

    function yieldListDoctor() {
        if (formOne.doctor) {
            return (
                <div className="wrap-panel-item scrollbar-1">
                    <div className="wrap-top">
                        <h4><GiHospitalCross/> Chọn bác sĩ</h4>
                    </div>
                    <div className="wrap-main">
                        <Select placeholder="Chọn bác sĩ"
                                menuPlacement={"top"}
                                value={selectDoctor}
                                onChange={onSelectDoctor}
                                options={listDoctor}/>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

    function yieldSymptom() {
        if (formOne.key === nhi_dong.key) {
            return (
                <Fragment>
                    <div className="wrap-panel-item">
                        <div className="wrap-top">
                            <h4><MdLabelOutline/> Nhập triệu chứng</h4>
                        </div>
                        <div className="wrap-main">
                            <Input className="w-100"
                                   onChange={onInputSymptom}
                                   value={valueSymptom}
                                   placeholder="Nhập triệu chứng"
                                   size="large"/>
                        </div>
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    function yieldListPaymentType() {
        if (formOne.show_pay_in_hos && listPaymentType.length > 0) {
            const mapListPaymentType = _.map(listPaymentType, (item, index) => {
                return (
                    <PaymentType key={index} onClick={onPressPaymentType}
                                 value={item.value}
                                 selected={valuePaymentType}
                                 label={item.name}/>
                );
            });
            return (
                <Fragment>
                    <div className="wrap-panel-item">
                        <div className="wrap-top">
                            <h4><MdPayment/> Chọn loại thanh toán</h4>
                        </div>
                        <div className="wrap-main">{mapListPaymentType}</div>
                        <hr/>
                        <div className="wrap-event-action">
                            <div className="box-button-event">
                                <div className="ant-btn ant-btn-lg btn-accept"
                                     onClick={onPressNext}><span>Tiếp tục</span> <AiOutlineRight/></div>
                            </div>
                        </div>
                    </div>
                </Fragment>
            );
        } else {
            return (
                <Fragment>
                    <div className="wrap-panel-item">
                        <div className="wrap-event-action">
                            <div className="box-button-event">
                                <div className="ant-btn ant-btn-lg btn-accept"
                                     onClick={onPressNext}><span>Tiếp tục</span> <AiOutlineRight/></div>
                            </div>
                        </div>
                    </div>
                </Fragment>
            );
        }
    }

    return (
        <Fragment>
            <div className="card card-custom">
                <div className="card-header">
                    <h3>Chọn chuyên khoa</h3>
                </div>
                <div className="card-body-top">
                    <div className="descript">
                        <p>Quý khách vui lòng chọn các thông tin/yêu cầu cho lịch khám</p>
                    </div>
                    <div className="step">
                        <div className="step-text">Lịch khám</div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="block-schedule-option-examination">
                        <div className="wrap-panel-content">
                            {yieldListExamType()}
                            {yieldInsurance()}
                            <div className="wrap-panel-item clearfix">
                                <div className="wrap-top">
                                    <h4><MdSchedule/> Chọn Ngày/Giờ đặt khám</h4>
                                </div>
                                <div className="wrap-datetime">
                                    <h4 className="text-muted">Ngày khám</h4>
                                    <div className="content">
                                        {yieldListDay()}
                                    </div>
                                </div>
                                {yieldListTime()}
                            </div>
                            {yieldListDoctor()}
                            {yieldSymptom()}
                            {yieldListPaymentType()}
                        </div>
                    </div>
                </div>
            </div>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_Appointment}, dispatch);
};

export default connect(null, mapDispatchToProps, null, {forwardRef: true})(StepFour);
