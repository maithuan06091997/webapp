import React, {Fragment, useEffect, useState} from "react";
import i18next from "i18next";
import {Button, Form, Modal} from "antd";
import {drkhoa, hong_duc, nhi_dong, ung_buou} from "../../../../constants/Default";
import BoxInputPhone from "../../../BoxInputPhone";
import Loading from "../../../Loading";
import {ModelUpdatePatientCode} from "../../../../models/ModelProfile";
import * as func from "../../../../helpers";
import "./style.scss";

const ModalConfirmProfile = (props) => {
    const [loading, setLoading] = useState(false);
    const [modalConfirmProfile, setModalConfirmProfile] = useState(false);
    const [profileName, setProfileName] = useState(undefined);
    const [message, setMessage] = useState(undefined);
    //*** Declare props ***//
    const {
        _TokenExpired, onOpenModalConfirmProfile, openModalConfirmProfile, detailHospital,
        detailProfile, detailConfirm
    } = props;

    const [modal, contextHolder] = Modal.useModal();
    const [form] = Form.useForm();

    useEffect(() => {
        if (openModalConfirmProfile) {
            form.resetFields();
            setMessage(undefined);
            setProfileName(detailConfirm.name);
            setModalConfirmProfile(openModalConfirmProfile);
        }
    }, [openModalConfirmProfile, form, detailConfirm]);

    const onCloseModalConfirmProfile = type => {
        setModalConfirmProfile(false);
        if (type === "changed") {
            onOpenModalConfirmProfile(false, type);
        } else {
            onOpenModalConfirmProfile(false);
        }
    };

    const onConfirmProfile = (values) => {
        setMessage(undefined);
        const phone = func.isValidPhone(values.itemPhone);
        if (phone === detailConfirm.phone) {
            const idProfile = detailProfile._id;
            let reqParamFt = {
                address: detailConfirm.address,
                province: detailConfirm.province,
                district: detailConfirm.district,
                ward: detailConfirm.ward
            }
            let objectHospital;
            switch(detailHospital.key) {
                case hong_duc.key:
                    objectHospital = {his_profile: {patient_code: detailConfirm.patient_code}};
                    break;
                case ung_buou.key:
                    objectHospital = {ung_buou: {patient_code: detailConfirm.patient_code}};
                    break;
                case nhi_dong.key:
                    objectHospital = {nhi_dong: {patient_code: detailConfirm.patient_code}};
                    break;
                case drkhoa.key:
                    objectHospital = {drkhoa: {patient_code: detailConfirm.patient_code}};
                    break;
                default:
                    objectHospital = "";
            }
            if (objectHospital) {
                reqParamFt = {...reqParamFt, ...objectHospital};
            }
            setLoading(true);
            ModelUpdatePatientCode(idProfile, reqParamFt).then(() => {
                setLoading(false);
                onCloseModalConfirmProfile("changed");
            }).catch(error => {
                if (error.code === "007") {
                    _TokenExpired(error);
                } else {
                    const msg = error.message ? error.message : error.description;
                    modal.warning({
                        title: "Thông báo",
                        okText: "Đóng",
                        centered: true,
                        content: i18next.t(msg)
                    });
                    setLoading(false);
                }
            });
        } else {
            setMessage("Số điện thoại xác nhận không đúng");
        }
    };

    let yieldMessage;
    if (message) {
        yieldMessage = (<div className="alert alert-danger" role="alert">{message}</div>);
    }

    return (
        <Fragment>
            <Modal title="Thông báo"
                   className="block-modal-confirm-profile-examination"
                   closable={true}
                   maskClosable={false}
                   footer={null}
                   visible={modalConfirmProfile}
                   onCancel={onCloseModalConfirmProfile}
                   centered>
                <p>Vì lí do bảo mật thông tin, bạn vui lòng xác nhận số điện thoại của hồ sơ "{profileName}"</p>
                <Form onFinish={onConfirmProfile}
                      form={form}
                      size="large"
                      className="form-confirm-profile"
                      name="form-confirm-profile">
                    {yieldMessage}
                    {" "}
                    <BoxInputPhone required={true}
                                   name="itemPhone"
                                   placeholder="Xác nhận số điện thoại"/>
                    {" "}
                    <Form.Item>
                        <Button className="btn-save w-100" htmlType="submit"><span>Xác nhận</span></Button>
                    </Form.Item>
                </Form>
            </Modal>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default ModalConfirmProfile;
