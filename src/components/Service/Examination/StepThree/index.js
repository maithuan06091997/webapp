import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { url, public_url } from "../../../../constants/Path";
import { nhi_dong, ung_buou } from "../../../../constants/Default";
import { Empty } from "antd";
import BoxSkeleton from "../../../BoxSkeleton";
import {
  ModelListSpecial,
  ModelGroupSpecial,
} from "../../../../models/ModelExamination";
import { _Appointment } from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Handle component payment type ***//
const GroupSpecial = (props) => {
  const { label, value, selected, onClick } = props;

  const onPress = () => {
    onClick(value, label);
  };

  const isActive = selected === value;
  const className = isActive ? "active wrap-panel-item" : "wrap-panel-item";

  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <span>{label}</span>
      </div>
    </Fragment>
  );
};

//*** Handle component list suggest ***//
const SuggestFrame = (props) => {
  const { label, value, selected, onClick } = props;

  const onPress = () => {
    onClick(value, label);
  };

  const isActive = selected === value;
  const className = isActive ? "active wrap-panel-item" : "wrap-panel-item";

  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <span>{label}</span>
      </div>
    </Fragment>
  );
};

const StepThree = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listSpecial, setListSpecial] = useState([]);
  const [listGroupSpecial, setListGroupSpecial] = useState([]);
  const [listSuggest, setListSuggest] = useState([]);
  const [groupSpecial, setGroupSpecial] = useState(undefined);
  const [valueSpecial, setValueSpecial] = useState(undefined);
  //*** Declare props ***//
  const { _TokenExpired, _Appointment, appointment, history } = props;
  const form = appointment && appointment.form ? appointment.form : [];
  const formOne = func.findDataOfStep(form, 1);

  const handleListSpecial = useCallback(
    (params) => {
      ModelListSpecial(params)
        .then((resultFt) => {
          if (!isRendered.current) {
            const dataListSpecial = resultFt.data.one_health_msg.list;
            if (formOne.key === nhi_dong.key) {
              let tmpArraySuggest = [];
              dataListSpecial.forEach(function (el) {
                if (el.images && el.images.length > 0) {
                  tmpArraySuggest.push(el);
                }
              });
              if (tmpArraySuggest.length > 0) {
                setListSuggest(tmpArraySuggest);
                setValueSpecial("goi-y");
              } else {
                setListSuggest([]);
                setValueSpecial(undefined);
              }
            }
            setListSpecial(dataListSpecial);
            setSkeleton(false);
          }
          return null;
        })
        .catch((error) => {
          if (!isRendered.current) {
            if (error.code !== "007") {
              console.log("Examination - StepThree - ListSpecial");
              setSkeleton(false);
            } else {
              _TokenExpired(error);
            }
          }
        });
      return () => {
        isRendered.current = true;
      };
    },
    [_TokenExpired, formOne.key]
  );

  useEffect(() => {
    const reqParamFt = {
      source: formOne.key,
    };
    setSkeleton(true);
    if (formOne.key === ung_buou.key || formOne.key === nhi_dong.key) {
      ModelGroupSpecial(reqParamFt)
        .then((resultFt) => {
          if (!isRendered.current) {
            const dataGroupSpecial = resultFt.data.one_health_msg;
            const reqParamSd = {
              source: formOne.key,
              speciality: dataGroupSpecial[0]._id,
            };
            handleListSpecial(reqParamSd);
            setGroupSpecial(dataGroupSpecial[0]._id);
            setListGroupSpecial(dataGroupSpecial);
          }
          return null;
        })
        .catch((error) => {
          if (!isRendered.current) {
            if (error.code !== "007") {
              console.log("Examination - StepThree - GroupSpecial");
              setSkeleton(false);
            }
          }
        });
    } else {
      if (!isRendered.current) {
        handleListSpecial(reqParamFt);
      }
    }
    return () => {
      isRendered.current = true;
    };
  }, [formOne, handleListSpecial]);

  const onPressNext = (item) => {
    const s3 = form.findIndex((key) => {
      return key.step === 3;
    });
    const s4 = form.findIndex((key) => {
      return key.step === 4;
    });
    if (s4 > -1) {
      form.pop();
    }
    let foundGroupSpecial = listGroupSpecial.find(function (element) {
      return element._id === groupSpecial;
    });
    const params = {
      step: 3,
      data: {
        ...item,
        ...{ group_special: foundGroupSpecial ? foundGroupSpecial : "" },
      },
    };
    if (s3 === -1) {
      form.push(params);
    } else {
      form[s3] = params;
    }
    _Appointment(form);
    history.push(url.name.examination_4);
  };

  const onPressGroupSpecial = (key, label) => {
    const reqParamFt = {
      source: formOne.key,
      speciality: key,
    };
    setListSuggest([]);
    setValueSpecial(undefined);
    setSkeleton(true);
    setGroupSpecial(key);
    handleListSpecial(reqParamFt);
  };

  const onPressSuggest = (key) => {
    setValueSpecial(key);
  };

  function yieldListGroupSpecial() {
    if (listGroupSpecial.length > 1) {
      const mapListGroupSpecial = _.map(listGroupSpecial, (item, index) => {
        return (
          <GroupSpecial
            key={index}
            onClick={onPressGroupSpecial}
            value={item._id}
            selected={groupSpecial}
            label={item.name}
          />
        );
      });

      return (
        <div className="block-select-partition-examination clearfix">
          <div className="wrap-top">
            <h4>Chọn phân khu</h4>
          </div>
          <div className="wrap-main">{mapListGroupSpecial}</div>
        </div>
      );
    }
    return null;
  }

  function yieldListSpecial() {
    if (formOne.key === nhi_dong.key) {
      // Processing display of "Nhi Dong"
      if (valueSpecial === "goi-y") {
        if (listSuggest.length > 0) {
          return _.map(listSuggest, (item, index) => {
            const icon_special = item.images[0];
            return (
              <div
                key={index}
                className="col-xl-6 col-lg-6 col-md-6 col-sm-6 wrap-panel-col"
              >
                <div
                  onClick={onPressNext.bind(this, item)}
                  className="wrap-suggest-item"
                >
                  <div className="wrap-item-img">
                    <img className="img-fluid" src={icon_special} alt="" />
                  </div>
                </div>
              </div>
            );
          });
        } else {
          if (!skeleton) {
            return (
              <Empty className="ant-empty-custom" description="Gợi ý trống" />
            );
          }
        }
      } else {
        if (listSpecial.length > 0) {
          return _.map(listSpecial, (item, index) => {
            const icon_special =
              item.image && item.image.url
                ? item.image.url
                : public_url.img + "/specialist/droh.png";
            return (
              <div
                key={index}
                className="col-xl-4 col-lg-4 col-md-6 col-sm-6 wrap-panel-col"
              >
                <div
                  onClick={onPressNext.bind(this, item)}
                  className="wrap-panel-item"
                >
                  <div className="wrap-item-img">
                    <img className="img-fluid" src={icon_special} alt="" />
                  </div>
                  <div className="wrap-item-info">
                    <span>{item.name}</span>
                  </div>
                </div>
              </div>
            );
          });
        } else {
          if (!skeleton) {
            return (
              <Empty
                className="ant-empty-custom"
                description="Chuyên khoa trống"
              />
            );
          }
        }
      }
    } else {
      if (listSpecial.length > 0) {
        return _.map(listSpecial, (item, index) => {
          const icon_special =
            item.image && item.image.url
              ? item.image.url
              : public_url.img + "/specialist/droh.png";
          return (
            <div
              key={index}
              className="col-xl-4 col-lg-4 col-md-6 col-sm-6 wrap-panel-col"
            >
              <div
                onClick={onPressNext.bind(this, item)}
                className="wrap-panel-item"
              >
                <div className="wrap-item-img">
                  <img className="img-fluid" src={icon_special} alt="" />
                </div>
                <div className="wrap-item-info">
                  <span>{item.name}</span>
                </div>
              </div>
            </div>
          );
        });
      } else {
        if (!skeleton) {
          return (
            <Empty
              className="ant-empty-custom"
              description="Chuyên khoa trống"
            />
          );
        }
      }
    }
  }

  function yieldListSuggest() {
    if (formOne.key === nhi_dong.key && listSuggest.length > 0) {
      return (
        <div className="block-select-suggest clearfix">
          <div className="wrap-main">
            <SuggestFrame
              onClick={onPressSuggest}
              value={"goi-y"}
              selected={valueSpecial}
              label={"Gợi ý"}
            />
            <SuggestFrame
              onClick={onPressSuggest}
              value={"chuyen-khoa"}
              selected={valueSpecial}
              label={"Chuyên khoa"}
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Chọn chuyên khoa</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>Quý khách vui lòng chọn chuyên khoa mình cần đặt khám</p>
          </div>
          <div className="step">
            <div className="step-text">Chuyên khoa</div>
          </div>
        </div>
        <div className="card-body">
          {yieldListGroupSpecial()}
          {yieldListSuggest()}
          <div className="clearfix" />
          <div className="wrap-content block-list-special-examination">
            <div className="wrap-panel-content">
              <div className="row">
                <BoxSkeleton
                  skeleton={skeleton}
                  full={false}
                  panelCol={3}
                  length={21}
                  rows={1}
                  data={yieldListSpecial()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _Appointment }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  StepThree
);
