import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import moment from "moment";
import { url } from "../../../../constants/Path";
import { MdSchedule } from "react-icons/md";
import { AiOutlineRight } from "react-icons/ai";
import { Modal } from "antd";
import Loading from "../../../Loading";
import BoxSkeleton from "../../../BoxSkeleton";
import { ModelListSchedule } from "../../../../models/ModelCallChat";
import { _CallChat } from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Handle component date ***//
const DateFrame = (props) => {
  const { labelText_2, labelText_1, value, selected, onClick } = props;
  const onPress = () => {
    onClick(value);
  };
  const isActive = selected === value;
  const className = isActive ? "active item" : "item";
  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <p>{labelText_2}</p>
        <p>{labelText_1}</p>
      </div>
    </Fragment>
  );
};

//*** Handle component time ***//
const TimeFrame = (props) => {
  const { label, id, value, selected, onClick } = props;
  const onPress = () => {
    onClick(value, id);
  };
  const isActive = selected === value;
  const className = isActive ? "active item" : "item";
  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <p>{label}</p>
      </div>
    </Fragment>
  );
};

const StepFour = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [listSchedule, setListSchedule] = useState([]);
  const [valueDate, setValueDate] = useState(undefined);
  const [valueTime, setValueTime] = useState(undefined);
  const [valueScheduleID, setValueScheduleID] = useState(undefined);
  const [valueBlockID, setValueBlockID] = useState(undefined);
  //*** Declare props ***//
  const { _CallChat, call_chat, history } = props;
  const [modal, contextHolder] = Modal.useModal();
  const form = call_chat && call_chat.form ? call_chat.form : [];
  const formThree = func.findDataOfStep(form, 3);
  const formFour = func.findDataOfStep(form, 4);

  //*** Handle data time **//
  const handleListSchedule = useCallback(
    (params, doctor_id, trigger = false) => {
      setLoading(true);
      setSkeleton(true);
      ModelListSchedule(params, doctor_id)
        .then((resultFt) => {
          const dataListSchedule = resultFt.data.one_health_msg.list;
          const scheduleID = resultFt.data.one_health_msg.doctor_schedule_id;
          if (!trigger) {
            setValueDate(dataListSchedule[0].date);
          } else {
            setValueDate(formFour.date);
            setValueTime(formFour.time);
            setValueBlockID(formFour.block_id);
          }
          setListSchedule(dataListSchedule);
          setValueScheduleID(scheduleID);
          setLoading(false);
          setSkeleton(false);
        })
        .catch((error) => {
          if (!isRendered.current) {
            if (error.code !== "007") {
              console.log("CallChat - StepFour - ListSchedule");
              setLoading(false);
              setSkeleton(false);
            }
          }
        });
    },
    [formFour]
  );

  useEffect(() => {
    if (!formFour) {
      if (!isRendered.current) {
        const tempDate = func.yieldDatetimeNowUTC();
        const reqParamFt = {
          date: func.isoDateToString(tempDate, "DD/MM/YYYY"),
        };
        handleListSchedule(reqParamFt, formThree._id);
      }
    }
    return () => {
      isRendered.current = true;
    };
  }, [formThree, formFour, handleListSchedule]);

  useEffect(() => {
    if (formFour) {
      if (!isRendered.current) {
        const tempDate = func.yieldDatetimeNowUTC();
        const reqParamFt = {
          date: func.isoDateToString(tempDate, "DD/MM/YYYY"),
        };
        handleListSchedule(reqParamFt, formThree._id, true);
      }
    }
    return () => {
      isRendered.current = true;
    };
  }, [formThree, formFour, handleListSchedule]);

  const onPressNext = () => {
    if (!valueTime) {
      modal.info({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Vui lòng chọn giờ khám",
      });
      return false;
    }
    const s4 = form.findIndex((key) => {
      return key.step === 4;
    });
    const data = {
      date: valueDate,
      time: valueTime,
      block_id: valueBlockID,
      schedule_id: valueScheduleID,
    };
    const params = {
      step: 4,
      data: data,
    };
    if (s4 === -1) {
      form.push(params);
    } else {
      form[s4] = params;
    }
    _CallChat(form);
    history.push(url.name.call_chat_5);
  };

  const onPressDate = (key) => {
    setValueDate(key);
    setValueTime(undefined);
  };

  const onPressTime = (key, id) => {
    setValueTime(key);
    setValueBlockID(id);
  };

  function yieldListDay() {
    if (listSchedule.length > 0) {
      return _.map(listSchedule, (item, index) => {
        const labelText_1 = moment(item.date).format("DD/MM");
        const labelText_2 = func.getDayOfWeekVN(
          moment(item.date).format("dddd")
        );
        return (
          <DateFrame
            key={index}
            onClick={onPressDate}
            value={item.date}
            selected={valueDate}
            labelText_2={labelText_2}
            labelText_1={labelText_1}
          />
        );
      });
    } else {
      if (!skeleton) {
        return (
          <div className="item-empty">
            Ngày khám đã được đặt hết. Quý khách vui lòng chọn bác sĩ khác. Trân
            trọng cảm ơn.
          </div>
        );
      }
    }
  }

  function yieldListTime() {
    if (listSchedule.length > 0) {
      const index = listSchedule.findIndex((key) => {
        return key.date === valueDate;
      });
      const yieldTimeEmpty = (
        <div className="wrap-datetime">
          <h4 className="text-muted">Giờ khám</h4>
          <div className="content">
            <div className="item-empty">
              Ngày khám đã được đặt hết. Quý khách vui lòng chọn bác sĩ khác.
              Trân trọng cảm ơn.
            </div>
          </div>
        </div>
      );
      if (index !== -1) {
        let tempDateNow = func.yieldDatetimeNowUTC();
        tempDateNow = moment(tempDateNow).toISOString();
        if (listSchedule[index].date === tempDateNow) {
          const d = new Date();
          const n = d.getHours() + ":" + d.getMinutes();
          const tempHourNow = func.convertHourMinToSec(n);
          const listScheduleNew = listSchedule[index].blocks.filter(function (
            el
          ) {
            return func.convertHourMinToSec(el.time) > tempHourNow;
          });
          if (listScheduleNew.length > 0) {
            listScheduleNew.sort(function (a, b) {
              if (
                func.convertHourMinToSec(a.time) <
                func.convertHourMinToSec(b.time)
              ) {
                return -1;
              }
              if (
                func.convertHourMinToSec(a.time) >
                func.convertHourMinToSec(b.time)
              ) {
                return 1;
              }
              return 0;
            });
            const mapListScheduleNew = _.map(listScheduleNew, (item, index) => {
              return (
                <TimeFrame
                  key={index}
                  onClick={onPressTime}
                  id={item._id}
                  value={item.time}
                  selected={valueTime}
                  label={item.time}
                />
              );
            });
            return (
              <div className="wrap-datetime">
                <h4 className="text-muted">Giờ</h4>
                <div className="content">{mapListScheduleNew}</div>
              </div>
            );
          } else {
            return yieldTimeEmpty;
          }
        } else {
          if (listSchedule[index].blocks.length > 0) {
            listSchedule[index].blocks.sort(function (a, b) {
              if (
                func.convertHourMinToSec(a.time) <
                func.convertHourMinToSec(b.time)
              ) {
                return -1;
              }
              if (
                func.convertHourMinToSec(a.time) >
                func.convertHourMinToSec(b.time)
              ) {
                return 1;
              }
              return 0;
            });
            const mapListSchedule = _.map(
              listSchedule[index].blocks,
              (item, index) => {
                return (
                  <TimeFrame
                    key={index}
                    onClick={onPressTime}
                    id={item._id}
                    value={item.time}
                    selected={valueTime}
                    label={item.time}
                  />
                );
              }
            );
            return (
              <div className="wrap-datetime">
                <h4 className="text-muted">Giờ</h4>
                <div className="content">{mapListSchedule}</div>
              </div>
            );
          } else {
            return yieldTimeEmpty;
          }
        }
      } else {
        return yieldTimeEmpty;
      }
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Đặt lịch gọi video</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>
              Quý khách vui lòng đặt lịch gọi video, bác sĩ sẽ gọi cho quý khách
              đúng giờ đã đặt
            </p>
          </div>
          <div className="step">
            <div className="step-text">Lịch gọi video</div>
          </div>
        </div>
        <div className="card-body">
          <div className="block-schedule-option-call-chat">
            <div className="wrap-panel-content">
              <div className="wrap-panel-item clearfix">
                <div className="wrap-top">
                  <h4>
                    <MdSchedule /> Chọn Ngày/Giờ gọi video
                  </h4>
                </div>
                <div className="wrap-datetime">
                  <h4 className="text-muted">Ngày</h4>
                  <div className="content">
                    <BoxSkeleton
                      skeleton={skeleton}
                      full={true}
                      length={1}
                      rows={1}
                      data={yieldListDay()}
                    />
                  </div>
                </div>
                {yieldListTime()}
              </div>
              <div className="wrap-panel-item">
                <div className="wrap-event-action">
                  <div className="box-button-event">
                    <div
                      className="ant-btn ant-btn-lg btn-accept"
                      onClick={onPressNext}
                    >
                      <span>Tiếp tục</span> <AiOutlineRight />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CallChat }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  StepFour
);
