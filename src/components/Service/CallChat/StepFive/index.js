import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import moment from "moment";
import { FaRegUserCircle, FaRegListAlt } from "react-icons/fa";
import Loading from "../../../Loading";
import FormPayment from "../../FormPayment";
import { ModelCheckVideoPrice } from "../../../../models/ModelCallChat";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const StepFive = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [dataPayment, setDataPayment] = useState(undefined);
  //*** Declare props ***//
  const { call_chat } = props;
  const form = call_chat && call_chat.form ? call_chat.form : [];
  const formOne = func.findDataOfStep(form, 1);
  const formTwo = func.findDataOfStep(form, 2);
  const formThree = func.findDataOfStep(form, 3);
  const formFour = func.findDataOfStep(form, 4);

  const handleCheckVideoPrice = useCallback(() => {
    const reqParamFt = {
      doctor: formThree._id,
      date: func.isoDateToString(formFour.date, "DD/MM/YYYY"),
      block: formFour.block_id,
    };
    setLoading(true);
    ModelCheckVideoPrice(reqParamFt)
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataVideoPrice = resultFt.data.one_health_msg;
          const tmpDataPayment = {
            type: "VIDEO_CALL_SCHEDULE",
            price: dataVideoPrice.price,
            amount: dataVideoPrice.payment,
            discount: dataVideoPrice.discount,
            promotion: dataVideoPrice.exchange ? dataVideoPrice.exchange : "",
            exchangeId: dataVideoPrice.exchange
              ? dataVideoPrice.exchange._id
              : "",
            profile_id: formOne._id,
            block: formFour.block_id,
            date: func.isoDateToString(formFour.date, "DD/MM/YYYY"),
            doctor: formThree._id,
            doctor_schedule_id: formFour.schedule_id,
          };
          setDataPayment(tmpDataPayment);
          setLoading(false);
        }
        return null;
      })
      .catch((error) => {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("CallChat - StepFive - CheckVideoPrice");
            setLoading(false);
          }
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, [formOne, formThree, formFour]);

  useEffect(() => {
    if (!isRendered.current) {
      handleCheckVideoPrice();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleCheckVideoPrice]);

  const handleRecalculationAmount = (flag, data) => {
    if (flag === "changed") {
      const tmpDataPayment = {
        type: "VIDEO_CALL_SCHEDULE",
        price: data.price,
        amount: data.payment,
        discount: data.discount,
        promotion: data.promotion ? data.promotion : "",
        exchangeId: data.promotion ? data.promotion.exchangeId : "",
        profile_id: formOne._id,
        block: formFour.block_id,
        date: func.isoDateToString(formFour.date, "DD/MM/YYYY"),
        doctor: formThree._id,
        doctor_schedule_id: formFour.schedule_id,
      };
      setDataPayment(tmpDataPayment);
    }
  };

  function yieldProfileInfo() {
    if (formOne) {
      const name = formOne.name ? formOne.name : formOne.names[0];
      const gender = func.findGender(formOne.sex, "text");
      const age = func.yieldAge(formOne.birthday);
      const relationship = func.findRelationship(formOne.relationship, "text");
      const phone = formOne.phone ? formOne.phone : formOne.phones[0];
      const location = func.yieldLocation(formOne.location);
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Họ và tên</p>
            <p className="value">{name}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Giới tính</p>
            <p className="value">{gender}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Tuổi</p>
            <p className="value">{age}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Mối quan hệ</p>
            <p className="value">{relationship}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Số điện thoại</p>
            <p className="value">{phone}</p>
          </div>
          <div className="wrap-line address">
            <p className="label">Địa chỉ</p>
            <p className="value">{location}</p>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  function yieldExamScheduleInfo() {
    if (formOne && formTwo && formThree && formFour) {
      const doctor = formThree.last_name + " " + formThree.first_name;
      const nameSpecialist = formTwo.name;
      const date = moment(formFour.date).format("DD/MM/YYYY");
      const time = formFour.time;
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Dịch vụ</p>
            <p className="value">
              {formFour ? "Đặt lịch gọi video" : "Chat với bác sĩ"}
            </p>
          </div>
          <div className="wrap-line">
            <p className="label">Bác sĩ</p>
            <p className="value">{doctor}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Chuyên khoa</p>
            <p className="value">{nameSpecialist}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Giờ khám</p>
            <p className="value">{time}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Ngày khám</p>
            <p className="value">{date}</p>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Xác nhận thông tin & Thanh toán</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>Quý khách vui lòng kiểm tra lại các thông tin đặt dịch vụ</p>
          </div>
          <div className="step">
            <div className="step-text">Thông tin & Thanh toán</div>
          </div>
        </div>
        <div className="card-body">
          <div className="wrap-content block-confirm-info-call-chat">
            <div className="row">
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item wrap-profile-info">
                  <div className="wrap-title">
                    <h4>
                      <FaRegUserCircle /> Thông tin bệnh nhân
                    </h4>
                  </div>
                  <div className="wrap-main">{yieldProfileInfo()}</div>
                  <div className="wrap-title">
                    <h4>
                      <FaRegListAlt /> Thông tin lịch khám
                    </h4>
                  </div>
                  <div className="wrap-main">{yieldExamScheduleInfo()}</div>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item wrap-payment-info">
                  <FormPayment
                    {...props}
                    handleRecalculationAmount={handleRecalculationAmount}
                    dataPayment={dataPayment}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default connect(null, null, null, { forwardRef: true })(StepFive);
