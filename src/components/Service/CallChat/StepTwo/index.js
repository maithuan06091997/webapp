import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import {url, public_url} from "../../../../constants/Path";
import {Empty} from "antd";
import BoxSkeleton from "../../../BoxSkeleton";
import {ModelListSpecial} from "../../../../models/ModelCallChat";
import {_CallChat} from "../../../../actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

const StepTwo = (props) => {
    const isRendered = useRef(false);
    const [skeleton, setSkeleton] = useState(false);
    const [listSpecial, setListSpecial] = useState([]);
    //*** Declare props ***//
    const {_CallChat, call_chat, history} = props;
    const form = call_chat && call_chat.form ? call_chat.form : [];

    const handleListSpecial = useCallback(() => {
        setSkeleton(true);
        ModelListSpecial().then(resultFt => {
            if (!isRendered.current) {
                const dataListSpecial = resultFt.data.one_health_msg.list;
                setListSpecial(dataListSpecial);
                setSkeleton(false);
            }
            return null;
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("CallChat - StepTwo - ListSpecial");
                    setSkeleton(false);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, []);

    useEffect(() => {
        if (!isRendered.current) {
            handleListSpecial();
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleListSpecial]);

    const onPressNext = item => {
        const s2 = form.findIndex(key => {return key.step === 2});
        const params = {
            step: 2,
            data: item
        };
        if (s2 === -1) {
            form.push(params);
        } else {
            form[s2] = params;
        }
        _CallChat(form);
        history.push(url.name.call_chat_3);
    };

    function yieldListSpecial() {
        if (listSpecial.length > 0) {
            return _.map(listSpecial, (item, index) => {
                const icon_code     = item.code ? item.code : 0;
                const icon_special  = public_url.img + "/specialist/"+func.yieldIconSpecialist(icon_code);
                return (
                    <div key={index} className="col-xl-4 col-lg-4 col-md-6 col-sm-6 wrap-panel-col">
                        <div onClick={onPressNext.bind(this, item)} className="wrap-panel-item">
                            <div className="wrap-item-img">
                                <img className="img-fluid" src={icon_special} alt=""/>
                            </div>
                            <div className="wrap-item-info">
                                <span>{item.name}</span>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Chuyên khoa trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <div className="card card-custom">
                <div className="card-header">
                    <h3>Chọn chuyên khoa</h3>
                </div>
                <div className="card-body-top">
                    <div className="descript">
                        <p>Quý khách vui lòng chọn chuyên khoa mình cần đặt dịch vụ</p>
                    </div>
                    <div className="step">
                        <div className="step-text">Chuyên khoa</div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="wrap-content block-list-special-call-chat">
                        <div className="wrap-panel-content">
                            <div className="row">
                                <BoxSkeleton skeleton={skeleton}
                                             full={false}
                                             panelCol={3}
                                             length={21}
                                             rows={1}
                                             data={yieldListSpecial()}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_CallChat}, dispatch);
};

export default connect(null, mapDispatchToProps, null, {forwardRef: true})(StepTwo);
