import React, {Fragment, useEffect, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import {Empty, Modal} from "antd";
import {url} from "../../../../constants/Path";
import {drkhoa, hong_duc, nhi_dong, ung_buou} from "../../../../constants/Default";
import ModalConfirmProfile from "./ModalConfirmProfile";
import Loading from "../../../Loading";
import {ModelUpdatePatientCode} from "../../../../models/ModelProfile";
import {FaPhoneAlt, FaTransgender, FaUserAlt} from "react-icons/fa";
import {BsChevronRight} from "react-icons/bs";
import * as func from "../../../../helpers";
import "./style.scss";

const ModalUpdatePatientCode = (props) => {
    const [loading, setLoading] = useState(false);
    const [modalUpdatePatientCode, setModalUpdatePatientCode] = useState(false);
    const [openModalConfirmProfile, setOpenModalConfirmProfile] = useState(false);
    const [detailConfirm, setDetailConfirm] = useState(undefined);
    //*** Declare props ***//
    const {
        _TokenExpired, _CallChat, history, onOpenModalUpdatePatientCode,
        openModalUpdatePatientCode, listPatientCode, detailProfile, patientCode, detailHospital
    } = props;

    const [modal, contextHolder] = Modal.useModal();
    const nameHospital = detailHospital ? detailHospital.name : "";

    const handleNextStep = (params) => {
        _CallChat(params);
        history.push(url.name.call_chat_2);
    };

    useEffect(() => {
        if (openModalUpdatePatientCode) {
            setModalUpdatePatientCode(openModalUpdatePatientCode);
        }
    }, [openModalUpdatePatientCode]);

    const onCloseModalUpdatePatientCode = flag => {
        setModalUpdatePatientCode(false);
        if (flag === "changed") {
            onOpenModalUpdatePatientCode(flag);
        } else {
            onOpenModalUpdatePatientCode();
        }
    };

    const onPressNext = () => {
        const params = [{
            step: 1,
            data: detailProfile
        }];
        handleNextStep(params);
    };

    const onUpdateProfile = type => {
        const idProfile = detailProfile._id;
        const patient_code = type === "delete" ? "" : "new";
        let reqParamFt = {
            address: detailProfile.location.address,
            province: detailProfile.location.province.province_id,
            district: detailProfile.location.district.district_id,
            ward: detailProfile.location.ward.ward_id
        }
        let objectHospital;
        switch(detailHospital.key) {
            case hong_duc.key:
                objectHospital = {his_profile: {patient_code: patient_code}};
                break;
            case ung_buou.key:
                objectHospital = {ung_buou: {patient_code: patient_code}};
                break;
            case nhi_dong.key:
                objectHospital = {nhi_dong: {patient_code: patient_code}};
                break;
            case drkhoa.key:
                objectHospital = {drkhoa: {patient_code: patient_code}};
                break;
            default:
                objectHospital = "";
        }
        if (objectHospital) {
            reqParamFt = {...reqParamFt, ...objectHospital};
        }
        setLoading(true);
        ModelUpdatePatientCode(idProfile, reqParamFt).then(resultFt => {
            if (type === "delete") {
                setLoading(false);
                onCloseModalUpdatePatientCode("changed");
            } else {
                const params = [{
                    step: 1,
                    data: resultFt.data.one_health_msg
                }];
                handleNextStep(params);
            }
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    const onOpenModalConfirmProfile =  (open, flag, item) => {
        setOpenModalConfirmProfile(open);
        if (flag === "open") {
            setDetailConfirm(item);
        } else if (flag === "changed") {
            onCloseModalUpdatePatientCode(flag);
        }
    };

    function yieldListPatientCode() {
        if (listPatientCode.length > 0) {
            return _.map(listPatientCode, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-main">
                                <div className="wrap-line">
                                    <FaUserAlt/> <span>{item.name}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaTransgender/> <span>{func.findGender(item.sex, "text")}</span>
                                </div>
                                <div className="wrap-line">
                                    <FaPhoneAlt/> <span>{func.yieldCoverPhone(item.phone)}</span>
                                </div>
                            </div>
                            <div onClick={onOpenModalConfirmProfile.bind(this, true, "open", item)}
                                 className="wrap-event-action"><BsChevronRight/></div>
                        </div>
                    </div>
                );
            });
        } else {
            return (<Empty className="ant-empty-custom"
                           description="Hồ sơ trống"/>);
        }
    }

    function yieldAction() {
        if (patientCode) {
            return (
                <div className="wrap-result-action">
                    <div onClick={onUpdateProfile.bind(this, "delete")} className="ant-btn ant-btn-lg btn-danger"><span>Xóa mã hồ sơ cũ</span></div>
                    <div onClick={onPressNext}
                         className="ant-btn ant-btn-lg btn-accept"><span>Bỏ qua và tiếp tục đặt ký</span></div>
                </div>
            );
        } else {
            return (
                <div className="wrap-result-action has-content">
                    <p>Nếu danh sách trên không có hồ sơ của bạn, vui lòng chọn tạo mã hồ sơ mới và tiếp tục đặt khám</p>
                    <p style={{color: "#ffc107"}}>Lưu ý: Nếu tạo mã hồ sơ mới, bạn không thể xem lịch sử bệnh án cũ trên ứng dụng DR.OH</p>
                    <div onClick={onUpdateProfile.bind(this, "new")}
                         className="ant-btn ant-btn-lg btn-accept"><span>Tạo mã hồ sơ mới</span></div>
                </div>
            );
        }
    }

    return (
        <Fragment>
            <Modal title={"Vui lòng chọn hồ sơ của bạn ở Bệnh viện " + nameHospital + " (Nếu có)"}
                   className="block-modal-update-patient-code-call-chat ant-modal-custom"
                   closable={true}
                   width={600}
                   footer={null}
                   visible={modalUpdatePatientCode}
                   onCancel={onCloseModalUpdatePatientCode}>
                <div className="wrap-panel-content">
                    <div className="row">
                        {yieldListPatientCode()}
                    </div>
                </div>
                {yieldAction()}
            </Modal>
            <ModalConfirmProfile {...props}
                                 detailConfirm={detailConfirm}
                                 openModalConfirmProfile={openModalConfirmProfile}
                                 onOpenModalConfirmProfile={onOpenModalConfirmProfile}/>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default ModalUpdatePatientCode;
