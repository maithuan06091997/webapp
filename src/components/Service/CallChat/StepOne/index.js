import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import {Modal, Empty} from "antd";
import {url} from "../../../../constants/Path";
import {hong_duc, nhi_dong, ung_buou, drkhoa} from "../../../../constants/Default";
import {AiFillPlusCircle, AiOutlineRight} from "react-icons/ai";
import ModalUpdatePatientCode from "./ModalUpdatePatientCode";
import BoxSkeleton from "../../../BoxSkeleton";
import Loading from "../../../Loading";
import {ModelListProfile, ModelFindPatientInfo, ModelUpdatePatientCode} from "../../../../models/ModelProfile";
import {_CallChat, _FindProfile} from "../../../../actions";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

const StepOne = (props) => {
    const isRendered = useRef(false);
    const [skeleton, setSkeleton] = useState(false);
    const [loading, setLoading] = useState(false);
    const [openModalUpdatePatientCode, setOpenModalUpdatePatientCode] = useState(false);
    const [listProfile, setListProfile] = useState([]);
    const [listPatientCode, setListPatientCode] = useState([]);
    const [detailHospital, setDetailHospital] = useState(undefined);
    const [detailProfile, setDetailProfile] = useState(undefined);
    const [patientCode, setPatientCode] = useState(undefined);
    //*** Declare props ***//
    const {_TokenExpired, _CallChat, _FindProfile, history} = props;

    const [modal, contextHolder] = Modal.useModal();

    const handleNextStep = (params) => {
        _CallChat(params);
        history.push(url.name.call_chat_2);
    };

    const handleListProfile = useCallback(() => {
        setSkeleton(true);
        ModelListProfile().then(resultFt => {
            if (!isRendered.current) {
                const dataListProfile = resultFt.data.one_health_msg.list;
                setListProfile(dataListProfile);
                setSkeleton(false);
            }
            return null;
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("CallChat - StepOne - ListProfile");
                    setSkeleton(false);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, []);

    const handleFindPatientCode = item => {
        const reqParamFt = {
            name: item.name ? item.name : item.names[0],
            source: hong_duc.key,
            birthday: item.birthday,
            sex: func.findGender(item.sex, "value"),
            province_id: item.location.province.province_id,
            identity_num: item.identity_num
        };
        setLoading(true);
        ModelFindPatientInfo(reqParamFt)
            .then(resultFt => {
                const dataListPatientCode = resultFt.data.one_health_msg;
                if (dataListPatientCode.length > 0) {
                    setLoading(false);
                    setDetailProfile(item);
                    setDetailHospital({
                        name: hong_duc.name,
                        key: hong_duc.key
                    });
                    setListPatientCode(dataListPatientCode);
                    setOpenModalUpdatePatientCode(true);
                } else {
                    const idProfile = item._id;
                    let reqParamSd = {
                        address: item.location.address,
                        province: item.location.province.province_id,
                        district: item.location.district.district_id,
                        ward: item.location.ward.ward_id
                    }
                    let objectHospital;
                    switch (hong_duc.key) {
                        case hong_duc.key:
                            objectHospital = {his_profile: {patient_code: "new"}};
                            break;
                        case ung_buou.key:
                            objectHospital = {ung_buou: {patient_code: "new"}};
                            break;
                        case nhi_dong.key:
                            objectHospital = {nhi_dong: {patient_code: "new"}};
                            break;
                        case drkhoa.key:
                            objectHospital = {drkhoa: {patient_code: "new"}};
                            break;
                        default:
                            objectHospital = "";
                    }
                    if (objectHospital) {
                        reqParamSd = {...reqParamSd, ...objectHospital};
                    }
                    ModelUpdatePatientCode(idProfile, reqParamSd)
                        .then(resultSd => {
                            const params = [{
                                step: 1,
                                data: resultSd.data.one_health_msg
                            }];
                            handleNextStep(params);
                        })
                        .catch(error => {
                            if (error.code === "007") {
                                _TokenExpired(error);
                            } else {
                                const msg = error.message ? error.message : error.description;
                                modal.warning({
                                    title: "Thông báo",
                                    okText: "Đóng",
                                    centered: true,
                                    content: i18next.t(msg)
                                });
                                setLoading(false);
                            }
                        });
                }
            })
            .catch(error => {
                if (error.code === "007") {
                    _TokenExpired(error);
                } else {
                    const msg = error.message ? error.message : error.description;
                    modal.warning({
                        title: "Thông báo",
                        okText: "Đóng",
                        centered: true,
                        content: i18next.t(msg)
                    });
                    setLoading(false);
                }
            });
    };

    //*** Handle yield list profile ***//
    useEffect(() => {
        if (!isRendered.current) {
            handleListProfile();
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleListProfile]);

    const onPressCreateNew = () => {
        _FindProfile();
        const pushState = {
            return_url: url.name.call_chat_1
        }
        history.push({
            pathname: url.name.account_create_relative_profile,
            state: pushState
        });
    };

    const onPressUpdatePatientCode = (item, code) => {
        handleFindPatientCode(item);
        setPatientCode(code);
    };

    const onOpenModalUpdatePatientCode = flag => {
        setOpenModalUpdatePatientCode(false);
        if (flag === "changed") {
            handleListProfile();
        }
    };

    const onPressNext = (item, code) => {
        if (code) {
            const params = [{
                step: 1,
                data: item
            }];
            handleNextStep(params);
        } else {
            handleFindPatientCode(item);
            setPatientCode(code);
        }
    };

    function yieldListProfile() {
        if (listProfile.length > 0) {
            const keyHospital   = func.findKeyNameHospital(hong_duc.key);
            return _.map(listProfile, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-top">
                                <h4> {item.name ? item.name : item.names[0]}</h4>
                            </div>
                            <div className="wrap-main">
                                <div className="wrap-line">
                                    <p className="label">Giới tính</p>
                                    <p className="value">{func.findGender(item.sex, "text")}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Tuổi</p>
                                    <p className="value">{func.yieldAge(item.birthday)}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Mối quan hệ</p>
                                    <p className="value">{func.findRelationship(item.relationship, "text")}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Số điện thoại</p>
                                    <p className="value">
                                        {item.phone ? item.phone : item.phones.length > 0 ? item.phones[0] : "-"}
                                    </p>
                                </div>
                                <div className="wrap-line drop-line">
                                    <p className="label">Địa chỉ</p>
                                    <p className="value">{func.yieldLocation(item.location)}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Mã hồ sơ</p>
                                    <p className="value">
                                        <span className="code">{item[keyHospital].patient_code}</span>
                                    </p>
                                </div>
                            </div>
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={onPressUpdatePatientCode.bind(this, item, item[keyHospital].patient_code)}
                                         className="ant-btn ant-btn-lg">
                                        <span>Cập nhật mã hồ sơ</span></div>
                                    <div onClick={onPressNext.bind(this, item, item[keyHospital].patient_code)}
                                         className="ant-btn btn-booking ant-btn-lg">
                                        <span>Tiếp tục</span> <AiOutlineRight/></div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Hồ sơ trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <div className="card card-custom">
                <div className="card-header">
                    <h3>Chọn hồ sơ</h3>
                    <div className="wrap-event-action">
                        <div className="box-button-event">
                            <div onClick={onPressCreateNew}
                                 className="ant-btn btn-accept">
                                <AiFillPlusCircle/> <span>Tạo mới hồ sơ</span></div>
                        </div>
                    </div>
                </div>
                <div className="card-body-top">
                    <div className="descript">
                        <p>Quý khách vui lòng kiểm tra thông tin cá nhân khi đặt dịch vụ <br/>
                            (Nếu quý khách chưa có thông tin xin vui lòng tạo mới)</p>
                    </div>
                    <div className="step">
                        <div className="step-text">Thông tin cá nhân</div>
                    </div>
                </div>
                <div className="card-body">
                    <div className="wrap-content block-list-profile-call-chat">
                        <div className="wrap-panel-content">
                            <div className="row">
                                <BoxSkeleton skeleton={skeleton}
                                             full={false}
                                             length={8}
                                             rows={4}
                                             data={yieldListProfile()}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ModalUpdatePatientCode {...props}
                                    listPatientCode={listPatientCode}
                                    detailProfile={detailProfile}
                                    detailHospital={detailHospital}
                                    patientCode={patientCode}
                                    onOpenModalUpdatePatientCode={onOpenModalUpdatePatientCode}
                                    openModalUpdatePatientCode={openModalUpdatePatientCode}/>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_CallChat, _FindProfile}, dispatch);
};

export default connect(null, mapDispatchToProps, null, {forwardRef: true})(StepOne);