import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react"; //, useCallback, useEffect, useRef, useState
import _ from "lodash";
import i18next from "i18next";
import { url, public_url } from "../../../../constants/Path";
import { FaUserAlt } from "react-icons/fa";
// import Select from "react-select";
import { Input, Empty, Modal } from "antd";
import Rating from "react-rating";
// import {dataSortDoctor} from "../../../../constants/Data";
import ImageViewer from "react-simple-image-viewer";
import BoxSkeleton from "../../../BoxSkeleton";
import Loading from "../../../Loading";
import {
  ModelListDoctor,
  ModelDetailDoctor,
  ModelListDoctorOnline,
  ModelListRating,
} from "../../../../models/ModelCallChat";
import { _CallChat } from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 100;
const LIMIT_RATING = 25;

const StepThree = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const [modalDetailDoctor, setModalDetailDoctor] = useState(false);
  const [modalDetailRating, setModalDetailRating] = useState(false);
  // const [selectSort, setSelectSort] = useState(dataSortDoctor(this)[0]);
  const [listDoctor, setListDoctor] = useState([]);
  const [listDoctorRating, setListDoctorRating] = useState([]);
  const [doctorRatingLimitRecord, setDoctorRatingLimitRecord] = useState([]);
  const [page, setPage] = useState(1);
  const [detailDoctor, setDetailDoctor] = useState(undefined);
  //*** Declare props ***//
  const { _TokenExpired, _CallChat, call_chat, history, user } = props;
  const form = call_chat && call_chat.form ? call_chat.form : [];
  const formTwo = func.findDataOfStep(form, 2);

  const [modal, contextHolder] = Modal.useModal();
  const { Search } = Input;

  const handleListDoctor = useCallback(
    (page, search, trigger = false) => {
      const reqParamFt = {
        page: page,
        limit: LIMIT,
        search: search,
        speciality: formTwo.code,
        filter: "all",
      };
      setSkeleton(true);
      const resListDoctor = ModelListDoctor(reqParamFt);
      const resListDoctorOnline = ModelListDoctorOnline();
      Promise.all([resListDoctor, resListDoctorOnline])
        .then((values) => {
          if (!isRendered.current) {
            const dataListDoctor = values[0].data.one_health_msg;
            const dataListDoctorOnline = values[1].data.one_health_msg;
            dataListDoctor.forEach((doctor) => {
              let seachDoctor = dataListDoctorOnline.find(
                (e) => e._id === doctor._id
              );
              doctor.status_online = seachDoctor ? seachDoctor.status : "";
            });
            dataListDoctor.sort(function (a, b) {
              if (a.status_online > b.status_online) {
                return -1;
              }
              if (a.status_online < b.status_online) {
                return 1;
              }
              return 0;
            });
            setListDoctor(dataListDoctor);
            setSkeleton(false);
          }
          return null;
        })
        .catch((error) => {
          if (!isRendered.current) {
            if (trigger && error.code === "007") {
              _TokenExpired(error);
            }
            if (error.code !== "007") {
              console.log("CallChat - StepThree - ListDoctor");
              setSkeleton(false);
            }
          }
        });
    },
    [_TokenExpired, formTwo]
  );

  //*** Handle yield list point history ***//
  const handleListRating = useCallback(
    (item, page, array, trigger = false) => {
      const reqParamFt = {
        page: page,
      };
      setLoading(true);
      ModelListRating(reqParamFt, item._id)
        .then((resultFt) => {
          const dataListRating = resultFt.data.one_health_msg[0]
            ? resultFt.data.one_health_msg[0].rate_info
            : [];
          const tmpArray = array.concat(dataListRating);
          setListDoctorRating(tmpArray);
          setDoctorRatingLimitRecord(dataListRating);
          setModalDetailRating(true);
          setLoading(false);
        })
        .catch((error) => {
          if (error.code !== "007") {
            console.log("CallChat - StepThree - ListRating");
            setLoading(false);
          }
          if (trigger && error.code === "007") {
            _TokenExpired(error);
          }
        });
    },
    [_TokenExpired]
  );

  useEffect(() => {
    if (!isRendered.current) {
      handleListDoctor(1, "");
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListDoctor]);

  const onPressModule = (item) => {
    const s3 = form.findIndex((key) => {
      return key.step === 3;
    });
    const params = {
      step: 3,
      data: item,
    };
    if (s3 === -1) {
      form.push(params);
    } else {
      form[s3] = params;
    }
    _CallChat(form);
    history.push(url.name.pay_chat);
  };

  const onPressFindDoctor = (value) => {
    const nameDoctor = value ? value.trim() : "";
    handleListDoctor(1, nameDoctor, true);
  };

  const onPressNext = (item) => {
    const s3 = form.findIndex((key) => {
      return key.step === 3;
    });
    const s4 = form.findIndex((key) => {
      return key.step === 4;
    });
    if (s4 > -1) {
      form.pop();
    }
    const params = {
      step: 3,
      data: item,
    };
    if (s3 === -1) {
      form.push(params);
    } else {
      form[s3] = params;
    }
    _CallChat(form);
    history.push(url.name.call_chat_4);
  };

  const onPressDetailDoctor = (item) => {
    const doctor_id = item._id;
    const reqParamFt = {
      user_id: user.personal._id,
    };
    setLoading(true);
    ModelDetailDoctor(reqParamFt, doctor_id)
      .then((resultFt) => {
        const dataDetailDoctor = resultFt.data.one_health_msg[0];
        setDetailDoctor(dataDetailDoctor);
        setModalDetailDoctor(true);
        setLoading(false);
      })
      .catch((error) => {
        if (error.code === "007") {
          _TokenExpired(error);
        } else {
          const msg = error.message ? error.message : error.description;
          modal.warning({
            title: "Thông báo",
            okText: "Đóng",
            centered: true,
            content: i18next.t(msg),
          });
          setLoading(false);
        }
      });
  };

  const onPressDetailRating = (item) => {
    handleListRating(item, page, []);
  };

  const onNextPage = () => {
    const next = page + 1;
    setPage(next);
    handleListRating(detailDoctor, next, listDoctorRating, true);
  };

  const onCloseModalDetailDoctor = () => {
    setModalDetailDoctor(false);
  };

  const onCloseModalDetailRating = () => {
    setPage(1);
    setListDoctorRating([]);
    setDoctorRatingLimitRecord([]);
    setModalDetailRating(false);
  };

  const openImageViewer = useCallback((index) => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const closeImageViewer = () => {
    setCurrentImage(0);
    setIsViewerOpen(false);
  };

  function yieldListDoctor() {
    if (listDoctor.length > 0) {
      return _.map(listDoctor, (item, index) => {
        const rate = item.rate ? item.rate.rate : 0;
        const total_rate = item.rate ? item.rate.total_rate : 0;
        const rating = Math.round(rate * 100) / 100;
        const full_name = item.last_name + " " + item.first_name;
        const avatar = item.avatar
          ? item.avatar.url
          : public_url.img + "/callchat/img_doctor.png";
        const status_text = item.status_online ? item.status_online : "Offline";
        const status_color = item.status_online ? "absent" : "offline";
        let specialities = "";
        const dataSpecialities = item.doctor_profile.specialities;
        dataSpecialities.forEach((el) => {
          if (el.name) {
            specialities += el.name + " - ";
          }
        });
        return (
          <div
            key={index}
            className="col-xl-4 col-lg-4 col-md-4 col-sm-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div
                onClick={onPressDetailDoctor.bind(this, item)}
                className="wrap-top"
              >
                <div className="round-frame">
                  <img className="avatar" src={avatar} alt="" />
                </div>
                <div className="description">
                  <h4 className="name">{full_name}</h4>
                  <Rating
                    initialRating={rating}
                    className="rating"
                    readonly
                    emptySymbol={
                      <img
                        src={public_url.img + "/icons/icon_star_non.png"}
                        alt=""
                        className="icon"
                      />
                    }
                    fullSymbol={
                      <img
                        src={public_url.img + "/icons/icon_star_rated.png"}
                        alt=""
                        className="icon"
                      />
                    }
                  />
                  <p>{specialities.substring(0, specialities.length - 2)}</p>
                </div>
              </div>
              <div className="wrap-main">
                <div className="info-subscribe">
                  <p className={status_color}>
                    <img
                      src={public_url.img + "/icons/icon_status.png"}
                      alt=""
                    />
                    {status_text}
                  </p>
                  <p>
                    <img
                      src={public_url.img + "/icons/icon_user_2.png"}
                      alt=""
                    />
                    {total_rate} Lượt tư vấn
                  </p>
                </div>
              </div>
              <div className="wrap-event-action">
                <div className="box-button-event">
                  <div
                    onClick={() => onPressModule(item)}
                    className="ant-btn ant-btn-chat"
                  >
                    <span className="addon-btn">
                      <img
                        className="bg-chat"
                        src={public_url.img + "/background/bg_chat.png"}
                        alt=""
                      />
                      <img
                        className="icon-chat"
                        src={public_url.img + "/icons/icon_chat.png"}
                        alt=""
                      />
                    </span>
                    <span className="label chat">Chat ngay</span>
                  </div>
                  <div
                    onClick={onPressNext.bind(this, item)}
                    className="ant-btn ant-btn-call"
                  >
                    <span className="addon-btn">
                      <img
                        className="bg-call"
                        src={public_url.img + "/background/bg_chat.png"}
                        alt=""
                      />
                      <img
                        className="icon-call"
                        src={public_url.img + "/icons/icon_video.png"}
                        alt=""
                      />
                    </span>
                    <span className="label call">Gọi video</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Danh sách bác sĩ trống"
          />
        );
      }
    }
  }

  function yieldListDoctorRating() {
    if (listDoctorRating.length > 0) {
      return _.map(listDoctorRating, (item, index) => {
        const avatar = item.user.avatar
          ? item.user.avatar.url
          : public_url.img + "/icons/icon_avatar2.jpg";
        const name = item.user.last_name + " " + item.user.first_name;
        let yieldContentReviews;
        if (item.comment) {
          yieldContentReviews = (
            <div className="content-reviews">{item.comment}</div>
          );
        }
        return (
          <div
            key={index}
            className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-img">
                <img className="img-fluid" src={avatar} alt="" />
              </div>
              <div className="wrap-item-info">
                <h4>{name}</h4>
                <p className="datetime">T2, 16/11/2020 14:09</p>
                <Rating
                  initialRating={item.rate}
                  className="rating"
                  readonly
                  emptySymbol={
                    <img
                      src={public_url.img + "/icons/icon_star_non.png"}
                      alt=""
                      className="icon"
                    />
                  }
                  fullSymbol={
                    <img
                      src={public_url.img + "/icons/icon_star_rated.png"}
                      alt=""
                      className="icon"
                    />
                  }
                />
                {yieldContentReviews}
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!loading) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Danh sách xếp hạng trống"
          />
        );
      }
    }
  }

  function yieldPaginationAction() {
    if (doctorRatingLimitRecord.length === LIMIT_RATING) {
      return (
        <div className="wrap-lazy-pagination">
          <hr className="w-100" />
          <div onClick={onNextPage} className="ant-btn btn-accept ant-btn-lg">
            <span>Xem thêm</span>
          </div>
        </div>
      );
    }
    return null;
  }

  let doctorName = "N/A";
  let doctorAvatar = "";
  let doctorTitle = "";
  let doctorAddress = "";
  let doctorGraduation = "";
  let doctorCertificateNum = "-";
  let lineDoctorGraduation = "";
  let doctorRating = 0;
  let doctorLikes = 0;
  let doctorVotes = 0;
  let doctorAdvice = 0;
  let doctorCertificateImg = [];
  let listDoctorCertificateImg;
  let specialist;
  if (detailDoctor) {
    doctorName = detailDoctor.last_name + " " + detailDoctor.first_name;
    doctorAvatar = detailDoctor.avatar
      ? detailDoctor.avatar.url
      : public_url.img + "/callchat/img_doctor.png";
    doctorLikes = detailDoctor.likes ? detailDoctor.likes : 0;
    doctorRating = detailDoctor.rate ? detailDoctor.rate.rate : 0;
    doctorVotes = detailDoctor.rate ? detailDoctor.rate.count : 0;
    doctorAdvice = detailDoctor.rate ? detailDoctor.rate.total_rate : 0;
    doctorTitle = detailDoctor.doctor_profile.doctor_title
      ? detailDoctor.doctor_profile.doctor_title
      : "-";
    doctorAddress = detailDoctor.province ? detailDoctor.province.vi_name : "-";
    doctorGraduation = detailDoctor.doctor_profile.graduation_year
      ? func.yieldGraduation(detailDoctor.doctor_profile.graduation_year)
      : "";
    if (doctorGraduation) {
      lineDoctorGraduation = (
        <div className="wrap-line">
          <p className="label">Kinh nghiệm</p>
          <p className="value">{doctorGraduation + " năm"}</p>
        </div>
      );
    }
    if (detailDoctor.doctor_profile.practice_certificate_img) {
      doctorCertificateImg.push(
        detailDoctor.doctor_profile.practice_certificate_img.url
      );
    }
    if (doctorCertificateImg.length > 0) {
      doctorCertificateNum = detailDoctor.doctor_profile.practice_certificate;
      listDoctorCertificateImg = doctorCertificateImg.map((src, index) => (
        <img
          src={src}
          onClick={() => openImageViewer(index)}
          key={index}
          alt=""
        />
      ));
    }

    const listSpecial = detailDoctor.doctor_profile.specialities;
    if (listSpecial.length > 0) {
      const mapListSpecial = _.map(listSpecial, (item, index) => {
        const icon_code = item.code ? item.code : 0;
        const icon_special =
          public_url.img + "/specialist/" + func.yieldIconSpecialist(icon_code);
        return (
          <div
            key={index}
            className="col-xl-4 col-lg-4 col-md-6 col-sm-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-img">
                <img className="avatar" src={icon_special} alt="" />
              </div>
              <div className="wrap-item-info">
                <p>{item.name}</p>
              </div>
            </div>
          </div>
        );
      });
      specialist = <div className="row">{mapListSpecial}</div>;
    }
  }

  return (
    <Fragment>
      <div className="card card-custom">
        <div className="card-header">
          <h3>Chọn bác sĩ</h3>
        </div>
        <div className="card-body-top">
          <div className="descript">
            <p>Quý khách vui lòng chỉ định bác sĩ mình mong muốn</p>
          </div>
          <div className="step">
            <div className="step-text">Thông tin bác sĩ</div>
          </div>
        </div>
        <div className="card-body">
          <div className="wrap-filter-call-chat">
            <div className="wrap-search">
              <div className="title">
                <span>Tìm kiếm bác sĩ</span>
              </div>
              <Search
                placeholder="Tìm kiếm bác sĩ"
                maxLength={50}
                readOnly={skeleton}
                onSearch={skeleton ? null : onPressFindDoctor}
                allowClear
                enterButton
              />
            </div>
            {/*<div className="wrap-sort">*/}
            {/*    <div className="title"><span>Hiển thị</span></div>*/}
            {/*    <Select value={selectSort}*/}
            {/*            options={dataSortDoctor(this)}/>*/}
            {/*</div>*/}
          </div>
          <div className="wrap-content block-list-doctor-call-chat">
            <div className="wrap-panel-content">
              <div className="row">
                <BoxSkeleton
                  skeleton={skeleton}
                  full={false}
                  panelCol={3}
                  length={12}
                  rows={3}
                  data={yieldListDoctor()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        title="Thông tin chi tiết Bác sĩ"
        className="block-modal-detail-doctor ant-modal-custom"
        closable={true}
        width={600}
        footer={null}
        visible={modalDetailDoctor}
        onCancel={onCloseModalDetailDoctor}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-line">
            <div className="short-info">
              <div className="round-frame">
                <img className="avatar" src={doctorAvatar} alt="" />
              </div>
              <div
                onClick={onPressDetailRating.bind(this, detailDoctor)}
                className="description"
              >
                <p className="doctor-name">{doctorName}</p>
                <Rating
                  initialRating={doctorRating}
                  className="rating"
                  readonly
                  emptySymbol={
                    <img
                      src={public_url.img + "/icons/icon_star_non.png"}
                      alt=""
                      className="icon"
                    />
                  }
                  fullSymbol={
                    <img
                      src={public_url.img + "/icons/icon_star_rated.png"}
                      alt=""
                      className="icon"
                    />
                  }
                />
                <div className="votes">
                  (<span>{doctorVotes}</span> <FaUserAlt />)
                </div>
              </div>
            </div>
            <div className="info-subscribe">
              <div className="consultation">
                <p className="label">Lượt tư vấn</p>
                <p>{doctorAdvice}</p>
              </div>
              <div className="thanks">
                <p className="label">Lượt cảm ơn</p>
                <p>{doctorLikes}</p>
              </div>
            </div>
            <div className="wrap-event-action">
              <div className="box-button-event">
                <div
                  onClick={onPressModule}
                  className="ant-btn ant-btn-lg ant-btn-chat"
                >
                  <span className="addon-btn">
                    <img
                      className="bg-chat"
                      src={public_url.img + "/background/bg_chat.png"}
                      alt=""
                    />
                    <img
                      className="icon-chat"
                      src={public_url.img + "/icons/icon_chat.png"}
                      alt=""
                    />
                  </span>
                  <span className="label chat">Chat ngay</span>
                </div>
                <div
                  onClick={onPressNext.bind(this, detailDoctor)}
                  className="ant-btn ant-btn-lg ant-btn-call"
                >
                  <span className="addon-btn">
                    <img
                      className="bg-call"
                      src={public_url.img + "/background/bg_chat.png"}
                      alt=""
                    />
                    <img
                      className="icon-call"
                      src={public_url.img + "/icons/icon_video.png"}
                      alt=""
                    />
                  </span>
                  <span className="label call">Gọi video</span>
                </div>
              </div>
            </div>
          </div>
          <div className="wrap-panel-line mt-2">
            <div className="full-info">
              <h4>Thông tin bác sĩ</h4>
              <div className="wrap-line">
                <p className="label">Chức danh</p>
                <p className="value">{doctorTitle}</p>
              </div>
              {lineDoctorGraduation}
              <div className="wrap-line">
                <p className="label">Địa chỉ</p>
                <p className="value">{doctorAddress}</p>
              </div>
            </div>
          </div>
          <div className="wrap-panel-line mt-2">
            <div className="certificate-info">
              <h4>Chứng chỉ hành nghề</h4>
              <div className="wrap-line">
                <p className="label">{doctorCertificateNum}</p>
                <p className="value">{listDoctorCertificateImg}</p>
              </div>
            </div>
          </div>
          <div className="wrap-panel-line mt-2">
            <div className="specialist">
              <h4>Chuyên khoa</h4>
              {specialist}
            </div>
          </div>
        </div>
      </Modal>
      <Modal
        title="Thông tin xếp hạng"
        className="block-modal-detail-rating ant-modal-custom"
        closable={true}
        width={600}
        footer={null}
        visible={modalDetailRating}
        onCancel={onCloseModalDetailRating}
      >
        <div className="total-rating">
          <Rating
            initialRating={doctorRating}
            className="rating"
            readonly
            emptySymbol={
              <img
                src={public_url.img + "/icons/icon_star_non.png"}
                alt=""
                className="icon"
              />
            }
            fullSymbol={
              <img
                src={public_url.img + "/icons/icon_star_rated.png"}
                alt=""
                className="icon"
              />
            }
          />
          <div className="votes">
            (<span className="value">{doctorVotes}</span> <FaUserAlt />)
          </div>
        </div>
        <div className="wrap-panel-content">
          <div className="row">{yieldListDoctorRating()}</div>
        </div>
        {yieldPaginationAction()}
      </Modal>
      {isViewerOpen && (
        <ImageViewer
          src={doctorCertificateImg}
          currentIndex={currentImage}
          onClose={closeImageViewer}
        />
      )}
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CallChat }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  StepThree
);
