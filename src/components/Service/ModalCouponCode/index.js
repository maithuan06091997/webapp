import React, {Fragment, useEffect, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import {Button, Empty, Form, Input, Modal} from "antd";
import BoxSkeleton from "../../BoxSkeleton";
import Loading from "../../Loading";
import {
    ModelListAvailable, ModelSearchPromotionOrderType,
    ModelApplyPromotion, ModelCheckPricePromotion
} from "../../../models/ModelPromotion";
import * as func from "../../../helpers";
import "./style.scss";

const ModalCouponCode = (props) => {
    const [loading, setLoading] = useState(false);
    const [skeleton, setSkeleton] = useState(false);
    const [modalCouponCode, setModalCouponCode] = useState(false);
    const [listAvailable, setListAvailable] = useState([]);
    //*** Declare props ***//
    const {
        _TokenExpired, _BadgeNotification, onOpenModalCouponCode, openModalCouponCode, dataPayment
    } = props;

    const [form] = Form.useForm();
    const [modal, contextHolder] = Modal.useModal();

    useEffect(() => {
        if (openModalCouponCode) {
            if (dataPayment && dataPayment.promotion) {
                let reqParamFt;
                switch(dataPayment.type) {
                    case "APPOINTMENT":
                        reqParamFt = {
                            page: 1,
                            orderType: dataPayment.type,
                            source: dataPayment.source
                        }
                        break;
                    case "VIDEO_CALL_SCHEDULE":
                        reqParamFt = {
                            page: 1,
                            orderType: dataPayment.type,
                            block: dataPayment.block,
                            doctor: dataPayment.doctor,
                            date: dataPayment.date,
                            profile: dataPayment.profile_id
                        }
                        break;
                    default:
                        return reqParamFt = "";
                }
                setSkeleton(true);
                ModelListAvailable(reqParamFt).then(resultFt => {
                    const dataListAvailable = resultFt.data.one_health_msg;
                    setListAvailable(dataListAvailable);
                    setSkeleton(false);
                }).catch(error => {
                    if (error.code !== "007") {
                        console.log("ModalCouponCode - ListAvailable");
                        setSkeleton(false);
                    } else {
                        _TokenExpired(error);
                    }
                })
            }
            form.resetFields();
            setModalCouponCode(openModalCouponCode);
        }
    }, [_TokenExpired, openModalCouponCode, dataPayment, form]);

    const onCloseModalCouponCode = (flag, data) => {
        setModalCouponCode(false);
        if (flag === "changed") {
            onOpenModalCouponCode(false, flag, data);
        } else {
            onOpenModalCouponCode(false);
        }
    };

    const onFinish = values => {
        const promotionCode = values.itemCouponCode;
        if (promotionCode && dataPayment) {
            let reqParamFt;
            switch(dataPayment.type) {
                case "APPOINTMENT":
                    reqParamFt = {
                        orderType: dataPayment.type,
                        code: promotionCode,
                        source: dataPayment.source,
                        exam_area: dataPayment.exam_area,
                        exam_type: dataPayment.exam_type,
                        appoint_date: dataPayment.appoint_date,
                        time: dataPayment.time,
                        payment: dataPayment.payment
                    }
                    break;
                case "VIDEO_CALL_SCHEDULE":
                    reqParamFt = {
                        orderType: dataPayment.type,
                        code: promotionCode,
                        block: dataPayment.block,
                        doctor: dataPayment.doctor,
                        date: dataPayment.date,
                        profile: dataPayment.profile_id
                    }
                    break;
                default:
                    return reqParamFt = "";
            }
            setSkeleton(true);
            ModelSearchPromotionOrderType(reqParamFt).then(resultFt => {
                const dataListPromotion = resultFt.data.one_health_msg;
                setListAvailable(dataListPromotion);
                setSkeleton(false);
            }).catch(error => {
                if (error.code !== "007") {
                    const msg = error.message ? error.message : error.description;
                    modal.warning({
                        title: "Thông báo",
                        okText: "Đóng",
                        centered: true,
                        content: i18next.t(msg)
                    });
                    setSkeleton(false);
                } else {
                    _TokenExpired(error);
                }
            })
        }
    };

    const onExchangeVoucher = item => {
        setLoading(true);
        let reqParamSd;
        switch(dataPayment.type) {
            case "APPOINTMENT":
                reqParamSd = {
                    orderType: dataPayment.type,
                    source: dataPayment.source,
                    exam_area: dataPayment.exam_area,
                    exam_type: dataPayment.exam_type,
                    appoint_date: dataPayment.appoint_date,
                    time: dataPayment.time,
                    payment: dataPayment.payment,
                    num_insurrance: dataPayment.num_insurrance
                }
                break;
            case "VIDEO_CALL_SCHEDULE":
                reqParamSd = {
                    orderType: dataPayment.type,
                    block: dataPayment.block,
                    doctor: dataPayment.doctor,
                    doctor_schedule_id: dataPayment.doctor_schedule_id,
                    date: dataPayment.date,
                    profile: dataPayment.profile_id
                }
                break;
            default:
                return reqParamSd = "";
        }
        if (item.promotion) {
            reqParamSd.exchangeId = item.exchangeId;
            ModelCheckPricePromotion(reqParamSd).then(resultFt => {
                let dataPromotion = resultFt.data.one_health_msg;
                if (dataPayment.type === "VIDEO_CALL_SCHEDULE") {
                    dataPromotion = {...dataPromotion, ...{promotion: item}};
                }
                onCloseModalCouponCode("changed", dataPromotion);
                setLoading(false);
            }).catch(error => {
                if (error.code !== "007") {
                    const msg = error.message ? error.message : error.description;
                    modal.warning({
                        title: "Thông báo",
                        okText: "Đóng",
                        centered: true,
                        content: i18next.t(msg)
                    });
                    setLoading(false);
                } else {
                    _TokenExpired(error);
                }
            });
        } else {
            const reqParamFt = {
                code: item.promotionCode
            }
            let dataApplyPromotion;
            ModelApplyPromotion(reqParamFt)
                .then(resultFt => {
                    dataApplyPromotion = resultFt.data.one_health_msg;
                    reqParamSd.exchangeId = dataApplyPromotion.exchangeId;
                    _BadgeNotification();
                    return ModelCheckPricePromotion(reqParamSd);
                })
                .then(resultFt => {
                    let dataPromotion = resultFt.data.one_health_msg;
                    if (dataPayment.type === "VIDEO_CALL_SCHEDULE") {
                        dataPromotion = {...dataPromotion, ...{promotion: dataApplyPromotion}};
                    }
                    onCloseModalCouponCode("changed", dataPromotion);
                    setLoading(false);
                })
                .catch(error => {
                    if (error.code !== "007") {
                        const msg = error.message ? error.message : error.description;
                        modal.warning({
                            title: "Thông báo",
                            okText: "Đóng",
                            centered: true,
                            content: i18next.t(msg)
                        });
                        setLoading(false);
                    } else {
                        _TokenExpired(error);
                    }
                });
        }
    };

    function yieldListAvailable() {
        if (listAvailable.length > 0) {
            return _.map(listAvailable, (item, index) => {
                let img, title, endTime, payment;
                if (item.end) {
                    img = item.image.url;
                    title = item.title;
                    endTime = func.yieldDatetime(item.end, "date");
                    payment = func.findTypePayment(item.payment, "text");
                } else {
                    img = item.promotion.image.url;
                    title = item.promotion.title;
                    endTime = func.yieldDatetime(item.expire, "date");
                    payment = func.findTypePayment(item.promotion.payment, "text");
                }
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-top">
                                <img className="img-fluid" src={img} alt=""/>
                            </div>
                            <div className="wrap-main">
                                <div className="label">{title}</div>
                                <div className="end-time">
                                    <strong>Hiệu lực đến: </strong>
                                    <span>{endTime}</span>
                                </div>
                                <div className="info">{payment ? payment : ""}</div>
                                <div onClick={onExchangeVoucher.bind(this, item)}
                                     className="ant-btn btn-accept"><span>Áp dụng</span></div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description={"Ưu đãi trống"}/>);
            }
        }
    }

    return (
        <Fragment>
            <Modal title="Ưu đãi"
                   className="block-modal-coupon-code ant-modal-custom"
                   closable={true}
                   footer={null}
                   width={600}
                   visible={modalCouponCode}
                   onCancel={onCloseModalCouponCode}>
                <Form onFinish={onFinish}
                      form={form}
                      className="form-find-coupon-code"
                      name="form-find-coupon-code">
                    <Input.Group compact>
                        <Form.Item name="itemCouponCode">
                            <Input placeholder="Nhập mã khuyến mãi"/>
                        </Form.Item>
                        {" "}
                        <Form.Item>
                            <Button className="btn-accept"
                                    htmlType="submit">Tìm</Button>
                        </Form.Item>
                    </Input.Group>
                </Form>
                <div className="wrap-panel-content">
                    <div className="row">
                        <BoxSkeleton skeleton={skeleton}
                                     full={false}
                                     length={10}
                                     rows={3}
                                     data={yieldListAvailable()}/>
                    </div>
                </div>
            </Modal>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default ModalCouponCode;
