import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { FaRegUserCircle, FaRegListAlt } from "react-icons/fa";
import Loading from "../../../Loading";
import FormPayment from "../../FormPayment";
import { getPriceChooseService } from "../../../../services/HealthService";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";
import CardServices from "../../../CardServices";
import { Redirect } from "react-router";
import { url } from "../../../../constants/Path";

const HealthPayment = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [dataPayment, setDataPayment] = useState(undefined);
  //*** Declare props ***//
  const { category_service, user } = props;
  const form =
    category_service && category_service.form ? category_service.form : [];
  const formOne = func.findDataOfStep(form, 1);
  const formTwo = func.findDataOfStep(form, 2);
  const formThree = func.findDataOfStep(form, 3);
  const handleCheckAppointmentRegister = useCallback(async () => {
    setLoading(true);
    const priceChooseService = await getPriceChooseService({
      payment: "prepay",
      app_service_id: formOne._id,
      services_reg: JSON.stringify(formThree.id),
    });
    const dataAppointment = priceChooseService.one_health_msg;
    const tmpDataPayment = {
      type: "HEALTH",
      source: formOne.key,
      payment_only: formOne.payment_only,
      amount: dataAppointment.payment,
      discount: dataAppointment.discount,
      price: dataAppointment.price,
      notice: dataAppointment.message,
      promotion: dataAppointment.promotion ? dataAppointment.promotion : "",
      exchangeId: dataAppointment.promotion
        ? dataAppointment.promotion.exchangeId
        : "",
      user: user.personal._id,
      service: formOne._id,
      profile: formTwo._id,
      services_reg: JSON.stringify(formThree.id),
    };
    setDataPayment(tmpDataPayment);
    setLoading(false);

    return () => {
      isRendered.current = true;
    };
  }, [formOne, formTwo, formThree, user]);

  useEffect(() => {
    if (!isRendered.current) {
      handleCheckAppointmentRegister();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleCheckAppointmentRegister]);
  if (form.length < 3) {
    return <Redirect to={url.name.service_health} />;
  }
  const handleRecalculationAmount = (flag, data) => {
    if (flag === "changed") {
      const tmpDataPayment = {
        type: "HEALTH",
        source: formOne.key,
        payment_only: formOne.payment_only,
        amount: data.payment,
        discount: data.discount,
        price: data.price,
        notice: data.message,
        promotion: data.promotion ? data.promotion : "",
        exchangeId: data.promotion ? data.promotion.exchangeId : "",
        user: user.personal._id,
        service: formOne._id,
        profile: formTwo._id,
        services_reg: JSON.stringify(formThree.id),
      };
      setDataPayment(tmpDataPayment);
    }
  };

  function yieldProfileInfo() {
    if (formTwo) {
      const name = formTwo.name ? formTwo.name : formTwo.names[0];
      const gender = func.findGender(formTwo.sex, "text");
      const age = func.yieldAge(formTwo.birthday);
      const relationship = func.findRelationship(formTwo.relationship, "text");
      const phone = formTwo.phone ? formTwo.phone : formTwo.phones[0];
      const location = func.yieldLocation(formTwo.location);
      let yieldInsurance;
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Họ và tên</p>
            <p className="value">{name}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Giới tính</p>
            <p className="value">{gender}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Tuổi</p>
            <p className="value">{age}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Mối quan hệ</p>
            <p className="value">{relationship}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Số điện thoại</p>
            <p className="value">{phone}</p>
          </div>
          <div className="wrap-line address">
            <p className="label">Địa chỉ</p>
            <p className="value">{location}</p>
          </div>
          {yieldInsurance}
          <div className="wrap-line">
            <p className="label">Mã hồ sơ</p>
            <p className="value">
              <span className="code">{formTwo.his_profile.patient_code}</span>
            </p>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  function yieldExamScheduleInfo() {
    return (
      <Fragment>
        {formThree.name.map((name, index) => (
          <div className="wrap-line" key={index}>
            <p className="label">{name}</p>
          </div>
        ))}
      </Fragment>
    );
  }

  return (
    <Fragment>
      <CardServices
        header="Xác nhận thông tin & Thanh toán"
        description="Quý khách vui lòng kiểm tra lại các thông tin đặt khám"
        step="Thông tin & Thanh toán"
      >
        <div className="wrap-content block-confirm-info-examination">
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item wrap-profile-info">
                <div className="wrap-title">
                  <h4>
                    <FaRegUserCircle /> Thông tin bệnh nhân
                  </h4>
                </div>
                <div className="wrap-main">{yieldProfileInfo()}</div>
                <div className="wrap-title">
                  <h4>
                    <FaRegListAlt /> Danh sách dịch vụ
                  </h4>
                </div>
                <div className="wrap-main">{yieldExamScheduleInfo()}</div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item wrap-payment-info">
                <FormPayment
                  {...props}
                  handleRecalculationAmount={handleRecalculationAmount}
                  dataPayment={dataPayment}
                />
              </div>
            </div>
          </div>
        </div>
      </CardServices>
      <Loading open={loading} />
    </Fragment>
  );
};

export default connect(null, null, null, { forwardRef: true })(HealthPayment);
