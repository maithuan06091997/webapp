import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { Modal, Empty } from "antd";
import { url } from "../../../../constants/Path";
import { nhi_dong } from "../../../../constants/Default";
import { AiOutlineRight } from "react-icons/ai";
import BoxSkeleton from "../../../BoxSkeleton";
import Loading from "../../../Loading";
import { ModelListProfile } from "../../../../models/ModelProfile";
import {
  _Appointment,
  _FindProfile,
  _CategoryService,
} from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as func from "../../../../helpers";
import "../../Examination/StepTwo/style.scss";
import CardServices from "../../../CardServices";
import { withRouter } from "react-router-dom";

const HealthServicesProfile = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [listProfile, setListProfile] = useState([]);
  //*** Declare props ***//
  const { _CategoryService, category_service, history, _FindProfile } = props;

  const [modal, contextHolder] = Modal.useModal();
  const form =
    category_service && category_service.form ? category_service.form : [];
  const formOne = func.findDataOfStep(form, 1);

  const handleNextStep = (params) => {
    const s2 = form.findIndex((key) => {
      return key.step === 2;
    });
    if (s2 === -1) {
      form.push(params);
    } else {
      form[s2] = params;
    }
    _CategoryService(form);
    history.push(url.name.service_health_package);
  };

  const handleListProfile = useCallback(() => {
    setSkeleton(true);
    ModelListProfile()
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataListProfile = resultFt.data.one_health_msg.list;
          if (formOne.key === nhi_dong.key) {
            const filterDataListProfile = dataListProfile.filter(
              (el) => func.calculateAge(el.birthday) < 16
            );
            setListProfile(filterDataListProfile);
          } else {
            setListProfile(dataListProfile);
          }
          setSkeleton(false);
        }
        return null;
      })
      .catch((error) => {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("Examination - StepTwo - ListProfile");
            setSkeleton(false);
          }
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, [formOne]);

  //*** Handle yield list profile ***//
  useEffect(() => {
    if (!isRendered.current) {
      handleListProfile();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListProfile]);

  const onPressNext = (item) => {
    if (func.yieldLocation(item.location) === "...") {
      onPressUpdatePatientCode(item);
    } else {
      const params = {
        step: 2,
        data: item,
      };
      handleNextStep(params);
    }
  };

  const onPressUpdatePatientCode = (item, code) => {
    _FindProfile();
    const pushState = {
      return_url: url.name.account_list_relative_profile,
      detail_profile: item,
    };
    history.push({
      pathname: url.name.account_edit_relative_profile,
      state: pushState,
    });
  };

  function yieldListProfile() {
    if (listProfile.length > 0) {
      const keyHospital = func.findKeyNameHospital("hong_duc");
      return _.map(listProfile, (item, index) => {
        return (
          <div
            key={index}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-top">
                <h4> {item.name ? item.name : item.names[0]}</h4>
              </div>
              <div className="wrap-main">
                <div className="wrap-line">
                  <p className="label">Giới tính</p>
                  <p className="value">{func.findGender(item.sex, "text")}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Tuổi</p>
                  <p className="value">{func.yieldAge(item.birthday)}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Mối quan hệ</p>
                  <p className="value">
                    {func.findRelationship(item.relationship, "text")}
                  </p>
                </div>
                <div className="wrap-line">
                  <p className="label">Số điện thoại</p>
                  <p className="value">
                    {item.phone
                      ? item.phone
                      : item.phones.length > 0
                      ? item.phones[0]
                      : "-"}
                  </p>
                </div>
                <div className="wrap-line drop-line">
                  <p className="label">Địa chỉ</p>
                  <p className="value">{func.yieldLocation(item.location)}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Mã hồ sơ</p>
                  <p className="value">
                    <span className="code">
                      {item[keyHospital]?.patient_code}
                    </span>
                  </p>
                </div>
              </div>
              <div className="wrap-event-action">
                <div className="box-button-event">
                  <div
                    onClick={onPressUpdatePatientCode.bind(
                      this,
                      item,
                      item[keyHospital].patient_code
                    )}
                    className="ant-btn ant-btn-lg"
                  >
                    <span>Cập nhật mã hồ sơ</span>
                  </div>
                  <div
                    onClick={onPressNext.bind(this, item)}
                    className="ant-btn btn-booking ant-btn-lg"
                  >
                    <span>Tiếp tục</span> <AiOutlineRight />
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return <Empty className="ant-empty-custom" description="Hồ sơ trống" />;
      }
    }
  }

  return (
    <Fragment>
      <CardServices
        header="Chọn người đặt khám"
        description="Quý khách vui lòng kiểm tra thông tin cá nhân khi đặt khám </br>
        (Nếu quý khách chưa có thông tin xin vui lòng tạo mới)"
        step="Thông tin cá nhân"
        // createProfile={true}
      >
        <div className="wrap-content block-list-profile-examination">
          <div className="wrap-panel-content">
            <div className="row">
              <BoxSkeleton
                skeleton={skeleton}
                full={false}
                length={8}
                rows={4}
                data={yieldListProfile()}
              />
            </div>
          </div>
        </div>
        <Loading open={loading} />
        {contextHolder}
      </CardServices>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { _Appointment, _FindProfile, _CategoryService },
    dispatch
  );
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    HealthServicesProfile
  )
);
