import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { FaRegUserCircle, FaRegListAlt } from "react-icons/fa";
import Loading from "../../../Loading";
import FormPayment from "../../FormPayment";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";
import Geocode from "react-geocode";
import CardServices from "../../../CardServices";
import { setting } from "../../../../api/Config";
import { getPriceHomeTestService } from "../../../../services/HomeTestService";

const RecordsPaymentContent = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [dataPayment, setDataPayment] = useState(undefined);
  const [latlng, setLatlng] = useState();
  //*** Declare props ***//
  const { home_test, user } = props;
  const form = home_test && home_test.form ? home_test.form : [];
  const formOne = func.findDataOfStep(form, 1);
  const formTwo = func.findDataOfStep(form, 2);
  const formThree = func.findDataOfStep(form, 3);
  const handleCheckAppointmentRegister = useCallback(async () => {
    setLoading(true);
    const location = func.yieldLocation(formThree.location);
    try {
      Geocode.setApiKey(setting.googleKeyApi);
      const response = await Geocode.fromAddress(location);
      const latlng = {
        lat: response.results[0].geometry.location.lat,
        lng: response.results[0].geometry.location.lng,
      };
      const reqParamFt = {
        type: "RECORD",
        service: formOne._id,
        user: user.personal._id,
        payment_type: 2, // 1: cash, 2: wallet - visa - atm
        profile: formThree._id,
        amount: formOne.price,
        takeblood_info: {
          lat: latlng.lat,
          lng: latlng.lng,
          date_appointment: formTwo.date,
          time_appointment: formTwo.time,
          address: location,
        },
      };
      const resultFt = await getPriceHomeTestService({
        one_health_msg: reqParamFt,
      });
      const tmpDataPayment = {
        ...reqParamFt,
        exchangeId: resultFt.one_health_msg.exchange
          ? resultFt.one_health_msg.exchange._id
          : "",
      };
      setLatlng(latlng);
      setDataPayment(tmpDataPayment);
    } catch (error) {
      console.error(error);
    }
    setLoading(false);

    return () => {
      isRendered.current = true;
    };
  }, [formOne, formThree, formTwo, user]);

  useEffect(() => {
    if (!isRendered.current) {
      handleCheckAppointmentRegister();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleCheckAppointmentRegister]);
  const handleRecalculationAmount = (flag, data) => {
    const location = func.yieldLocation(formThree.location);
    if (flag === "changed") {
      const tmpDataPayment = {
        type: "RECORD",
        service: formOne._id,
        user: user.personal._id,
        payment_type: 2, // 1: cash, 2: wallet - visa - atm
        profile: formThree._id,
        amount: formOne.price,
        takeblood_info: {
          lat: latlng.lat,
          lng: latlng.lng,
          date_appointment: formTwo.date,
          time_appointment: formTwo.time,
          address: location,
        },
      };
      setDataPayment(tmpDataPayment);
    }
  };

  function yieldProfileInfo() {
    if (formThree) {
      const name = formThree.name ? formThree.name : formThree.names[0];
      const age = func.yieldAge(formThree.birthday);
      const phone = formThree.phone ? formThree.phone : formThree.phones[0];
      const location = func.yieldLocation(formThree.location);
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Họ và tên</p>
            <p className="value">{name}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Tuổi</p>
            <p className="value">{age}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Số điện thoại</p>
            <p className="value">{phone}</p>
          </div>
          <div className="wrap-line address">
            <p className="label">Địa chỉ</p>
            <p className="value">{location}</p>
          </div>
        </Fragment>
      );
    }
  }
  function yieldExamScheduleInfo() {
    if (formThree && formOne && formTwo) {
      const location = func.yieldLocation(formThree.location);
      return (
        <Fragment>
          <div className="wrap-line">
            <p className="label">Dịch vụ</p>
            <p className="value">{formOne.title}</p>
          </div>
          <div className="wrap-line">
            <p className="label">Ngày khám</p>
            <p className="value">
              {func.getStringDateFromDate(formTwo.date, "DD/MM")}
            </p>
          </div>
          <div className="wrap-line">
            <p className="label">Giờ khám</p>
            <p className="value">{formTwo.time}</p>
          </div>
          <div className="wrap-line address">
            <p className="label">Địa chỉ khám</p>
            <p className="value">{location}</p>
          </div>
        </Fragment>
      );
    }
  }

  return (
    <Fragment>
      <CardServices
        header="Xác nhận thông tin & Thanh toán"
        description="Quý khách vui lòng kiểm tra lại các thông tin đặt khám"
        step="Thông tin & Thanh toán"
      >
        <div className="wrap-content block-confirm-info-examination">
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item wrap-profile-info">
                <div className="wrap-title">
                  <h4>
                    <FaRegUserCircle /> Thông tin cá nhân
                  </h4>
                </div>
                <div className="wrap-main">{yieldProfileInfo()}</div>
                <div className="wrap-title">
                  <h4>
                    <FaRegListAlt /> Thông tin lịch khám
                  </h4>
                </div>
                <div className="wrap-main">{yieldExamScheduleInfo()}</div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item wrap-payment-info">
                <FormPayment
                  {...props}
                  handleRecalculationAmount={handleRecalculationAmount}
                  dataPayment={dataPayment}
                />
              </div>
            </div>
          </div>
        </div>
      </CardServices>
      <Loading open={loading} />
    </Fragment>
  );
};

export default connect(null, null, null, { forwardRef: true })(
  RecordsPaymentContent
);
