import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { url } from "../../../../constants/Path";
import { MdSchedule } from "react-icons/md";
import { AiOutlineRight } from "react-icons/ai";
import { Modal } from "antd";
import Loading from "../../../Loading";
import BoxSkeleton from "../../../BoxSkeleton";
import { _HomeTest } from "../../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as func from "../../../../helpers";
import "./style.scss";
import { getListScheduleTest } from "../../../../services/HomeTestService";
import CardServices from "../../../CardServices";

//*** Handle component date ***//
const DateFrame = (props) => {
  const { labelText_2, labelText_1, value, selected, onClick } = props;
  const onPress = () => {
    onClick(value);
  };
  const isActive =
    func.getStringDateFromDate(selected, "DD/MM") ===
    func.getStringDateFromDate(value, "DD/MM");
  const className = isActive ? "active item" : "item";
  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <p>{labelText_2}</p>
        <p>{labelText_1}</p>
      </div>
    </Fragment>
  );
};

//*** Handle component time ***//
const TimeFrame = (props) => {
  const { label, value, selected, onClick } = props;
  const onPress = () => {
    onClick(value);
  };
  const isActive = selected === value;
  const className = isActive ? "active item" : "item";
  return (
    <Fragment>
      <div className={className} onClick={onPress}>
        <p>{label}</p>
      </div>
    </Fragment>
  );
};

const ScheduleTestContent = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [listSchedule, setListSchedule] = useState([]);
  const [valueDate, setValueDate] = useState(undefined);
  const [valueTime, setValueTime] = useState(undefined);
  //*** Declare props ***//
  const { _HomeTest, home_test, history } = props;
  const [modal, contextHolder] = Modal.useModal();
  const form = home_test && home_test.form ? home_test.form : [];
  //*** Handle data time **//
  const handleListSchedule = useCallback(async (params) => {
    setLoading(true);
    setSkeleton(true);
    let temp = new Date();
    temp.setDate(temp.getDate());
    setValueDate(temp.toISOString());
    try {
      const resultFt = await getListScheduleTest({
        appoint_date: encodeURIComponent(new Date(params).toISOString()),
      });
      const dataListSchedule = resultFt.one_health_msg;
      setListSchedule(dataListSchedule);
      setLoading(false);
      setSkeleton(false);
    } catch (error) {
      if (!isRendered.current) {
        if (error.code !== "007") {
          console.log("CallChat - StepFour - ListSchedule");
          setLoading(false);
          setSkeleton(false);
        }
      }
    }
  }, []);

  useEffect(() => {
    if (!isRendered.current) {
      const tempDate = func.yieldDatetimeNowUTC();
      handleListSchedule(tempDate);
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListSchedule]);

  const onPressNext = () => {
    if (!valueTime) {
      modal.info({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Vui lòng chọn giờ khám",
      });
      return false;
    }
    const s2 = form.findIndex((key) => {
      return key.step === 2;
    });
    const data = {
      date: valueDate,
      time: valueTime,
    };
    const params = {
      step: 2,
      data: data,
    };
    if (s2 === -1) {
      form.push(params);
    } else {
      form[s2] = params;
    }
    _HomeTest(form);
    history.push(url.name.service_records_test);
  };

  const onPressDate = (key) => {
    setValueDate(key);
    setValueTime(undefined);
  };

  const onPressTime = (key, id) => {
    setValueTime(key);
  };

  function yieldListDay() {
    let listDate = [];
    for (let i = 0; i < 10; i++) {
      let temp = new Date();
      temp.setDate(temp.getDate() + i);
      let dayOfWeek = i === 0 ? "Hôm nay" : func.getDayOfWeek(temp);
      let newItem = {
        date: temp,
        dayOfWeek: dayOfWeek,
        dateText: temp.toISOString(),
      };
      listDate.push(newItem);
    }
    if (listDate.length > 0) {
      return _.map(listDate, (item, index) => {
        const convertDate = func.getStringDateFromDate(item.dateText, "DD/MM");
        return (
          <DateFrame
            key={index}
            onClick={() => onPressDate(item.dateText)}
            value={item.dateText}
            selected={valueDate}
            labelText_2={item.dayOfWeek}
            labelText_1={convertDate}
          />
        );
      });
    } else {
      if (!skeleton) {
        return (
          <div className="item-empty">
            Ngày khám đã được đặt hết. Quý khách vui lòng chọn bác sĩ khác. Trân
            trọng cảm ơn.
          </div>
        );
      }
    }
  }

  function yieldListTime() {
    if (listSchedule.length > 0) {
      const mapListScheduleNew = _.map(listSchedule, (item, index) => {
        return (
          <TimeFrame
            key={index}
            onClick={onPressTime}
            value={item}
            selected={valueTime}
            label={item}
          />
        );
      });
      return (
        <div className="wrap-datetime">
          <h4 className="text-muted">Giờ</h4>
          <div className="content">{mapListScheduleNew}</div>
        </div>
      );
    } else {
      return (
        <div className="wrap-datetime">
          <h4 className="text-muted">Giờ</h4>
          <div className="content">
            <div className="item-empty">
              Ngày khám đã được đặt hết. Quý khách vui lòng chọn bác sĩ khác.
              Trân trọng cảm ơn.
            </div>
          </div>
        </div>
      );
    }
  }

  return (
    <CardServices
      header="Đặt lịch xét nghiệm"
      description="Quý khách vui lòng đặt lịch xét nghiệm, bác sĩ sẽ gọi cho quý khách
      đúng giờ đã đặt"
      step="Lịch xét nghiệm"
    >
      <div className="block-schedule-option-call-chat">
        <div className="wrap-panel-content">
          <div className="wrap-panel-item clearfix">
            <div className="wrap-top">
              <h4>
                <MdSchedule /> Chọn Ngày/Giờ gọi video
              </h4>
            </div>
            <div className="wrap-datetime">
              <h4 className="text-muted">Ngày</h4>
              <div className="content">
                <BoxSkeleton
                  skeleton={skeleton}
                  full={true}
                  length={1}
                  rows={1}
                  data={yieldListDay()}
                />
              </div>
            </div>
            {yieldListTime()}
          </div>
          <div className="wrap-panel-item">
            <div className="wrap-event-action">
              <div className="box-button-event">
                <div
                  className="ant-btn ant-btn-lg btn-accept"
                  onClick={onPressNext}
                >
                  <span>Tiếp tục</span> <AiOutlineRight />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
      {contextHolder}
    </CardServices>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _HomeTest }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  ScheduleTestContent
);
