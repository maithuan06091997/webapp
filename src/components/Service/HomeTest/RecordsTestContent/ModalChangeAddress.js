import React, { Fragment, useEffect, useState } from "react";
import { Button, Modal } from "antd";

import "./style.scss";
import { withRouter } from "react-router";
import MapRecords from "./MapRecords";
import LocationSearchInput from "./LocationSearchInput";

const ModalChangeAddress = (props) => {
  const [modalAddress, setModalAddress] = useState(false);
  //*** Declare props ***//
  const { onOpenModalAddress, openModalAddress, profile, setProfile } = props;

  useEffect(() => {
    if (openModalAddress) {
      setModalAddress(openModalAddress);
    }
  }, [openModalAddress]);

  const onCloseModalAddress = (flag) => {
    setModalAddress(false);
    if (flag === "changed") {
      onOpenModalAddress(flag);
    } else {
      onOpenModalAddress();
    }
  };

  function yieldListAddress() {
    if (!profile) return;
    return (
      <div className="w-100">
        <LocationSearchInput />
        <MapRecords profile={profile} height="550px" />
      </div>
    );
  }
  return (
    <Fragment>
      <Modal
        title={"Tìm địa chỉ"}
        className="block-modal-update-patient-code-call-chat ant-modal-custom"
        closable={true}
        width={800}
        footer={null}
        visible={modalAddress}
        onCancel={onCloseModalAddress}
      >
        <div className="records-test">
          <div className="wrap-panel-content">
            <div className="row px-3">{yieldListAddress()}</div>
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};

export default withRouter(ModalChangeAddress);
