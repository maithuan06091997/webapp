import React, { useEffect, useState } from "react";
import { url } from "../../../../constants/Path";
import { withRouter } from "react-router-dom";
import * as func from "../../../../helpers";
import { connect } from "react-redux";
import CardServices from "../../../CardServices";
import "./style.scss";
import { getListProfiles } from "../../../../services/HomeTestService";
import BoxSkeleton from "../../../BoxSkeleton";
import { _HomeTest } from "../../../../actions";
import { Button } from "antd";
import ModalSelectRecords from "./ModalSelectRecords";
import MapRecords from "./MapRecords";
import ModalChangeAddress from "./ModalChangeAddress";
import { bindActionCreators } from "redux";

const RecordsTestContent = ({ user, home_test, history }) => {
  const [listProfiles, setListProfiles] = useState();
  const [profile, setProfile] = useState();
  const [openModalRecords, setOpenModalRecords] = useState(false);
  const [openModalAddress, setOpenModalAddress] = useState(false);
  const form = home_test && home_test.form ? home_test.form : [];

  useEffect(() => {
    const getApi = async () => {
      const listProfiles = await getListProfiles();
      setListProfiles(listProfiles);
      listProfiles.one_health_msg.list.map((profile) => {
        if (user.personal.first_name === profile.first_name) {
          setProfile(profile);
        } else {
          setProfile(
            listProfiles.one_health_msg.list[
              listProfiles.one_health_msg.list.length
            ]
          );
        }
        return true;
      });
    };
    getApi();
  }, [user.personal.first_name]);

  const onOpenModalRecords = (flag) => {
    setOpenModalRecords(false);
  };

  const onOpenModalAddress = (flag) => {
    setOpenModalAddress(false);
  };

  const nextPage = () => {
    const s3 = form.findIndex((key) => {
      return key.step === 3;
    });
    const params = {
      step: 3,
      data: profile,
    };
    if (s3 === -1) {
      form.push(params);
    } else {
      form[s3] = params;
    }
    _HomeTest(form);
    history.push(url.name.service_home_test_payment);
  };

  const renderProfile = () => {
    if (!profile) return;
    return (
      <>
        <div className="col-xl-12 wrap-panel-col">
          <div className="wrap-panel-item">
            <div className="wrap-top d-flex justify-content-between">
              <h4> {profile.name}</h4>
              <div className="d-flex align-items-center">
                <Button
                  className="btn-accept custom-btn mr-2"
                  onClick={() => setOpenModalAddress(true)}
                >
                  <span>Thay đổi địa chỉ</span>
                </Button>
                <Button
                  className="btn-accept custom-btn"
                  onClick={() => setOpenModalRecords(true)}
                >
                  <span>Thay đổi hồ sơ</span>
                </Button>
              </div>
            </div>
            <div className="wrap-main">
              <div className="wrap-line">
                <p className="label">Giới tính</p>
                <p className="value">{func.findGender(profile.sex, "text")}</p>
              </div>
              <div className="wrap-line">
                <p className="label">Tuổi</p>
                <p className="value">{func.yieldAge(profile.birthday)}</p>
              </div>
              <div className="wrap-line">
                <p className="label">Mối quan hệ</p>
                <p className="value">
                  {func.findRelationship(profile.relationship, "text")}
                </p>
              </div>
              <div className="wrap-line">
                <p className="label">Số điện thoại</p>
                <p className="value">
                  {profile.phone
                    ? profile.phone
                    : profile.phones.length > 0
                    ? profile.phones[0]
                    : "-"}
                </p>
              </div>
              <div className="wrap-line drop-line">
                <p className="label">Địa chỉ</p>
                <p className="value">{func.yieldLocation(profile.location)}</p>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-12 wrap-panel-col">
          <MapRecords profile={profile} />
        </div>
        <div className="col-xl-12 wrap-panel-col">
          <Button
            className="btn-accept custom-btn w-100 "
            onClick={() => nextPage()}
          >
            <span>Tiếp tục</span>
          </Button>
        </div>
      </>
    );
  };
  return (
    <CardServices
      header="Chọn người dùng đặt xét nghiệm"
      description="Quý khách vui lòng chọn người dùng đặt xét nghiệm"
      step="đặt xét nghiệm"
    >
      <div className="records-test">
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={!listProfiles}
              full={true}
              length={2}
              rows={1}
              data={renderProfile()}
            />
          </div>
        </div>
      </div>
      <ModalSelectRecords
        listProfiles={listProfiles}
        setProfile={setProfile}
        onOpenModalRecords={onOpenModalRecords}
        openModalRecords={openModalRecords}
      />
      <ModalChangeAddress
        profile={profile}
        setProfile={setProfile}
        onOpenModalAddress={onOpenModalAddress}
        openModalAddress={openModalAddress}
      />
    </CardServices>
  );
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _HomeTest }, dispatch);
};
export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    RecordsTestContent
  )
);
