import React, { Fragment, useEffect, useState } from "react";
import { Button, Modal } from "antd";
import * as func from "../../../../helpers";

import "./style.scss";
import { url } from "../../../../constants/Path";
import { withRouter } from "react-router";

const ModalSelectRecords = (props) => {
  const [modalRecords, setModalRecords] = useState(false);
  //*** Declare props ***//
  const {
    onOpenModalRecords,
    openModalRecords,
    listProfiles,
    setProfile,
    history,
  } = props;

  useEffect(() => {
    if (openModalRecords) {
      setModalRecords(openModalRecords);
    }
  }, [openModalRecords]);

  const onCloseModalRecords = (flag) => {
    setModalRecords(false);
    if (flag === "changed") {
      onOpenModalRecords(flag);
    } else {
      onOpenModalRecords();
    }
  };

  const selectProfile = (profile) => {
    setProfile(profile);
    setModalRecords(false);
    onOpenModalRecords();
  };

  const redirectAddProfile = () => {
    history.push(url.name.account_create_relative_profile);
  };

  function yieldListRecords() {
    if (!listProfiles) return;
    return listProfiles.one_health_msg.list.map((profile, index) => (
      <div className="col-xl-6 wrap-panel-col mt-0 mb-3" key={index}>
        <div className="wrap-panel-item">
          <div className="wrap-top d-flex justify-content-between">
            <h4> {profile.name}</h4>
            <Button
              className="btn-accept custom-btn"
              onClick={() => selectProfile(profile)}
            >
              <span>Chọn</span>
            </Button>
          </div>
          <div className="wrap-main">
            <div className="wrap-line">
              <p className="label">Giới tính</p>
              <p className="value">{func.findGender(profile.sex, "text")}</p>
            </div>
            <div className="wrap-line">
              <p className="label">Tuổi</p>
              <p className="value">{func.yieldAge(profile.birthday)}</p>
            </div>
            <div className="wrap-line">
              <p className="label">Mối quan hệ</p>
              <p className="value">
                {func.findRelationship(profile.relationship, "text")}
              </p>
            </div>
            <div className="wrap-line">
              <p className="label">Số điện thoại</p>
              <p className="value">
                {profile.phone
                  ? profile.phone
                  : profile.phones.length > 0
                  ? profile.phones[0]
                  : "-"}
              </p>
            </div>
            <div className="wrap-line drop-line">
              <p className="label">Địa chỉ</p>
              <p className="value">{func.yieldLocation(profile.location)}</p>
            </div>
          </div>
        </div>
      </div>
    ));
  }
  return (
    <Fragment>
      <Modal
        title={"Danh sách hồ sơ"}
        className="ant-modal-custom"
        closable={true}
        width={800}
        footer={null}
        visible={modalRecords}
        onCancel={onCloseModalRecords}
      >
        <div className="records-test">
          <div className="wrap-panel-content">
            <div className="row px-3">{yieldListRecords()}</div>
          </div>
        </div>
        <Button className="btn-accept custom-btn" onClick={redirectAddProfile}>
          <span>Thêm người đặt khám</span>
        </Button>
      </Modal>
    </Fragment>
  );
};

export default withRouter(ModalSelectRecords);
