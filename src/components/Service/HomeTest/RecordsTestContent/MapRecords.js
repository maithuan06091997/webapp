import React, { useEffect, useState } from "react";
import GoogleMapReact from "google-map-react";
import { setting } from "../../../../api/Config";
import Geocode from "react-geocode";
import { yieldLocation } from "../../../../helpers";
import { public_url } from "../../../../constants/Path";

const MapRecords = ({ profile, height }) => {
  const [center, setCenter] = useState();
  const AnyReactComponent = ({ name, address }) => (
    <div className="address">
      <div>
        <b>{name}</b>
        <p>{address}</p>
      </div>
      <img src={public_url.img + "/icons/pin_orange.png"} alt="pin" />
    </div>
  );

  useEffect(() => {
    const geocode = async () => {
      setCenter();
      const location = yieldLocation(profile.location);
      try {
        Geocode.setApiKey(setting.googleKeyApi);
        const response = await Geocode.fromAddress(location);
        const latlng = {
          lat: response.results[0].geometry.location.lat,
          lng: response.results[0].geometry.location.lng,
        };
        setCenter(latlng);
      } catch (error) {
        console.error(error);
      }
    };
    geocode();
  }, [profile.location]);

  if (!center)
    return (
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyBGs0YlUGtkvh3lEaUhhWSZNhDA7pzk8JE" }}
        defaultCenter={{ lat: 11.812235, lng: 108.7660364 }}
        defaultZoom={11}
      ></GoogleMapReact>
    );
  return (
    <div style={{ height: height ?? "350px", width: "100%" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyBGs0YlUGtkvh3lEaUhhWSZNhDA7pzk8JE" }}
        defaultCenter={center}
        defaultZoom={11}
      >
        <AnyReactComponent
          lat={center.lat}
          lng={center.lng}
          name={profile.name}
          address={yieldLocation(profile.location)}
        />
      </GoogleMapReact>
    </div>
  );
};

export default MapRecords;
