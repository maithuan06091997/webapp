import React, { Fragment, useEffect, useState } from "react";
import { Button, Input, Modal } from "antd";
import BoxSkeleton from "../../BoxSkeleton";
import Loading from "../../Loading";
import { confirmOtp } from "../../../services/CarePayService";
import { url } from "../../../constants/Path";
import { _Confirmation } from "../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

const SendOtp = (props) => {
  const [loading, setLoading] = useState(false);
  const [modalOtp, setModalOtp] = useState(false);
  const [valueOtp, setValueOtp] = useState();
  const [error, setError] = useState();
  //*** Declare props ***//
  const {
    onOpenModalOtp,
    openModalOtp,
    payOrder,
    history,
    dataPayment,
    _Confirmation,
  } = props;
  useEffect(() => {
    async function getApi() {
      if (openModalOtp) {
        setModalOtp(openModalOtp);
        setError();
      }
    }
    getApi();
  }, [openModalOtp]);

  const onCloseModalOtp = () => {
    onOpenModalOtp(false);
    setModalOtp(false);
  };
  const handChangeOtp = (e) => {
    setValueOtp(e.target.value);
  };
  const sendOtp = async () => {
    setError();
    if (!valueOtp) {
      setError("Vui lòng nhập mã OTP gồm 8 chữ số");
      return false;
    }
    try {
      setLoading(true);
      const postData = {
        one_health_msg: {
          otp: valueOtp,
          requestId: payOrder.requestId,
          mbBankId: payOrder.mbBankId,
          transactionId: payOrder.transactionId,
        },
      };
      const confirm = await confirmOtp(postData);
      if (confirm.error) {
        setError(confirm.error);
      } else {
        _Confirmation({
          status: true,
          type: "service",
          name: dataPayment.type,
          gateway: "Wallet",
        });
        history.push({
          pathname: url.name.service_confirmation,
          state: confirm.one_health_msg,
        });
      }
      setLoading(false);
    } catch (error) {}
  };
  function yieldOtp() {
    return (
      <div>
        <p className="mb-1">
          Một mã OTP được gửi về trong tin nhắn của Qúy khách. Vui lòng nhập mã
          OTP để xác nhận.
        </p>
        <Input
          type="text"
          className={`text-center ${error && "border-danger"}`}
          value={valueOtp}
          onChange={handChangeOtp}
        />
        <p className="text-center text-danger mb-0 mt-1">{error}</p>
        <div className="mt-0 mb-2 text-center">Gửi lại mã OTP</div>
        <Button className="btn-accept w-100" onClick={sendOtp}>
          Xác nhận
        </Button>
      </div>
    );
  }

  return (
    <Fragment>
      <Modal
        title="Xác nhận giao dịch"
        className=""
        closable={true}
        centered
        width={450}
        footer={null}
        visible={modalOtp}
        onCancel={onCloseModalOtp}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={loading}
              full={true}
              length={1}
              rows={3}
              data={yieldOtp()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _Confirmation }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  SendOtp
);
