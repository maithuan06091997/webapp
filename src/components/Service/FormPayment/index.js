import React, { Fragment, useEffect, useRef, useState } from "react";
import _ from "lodash";
import i18next from "i18next";
import { Radio, Input, Form, Button, Modal } from "antd";
import { MdPayment } from "react-icons/md";
import { RiCoupon3Line } from "react-icons/ri";
import { url, public_url } from "../../../constants/Path";
import Loading from "../../Loading";
import ModalCouponCode from "../ModalCouponCode";
import {
  ModelPaymentWalletExamination,
  ModelPaymentDebitExamination,
} from "../../../models/ModelExamination";
import {
  ModelPaymentWalletVideoCall,
  ModelPaymentDebitVideoCall,
  ModelPaymentWalletVideoCall1,
  ModelPaymentWalletRecord,
} from "../../../models/ModelCallChat";
import { _Confirmation } from "../../../actions";
import { ModelGetListCardUsed } from "../../../models/ModelWallet";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";
import { getMbBank, postMbOrder } from "../../../services/CarePayService";
import SendOtp from "./SendOtp";

const FormPayment = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [openModalCouponCode, setOpenModalCouponCode] = useState(false);
  const [fields, setFields] = useState([]);
  const [method, setMethod] = useState("carePay");
  const [amount, setAmount] = useState(0);
  const [notice, setNotice] = useState(undefined);
  const [couponCode, setCouponCode] = useState(undefined);
  const [listCardATM, setListCardATM] = useState([]);
  const [listCardVisa, setListCardVisa] = useState([]);
  const [carePay, setCarePay] = useState();
  const [payOrder, setPayOrder] = useState();
  const [openModalOtp, setOpenModalOtp] = useState(false);
  //*** Declare props ***//
  const {
    _TokenExpired,
    _Confirmation,
    history,
    balance,
    handleRecalculationAmount,
    dataPayment,
  } = props;
  const [modal, contextHolder] = Modal.useModal();

  const handlePayAppointment = async (values, payMethod) => {
    setLoading(true);
    const reqParamFt = {
      source: dataPayment.source,
      reason: dataPayment.reason,
      profile: dataPayment.profile_id,
      appoint_date: dataPayment.appoint_date,
      time: dataPayment.time,
      payment_type: 2,
      payment: dataPayment.payment,
      exam_type: dataPayment.exam_type,
      exam_area: dataPayment.exam_area,
      num_insurrance: dataPayment.num_insurrance,
      doctor: dataPayment.doctor ? dataPayment.doctor : "",
      exchangeId: dataPayment.exchangeId ? dataPayment.exchangeId : "",
    };
    if (payMethod === "carePay") {
      paymentCarePay(reqParamFt);
    } else if (payMethod === "wallet") {
      ModelPaymentWalletExamination(reqParamFt)
        .then((resultFt) => {
          const dataPaymentService = resultFt.data.one_health_msg;
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "Wallet",
          });
          history.push({
            pathname: url.name.service_confirmation,
            state: dataPaymentService,
          });
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    } else {
      const amount = dataPayment.amount;
      const reqParamSd = {
        amount: amount,
        return_url:
          window.location.protocol +
          "//" +
          window.location.host +
          url.name.service_confirmation,
        orderType: dataPayment.type,
        orderMeta: reqParamFt,
      };
      let cardToken = "";
      if (values.itemCardATMInfo && values.itemCardATMInfo !== "new") {
        cardToken = values.itemCardATMInfo.verifyToken;
      }
      if (values.itemCardVisaInfo && values.itemCardVisaInfo !== "new") {
        cardToken = values.itemCardVisaInfo.verifyToken;
      }
      ModelPaymentDebitExamination(reqParamSd, payMethod, cardToken)
        .then((resultFt) => {
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "OnePay",
          });
          window.location.href = resultFt.data.one_health_msg;
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    }
  };

  const handlePayCallChat = async (values, payMethod) => {
    setLoading(true);
    const reqParamFt = {
      block: dataPayment.block,
      doctor: dataPayment.doctor,
      date: dataPayment.date,
      profile: dataPayment.profile_id,
      exchange: dataPayment.exchangeId,
    };
    if (payMethod === "carePay") {
      paymentCarePay(reqParamFt);
    } else if (payMethod === "wallet") {
      ModelPaymentWalletVideoCall(reqParamFt)
        .then((resultFt) => {
          const dataPaymentService = resultFt.data.one_health_msg;
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "Wallet",
          });
          history.push({
            pathname: url.name.service_confirmation,
            state: dataPaymentService,
          });
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    } else {
      const amount = dataPayment.amount;
      const reqParamSd = {
        amount: amount,
        return_url:
          window.location.protocol +
          "//" +
          window.location.host +
          url.name.service_confirmation,
        orderType: dataPayment.type,
        orderMeta: reqParamFt,
      };
      let cardToken = "";
      if (values.itemCardATMInfo && values.itemCardATMInfo !== "new") {
        cardToken = values.itemCardATMInfo.verifyToken;
      }
      if (values.itemCardVisaInfo && values.itemCardVisaInfo !== "new") {
        cardToken = values.itemCardVisaInfo.verifyToken;
      }
      ModelPaymentDebitVideoCall(reqParamSd, payMethod, cardToken)
        .then((resultFt) => {
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "OnePay",
          });
          window.location.href = resultFt.data.one_health_msg;
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    }
  };

  const handlePayHealth = (values, payMethod) => {
    setLoading(true);
    const reqParamFt = {
      user: dataPayment.user,
      service: dataPayment.service,
      payment_type: 2,
      profile: dataPayment.profile,
      services_reg: dataPayment.services_reg,
      exchangeId: dataPayment.exchangeId,
      source: "miniapp",
    };
    if (payMethod === "carePay") {
      paymentCarePay(reqParamFt, "ORDER");
    } else if (payMethod === "wallet" || payMethod === "cash") {
      if (payMethod === "cash") reqParamFt.payment_type = 1;
      ModelPaymentWalletVideoCall1(reqParamFt)
        .then((resultFt) => {
          const dataPaymentService = resultFt.data.one_health_msg;
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "Wallet",
          });
          history.push({
            pathname: url.name.service_confirmation,
            state: dataPaymentService,
          });
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    } else {
      const amount = dataPayment.amount;
      const reqParamSd = {
        amount: amount,
        return_url:
          window.location.protocol +
          "//" +
          window.location.host +
          url.name.service_confirmation,
        orderType: "ORDER",
        orderMeta: reqParamFt,
      };
      let cardToken = "";
      if (values.itemCardATMInfo && values.itemCardATMInfo !== "new") {
        cardToken = values.itemCardATMInfo.verifyToken;
      }
      if (values.itemCardVisaInfo && values.itemCardVisaInfo !== "new") {
        cardToken = values.itemCardVisaInfo.verifyToken;
      }
      ModelPaymentDebitVideoCall(reqParamSd, payMethod, cardToken)
        .then((resultFt) => {
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "OnePay",
          });
          window.location.href = resultFt.data.one_health_msg;
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    }
  };

  const handlePayRecord = (values, payMethod) => {
    setLoading(true);
    const reqParamFt = {
      service: dataPayment.service,
      user: dataPayment.user,
      payment_type: 2, // 1: cash, 2: wallet - visa - atm
      profile: dataPayment.profile,
      exchangeId: dataPayment.exchangeId,
      takeblood_info: {
        lat: dataPayment.takeblood_info.lat,
        lng: dataPayment.takeblood_info.lng,
        date_appointment: dataPayment.takeblood_info.date_appointment,
        time_appointment: dataPayment.takeblood_info.time_appointment,
        address: dataPayment.takeblood_info.address,
      },
    };
    if (payMethod === "carePay") {
      paymentCarePay(reqParamFt);
    } else if (payMethod === "wallet" || payMethod === "cash") {
      if (payMethod === "cash") reqParamFt.payment_type = 1;
      ModelPaymentWalletRecord(reqParamFt)
        .then((resultFt) => {
          const dataPaymentService = resultFt.data.one_health_msg;
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "Wallet",
          });
          history.push({
            pathname: url.name.service_confirmation,
            state: dataPaymentService,
          });
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    } else {
      const amount = dataPayment.amount;
      const reqParamSd = {
        amount: amount,
        return_url:
          window.location.protocol +
          "//" +
          window.location.host +
          url.name.service_confirmation,
        orderType: "TAKE_BLOOD",
        orderMeta: reqParamFt,
      };
      let cardToken = "";
      if (values.itemCardATMInfo && values.itemCardATMInfo !== "new") {
        cardToken = values.itemCardATMInfo.verifyToken;
      }
      if (values.itemCardVisaInfo && values.itemCardVisaInfo !== "new") {
        cardToken = values.itemCardVisaInfo.verifyToken;
      }
      ModelPaymentDebitVideoCall(reqParamSd, payMethod, cardToken)
        .then((resultFt) => {
          _Confirmation({
            status: true,
            type: "service",
            name: dataPayment.type,
            gateway: "OnePay",
          });
          window.location.href = resultFt.data.one_health_msg;
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    }
  };

  useEffect(() => {
    const reqParams = null;
    setLoading(true);
    const getCarePay = async () => {
      const carePay = await getMbBank();
      setCarePay(carePay.one_health_msg);
      setLoading(false);
    };
    getCarePay();
    setLoading(false);

    ModelGetListCardUsed(reqParams)
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataListCard = resultFt.data.one_health_msg.data;
          let cardATM = [];
          let cardVisa = [];
          _.map(dataListCard, (item) => {
            if (item.cardType === "atm_card") {
              cardATM.push(item);
            } else {
              cardVisa.push(item);
            }
          });
          setListCardATM(cardATM);
          setListCardVisa(cardVisa);
          setLoading(false);
        }
        return null;
      })
      .catch((error) => {
        if (error.code !== "007") {
          console.log("WalletRechargePage - GetListCardUsed");
          setLoading(false);
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, []);

  useEffect(() => {
    if (dataPayment) {
      const _amount = dataPayment.amount ? dataPayment.amount : 0;
      const _notice = dataPayment.notice ? dataPayment.notice : "";
      let promotionCode;
      switch (dataPayment.type) {
        case "APPOINTMENT":
          promotionCode = dataPayment.promotion
            ? dataPayment.promotion.promotion.promotionCode
            : "";
          break;
        case "VIDEO_CALL_SCHEDULE":
          promotionCode = dataPayment.promotion
            ? dataPayment.promotion.promotionCode
            : "";
          break;
        case "HEALTH":
          promotionCode = dataPayment.promotion
            ? dataPayment.promotion.promotionCode
            : "";
          break;
        case "RECORD":
          promotionCode = dataPayment.promotion
            ? dataPayment.promotion.promotionCode
            : "";
          break;
        default:
          promotionCode = "";
      }
      setCouponCode(promotionCode);
      setFields([{ name: ["itemPromotion"], value: promotionCode }]);
      setAmount(_amount);
      setNotice(_notice);
    }
  }, [dataPayment]);

  const paymentCarePay = async (reqParamFt, type = dataPayment.type) => {
    const amount = dataPayment.amount;
    const reqParamSd = {
      amount: amount,
      orderType: type,
      orderMeta: reqParamFt,
    };
    const payOder = await postMbOrder({ one_health_msg: reqParamSd });
    if (payOder.error) {
      modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: i18next.t(payOder.error),
      });
    } else {
      setPayOrder(payOder.one_health_msg);
      setOpenModalOtp(true);
    }
    setLoading(false);
  };

  const onApplyNow = (values) => {
    const payMethod = values.itemMethod;
    switch (dataPayment.type) {
      case "APPOINTMENT":
        handlePayAppointment(values, payMethod);
        break;
      case "VIDEO_CALL_SCHEDULE":
        handlePayCallChat(values, payMethod);
        break;
      case "HEALTH":
        handlePayHealth(values, payMethod);
        break;
      case "RECORD":
        handlePayRecord(values, payMethod);
        break;
      default:
        return null;
    }
  };

  const onFinish = (values) => {
    if (method === "wallet" && balance.vnd < amount) {
      modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Số dư DR.OH không đủ. Quý khách vui lòng nạp thêm.",
      });
      return false;
    }
    let textConfirm = "";
    switch (dataPayment.type) {
      case "APPOINTMENT":
        textConfirm = "Đặt khám";
        break;
      case "VIDEO_CALL_SCHEDULE":
        textConfirm = "Đặt cuộc gọi";
        break;
      case "HEALTH":
        textConfirm = "Đặt dịch vụ";
        break;
      case "RECORD":
        textConfirm = "Đặt khám";
        break;
      default:
        return null;
    }
    if (method === "carePay") {
      onApplyNow(values);
      return false;
    }
    modal.confirm({
      title: "Xác nhận",
      okText: textConfirm,
      cancelText: "Đóng",
      centered: true,
      onOk: onApplyNow.bind(this, values),
      content: "Bạn có chắc chắn muốn đặt lịch?",
    });
  };

  const onOpenModalOtp = (flag) => {
    setOpenModalOtp(flag);
  };

  const onOpenModalCouponCode = (open, flag, data) => {
    setOpenModalCouponCode(open);
    if (flag === "changed") {
      handleRecalculationAmount(flag, data);
    }
  };

  const onChangeMethod = (e) => {
    const value = e.target.value;
    const findVisa = document.querySelector(".method-visa-panel");
    const findATM = document.querySelector(".method-atm-panel");
    if (value === "visa") {
      findVisa.classList.add("active");
      findATM.classList.remove("active");
    } else if (value === "atm") {
      findATM.classList.add("active");
      findVisa.classList.remove("active");
    } else {
      findVisa.classList.remove("active");
      findATM.classList.remove("active");
    }
    setMethod(value);
  };

  function yieldMethodVisaPanel() {
    if (method === "visa") {
      const list = _.map(listCardVisa, (item, index) => {
        const bankName = func.yieldCardUsed(item).bankName;
        const logo = func.yieldCardUsed(item).logo;
        const cardNumber = item.cardNumber;
        return (
          <Radio key={index} value={item}>
            <div className="info">
              <img alt="" src={public_url.img + logo} />
              <div className="text">
                <p>{bankName}</p>
                <p>{cardNumber}</p>
              </div>
            </div>
          </Radio>
        );
      });
      return (
        <Form.Item name="itemCardVisaInfo" initialValue={"new"}>
          <Radio.Group className="card-info">
            <Radio value={"new"}>
              <div className="label">
                <div>
                  <p>Sử dụng thẻ mới</p>
                </div>
              </div>
            </Radio>
            {list}
          </Radio.Group>
        </Form.Item>
      );
    }
  }

  function yieldMethodATMPanel() {
    if (method === "atm") {
      const detail = _.map(listCardATM, (item, index) => {
        const bankName = item.bankName;
        const cardNumber = item.cardNumber;
        return (
          <Radio key={index} value={item}>
            <div className="label">
              <div>
                <p>{bankName}</p>
                <p>{cardNumber}</p>
              </div>
            </div>
          </Radio>
        );
      });
      return (
        <Form.Item name="itemCardATMInfo" initialValue={"new"}>
          <Radio.Group className="card-info">
            <Radio value={"new"}>
              <div className="label">
                <div>
                  <p>Sử dụng thẻ mới</p>
                </div>
              </div>
            </Radio>
            {detail}
          </Radio.Group>
        </Form.Item>
      );
    }
  }

  return (
    <Fragment>
      <Form
        onFinish={onFinish}
        fields={fields}
        size="large"
        className="form-payment-service"
        name="form-payment-service"
      >
        <h4>
          <MdPayment /> Phương thức thanh toán
        </h4>
        <div className="wrap-method">
          <Form.Item name="itemMethod" initialValue={method}>
            <Radio.Group className="wrap-method-item" onChange={onChangeMethod}>
              {dataPayment?.payment_only !== "postpaid" && (
                <>
                  <Radio value={"carePay"}>
                    <div className="method-wallet">
                      <div className="icon">
                        <img
                          src={public_url.img + "/icons/icon_method_card.png"}
                          alt=""
                        />
                      </div>
                      <div className="label">
                        <p>Thanh toán bằng CarePay</p>
                        <p className="amount">
                          {!carePay && "Loading..."}
                          {carePay && carePay.walletInfo && (
                            <>
                              {(+carePay.walletInfo
                                .balanceWallet).toLocaleString()}{" "}
                              <sup>đ</sup>
                            </>
                          )}
                          {carePay && !carePay.walletInfo && (
                            <>
                              0 <sup> đ</sup>
                            </>
                          )}
                        </p>
                      </div>
                    </div>
                  </Radio>
                  <Radio value={"wallet"}>
                    <div className="method-wallet">
                      <div className="icon">
                        <img
                          src={public_url.img + "/icons/icon_method_wallet.png"}
                          alt=""
                        />
                      </div>
                      <div className="label">
                        <p>Thanh toán bằng tài khoản OH</p>
                        <p className="amount">
                          {balance && balance.vnd
                            ? balance.vnd.toLocaleString()
                            : 0}{" "}
                          <sup>đ</sup>
                        </p>
                      </div>
                    </div>
                  </Radio>
                </>
              )}
              {dataPayment?.type !== "APPOINTMENT" &&
                dataPayment?.type !== "VIDEO_CALL_SCHEDULE" &&
                dataPayment?.payment_only !== "prepay" && (
                  <Radio value={"cash"}>
                    <div className="method-wallet">
                      <div className="icon">
                        <img
                          src={public_url.img + "/icons/icon_method_dollar.png"}
                          alt=""
                        />
                      </div>
                      <div className="label">
                        <p>Thanh toán tại bệnh viện</p>
                        <p className="text-muted">Thanh toán sau</p>
                      </div>
                    </div>
                  </Radio>
                )}
              {dataPayment?.payment_only !== "postpaid" && (
                <>
                  <Radio value={"visa"}>
                    <div className="method-visa">
                      <div className="icon">
                        <img
                          src={public_url.img + "/icons/icon_method_visa.png"}
                          alt=""
                        />
                      </div>
                      <div className="label">
                        <p>Thanh toán bằng thẻ quốc tế</p>
                        <p className="text-muted">Visa - Master</p>
                      </div>
                    </div>
                  </Radio>{" "}
                  <div className="method-visa-panel">
                    {yieldMethodVisaPanel()}
                  </div>{" "}
                  <Radio value={"atm"}>
                    <div className="method-atm">
                      <div className="icon">
                        <img
                          src={public_url.img + "/icons/icon_method_atm.png"}
                          alt=""
                        />
                      </div>
                      <div className="label">
                        <p>Thanh toán bằng thẻ ATM</p>
                        <p className="text-muted">Vpbank, Vietcombank ...</p>
                      </div>
                    </div>
                  </Radio>{" "}
                </>
              )}
              <div className="method-atm-panel">{yieldMethodATMPanel()}</div>
            </Radio.Group>
          </Form.Item>
        </div>
        <h4>
          <RiCoupon3Line /> Mã giảm giá
        </h4>
        <div className="wrap-promotion">
          <Form.Item name="itemPromotion">
            <Input
              onClick={onOpenModalCouponCode.bind(this, true, "", "")}
              placeholder="Mã giảm giá"
              readOnly={true}
            />
          </Form.Item>
          <div className="text-right color-blue">
            {couponCode ? "Áp dụng mã khuyến mãi thành công" : ""}
          </div>
        </div>
        <div className="wrap-price">
          <div className="price">
            <p className="label">Giá khám</p>
            <p className="value">{amount ? amount.toLocaleString() : 0} đ</p>
          </div>
          <p className="text-muted">{notice ? "(" + notice + ")" : ""}</p>
          <Form.Item className="mb-0">
            <Button
              loading={loading}
              className="btn-accept w-100"
              htmlType="submit"
            >
              <span>Thanh toán</span>
            </Button>
          </Form.Item>
        </div>
      </Form>
      <ModalCouponCode
        {...props}
        onOpenModalCouponCode={onOpenModalCouponCode}
        openModalCouponCode={openModalCouponCode}
      />
      {carePay && (
        <SendOtp
          {...props}
          onOpenModalOtp={onOpenModalOtp}
          openModalOtp={openModalOtp}
          payOrder={payOrder}
          dataPayment={dataPayment}
        />
      )}
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _Confirmation }, dispatch);
};

export default connect(null, mapDispatchToProps, null, { forwardRef: true })(
  FormPayment
);
