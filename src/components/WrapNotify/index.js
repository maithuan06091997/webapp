import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import {url} from "../../constants/Path";
import {Dropdown, Menu, Badge, Modal} from "antd";
import {FaBell} from "react-icons/fa";
import BoxSkeleton from "../BoxSkeleton";
import ModalPointHistory from "../Promotion/ModalPointHistory";
import {ModelGetListNotification, ModelPutClicked} from "../../models/ModelNotification";
import {Link} from "react-router-dom";
import * as func from "../../helpers";

const WrapNotify = (props) => {
    const isRendered        = useRef(false);
    const [skeleton, setSkeleton] = useState(false);
    const [openModalPointHistory, setOpenModalPointHistory] = useState(false);
    const [listNotify, setListNotify] = useState([]);
    //*** Declare props ***//
    const {_TokenExpired, _BadgeNotification, history, notify} = props;

    const [modal, contextHolder] = Modal.useModal();

    const handleListNotification = useCallback(() => {
        const reqParamFt = {
            page: 1
        };
        setSkeleton(true);
        ModelGetListNotification(reqParamFt).then(resultFt => {
            if (!isRendered.current) {
                const dataListNotify = resultFt.data.one_health_msg;
                setSkeleton(false);
                setListNotify(dataListNotify);
            }
            return null;
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                console.log("WrapNotify - ListNotification");
                setSkeleton(false);
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, [_TokenExpired]);

    useEffect(() => {
        if (listNotify.length > 0) {
            _BadgeNotification();
        }
    }, [_BadgeNotification, listNotify]);

    const onPressBell = flag => {
        if (flag) {
            handleListNotification();
        }
    };

    const onPutClicked = item => {
        const reqParamFt = {
            notification: item._id
        };
        ModelPutClicked(reqParamFt).then(() => {
            gotoDetailNotify(item);
            handleListNotification();
        }).catch(error => {
            if (error.code !== "007") {
                const msg = error.message ? error.message : error.description;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
            } else {
                _TokenExpired(error);
            }
        });
    };

    const onOpenModalPointHistory = flag => {
        setOpenModalPointHistory(flag);
    };

    const gotoDetailNotify = item => {
        console.log(item);
        switch (item.type + '') {
            case "1":
                if (item.extra && item.extra !== "") {
                    let extra = item.extra;
                    if (extra && extra.url) {
                        // OHNotiWebview
                        window.open(extra.url, "_blank");
                    } else if (extra && extra.data) {
                        // OHNotiDetails
                        // notiSelected = extra.data;
                        // setModalNotify(true);
                    }
                }
                break;
            case "2":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.url) {
                        // OHAccountPayHisDetails , Chi tiết cuộc gọi
                    }
                }
                break;
            case "5":   // case này duplicate vs case 13
            case "13":  // case này duplicate vs case 5
            case "37":  // Nhà thuốc Hủy giao thuốc
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.prescription && extra.prescription.id) {
                        //title: "Chi tiết toa thuốc",
                        //name: 'OHPrescriptionDetails',
                    }
                }
                break;
            case "6":
            case "17":
            case "21":
            case "22":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.payment_his && extra.payment_his.id) {
                        //title: "Chi tiết giao dịch",
                        //name: 'OHAccountPayHisDetails',
                    }
                }
                break;
            case "8":
            case "14":
            case "16": // KQ CLS
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.paraclinical && extra.paraclinical.id) {
                        //title: "Chỉ định cận lâm sàng",
                        //name: 'OHSubclinicalDetails',
                    }
                }
                break;
            case "11":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.question) {
                        //title: "Câu hỏi",
                        //name: 'OHNewFeedDetails',
                    }
                }
                break;
            case "20": //Notification of SubClinicalResult
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.doctor && extra.doctor.id) {
                        //title: doctorName,
                        //name: 'OHDoctorDetail'
                    }
                }
                break;
            case "23":
            case "24":
            case "25":
            case "26":
            case "30":
            case "31":
            case "32":
            case "33":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.order) {
                        //title: "Chi tiết đặt dịch vụ",
                        //name: 'OHServicesBagDetails',
                    }
                }
                break;
            case "38":  // Đăng ký khám tại bệnh viện - Đã đăng ký
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.schedule) {
                        //title: "Chi tiết đặt khám",
                        //name: 'OHApptHospitalHistoryDetail',
                        history.push({
                            pathname: url.name.account_detail_history_examination,
                            state: {id: extra.schedule}
                        });
                    }
                }
                break;
            case "39": {    // Regist Service from HIS
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.data && extra.data.hospital) {
                        // OHCollectPaymentRoot
                    }
                }
                break;
            }

            //SOCIAL NETWORK

            case "100": //Post
            case "101": //Like
            case "102": //Comment
            case "104": //mention(tag) user
                if (item.extra) {
                    if (item.extra && item.extra.postId) {
                        // OHNewsFeedDetailsRoot
                    }
                }
                break;
            case "103": //Follow
            {
                if (item.extra) {
                    if (item.extra && item.extra.profileId) {
                        //title: "Trang cá nhân",
                        //name: 'OHSocialNetworkProfileMain',
                    }
                }
                break;
            }
            case "110": //Birthday
            {
                if (item.extra) {
                    let extra = item.extra;
                    let userType = extra.type + '';
                    if (userType && userType.trim() !== '') {
                        if (userType === "2") { // doctor
                            //title: "Trang cá nhân",
                            //name: 'OHSocialNetworkProfileMain',
                        } else { // patient, nurse, pt, admin, moderator
                            // open chat
                        }
                    }
                }
                break;
            }
            case "200": // Apply Promotion
                if (item.extra) {
                    //title: "Ưu đãi",
                    //name: 'OHPromotionMain',
                }
                break;
            case "201": // Point History
                if (item.extra) {
                    //title: "Lịch sử điểm",
                    //name: 'OHAccountPromotionHis',
                    setOpenModalPointHistory(true);
                }
                break;
            case "300": // BMI PT
                if (item.extra) {
                    //OHHealthTrackingUsersMainRoot
                }
                break;
            case "700": // Drug Order
            case "701":
            case "702":
            case "703":
            case "704":
                if (item.extra) {
                    //OHDrugOrderDetailRoot
                }
                break;
            case "705":
            {
                // let extra = item && item.extra;
                // detail lucky money
                break;
            }
            case "1001":
            case "1002":
            case "1003": {
                // Collect Payment
                if (item.extra) {
                    // let extra = item.extra;
                    //title: "Chi tiết giao dịch",
                    //name: 'OHAccountPayHisDetails',
                }
                break;
            }
            case "1006": {
                // book video schedule
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.examId) {
                        //title: "Chi tiết lịch gọi video",
                        //name: 'OHDoctorVideoHistoryDetail',
                        history.push({
                            pathname: url.name.account_detail_history_call,
                            state: {id: extra.examId}
                        });
                    }
                }
                break;
            }
            case "400": {
                // take blood
                if (item.extra) {
                    let extra = item.extra;
                    if (extra) {
                        //title: "Chi tiết lịch hẹn",
                        //name: 'OHApptHomeHistoryDetail',
                    }
                }
                break;
            }
            default:
        }
    }

    function yieldShortNotify() {
        if (listNotify.length > 0) {
            const list = _.map(listNotify.slice(0, 5), (item, index) => {
                const clicked = item.clicked;
                const title = item.title;
                const detail = item.body;
                const time = func.formatTimeSocial(item.created_time);
                return (
                    <Menu.Item key={index} onClick={() => onPutClicked(item)}>
                        <div className={clicked ? "notify-title seen" : "notify-title"}>{title}</div>
                        <div className={clicked ? "notify-detail seen" : "notify-detail"}>{detail}</div>
                        <div className={clicked ? "notify-date seen" : "notify-date"}>{time}</div>
                    </Menu.Item>
                );
            });
            let yieldActionMore;
            if (listNotify.length > 5) {
                yieldActionMore = (
                    <Menu.Item>
                        <Link to={url.name.account_notification}
                              className="text-center">Xem thêm</Link>
                    </Menu.Item>
                );
            }
            return (
                <Menu>
                    <h4><FaBell/> Thông báo</h4>
                    {list}
                    {yieldActionMore}
                </Menu>
            );
        } else {
            if (!skeleton) {
                return (
                    <Menu>
                        <h4><FaBell/> Thông báo</h4>
                        <div className="text-center mt-2 mb-2">Thông báo trống</div>
                    </Menu>
                );
            } else {
                return (<Menu>
                        <h4><FaBell/> Thông báo</h4>
                        <BoxSkeleton skeleton={skeleton}
                                           full={true}
                                           length={5}
                                           rows={1}
                                           data={""}/></Menu>);
            }
        }
    }

    return (
        <Fragment>
            <Dropdown overlayClassName="block-notify"
                      trigger={['click']}
                      onVisibleChange={onPressBell}
                      overlay={yieldShortNotify()}
                      placement="bottomRight">
                <div className="menu-notify">
                    <Badge count={notify}>
                        <FaBell size={`1.3em`}/>
                    </Badge>
                </div>
            </Dropdown>
            <ModalPointHistory {...props}
                               onOpenModalPointHistory={onOpenModalPointHistory}
                               openModalPointHistory={openModalPointHistory}/>
            {contextHolder}
        </Fragment>
    );
};

export default WrapNotify;
