import React, {Fragment} from "react";
import _ from "lodash";
import {Form, Input} from "antd";
import * as func from "../../helpers";

const BoxInputFullName = (props) => {
    //*** Declare props ***//
    const {
        name, label, placeholder, validator,
        onChange, required, dependencies
    } = props;

    const validatorFullName = ((rule, value) => {
        if (value) {
            const tmp = value.trim().split(" ");
            if (tmp.length <= 1) {
                return Promise.reject("Họ và tên không hợp lệ");
            } else {
                if (func.checkSpecialChar(value)) {
                    return Promise.reject("Họ và tên không hợp lệ");
                } else {
                    return Promise.resolve();
                }
            }
        } else {
            return Promise.resolve();
        }
    });

    let rules = [];
    if (required) {
        const tmpObjRequired = {required: true, message: "Vui lòng nhập họ và tên"};
        rules.push(tmpObjRequired);
    }

    if (validator && validator.length > 0) {
        _.map(validator, (item) => {
            rules.push(item);
        });
    }

    const tmpObjValidatorFullName = {validateTrigger: 'onBlur', validator: validatorFullName};
    rules.push(tmpObjValidatorFullName);

    return (
        <Fragment>
            <Form.Item
                label={label}
                name={name}
                dependencies={dependencies}
                rules={rules}>
                <Input onChange={onChange ? onChange : null}
                       maxLength={60}
                       placeholder={placeholder}/>
            </Form.Item>
        </Fragment>
    );
};

export default BoxInputFullName;
