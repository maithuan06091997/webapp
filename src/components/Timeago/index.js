import React from "react";
import ReactTimeago from "react-timeago";

const Timeago = ({ date, format, ...props }) => {

    const formatter = (value, unit, suffix) => {
        let transSuffix = format !== "short" ? suffix === 'from now' ? ' tới' : ' trước' : "";
        switch (unit) {
            case 'second':
                return `vài giây${transSuffix}`
            case 'minute':
                return `${value} phút${transSuffix}`
            case 'hour':
                return `${value} giờ${transSuffix}`
            case 'day':
                return `${value} ngày${transSuffix}`
            case 'week':
                return `${value} tuần${transSuffix}`
            case 'month':
                return `${value} tháng${transSuffix}`
            case 'year':
                return `${value} năm${transSuffix}`
            default:
                return ""
        }
    }

    return (
        <ReactTimeago date={date} formatter={formatter} {...props} />
    );
};

export default Timeago;
