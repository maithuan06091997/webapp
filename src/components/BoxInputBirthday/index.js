import React, {Fragment} from "react";
import {Form, Input} from "antd";
import * as func from "../../helpers";

const BoxInputBirthday = (props) => {
    //*** Declare props ***//
    const {name, label, placeholder, onChange} = props;

    const validatorBirthday = ((rule, value) => {
        if (value) {
            const currentDate = new Date();
            const tmp   = value.split("/");
            const day   = tmp[0] ? tmp[0].trim() : undefined;
            const month = tmp[1] ? tmp[1].trim() : undefined;
            const year  = tmp[2] ? tmp[2].trim() : undefined;
            if (day && month && year && year.length === 4) {
                const isValid = func.isValidDate(day, month, year);
                if (!isValid) {
                    return Promise.reject("Ngày sinh của bạn không hợp lệ");
                } else {
                    if (year <= 1900 || year > currentDate.getFullYear()) {
                        return Promise.reject("Ngày sinh của bạn không hợp lệ");
                    } else {
                        return Promise.resolve();
                    }
                }
            } else {
                return Promise.reject("Ngày sinh của bạn không hợp lệ");
            }
        } else {
            return Promise.resolve();
        }
    });

    return (
        <Fragment>
            <Form.Item
                label={label}
                name={name}
                rules={[
                    {required: true, message: "Vui lòng nhập ngày sinh"},
                    {validateTrigger: 'onBlur', validator: validatorBirthday}]}>
                <Input placeholder={placeholder}
                       onChange={onChange}
                       autoComplete="off"/>
            </Form.Item>
        </Fragment>
    );
};

export default BoxInputBirthday;
