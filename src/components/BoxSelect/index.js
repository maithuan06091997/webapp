import React, { Fragment } from "react";
import _ from "lodash";
import Select from "react-select";

const BoxSelect = (props) => {
  const handleData = () => {
    let data = [];
    _.map(props.data, (item) => {
      const value =
        item.ward_id ||
        item.district_id ||
        item.province_id ||
        item.code ||
        item.id ||
        item.value;
      const label = item.name || item.text || item.vi_name;
      const tmp = {
        value: value,
        label: label,
      };
      data.push(tmp);
    });
    return data;
  };
  return (
    <Fragment>
      <Select
        {...props}
        maxMenuHeight={150}
        menuPlacement={props.placement ? props.placement : "top"}
        options={handleData(this)}
      />
    </Fragment>
  );
};

export default BoxSelect;
