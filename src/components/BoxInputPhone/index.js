import React, {Fragment} from "react";
import _ from "lodash";
import {Form, InputNumber} from "antd";
import * as func from "../../helpers";

const BoxInputPhone = (props) => {
    //*** Declare props ***//
    const {
        name, label, placeholder, required, autoFocus,
        dependencies, initialValue, validator, disabled
    } = props;

    const validatorPhone = ((rule, value) => {
        if (value) {
            const phone = func.isValidPhone(value);
            const lengthInput = phone.toString().length;
            if (lengthInput === 10 || lengthInput === 11) {
                return Promise.resolve();
            } else {
                return Promise.reject("Số điện thoại không đúng");
            }
        } else {
            return Promise.resolve();
        }
    });

    let rules = [];
    if (required) {
        const tmpObjRequired = {required: true, message: "Vui lòng nhập số điện thoại"};
        rules.push(tmpObjRequired);
    }

    if (validator && validator.length > 0) {
        _.map(validator, (item) => {
            rules.push(item);
        });
    }

    const tmpObjValidatorPhone= {validateTrigger: 'onBlur', validator: validatorPhone};
    rules.push(tmpObjValidatorPhone);

    return (
        <Fragment>
            <Form.Item
                initialValue={initialValue ? initialValue : undefined}
                label={label}
                name={name}
                dependencies={dependencies}
                rules={rules}>
                <InputNumber type="number"
                             formatter={value => {
                                 if (value) {
                                     value = `0${value}`.replace(/0*/, "0");
                                     return value.toString().substring(0, 11);
                                 }
                                 return value;
                             }}
                             autoFocus={autoFocus}
                             disabled={disabled}
                             placeholder={placeholder}/>
            </Form.Item>
        </Fragment>
    );
};

export default BoxInputPhone;
