import React, {Fragment} from "react";
import ReactLoading from "react-loading";
import {public_url} from "../../constants/Path";
import "./style.scss";

const Loading = (props) => {
    //*** Declare props ***//
    const {open} = props;
    let yieldLoading = "";
    if(open) {
        yieldLoading = (<div className={"panel-loading"}>
            <ReactLoading className={"loading"} color={"rgb(2 255 215)"} type={"spin"}/>
            <img src={public_url.img + "/logo_oh_round.png"} alt="Dr.OH"/>
        </div>);
    }
    return (
        <Fragment>{yieldLoading}</Fragment>
    );
};

export default Loading;
