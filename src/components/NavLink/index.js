import React, {Fragment} from "react";
import {url} from "../../constants/Path";
import {Link} from "react-router-dom";

const NavLink = (props) => {
    let arrayPathName = window.location.pathname.split("/").filter(Boolean);
    if (arrayPathName.length > 0) {
        const index = arrayPathName.findIndex((value) => {
            return value === "my-account";
        });
        arrayPathName = arrayPathName.slice(index);
    }
    const pathName = `${url.name.index}${arrayPathName[0]}/${arrayPathName[1]}`;
    const isActive  = pathName === props.to;
    const className = isActive ? 'active' : '';

    return (
        <Fragment>
            <Link className={className}  {...props}>
                {props.children}
            </Link>
        </Fragment>
    );
};

export default NavLink;
