import React from "react";
import { FaChevronRight } from "react-icons/fa";
import { Link } from "react-router-dom";

const ServiceItem = ({ url, img, title }) => {
  return (
    <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
      <Link to={url}>
        <div className="wrap-panel-item">
          <div className="wrap-item-img">
            <img className="img-fluid" src={img} alt={title} />
          </div>
          <div className="wrap-item-info">{title}</div>
          <div className="wrap-event-action">
            <FaChevronRight size={`2em`} />
          </div>
        </div>
      </Link>
    </div>
  );
};

export default ServiceItem;
