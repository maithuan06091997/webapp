import React, { useEffect, useState } from "react";
import moment from "moment";
import ReactTooltip from "react-tooltip";
import { public_url } from "../../../constants/Path";
import { Skeleton } from "antd";
import "./style.scss";
import * as func from "../../../helpers";
import { FaPaperPlane, FaImage } from "react-icons/fa";

import { Link } from "react-router-dom";
import ImgsViewer from "react-images-viewer";

let listMessagesRef = null;
let messageTextRef = null;
let scrollBottom = false;
let bottomPosition = 0;
let isShowAvatar = false;
let senderId = null;

const SentMessage = ({
  id,
  message,
  type,
  sender,
  created_time,
  sending,
  error,
  resendMessage,
}) => {
  const [isPopup, setPopup] = useState(false);
  return (
    <div className={`message sent ${sending || error ? "blur" : ""}`}>
      {sending ? (
        <span className="left-icon mr-2">
          <i className={`fa fa-spinner fa-pulse`}></i>
        </span>
      ) : null}
      {error ? (
        <span
          className="left-icon mr-2"
          onClick={() => resendMessage(id)}
          style={{ cursor: "pointer" }}
          data-tip={"Thử lại"}
        >
          <i className={`fa fa-repeat`}></i>
          <ReactTooltip effect="solid" />
        </span>
      ) : null}
      <div
        className="content"
        data-tip={moment(created_time).format("hh:mm A DD/MM/YYYY")}
      >
        {type === 1 && message}
        {type === 2 && (
          <>
            <img
              src={message}
              alt="#"
              className="image-mess"
              onClick={() => setPopup(true)}
            />
            {isPopup && (
              <ImgsViewer
                imgs={[{ src: message }]}
                currImg={0}
                isOpen={isPopup}
                onClose={() => setPopup(false)}
              />
            )}
          </>
        )}

        <ReactTooltip effect="solid" />
      </div>
      {error ? (
        <small className="text-danger w-100 text-right">
          Gửi thất bại
          <Link
            className="text-primary"
            onClick={() => resendMessage(id)}
            to={"#"}
          >
            {" "}
            <u>Thử lại</u>
          </Link>
        </small>
      ) : null}
    </div>
  );
};

const ReceivedMessage = ({
  id,
  message,
  type,
  sender,
  isShowAvatar,
  created_time,
}) => {
  const [isPopup, setPopup] = useState(false);
  return (
    <div className="message received">
      {isShowAvatar ? (
        <img
          className="avatar"
          src={
            sender?.avatar && sender.avatar.url
              ? sender.avatar.url
              : `${public_url.img}/icons/icon_avatar.png`
          }
          alt=""
        />
      ) : null}
      <div
        className="content"
        data-tip={moment(created_time).format("hh:mm A DD/MM/YYYY")}
      >
        {type === 1 && message}
        {type === 2 && (
          <>
            <img
              src={message}
              alt="#"
              className="image-mess"
              onClick={() => setPopup(true)}
            />
            {isPopup && (
              <ImgsViewer
                imgs={[{ src: message }]}
                currImg={0}
                isOpen={isPopup}
                onClose={() => setPopup(false)}
              />
            )}
          </>
        )}
        <ReactTooltip effect="solid" />
      </div>
    </div>
  );
};

const ConversationDetails = ({
  conversation,
  isLoading,
  messages,
  loadMore,
  hasMore,
  onSend,
  currentUser,
  resendMessage,
}) => {
  const [listMessages, setListMessages] = useState(messages);
  const [messageText, setMessageText] = useState("");
  const [partner, setPartner] = useState(null);
  const [ended, setEnded] = useState(false);
  const [messFileImg, setMessFileImg] = useState();

  useEffect(() => {
    setMessageText("");
    setEnded(
      conversation && conversation.room && conversation.room.status === 2
    );
  }, [conversation]);

  useEffect(() => {
    if (conversation) {
      if (currentUser.personal._id) {
        if (
          conversation.receiver &&
          conversation.receiver._id !== currentUser.personal._id
        ) {
          setPartner(conversation.receiver);
        } else {
          setPartner(conversation.sender);
        }
      }
    }
  }, [conversation, currentUser.personal._id]);

  useEffect(() => {
    let newListMessages = [];
    for (let i = messages.length - 1; i >= 0; i--) {
      newListMessages.push(messages[i]);
    }
    if (!(listMessages && listMessages.length)) {
      scrollBottom = true;
    }
    setListMessages(newListMessages);
  }, [messages]);

  useEffect(() => {
    if (scrollBottom) {
      scrollBottom = false;
      listMessagesRef.scroll({
        top: listMessagesRef.scrollHeight,
        behavior: "auto",
      });
    } else if (bottomPosition) {
      listMessagesRef.scroll({
        top: listMessagesRef.scrollHeight - bottomPosition,
        behavior: "auto",
      });
    }
  }, [listMessages]);

  const onScrollMessages = (e) => {
    // if (e.target.scrollTop + e.target.offsetHeight >= e.target.scrollHeight && !bottomScrolled) {
    //     this.setState({ bottomScrolled: true })
    // } else if (e.target.scrollTop + e.target.offsetHeight < e.target.scrollHeight && bottomScrolled) {
    //     this.setState({ bottomScrolled: false })
    // }
    // const { getMessagesLoading, Conversation } = this.props;
    if (e.target.scrollTop < 250 && !isLoading && hasMore) {
      loadMore();
    }
    bottomPosition = e.target.scrollHeight - e.target.scrollTop;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (messageText && messageText.trim()) {
      onSend && onSend(messageText);
      setMessageText("");
    }
  };
  const submitImg = (e) => {
    const file = e.target.files[0];

    const reader = new FileReader();
    reader.onloadend = () => {
      onSend(reader.result, 2);
      setMessFileImg("");
    };
    reader.readAsDataURL(file);
  };

  const onKeyUp = (e) => {
    if (e.key == "Enter") {
      e.preventDefault();
      e.value = "";
      onSubmit(e);
    } else {
      setMessageText(e.target.value);
    }
  };

  return (
    <div className="chat-box">
      {partner ? (
        <div className="chat-box-header">
          <img
            className="avatar"
            src={
              partner.avatar && partner.avatar.url
                ? partner.avatar.url
                : `${public_url.img}/icons/icon_avatar.png`
            }
            alt="#"
          />
          <div className="flex-fill">
            <p className="fullname mb-1">{func.getFullName(partner)}</p>
            {conversation.room && conversation.room.status === 2 ? (
              <p className="text-danger mb-0">Đã kết thúc</p>
            ) : null}
          </div>
        </div>
      ) : null}
      <div
        className="list-messages"
        onScroll={onScrollMessages}
        ref={(ref) => (listMessagesRef = ref)}
      >
        {isLoading || hasMore ? (
          <div className="mb-2">
            <Skeleton.Avatar
              active
              size={30}
              shape={"circle"}
              className="mr-1"
            />
            <Skeleton.Input
              active
              style={{ width: 200, height: 30, borderRadius: 5 }}
            />
            <div className="text-right">
              <Skeleton.Input
                active
                style={{ width: 200, height: 30, borderRadius: 5 }}
              />
            </div>
          </div>
        ) : null}
        {listMessages && listMessages.length ? (
          listMessages.map((item) => {
            if (item.sender && currentUser.personal._id === item.sender._id) {
              senderId = item.sender._id;
              return (
                <SentMessage
                  id={item._id}
                  key={`message_${item._id}`}
                  message={item.message}
                  type={item.type}
                  sender={item.sender}
                  created_time={item.created_time}
                  sending={item.sending}
                  error={item.error}
                  resendMessage={resendMessage}
                />
              );
            }
            isShowAvatar = item.sender?._id !== senderId;
            senderId = item.sender?._id;
            return (
              <ReceivedMessage
                id={item._id}
                key={`message_${item._id}`}
                message={item.message}
                type={item.type}
                sender={item.sender}
                created_time={item.created_time}
                isShowAvatar={isShowAvatar}
              />
            );
          })
        ) : (
          <div />
        )}
      </div>
      <div className={`message-box ${ended ? "bg-light" : ""}`}>
        <form onSubmit={onSubmit}>
          <textarea
            className="input-message"
            ref={(ref) => (messageTextRef = ref)}
            disabled={ended}
            onKeyUp={onKeyUp}
            placeholder={
              ended
                ? "Bạn không thể trả lời cuộc trò chuyện đã kết thúc."
                : "Nhập nội dung tin nhắn ..."
            }
            value={messageText}
            onChange={(e) => setMessageText(e.target.value)}
          />

          {!ended ? (
            <>
              <label className="option-file">
                <FaImage />
                <input
                  type="file"
                  onChange={submitImg}
                  value={messFileImg}
                  accept="image/*"
                  hidden
                />
              </label>
              <button className="btn btn-submit" type="submit">
                <FaPaperPlane />
              </button>
            </>
          ) : null}
        </form>
      </div>
    </div>
  );
};

export default ConversationDetails;
