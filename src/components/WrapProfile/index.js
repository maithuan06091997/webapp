import React, { Fragment, useState } from "react";
import { Dropdown, Menu } from "antd";
import { public_url, url } from "../../constants/Path";
import { FaAngleDown, FaUserCircle } from "react-icons/fa";
// import {AiTwotoneSetting} from "react-icons/ai";
import { VscSignOut } from "react-icons/vsc";
import ModalPointHistory from "../Promotion/ModalPointHistory";
import { Link } from "react-router-dom";

const WrapProfile = (props) => {
  const [openModalPointHistory, setOpenModalPointHistory] = useState(false);
  //*** Declare props ***//
  const { onSignOut, balance, userInfo, user } = props;

  const onOpenModalPointHistory = (flag) => {
    setOpenModalPointHistory(flag);
  };

  let yieldAvatar;
  if (user.avatar && user.avatar.url) {
    yieldAvatar = <img className="img-fluid" src={user.avatar.url} alt="" />;
  } else {
    yieldAvatar = <FaUserCircle size={`4.5em`} />;
  }

  const profile = (
    <Menu>
      <Menu.Item className="clearfix">
        <div className="detail-profile">
          <div className="wrap-left">{yieldAvatar}</div>
          <div className="wrap-right">
            <div className="full-name">
              <strong>
                {userInfo && userInfo.full_name ? userInfo.full_name : "Guest"}
              </strong>
            </div>
            <div>
              <strong className="text-muted">
                {userInfo && userInfo.phone ? userInfo.phone : "-"}
              </strong>
            </div>
            <Link
              to={url.name.account_edit_personal}
              className="ant-btn btn-accept w-100 mt-1"
            >
              <span>Cập nhật hồ sơ</span>
            </Link>
          </div>
        </div>
      </Menu.Item>
      <hr />
      <Menu.Item className="clearfix">
        <div className="balance-profile">
          <Link className="wrap-left" to={url.name.account_list_invoice}>
            <div className="icon">
              <img
                className="img-fluid"
                src={public_url.img + "/icons/ic_wallet.png"}
                alt=""
              />
            </div>
            <div className="info">
              <p className="label">Số dư DR.OH</p>
              <p className="value">
                {balance && balance.vnd ? balance.vnd.toLocaleString() : 0} đ
              </p>
            </div>
          </Link>
          <div
            onClick={onOpenModalPointHistory.bind(this, true)}
            className="wrap-right"
          >
            <div className="icon">
              <img
                className="img-fluid"
                src={public_url.img + "/icons/ic_point.png"}
                alt=""
              />
            </div>
            <div className="info">
              <p className="label">Điểm tích lũy</p>
              <p className="value">
                {balance.point ? balance.point.toLocaleString() : 0}
              </p>
            </div>
          </div>
        </div>
        <div className="balance-action">
          <Link to={url.name.account_wallet} className="ant-btn w-100 mt-1">
            <span>Nạp tiền vào tài khoản</span>
          </Link>
        </div>
      </Menu.Item>
      <hr />
      <Menu.Item>
        <Link to={url.name.account_list_relative_profile}>
          <div className="menu-item-detail">
            <div className="round-frame mr-2">
              <img
                className="img-fluid"
                src={public_url.img + "/icons/icon_profile.png"}
                alt=""
              />
            </div>
            <span>Hồ sơ người thân</span>
          </div>
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to={url.name.account_promotion}>
          <div className="menu-item-detail">
            <div className="round-frame mr-2">
              <img
                className="img-fluid"
                src={public_url.img + "/icons/icon_gift.png"}
                alt=""
              />
            </div>
            <span>Ưu đãi</span>
          </div>
        </Link>
      </Menu.Item>
      {/*<Menu.Item>*/}
      {/*    <Link to={url.name.account_detail_personal}>*/}
      {/*        <div className="menu-item-detail">*/}
      {/*            <AiTwotoneSetting className="mr-2"/>*/}
      {/*            <span>Cài đặt</span>*/}
      {/*        </div>*/}
      {/*    </Link>*/}
      {/*</Menu.Item>*/}
      <Menu.Item>
        <Link to={url.name.account_change_password}>
          <div className="menu-item-detail">
            <div className="round-frame mr-2">
              <img
                className="img-fluid"
                src={public_url.img + "/icons/icon_change_pass.png"}
                alt=""
              />
            </div>
            <span>Đổi mật khẩu</span>
          </div>
        </Link>
      </Menu.Item>
      <Menu.Item onClick={onSignOut}>
        <div className="menu-item-detail">
          <VscSignOut className="mr-2" />
          <span>Đăng xuất</span>
        </div>
      </Menu.Item>
    </Menu>
  );

  return (
    <Fragment>
      <Dropdown
        overlayClassName="block-profile"
        trigger={["click"]}
        overlay={profile}
        placement="bottomRight"
      >
        <div className="menu-profile">
          <FaAngleDown size={`1.3em`} />
        </div>
      </Dropdown>
      <ModalPointHistory
        {...props}
        onOpenModalPointHistory={onOpenModalPointHistory}
        openModalPointHistory={openModalPointHistory}
      />
    </Fragment>
  );
};

export default WrapProfile;
