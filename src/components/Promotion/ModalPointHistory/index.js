import React, {Fragment, useCallback, useEffect, useState} from "react";
import _ from "lodash";
import {Empty, Modal} from "antd";
import {ModelPointHistory} from "../../../models/ModelPromotion";
import BoxSkeleton from "../../BoxSkeleton";
import {public_url} from "../../../constants/Path";
import * as func from "../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 20;

const ModalPointHistory = (props) => {
    const [modalPointHistory, setModalPointHistory] = useState(false);
    const [skeleton, setSkeleton] = useState(false);
    const [listPointHistory, setListPointHistory] = useState([]);
    const [limitRecord, setLimitRecord] = useState([]);
    const [page, setPage] = useState(1);
    //*** Declare props ***//
    const {_TokenExpired, onOpenModalPointHistory, openModalPointHistory} = props;

    //*** Handle yield list point history ***//
    const handlePointHistory = useCallback((page, array, trigger = false) => {
        const reqParamFt = {
            page: page
        }
        setSkeleton(true);
        ModelPointHistory(reqParamFt).then(resultFt => {
            const dataListPointHistory = resultFt.data.one_health_msg;
            const tmpArray = array.concat(dataListPointHistory);
            setListPointHistory(tmpArray);
            setLimitRecord(dataListPointHistory);
            setSkeleton(false);
        }).catch(error => {
            if (error.code !== "007") {
                console.log("ModalPointHistory - ListPointHistory");
                setSkeleton(false);
            }
            if (trigger && error.code === "007"){
                _TokenExpired(error);
            }
        });
    }, [_TokenExpired]);

    useEffect(() => {
        if (openModalPointHistory) {
            setModalPointHistory(openModalPointHistory);
        }
    }, [openModalPointHistory]);

    useEffect(() => {
        if (openModalPointHistory) {
            handlePointHistory(1, []);
        }
    }, [openModalPointHistory, handlePointHistory]);

    const onCloseModalPointHistory = () => {
        //*** Handle return the original state when modal close ***//
        onOpenModalPointHistory(false);
        setPage(1);
        setListPointHistory([]);
        setLimitRecord([]);
        setModalPointHistory(false);
    };

    const onNextPage = () => {
        const next = page+1;
        setPage(next);
        handlePointHistory(next, listPointHistory, true);
    };

    function yieldListPointHistory() {
        if (listPointHistory.length > 0) {
            return _.map(listPointHistory, (item, index) => {
                return (
                    <div key={index} className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-item-img round-frame">
                                <img className="img-fluid" src={public_url.img + "/icons/icon_gift2.png"} alt=""/>
                            </div>
                            <div className="wrap-item-info">
                                <div className="info">
                                    <p className="info-title">{item.type_des}</p>
                                    <p>{func.yieldDatetime(item.modified_time)}</p>
                                </div>
                                <div className={item.deposit ? "point add" : "point sub"}>
                                    <p>{item.deposit ? "+" + item.deposit : item.withdraw} <span>điểm</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Lịch sử điểm trống"/>);
            }
        }
    }

    function yieldPaginationAction() {
        if (limitRecord.length === LIMIT) {
            return (
                <div className="wrap-lazy-pagination">
                    <hr className="w-100"/>
                    <div onClick={onNextPage} className="ant-btn btn-accept ant-btn-lg"><span>Xem thêm</span></div>
                </div>
            );
        }
        return null;
    }

    return (
        <Fragment>
            <Modal title="Lịch sử điểm"
                   className="block-modal-point-history ant-modal-custom"
                   closable={true}
                   width={600}
                   footer={null}
                   visible={modalPointHistory}
                   onCancel={onCloseModalPointHistory}>
                <div className="wrap-panel-content">
                    <div className="row">
                        <BoxSkeleton skeleton={skeleton}
                                     full={true}
                                     length={6}
                                     rows={2}
                                     data={yieldListPointHistory()}/>
                    </div>
                </div>
                {yieldPaginationAction()}
            </Modal>
        </Fragment>
    );
};

export default ModalPointHistory;
