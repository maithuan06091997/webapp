import React, { Fragment, useEffect, useState } from "react";
import i18next from "i18next";
import { Modal } from "antd";
import { msg_text } from "../../../constants/Message";
import Loading from "../../Loading";
import BoxSkeleton from "../../BoxSkeleton";
import {
  ModelDetailVoucher,
  ModelExchangeVoucher,
  ModelPointPromotion,
} from "../../../models/ModelPromotion";
import { _UpdatePointPromotion } from "../../../actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

const ModalDetailVoucher = (props) => {
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalDetailVoucher, setModalDetailVoucher] = useState(false);
  const [infoVoucher, setInfoVoucher] = useState(undefined);
  //*** Declare props ***//
  const {
    _TokenExpired,
    _UpdatePointPromotion,
    _BadgeNotification,
    balance,
    onOpenModalDetailVoucher,
    detailVoucher,
    openModalDetailVoucher,
  } = props;

  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    if (openModalDetailVoucher) {
      const id = detailVoucher._id;
      const point = detailVoucher.rewards;
      setLoading(true);
      setSkeleton(true);
      setModalDetailVoucher(openModalDetailVoucher);
      ModelDetailVoucher(id, point)
        .then((resultFt) => {
          const dataDetailVoucher = resultFt.data.one_health_msg;
          setInfoVoucher(dataDetailVoucher);
          setLoading(false);
          setSkeleton(false);
        })
        .catch((error) => {
          if (error.code !== "007") {
            console.log("ModalDetailVoucher - DetailVoucher");
            setLoading(false);
            setSkeleton(false);
          } else {
            _TokenExpired(error);
          }
        });
    }
  }, [_TokenExpired, openModalDetailVoucher, detailVoucher]);

  const onExchangeVoucher = (item) => {
    const idVoucher = item._id;
    ModelExchangeVoucher(idVoucher)
      .then(() => {
        return ModelPointPromotion();
      })
      .then((resultSd) => {
        modal.success({
          title: "Thành công",
          okText: "Đồng ý",
          centered: true,
          content: "Bạn đã đổi quà, đi đến *QÙA CỦA TÔI* để sử dụng",
        });
        setLoading(false);
        const dataPointPromotion = resultSd.data.one_health_msg;
        _BadgeNotification();
        _UpdatePointPromotion(dataPointPromotion);
      })
      .catch((error) => {
        if (error.code === "007") {
          _TokenExpired(error);
        } else {
          const msg = error.message
            ? error.message
            : error.description
              ? error.description
              : msg_text.exchange_voucher_error;
          modal.warning({
            title: "Thông báo",
            okText: "Đóng",
            centered: true,
            content: i18next.t(msg),
          });
          setLoading(false);
        }
      });
  };

  const onCloseModalDetailVoucher = () => {
    setInfoVoucher(undefined);
    onOpenModalDetailVoucher(false);
    setModalDetailVoucher(false);
  };

  function yieldDetailVoucher() {
    if (infoVoucher) {
      const startTime = func.yieldDatetime(infoVoucher.start, "date");
      const endTime = func.yieldDatetime(infoVoucher.end, "date");
      const myPoint = balance.point;
      let yieldEventExchange;
      if (myPoint >= infoVoucher.rewards) {
        yieldEventExchange = (
          <div
            onClick={onExchangeVoucher.bind(this, infoVoucher)}
            className="ant-btn btn-accept"
          >
            <span>Đổi quà</span>
          </div>
        );
      }
      return (
        <Fragment>
          <div className="wrap-top">
            <img className="img-fluid" src={infoVoucher.image.url} alt="" />
            <h4>{infoVoucher.title}</h4>
          </div>
          <div className="wrap-main">
            <div className="info-short-desc">
              <div className="left">
                <p className="label">Điểm:</p>
                <p className="point">{infoVoucher.rewards.toLocaleString()}</p>
              </div>
              <div className="right">
                <p className="label">Hiệu lực:</p>
                <p className="end-time">{startTime + " đến " + endTime}</p>
              </div>
            </div>
            <div className="info-desc">
              {yieldEventExchange}
              <h4 className="title">Mô tả</h4>
              <div dangerouslySetInnerHTML={{ __html: infoVoucher.content }} />
            </div>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <Modal
        title="Chi tiết quà"
        className="block-modal-detail-voucher"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modalDetailVoucher}
        onCancel={onCloseModalDetailVoucher}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetailVoucher()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _UpdatePointPromotion }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    ModalDetailVoucher
  )
);
