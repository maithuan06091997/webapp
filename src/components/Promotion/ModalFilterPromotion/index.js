import React, {Fragment, useCallback, useEffect, useState} from "react";
import _ from "lodash";
import {Empty, Modal} from "antd";
import Select from "react-select";
import {dataSortPriority} from "../../../constants/Data";
import InputRange from "react-input-range";
import ModalDetailVoucher from "../ModalDetailVoucher";
import {ModelFilterPromotion} from "../../../models/ModelPromotion";
import BoxSkeleton from "../../BoxSkeleton";
import "react-input-range/lib/css/index.css";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 20;
const MIN_RANGE = 0;
const MAX_RANGE = 1000000;

const ModalFilterPromotion = (props) => {
    const [skeleton, setSkeleton] = useState(false);
    const [modalFilterPromotion, setModalFilterPromotion] = useState(false);
    const [openModalDetailVoucher, setOpenModalDetailVoucher] = useState(false);
    const [dataStore, setDataStore] = useState([]);
    const [listVoucher, setListVoucher] = useState([]);
    const [limitRecord, setLimitRecord] = useState([]);
    const [selectCat, setSelectCat] = useState("");
    const [selectSort, setSelectSort] = useState(dataSortPriority(this)[0]);
    const [minRange, setMinRange] = useState(MIN_RANGE);
    const [compMinRange, setCompMinRange] = useState(MIN_RANGE);
    const [maxRange, setMaxRange] = useState(MAX_RANGE);
    const [compMaxRange, setCompMaxRange] = useState(MAX_RANGE);
    const [detailVoucher, setDetailVoucher] = useState("");
    const [page, setPage] = useState(1);
    //*** Declare props ***//
    const {_TokenExpired, onOpenModalFilterPromotion, openModalFilterPromotion, listStore, filterPromotion} = props;

    //*** Handle yield list voucher ***//
    const handleListVoucher = useCallback((page, array) => {
        const reqParamFt = {
            page: page,
            point_range: compMinRange+","+compMaxRange,
            category_id: selectCat ? selectCat.value : filterPromotion,
            priority: selectSort.value,
        };
        setSkeleton(true);
        ModelFilterPromotion(reqParamFt).then(resultFt => {
            const dataListVoucher = resultFt.data.one_health_msg;
            const tmpArray = array.concat(dataListVoucher);
            setListVoucher(tmpArray);
            setLimitRecord(dataListVoucher);
            setSkeleton(false);
        }).catch(error => {
            if (error.code !== "007") {
                console.log("ModalFilterPromotion - FilterPromotion");
                setSkeleton(false);
            } else {
                _TokenExpired(error);
            }
        });
    }, [_TokenExpired, filterPromotion, compMinRange, compMaxRange, selectSort, selectCat]);

    useEffect(() => {
        if (openModalFilterPromotion) {
            let dataStore = [];
            _.map(listStore, (item) => {
                const tmp = {value: item.typeId, label: item.typeName}
                dataStore.push(tmp);
                setDataStore(dataStore);
            });
            let indexStore = dataStore.findIndex(el => {
                return el.value === filterPromotion;
            });
            setModalFilterPromotion(openModalFilterPromotion);
            setSelectCat(dataStore[indexStore]);
        }
    }, [openModalFilterPromotion, listStore, filterPromotion]);

    useEffect(() => {
        if (openModalFilterPromotion) {
            handleListVoucher(1, []);
        }
    }, [openModalFilterPromotion, handleListVoucher]);

    const onNextPage = () => {
        const next = page+1;
        setPage(next);
        handleListVoucher(next, listVoucher);
    };

    const onSelectCat = values => {
        setSelectCat(values);
    };

    const onSelectSort = values => {
        setSelectSort(values);
    };

    const onOpenModalDetailVoucher = (flag, item) => {
        setOpenModalDetailVoucher(flag);
        setDetailVoucher(item);
    };

    const onCloseModalFilterPromotion = () => {
        //*** Handle return the original state when modal close ***//
        setDetailVoucher("");
        setSelectCat("");
        setDataStore([]);
        setListVoucher([]);
        setLimitRecord([]);
        setPage(1);
        setSelectSort(dataSortPriority(this)[0]);
        setMinRange(MIN_RANGE);
        setCompMinRange(MIN_RANGE);
        setMaxRange(MAX_RANGE);
        setCompMaxRange(MAX_RANGE);
        onOpenModalFilterPromotion(false);
        setModalFilterPromotion(false);
        setOpenModalDetailVoucher(false);
    };

    const onChangeRangePoint = values => {
        setMinRange(values.min < MIN_RANGE ? MIN_RANGE : values.min);
        setMaxRange(values.max > MAX_RANGE ? MAX_RANGE : values.max);
    };

    const onCompleteRangePoint = values => {
        setCompMinRange(values.min < MIN_RANGE ? MIN_RANGE : values.min);
        setCompMaxRange(values.max > MAX_RANGE ? MAX_RANGE : values.max);
    };

    function yieldListVoucher() {
        if (listVoucher.length > 0) {
            return _.map(listVoucher, (item, index) => {
                return (
                    <div key={index}
                         onClick={onOpenModalDetailVoucher.bind(this, true, item)}
                         className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-top">
                                <img className="img-fluid" src={item.image.url} alt=""/>
                            </div>
                            <div className="wrap-main">
                                <div className="label">{item.title}</div>
                                <div className="info"><span>{item.rewards.toLocaleString()}</span> điểm</div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description={"Cửa hàng quà trống"}/>);
            }
        }
    }

    function yieldPaginationAction() {
        if (limitRecord.length === LIMIT) {
            return (
                <div className="wrap-lazy-pagination">
                    <hr className="w-100"/>
                    <div onClick={onNextPage} className="ant-btn btn-accept ant-btn-lg"><span>Xem thêm</span></div>
                </div>
            );
        }
        return null;
    }

    return (
        <Fragment>
            <Modal title="Cửa hàng quà"
                   className="block-modal-filter-promotion ant-modal-custom"
                   closable={true}
                   maskClosable={false}
                   width={600}
                   footer={null}
                   visible={modalFilterPromotion}
                   onCancel={onCloseModalFilterPromotion}>
                <div className="wrap-filter-params">
                    <div className="wrap-cat">
                        <div className="title"><span>Cửa hàng</span></div>
                        <Select value={selectCat} onChange={onSelectCat} options={dataStore}/>
                    </div>
                    <div className="wrap-sort">
                        <div className="title"><span>Sắp xếp</span></div>
                        <Select value={selectSort}
                                onChange={onSelectSort}
                                options={dataSortPriority(this)}/>
                    </div>
                </div>
                <div className="wrap-filter-point">
                    <div className="title"><span>Lọc theo số điểm</span></div>
                    <div className="range">
                        <span>{minRange.toLocaleString()} điểm - {maxRange.toLocaleString()} điểm</span></div>
                    <InputRange maxValue={MAX_RANGE}
                                minValue={MIN_RANGE}
                                step={20}
                                value={{min: minRange, max: maxRange}}
                                onChangeComplete={onCompleteRangePoint}
                                onChange={onChangeRangePoint}/>
                </div>
                <hr className="w-100"/>
                <div className="wrap-panel-content">
                    <div className="row">
                        <BoxSkeleton skeleton={skeleton}
                                     full={false}
                                     length={8}
                                     rows={3}
                                     data={yieldListVoucher()}/>
                    </div>
                </div>
                {yieldPaginationAction()}
            </Modal>
            <ModalDetailVoucher {...props}
                                onOpenModalDetailVoucher={onOpenModalDetailVoucher}
                                openModalDetailVoucher={openModalDetailVoucher}
                                detailVoucher={detailVoucher}/>
        </Fragment>
    );
};

export default ModalFilterPromotion;
