import React, {Fragment} from "react";
import {Tabs} from "antd";
import MyPromotion from "../MyPromotion";
import StorePromotion from "../StorePromotion";
import "./style.scss";

const TabPanePromotion = (props) => {
    const {TabPane} = Tabs;
    return (
        <Fragment>
            <Tabs className="tabs-promotion" defaultActiveKey="1">
                <TabPane className="tab-my-promotion" tab="Quà của tôi" key="1">
                    <MyPromotion {...props}/>
                </TabPane>
                {" "}
                <TabPane className="tab-store-promotion" tab="Cửa hàng quà" key="2">
                    <StorePromotion {...props}/>
                </TabPane>
            </Tabs>
        </Fragment>
    );
};

export default TabPanePromotion;
