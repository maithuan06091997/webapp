import React, { Fragment, useEffect, useState } from "react";
import i18next from "i18next";
import { Modal } from "antd";
import { msg_text } from "../../../constants/Message";
import { url } from "../../../constants/Path";
import Loading from "../../Loading";
import BoxSkeleton from "../../BoxSkeleton";
import {
  ModelApplyPromotion,
  ModelApplyVoucher,
  ModelDetailVoucher,
} from "../../../models/ModelPromotion";
import * as func from "../../../helpers";
import "./style.scss";

const ModalDetailCoupon = (props) => {
  const [skeleton, setSkeleton] = useState(false);
  const [modalDetailCoupon, setModalDetailCoupon] = useState(false);
  const [loading, setLoading] = useState(false);
  const [detailVoucher, setDetailVoucher] = useState(undefined);
  //*** Declare props ***//
  const {
    _TokenExpired,
    _BadgeNotification,
    history,
    onOpenModalDetailCoupon,
    openModalDetailCoupon,
    handleApplyLater,
    handleUpdateListOwnerGift,
    detailCoupon,
    typeAction,
  } = props;

  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    if (openModalDetailCoupon) {
      const type = detailCoupon.type;
      if (type === "voucher") {
        const id = detailCoupon.voucher._id;
        setLoading(true);
        setSkeleton(true);
        ModelDetailVoucher(id)
          .then((resultFt) => {
            const dataDetailVoucher = resultFt.data.one_health_msg;
            setDetailVoucher(dataDetailVoucher);
            setLoading(false);
            setSkeleton(false);
          })
          .catch((error) => {
            if (error.code !== "007") {
              console.log("ModalDetailCoupon - DetailVoucher");
              setLoading(false);
              setSkeleton(false);
            } else {
              _TokenExpired(error);
            }
          });
      }
      setModalDetailCoupon(openModalDetailCoupon);
    }
  }, [_TokenExpired, openModalDetailCoupon, detailCoupon]);

  const handleGoToService = (service) => {
    switch (service) {
      case "schedule_appointment":
        history.push({ pathname: url.name.examination_1 });
        break;
      case "video_call_schedule":
        history.push({ pathname: url.name.call_chat_1 });
        break;
      default:
        return "";
    }
  };

  const onCloseModalDetailCoupon = () => {
    setDetailVoucher(undefined);
    onOpenModalDetailCoupon(false);
    setModalDetailCoupon(false);
  };

  const onApplyNow = (service) => {
    handleGoToService(service);
  };

  const onApplyVoucherNow = () => {
    const idVoucher = detailCoupon._id;
    // const title     = detailCoupon.voucher.title;
    setLoading(true);
    ModelApplyVoucher(idVoucher)
      .then(() => {
        _BadgeNotification();
        onCloseModalDetailCoupon();
        handleUpdateListOwnerGift();
        setLoading(false);
      })
      .catch((error) => {
        if (error.code === "007") {
          _TokenExpired(error);
        } else {
          const msg = error.message
            ? error.message
            : error.description
            ? error.description
            : msg_text.apply_voucher_error;
          modal.warning({
            title: "Thông báo",
            okText: "Đóng",
            centered: true,
            content: i18next.t(msg),
          });
          setLoading(false);
        }
      });
  };

  const onApplyLater = () => {
    onCloseModalDetailCoupon();
    handleApplyLater();
  };

  const onApplyPromotion = () => {
    const code = detailCoupon.promotionCode;
    const service = detailCoupon.serviceType
      ? detailCoupon.serviceType
      : detailCoupon.promotion.serviceType;
    if (typeAction === "find_coupon") {
      const reqParamFt = { code: code };
      setLoading(true);
      ModelApplyPromotion(reqParamFt)
        .then(() => {
          modal.confirm({
            title: "Thành công",
            okText: "Sử dụng ngay",
            cancelText: "Để sau",
            centered: true,
            onOk: onApplyNow.bind(this, service),
            onCancel: onApplyLater,
            content: "Bạn có muốn sử dụng khuyến mãi ngay bây giờ?",
          });
          _BadgeNotification();
          setLoading(false);
        })
        .catch((error) => {
          if (error.code === "007") {
            _TokenExpired(error);
          } else {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setLoading(false);
          }
        });
    } else {
      handleGoToService(service);
    }
  };

  const onApplyVoucher = () => {
    modal.confirm({
      title: "Thông báo",
      okText: "Sử dụng ngay",
      cancelText: "Để sau",
      centered: true,
      onOk: onApplyVoucherNow,
      content: "Bạn có muốn sử dụng quà tặng?",
    });
  };

  function yieldDetailCoupon() {
    if (detailCoupon) {
      let data,
        title,
        endTime,
        startTime,
        code,
        rewards,
        description,
        payment,
        image;
      let yieldEventApply = (
        <div onClick={onApplyPromotion} className="ant-btn btn-accept">
          <span>Sử dụng bây giờ</span>
        </div>
      );
      if (typeAction === "find_coupon") {
        title = detailCoupon.title;
        startTime = func.yieldDatetime(detailCoupon.start, "date");
        endTime = func.yieldDatetime(detailCoupon.end, "date");
        code = detailCoupon.promotionCode;
        payment = func.findTypePayment(detailCoupon.payment, "text");
        image = detailCoupon.image.url;
      } else {
        const type = detailCoupon.type;
        if (type === "voucher") {
          if (detailVoucher) {
            data = detailVoucher;
            title = data.title;
            startTime = func.yieldDatetime(data.start, "date");
            endTime = func.yieldDatetime(data.end, "date");
            rewards = data.rewards;
            description = data.content;
            image = data.image.url;
            yieldEventApply = (
              <div onClick={onApplyVoucher} className="ant-btn btn-accept">
                <span>Sử dụng bây giờ</span>
              </div>
            );
          }
        } else if (type === "promotion") {
          data = detailCoupon.promotion;
          title = data.title;
          startTime = func.yieldDatetime(detailCoupon.created_time, "date");
          endTime = func.yieldDatetime(detailCoupon.expire, "date");
          code = detailCoupon.promotionCode;
          payment = func.findTypePayment(data.payment, "text");
          image = data.image.url;
        }
      }
      return (
        <Fragment>
          <div className="wrap-top">
            <img className="img-fluid" src={image} alt="" />
            <h4>{title}</h4>
          </div>
          <div className="wrap-main">
            <div className="info-short-desc">
              <div className="left">
                <p className="label">{rewards ? "Điểm:" : "Mã phiếu:"}</p>
                <p>{rewards ? rewards : code}</p>
              </div>
              <div className="right">
                <p className="label">Hiệu lực:</p>
                <p className="end-time">{startTime + " đến " + endTime}</p>
              </div>
            </div>
            <div className="info-desc">
              {yieldEventApply}
              <h4 className="title">Mô tả</h4>
              {payment ? <p>{payment}</p> : ""}
              {description ? (
                <div dangerouslySetInnerHTML={{ __html: description }} />
              ) : (
                ""
              )}
            </div>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <Modal
        title="Chi tiết quà"
        className="block-modal-detail-coupon"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modalDetailCoupon}
        onCancel={onCloseModalDetailCoupon}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetailCoupon()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

export default ModalDetailCoupon;
