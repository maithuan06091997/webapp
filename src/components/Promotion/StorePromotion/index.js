import React, { Fragment, useEffect, useRef, useState } from "react";
import _ from "lodash";
import { Empty } from "antd";
import { public_url } from "../../../constants/Path";
import { FaChevronRight } from "react-icons/fa";
import BoxSkeleton from "../../BoxSkeleton";
import ModalPointHistory from "../ModalPointHistory";
import ModalDetailVoucher from "../ModalDetailVoucher";
import ModalFilterPromotion from "../ModalFilterPromotion";
import { ModelStorePromotion } from "../../../models/ModelPromotion";
import "./style.scss";

const StorePromotion = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [openModalPointHistory, setOpenModalPointHistory] = useState(false);
  const [openModalFilterPromotion, setOpenModalFilterPromotion] = useState(
    false
  );
  const [openModalDetailVoucher, setOpenModalDetailVoucher] = useState(false);
  const [filterPromotion, setFilterPromotion] = useState(undefined);
  const [detailVoucher, setDetailVoucher] = useState(undefined);
  const [listStore, setListStore] = useState([]);
  //*** Declare props ***//
  const { balance } = props;

  //*** Handle yield list store promotion ***//
  useEffect(() => {
    setSkeleton(true);
    ModelStorePromotion()
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataListStore = resultFt.data.one_health_msg;
          setListStore(dataListStore);
          setSkeleton(false);
        }
        return null;
      })
      .catch((error) => {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("StorePromotion - ListStore");
            setSkeleton(false);
          }
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, []);

  const onOpenModalPointHistory = (flag) => {
    setOpenModalPointHistory(flag);
  };

  const onOpenModalFilterPromotion = (flag, item) => {
    setOpenModalFilterPromotion(flag);
    setFilterPromotion(item);
  };

  const onOpenModalDetailVoucher = (flag, item) => {
    setOpenModalDetailVoucher(flag);
    setDetailVoucher(item);
  };

  function yieldVoucher(obj) {
    if (obj.length > 0) {
      return _.map(obj.slice(0, 3), (item, index) => {
        return (
          <div
            key={index}
            onClick={onOpenModalDetailVoucher.bind(this, true, item)}
            className="col-xl-4 col-lg-4 col-md-4 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-top">
                <img className="img-fluid" src={item.image.url} alt="" />
              </div>
              <div className="wrap-main">
                <div className="label">{item.title}</div>
                <div className="info">
                  <span>{item.rewards.toLocaleString()}</span> điểm
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      return <Empty className="w-100" description={"Cửa hàng quà trống"} />;
    }
  }

  function yieldListStore() {
    if (listStore.length > 0) {
      return _.map(listStore, (item, index) => {
        return (
          <div key={index} className="wrap-panel-line">
            <div className="wrap-content-header">
              <div className="title">{item.typeName}</div>
              <div
                onClick={onOpenModalFilterPromotion.bind(
                  this,
                  true,
                  item.typeId
                )}
                className="read-more"
              >
                Xem tất cả ({item.total})
              </div>
            </div>
            <div className="wrap-content-body">
              <div className="row">{yieldVoucher(item.datas)}</div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description={"Cửa hàng quà trống"}
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <div className="wrap-coupon-point">
        <img
          className="img-fluid"
          src={public_url.img + "/icons/icon_reward.png"}
          alt=""
        />
        <p onClick={onOpenModalPointHistory.bind(this, true)}>
          Bạn đang có <span>{balance.point.toLocaleString()}</span> điểm
        </p>
        <FaChevronRight />
      </div>
      <div
        className={
          skeleton ? "skeleton wrap-panel-content" : "wrap-panel-content"
        }
      >
        <BoxSkeleton
          skeleton={skeleton}
          full={true}
          length={2}
          rows={5}
          data={yieldListStore()}
        />
      </div>
      <ModalPointHistory
        {...props}
        onOpenModalPointHistory={onOpenModalPointHistory}
        openModalPointHistory={openModalPointHistory}
      />
      <ModalFilterPromotion
        {...props}
        onOpenModalFilterPromotion={onOpenModalFilterPromotion}
        openModalFilterPromotion={openModalFilterPromotion}
        listStore={listStore}
        filterPromotion={filterPromotion}
      />
      <ModalDetailVoucher
        {...props}
        onOpenModalDetailVoucher={onOpenModalDetailVoucher}
        openModalDetailVoucher={openModalDetailVoucher}
        detailVoucher={detailVoucher}
      />
    </Fragment>
  );
};

export default StorePromotion;
