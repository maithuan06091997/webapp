import React, { Fragment, useEffect, useState } from "react";
import _ from "lodash";
import i18next from "i18next";
import { Button, Empty, Form, Input, Modal } from "antd";
import { BsSearch } from "react-icons/bs";
import ModalDetailCoupon from "../ModalDetailCoupon";
import { ModelSearchPromotion } from "../../../models/ModelPromotion";
import BoxSkeleton from "../../BoxSkeleton";
import * as func from "../../../helpers";
import "./style.scss";

const ModalFindCoupon = (props) => {
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(false);
  const [modalFindCoupon, setModalFindCoupon] = useState(false);
  const [openModalDetailCoupon, setOpenModalDetailCoupon] = useState(false);
  const [detailCoupon, setDetailCoupon] = useState(false);
  const [searchPromotion, setSearchPromotion] = useState([]);
  //*** Declare props ***//
  const {
    _TokenExpired,
    handleUpdateListOwnerGift,
    onOpenModalFindCoupon,
    openModalFindCoupon,
  } = props;

  const [form] = Form.useForm();
  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    if (openModalFindCoupon) {
      form.resetFields();
      setModalFindCoupon(openModalFindCoupon);
    }
  }, [openModalFindCoupon, form]);

  const onFinish = (values) => {
    if (values.itemCouponCode) {
      const code = values.itemCouponCode.trim();
      setSkeleton(true);
      setLoading(true);
      ModelSearchPromotion(code)
        .then((resultFt) => {
          const dataSearchPromotion = resultFt.data.one_health_msg;
          setSearchPromotion(dataSearchPromotion);
          setSkeleton(false);
          setLoading(false);
        })
        .catch((error) => {
          if (error.code !== "007") {
            const msg = error.message ? error.message : error.description;
            modal.warning({
              title: "Thông báo",
              okText: "Đóng",
              centered: true,
              content: i18next.t(msg),
            });
            setSkeleton(false);
            setLoading(false);
          } else {
            _TokenExpired(error);
          }
        });
    }
  };

  const onCloseModalFindCoupon = () => {
    onOpenModalFindCoupon(false);
    setSearchPromotion([]);
    setModalFindCoupon(false);
  };

  const onOpenModalDetailCoupon = (flag, item) => {
    setOpenModalDetailCoupon(flag);
    setDetailCoupon(item);
  };

  const handleApplyLater = () => {
    onCloseModalFindCoupon();
    handleUpdateListOwnerGift();
  };

  function yieldDataSearchPromotion() {
    if (searchPromotion.length > 0) {
      return _.map(searchPromotion, (item, index) => {
        return (
          <div
            key={index}
            onClick={onOpenModalDetailCoupon.bind(this, true, item)}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-top">
                <img className="img-fluid" src={item.image.url} alt="" />
              </div>
              <div className="wrap-main">
                <div className="label">{item.title}</div>
                <div className="end-time">
                  <strong>Hiệu lực đến: </strong>
                  <span>{func.yieldDatetime(item.end, "date")}</span>
                </div>
                <div className="info">
                  {func.findTypePayment(item.payment, "text")}
                </div>
                <div className="ant-btn btn-accept">
                  <span>Sử dụng bây giờ</span>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Mã khuyến mãi trống"
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <Modal
        title="Tìm mã khuyến mãi"
        className="block-modal-find-coupon ant-modal-custom"
        closable={true}
        maskClosable={false}
        width={600}
        footer={null}
        visible={modalFindCoupon}
        onCancel={onCloseModalFindCoupon}
      >
        <Form
          onFinish={onFinish}
          form={form}
          size="large"
          className="form-find-coupon-code"
          name="form-find-coupon-code"
        >
          <Input.Group compact>
            <Form.Item name="itemCouponCode">
              <Input placeholder="Nhập mã khuyến mãi" />
            </Form.Item>{" "}
            <Form.Item>
              <Button
                loading={loading}
                className="btn-accept"
                htmlType="submit"
              >
                <BsSearch /> <span>Tìm</span>
              </Button>
            </Form.Item>
          </Input.Group>
        </Form>
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={skeleton}
              full={false}
              length={10}
              rows={3}
              data={yieldDataSearchPromotion()}
            />
          </div>
        </div>
      </Modal>
      <ModalDetailCoupon
        {...props}
        onOpenModalDetailCoupon={onOpenModalDetailCoupon}
        openModalDetailCoupon={openModalDetailCoupon}
        handleApplyLater={handleApplyLater}
        detailCoupon={detailCoupon}
        typeAction="find_coupon"
      />
      {contextHolder}
    </Fragment>
  );
};

export default ModalFindCoupon;
