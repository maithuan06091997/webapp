import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import {Empty, Input} from "antd";
import {public_url} from "../../../constants/Path";
import {FaChevronRight} from "react-icons/fa";
import BoxSkeleton from "../../BoxSkeleton";
import ModalPointHistory from "../ModalPointHistory";
import ModalFindCoupon from "../ModalFindCoupon";
import ModalDetailCoupon from "../ModalDetailCoupon";
import {ModelOwnerGift} from "../../../models/ModelPromotion";
import * as func from "../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 12;

const MyPromotion = (props) => {
    const isRendered = useRef(false);
    const prevBalancePoint = useRef();
    const [skeleton, setSkeleton] = useState(false);
    const [openModalPointHistory, setOpenModalPointHistory] = useState(false);
    const [openModalFindCoupon, setOpenModalFindCoupon] = useState(false);
    const [openModalDetailCoupon, setOpenModalDetailCoupon] = useState(false);
    const [detailCoupon, setDetailCoupon] = useState(undefined);
    const [listOwnerGift, setListOwnerGift] = useState([]);
    const [limitRecord, setLimitRecord] = useState([]);
    const [page, setPage] = useState(1);
    //*** Declare props ***//
    const {balance} = props;

    //*** Handle yield list owner gift ***//
    const handleOwnerGift = useCallback((page, array) => {
        const reqParamFt = {
            page: page,
            limit: LIMIT
        }
        setSkeleton(true);
        ModelOwnerGift(reqParamFt).then(resultFt => {
            if (!isRendered.current) {
                const dataListOwnerGift = resultFt.data.one_health_msg;
                const tmpArray = array.concat(dataListOwnerGift);
                setListOwnerGift(tmpArray);
                setLimitRecord(dataListOwnerGift);
                setSkeleton(false);
            }
            return null;
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("MyPromotion - OwnerGift");
                    setSkeleton(false);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, []);

    useEffect(() => {
        if (!isRendered.current) {
            handleOwnerGift(1, []);
            prevBalancePoint.current = balance.point;
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleOwnerGift, balance.point]);

    //*** Handle update list owner gift when balance point change ***//
    useEffect(() => {
        if (prevBalancePoint.current !== balance.point) {
            isRendered.current       = false;
            prevBalancePoint.current = balance.point;
            handleOwnerGift(1, []);
        }
    }, [handleOwnerGift, balance.point]);

    const onNextPage = () => {
        const next = page+1;
        setPage(next);
        handleOwnerGift(next, listOwnerGift);
    };

    const onOpenModalPointHistory = flag => {
        setOpenModalPointHistory(flag);
    };

    const onOpenModalFindCoupon = flag => {
        setOpenModalFindCoupon(flag);
    };

    const onOpenModalDetailCoupon = (flag, item) => {
        setOpenModalDetailCoupon(flag);
        setDetailCoupon(item);
    };

    const handleUpdateListOwnerGift = () => {
        handleOwnerGift(1, []);
    };

    function yieldListOwnerGift() {
        if (listOwnerGift.length > 0) {
            return _.map(listOwnerGift, (item, index) => {
                const type      = item.type;
                const endTime   = func.yieldDatetime(item.expire, "date");
                let data, payment;
                if (type === "voucher") {
                    data = item.voucher;
                } else if (type === "promotion") {
                    data    = item.promotion;
                    payment = func.findTypePayment(data.payment, "text");
                }
                return (
                    <div key={index}
                         onClick={onOpenModalDetailCoupon.bind(this, true, item)}
                         className="col-xl-4 col-lg-4 col-md-4 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-top">
                                <img className="img-fluid" src={data.image.url} alt=""/>
                            </div>
                            <div className="wrap-main">
                                <div className="label">{data.title}</div>
                                <div className="end-time">
                                    <strong>Hiệu lực đến: </strong>
                                    <span>{endTime}</span>
                                </div>
                                <div className="info">{payment ? payment : ""}</div>
                                <div className="ant-btn btn-accept"><span>Sử dụng bây giờ</span></div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description={"Quà của tôi trống"}/>);
            }
        }
    }

    function yieldPaginationAction() {
        if (limitRecord.length === LIMIT) {
            return (
                <div className="wrap-lazy-pagination">
                    <hr className="w-100"/>
                    <div onClick={onNextPage} className="ant-btn btn-accept ant-btn-lg"><span>Xem thêm</span></div>
                </div>
            );
        }
        return null;
    }

    return (
        <Fragment>
            <div className="wrap-find-coupon">
                <Input onClick={onOpenModalFindCoupon.bind(this, true)}
                       readOnly={true}
                       placeholder="Tìm mã khuyến mãi"/>
            </div>
            <div className="wrap-coupon-point">
                <img className="img-fluid" src={public_url.img + "/icons/icon_reward.png"} alt=""/>
                <p onClick={onOpenModalPointHistory.bind(this, true)}>
                    Bạn đang có <span>{balance.point.toLocaleString()}</span> điểm
                </p>
                <FaChevronRight/>
            </div>
            <div className="wrap-panel-content">
                <div className="row">
                    <BoxSkeleton skeleton={skeleton}
                                 full={false}
                                 panelCol={3}
                                 length={9}
                                 rows={3}
                                 data={yieldListOwnerGift()}/>
                </div>
            </div>
            {yieldPaginationAction()}
            <ModalPointHistory {...props}
                               onOpenModalPointHistory={onOpenModalPointHistory}
                               openModalPointHistory={openModalPointHistory}/>
            <ModalFindCoupon {...props}
                             onOpenModalFindCoupon={onOpenModalFindCoupon}
                             openModalFindCoupon={openModalFindCoupon}
                             handleUpdateListOwnerGift={handleUpdateListOwnerGift}/>
            <ModalDetailCoupon {...props}
                               onOpenModalDetailCoupon={onOpenModalDetailCoupon}
                               detailCoupon={detailCoupon}
                               handleUpdateListOwnerGift={handleUpdateListOwnerGift}
                               openModalDetailCoupon={openModalDetailCoupon}
                               typeAction="coupon"/>
        </Fragment>
    );
};

export default MyPromotion;