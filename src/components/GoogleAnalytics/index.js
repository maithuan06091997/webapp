import React, { useEffect } from "react";
import ReactGA from "react-ga4";
import { useLocation } from "react-router-dom";

ReactGA.initialize("G-W985BQY1YV");
function GoogleAnalytics(props) {
  const location = useLocation();
  const pageView = location.pathname + location.search;
  useEffect(() => {
    ReactGA.send({
      hitType: "pageview",
      page: pageView,
      title: pageView,
    });
  }, [pageView]);
  return <></>;
}

export default GoogleAnalytics;
