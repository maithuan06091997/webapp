import React, { Fragment, useEffect } from "react";
import MetaTags from "react-meta-tags";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./VideoCallLayout.scss";

const VideoCallLayout = (props) => {
  useEffect(() => {
    document.documentElement.classList.add("other-layout");
    return () => {
      document.documentElement.classList.remove("other-layout");
    };
  }, []);

  return (
    <Fragment>
      <MetaTags>
        <title>Dr.OH - Bệnh viện đa khoa bỏ túi</title>
        <meta name="description" content="Dr.OH - Bệnh viện đa khoa bỏ túi" />
      </MetaTags>
      <div className="block-wrap-video-call">{props.children}</div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(VideoCallLayout)
);
