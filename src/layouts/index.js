import MainLayout from "./MainLayout";
import OtherLayout from "./OtherLayout";
import NotFoundLayout from "./NotFoundLayout";
import VideoCallLayout from "./VideoCallLayout";
import ChatLayout from "./ChatLayout";

const Layouts = {
  MainLayout,
  OtherLayout,
  VideoCallLayout,
  NotFoundLayout,
  ChatLayout,
};

export default Layouts;
