import React, { Fragment, useEffect, useState } from "react";
import MetaTags from "react-meta-tags";
import { Layout, Menu, Modal } from "antd";
import { url, public_url } from "../constants/Path";
import { FaUserCircle } from "react-icons/fa";
import MenuLeft from "./Common/MenuLeft";
import WrapNotify from "../components/WrapNotify";
import WrapProfile from "../components/WrapProfile";
import MenuMobile from "./Common/MenuMobile";
import InComingVideoCall from "./Common/InComingVideoCall";
import { _SignOut, _Appointment, _Confirmation } from "../actions";
import { withRouter, Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as func from "../helpers";
import "./ChatLayout.scss";

const { Header, Content, Footer } = Layout;
const arrayMainMenu = [
  url.name.home,
  url.name.account_list_appointment_schedule,
];

const ChatLayout = (props) => {
  const [menuActive, setMenuActive] = useState(["0"]);
  const [userInfo, setUserInfo] = useState(undefined);
  //*** Declare props ***//
  const {
    _SignOut,
    _CleanBalanceUser,
    _Confirmation,
    _Appointment,
    widthScreen,
    path,
    user,
  } = props;

  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    //*** Handle yield info user ***//
    const fullInfoUser = func.yieldFullInfoUser(user.personal);
    setUserInfo(fullInfoUser);

    //*** Handle status active main menu ***//
    const indexMainMenu = arrayMainMenu.findIndex((value) => {
      return value === path;
    });
    if (indexMainMenu > -1) {
      const key = indexMainMenu + 1;
      setMenuActive([key.toString()]);
    } else {
      setMenuActive(["0"]);
    }

    //*** Handle empty data of the appointment form ***//
    const tmpPath = path ? path.split("/") : "";

    //*** Page confirmation ***//
    const isConfirmation = tmpPath.indexOf("confirmation");
    if (isConfirmation === -1) {
      _Confirmation({
        status: false,
        type: "",
        name: "",
        gateway: "",
      });
    }
  }, [_Confirmation, _Appointment, path, user]);

  const onSelectMenu = (item) => {
    setMenuActive(item.keyPath);
  };

  const onSignOut = () => {
    _SignOut();
    _CleanBalanceUser();
  };

  let yieldAvatar;
  if (user.avatar && user.avatar.url) {
    yieldAvatar = <img className="img-fluid" src={user.avatar.url} alt="" />;
  } else {
    yieldAvatar = <FaUserCircle size={`4.5em`} />;
  }

  let yieldMenu = (
    <Menu
      className="wrap-home-menu d-block"
      theme="dark"
      mode="horizontal"
      onSelect={onSelectMenu}
      selectedKeys={menuActive}
    >
      <Menu.Item key="1">
        <Link title="Trang chủ" to={url.name.home}>
          <img
            className="img-fluid"
            src={
              menuActive[0] === "1"
                ? public_url.img + "/icons/icon_home_active.png"
                : public_url.img + "/icons/icon_home.png"
            }
            alt=""
          />
        </Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link title="Lịch hẹn" to={url.name.account_list_appointment_schedule}>
          <img
            className="img-fluid"
            src={
              menuActive[0] === "2"
                ? public_url.img + "/icons/icon_schedule_active.png"
                : public_url.img + "/icons/icon_schedule.png"
            }
            alt=""
          />
        </Link>
      </Menu.Item>
      <Menu.Item key="3">
        <Link title="Cộng đồng" to={url.name.social_network}>
          <img
            className="img-fluid"
            src={
              menuActive[0] === "3"
                ? public_url.img + "/icons/icon_social_active.png"
                : public_url.img + "/icons/icon_social.png"
            }
            alt=""
          />
        </Link>
      </Menu.Item>
      <Menu.Item key="4">
        <Link title="CarePay" to={url.name.care_pay}>
          <img
            className="img-fluid"
            src={
              menuActive[0] === "4"
                ? public_url.img + "/icons/icon_carepay_active.png"
                : public_url.img + "/icons/icon_carepay.png"
            }
            alt=""
          />
        </Link>
      </Menu.Item>
    </Menu>
  );

  if (widthScreen <= 991) {
    yieldMenu = <MenuMobile {...props} />;
  }

  const yieldLeftLayout = <MenuLeft {...props} />;

  return (
    <Fragment>
      <MetaTags>
        <title>Dr.OH - Bệnh viện đa khoa bỏ túi</title>
        <meta name="description" content="Dr.OH - Bệnh viện đa khoa bỏ túi" />
      </MetaTags>
      <Layout className="chat-layout">
        <Header style={{ position: "fixed", zIndex: 99, width: "100%" }}>
          <Link to={url.name.home} className="logo">
            <img src={public_url.img + "/logo_oh_round.png"} alt="Dr.OH" />
            <span>Bệnh viện đa khoa</span>
          </Link>
          {yieldMenu}
          <div className="nav-top-right">
            <div className="menu-avatar">
              <div className="menu-avatar-left">{yieldAvatar}</div>
              <div className="menu-avatar-right">
                <strong>
                  {userInfo && userInfo.full_name
                    ? userInfo.full_name
                    : "Guest"}
                </strong>
              </div>
            </div>
            <ul className="navbar-nav">
              <li className="nav-item mr-3">
                <div className="navbar-nav-link">
                  <WrapNotify {...props} />
                </div>
              </li>
              <li className="nav-item">
                <div className="navbar-nav-link">
                  <WrapProfile
                    {...props}
                    onSignOut={onSignOut}
                    userInfo={userInfo}
                  />
                </div>
              </li>
            </ul>
          </div>
        </Header>
        <Layout style={{ marginTop: 64 }}>
          {yieldLeftLayout}
          <Content>
            <div className="site-layout-content">
              <div className="block-main">{props.children}</div>
            </div>
          </Content>
        </Layout>
        <Footer className="text-center">Dr.OH © 2020</Footer>
      </Layout>
      <InComingVideoCall {...props} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      _SignOut,
      _Appointment,
      _Confirmation,
    },
    dispatch
  );
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(ChatLayout)
);
