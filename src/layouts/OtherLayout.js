import React, { Fragment, useEffect } from "react";
import MetaTags from "react-meta-tags";
import { Modal } from "antd";
import { msg_text } from "../constants/Message";
import { _CleanStoreUser, _ChangePassword } from "../actions";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import "./OtherLayout.scss";

const OtherLayout = (props) => {
  //*** Declare props ***//
  const { _CleanStoreUser, _ChangePassword, _CleanBalanceUser, user } = props;

  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    document.documentElement.classList.add("other-layout");
    return () => {
      document.documentElement.classList.remove("other-layout");
    };
  }, []);

  useEffect(() => {
    if (user.update) {
      modal.success({
        title: "Thành công",
        okText: "Đóng",
        centered: true,
        content: msg_text.update_pass_success,
      });
      _CleanBalanceUser();
      _ChangePassword(false);
    }
    if (user.type === "token_expired") {
      modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: msg_text.token_expired,
      });
      _CleanBalanceUser();
      _CleanStoreUser();
    }
  }, [
    _CleanStoreUser,
    _CleanBalanceUser,
    _ChangePassword,
    user.type,
    user.update,
    modal,
  ]);

  return (
    <Fragment>
      <MetaTags>
        <title>Dr.OH - Bệnh viện đa khoa bỏ túi</title>
        <meta name="description" content="Dr.OH - Bệnh viện đa khoa bỏ túi" />
      </MetaTags>
      <div className="block-wrap-index">{props.children}</div>
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CleanStoreUser, _ChangePassword }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(OtherLayout)
);
