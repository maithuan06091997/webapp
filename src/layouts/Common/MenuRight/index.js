import React, {Fragment} from "react";
import {Layout} from "antd";

const {Sider} = Layout;

const MenuRight = (props) => {
    return (
        <Fragment>
            <Sider theme={"light"} width={250} className="sider-layout-right"/>
        </Fragment>
    );
};

export default MenuRight;