import React, { Fragment } from "react";
import { Layout } from "antd";
import NavLink from "../../../components/NavLink";
import { url, public_url } from "../../../constants/Path";
import { hotline } from "../../../constants/Default";
// import {Link} from "react-router-dom";

// let wheeling;
const { Sider } = Layout;

const MenuLeft = (props) => {
  //*** Declare props ***//
  const { heightScreen } = props;

  // const mouseWheel = e => {
  //     const element = document.getElementById("block-sider-layout-menu");
  //     element.classList.add("mystyle");
  //     if (!wheeling) {
  //         console.log('start wheeling!');
  //         element.classList.add("mystyle");
  //     }
  //
  //     clearTimeout(wheeling);
  //     wheeling = setTimeout(function() {
  //         console.log('stop wheeling!');
  //         wheeling = undefined;
  //         element.classList.remove("mystyle");
  //     }, 250);
  // } onWheel={mouseWheel}

  function yieldMenu() {
    return (
      <Fragment>
        <ul>
          <li>
            <NavLink to={url.name.account_notification}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_notification.png"}
                  alt=""
                />
              </div>
              <span>Thông báo</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_detail_personal}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_user.png"}
                  alt=""
                />
              </div>
              <span>Hồ sơ cá nhân</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_change_password}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_change_pass.png"}
                  alt=""
                />
              </div>
              <span>Đổi mật khẩu</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_list_relative_profile}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_profile.png"}
                  alt=""
                />
              </div>
              <span>Hồ sơ người thân</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_promotion}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_gift.png"}
                  alt=""
                />
              </div>
              <span>Ưu đãi</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_list_appointment_schedule}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_calendar.png"}
                  alt=""
                />
              </div>
              <span>Lịch hẹn</span>
            </NavLink>
          </li>
          {/* <li>
            <NavLink to={url.name.account_list_history_examination}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_history.png"}
                  alt=""
                />
              </div>
              <span>Lịch sử đặt khám</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_list_history_call}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_history.png"}
                  alt=""
                />
              </div>
              <span>Lịch sử cuộc gọi</span>
            </NavLink>
          </li> */}
          <li>
            <NavLink to={url.name.account_list_invoice}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_history.png"}
                  alt=""
                />
              </div>
              <span>Lịch sử giao dịch</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_list_history}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/specialist/ambulance.png"}
                  alt=""
                />
              </div>
              <span>Sức khỏe của tôi</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.chat}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/myhealth/ic_chat.png"}
                  alt=""
                />
              </div>
              <span>Trò chuyện</span>
            </NavLink>
          </li>
          <li>
            <NavLink to={url.name.account_wallet}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_wallet.png"}
                  alt=""
                />
              </div>
              <span>Số dư DR.OH</span>
            </NavLink>
          </li>
        </ul>
        <hr />
        <ul>
          <li>
            <a href={"tel:" + hotline}>
              <div className="round-frame mr-2">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_cskh.png"}
                  alt=""
                />
              </div>
              <span>
                Gọi CSKH
                <br />({hotline})
              </span>
            </a>
          </li>
          {/*<li>
                        <Link to={""}>
                            <div className="round-frame mr-2">
                                <img
                                    className="img-fluid"
                                    src={public_url.img + "/icons/icon_terms.png"}
                                    alt=""
                                />
                            </div>
                            <span>Điều khoản sử dụng</span>
                        </Link>
                    </li>*/}
          {/*<li>
                        <Link to={""}>
                            <div className="round-frame mr-2">
                                <img
                                    className="img-fluid"
                                    src={public_url.img + "/icons/icon_about.png"}
                                    alt=""
                                />
                            </div>
                            <span>Giới thiệu</span>
                        </Link>
                    </li>*/}
        </ul>
      </Fragment>
    );
  }

  return (
    <Fragment>
      <Sider
        theme={"light"}
        width={250}
        className="sider-layout-left scrollbar-2"
      >
        <div
          id="block-sider-layout-menu"
          className="block-sider-layout-menu"
          style={{ height: heightScreen - 133 }}
        >
          {yieldMenu()}
        </div>
      </Sider>
    </Fragment>
  );
};

export default MenuLeft;
