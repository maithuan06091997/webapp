import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import io from "socket.io-client";
import { Modal } from "antd";
import { url, public_url } from "../../../constants/Path";
import { FiPhoneCall } from "react-icons/fi";
import { setting } from "../../../api/Config";
import "./style.scss";

let socket;
let intervalMetaTags;

const InComingVideoCall = (props) => {
  const isRendered = useRef(false);
  const [audio] = useState(new Audio(public_url.media + "/so_ring01.mp3"));
  const [modalIncomingVideoCall, setModalIncomingVideoCall] = useState(false);
  const [incomingCallData, setIncomingCallData] = useState(undefined);
  //*** Declare props ***//
  const { path, user } = props;

  const patientConnect = useCallback(() => {
    if (user && user.personal.username) {
      socket
        .on("connect", () => {
          console.log("Connected");
          socket.emit("patient-connect", {
            phone: user.personal.username,
          });
        })
        .on("call-request", (data) => {
          socket.emit(
            "request-displayed",
            { room: data.extra.roomID },
            (isSuccess) => {
              if (isSuccess) {
                setIncomingCallData(data);
                setModalIncomingVideoCall(data);
              }
            }
          );
        })
        .on("leave", () => {
          clearInterval(intervalMetaTags);
          setIncomingCallData(undefined);
          setModalIncomingVideoCall(false);
        })
        .on("cancel-incoming", () => {
          clearInterval(intervalMetaTags);
          setIncomingCallData(undefined);
          setModalIncomingVideoCall(false);
        })
        .on("disconnect", (err) => {
          clearInterval(intervalMetaTags);
          setIncomingCallData(undefined);
          setModalIncomingVideoCall(false);
          console.log("disconnect", err);
        })
        .on("connect_error", (err) => {
          clearInterval(intervalMetaTags);
          console.log("Connection error", err);
        });
      socket.connect();
    }
  }, [user]);

  useEffect(() => {
    if (!isRendered.current) {
      if (!socket) {
        socket = io(setting.ohDomainVideoCall, { transports: ["websocket"] });
      }
      patientConnect();
    }
    return () => {
      isRendered.current = true;
    };
  }, [path, patientConnect, user]);

  const acceptVideoCall = () => {
    if (incomingCallData) {
      const widthPopup = 1200;
      const heightPopup = 800;
      const dualScreenLeft =
        window.screenLeft !== undefined ? window.screenLeft : window.screenX;
      const dualScreenTop =
        window.screenTop !== undefined ? window.screenTop : window.screenY;

      const width = window.innerWidth
        ? window.innerWidth
        : document.documentElement.clientWidth
        ? document.documentElement.clientWidth
        : window.screen.width;
      const height = window.innerHeight
        ? window.innerHeight
        : document.documentElement.clientHeight
        ? document.documentElement.clientHeight
        : window.screen.height;

      const systemZoom = width / window.screen.availWidth;
      const left = (width - widthPopup) / 2 / systemZoom + dualScreenLeft;
      const top = (height - heightPopup) / 2 / systemZoom + dualScreenTop;

      const windowFeatures = `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=${
        widthPopup / systemZoom
      },height=${heightPopup / systemZoom},left=${left},top=${top}`;
      const windowPopup = window.open(
        `${url.name.screen_video_call}`,
        "Video Call",
        windowFeatures
      );
      windowPopup.callData = incomingCallData;
      clearInterval(intervalMetaTags);
      setIncomingCallData(undefined);
      setModalIncomingVideoCall(false);
    }
  };

  const declineVideoCall = () => {
    if (socket.connected && incomingCallData && incomingCallData.extra) {
      socket.emit(
        "decline-from-client",
        { toSocket: incomingCallData.extra.from_socket },
        () => {}
      );
    }
    clearInterval(intervalMetaTags);
    setIncomingCallData(undefined);
    setModalIncomingVideoCall(false);
  };

  const onCloseModalIncomingVideoCall = () => {
    declineVideoCall();
  };

  let yieldAvatarVideoCall = (
    <img src={public_url.img + "/icons/icon_user_2.png"} alt="" />
  );
  let yieldDoctorName = "N/A";
  if (incomingCallData) {
    yieldAvatarVideoCall = (
      <img src={incomingCallData.extra.from_user.avatar.url} alt="" />
    );
    yieldDoctorName =
      incomingCallData.extra.from_user.last_name +
      " " +
      incomingCallData.extra.from_user.first_name;
  }

  if (modalIncomingVideoCall) {
    let count = 1;
    intervalMetaTags = setInterval(() => {
      if (count % 2 === 0) {
        document.title = "Bác sĩ " + yieldDoctorName + " đang gọi cho bạn...";
      } else {
        document.title = "Dr.OH - Bệnh viện đa khoa bỏ túi";
      }
      count++;
    }, 1000);
    audio
      .play()
      .then((_) => {})
      .catch(() => {});
  } else {
    clearInterval(intervalMetaTags);
    document.title = "Dr.OH - Bệnh viện đa khoa bỏ túi";
    audio.pause();
  }

  return (
    <Fragment>
      <Modal
        title="Cuộc gọi đến"
        className="block-modal-incoming-video-call"
        closable={false}
        maskClosable={false}
        footer={null}
        visible={modalIncomingVideoCall}
        onCancel={onCloseModalIncomingVideoCall}
        centered
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <div className="wrap-main">
              <div className="avatar">{yieldAvatarVideoCall}</div>
              <div className="info">
                <p className="title">
                  Bác sĩ <span>{yieldDoctorName}</span> đang gọi cho bạn.
                </p>
                <p className="text-muted">
                  Cuộc gọi sẽ bắt đầu ngay khi bạn trả lời.
                </p>
              </div>
            </div>
            <div className="wrap-event-action">
              <div className="box-button-event">
                <div onClick={declineVideoCall} className="ant-btn">
                  <span>Từ chối</span>
                </div>
                <div onClick={acceptVideoCall} className="ant-btn btn-primary">
                  <FiPhoneCall /> <span>Chấp nhận</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};

export default InComingVideoCall;
