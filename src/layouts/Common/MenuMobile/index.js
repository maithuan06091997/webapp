import React, { Fragment, useEffect, useState } from "react";
import Drawer from "react-drag-drawer";
import { hotline } from "../../../constants/Default";
import { public_url, url } from "../../../constants/Path";
import { FaAlignJustify, FaUserCircle } from "react-icons/fa";
import { Link } from "react-router-dom";
import * as func from "../../../helpers";
import "./style.scss";

const MenuMobile = (props) => {
  const [touch, setTouch] = useState(false);
  const [userInfo, setUserInfo] = useState(undefined);
  //*** Declare props ***//
  const { user } = props;

  useEffect(() => {
    const fullInfoUser = func.yieldFullInfoUser(user.personal);
    setUserInfo(fullInfoUser);
  }, [user.personal]);

  const onTouch = (value) => (event) => {
    setTouch(value);
  };

  let yieldAvatar;
  if (user.avatar && user.avatar.url) {
    yieldAvatar = <img className="img-fluid" src={user.avatar.url} alt="" />;
  } else {
    yieldAvatar = <FaUserCircle size={`3.5em`} />;
  }

  return (
    <Fragment>
      <div className="block-menu-mobile">
        <button
          onClick={onTouch(true)}
          className="navbar-toggler sidebar-mobile-main-toggle"
          type="button"
        >
          <FaAlignJustify color="#333333" size="1.5em" />
        </button>
        <Drawer
          open={touch}
          direction="left"
          onRequestClose={onTouch(false)}
          modalElementClass="sidebar-mobi"
        >
          <ul className="nav nav-sidebar wrap-nav-profile">
            <li className="nav-item-profile">
              <div className="avatar">{yieldAvatar}</div>
              <div className="personal">
                <div>
                  <strong>
                    {userInfo && userInfo.full_name
                      ? userInfo.full_name
                      : "Guest"}
                  </strong>
                </div>
                <div>
                  <strong className="text-muted">
                    {userInfo && userInfo.phone ? userInfo.phone : "-"}
                  </strong>
                </div>
              </div>
            </li>
          </ul>
          <ul className="nav nav-sidebar" data-nav-type="accordion">
            <Link
              to={url.name.home}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_home_active.png"}
                  alt=""
                />
              </div>
              <span>
                <b>Trang chủ</b>
              </span>
            </Link>
            <Link
              to={url.name.account_list_appointment_schedule}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_schedule_active.png"}
                  alt=""
                />
              </div>
              <span>
                <b>Lịch hẹn</b>
              </span>
            </Link>
            <Link
              to={url.name.social_network}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_social_active.png"}
                  alt=""
                />
              </div>
              <span>
                <b>Cộng đồng</b>
              </span>
            </Link>
            <Link
              to={url.name.social_network}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_carepay_active.png"}
                  alt=""
                />
              </div>
              <span>
                <b>CarePay</b>
              </span>
            </Link>
          </ul>
          <hr />
          <ul className="nav nav-sidebar" data-nav-type="accordion">
            <Link
              to={url.name.account_notification}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_notification.png"}
                  alt=""
                />
              </div>
              <span>Thông báo</span>
            </Link>
            <Link
              to={url.name.account_detail_personal}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_user.png"}
                  alt=""
                />
              </div>
              <span>Hồ sơ cá nhân</span>
            </Link>
            <Link
              to={url.name.account_change_password}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_change_pass.png"}
                  alt=""
                />
              </div>
              <span>Đổi mật khẩu</span>
            </Link>
            {/* <Link
              to={url.name.account_wallet}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/ic_wallet.png"}
                  alt=""
                />
              </div>
              <span>Nạp tiền vào tài khoản</span>
            </Link> */}
            <Link
              to={url.name.account_list_relative_profile}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_profile.png"}
                  alt=""
                />
              </div>
              <span>Hồ sơ người thân</span>
            </Link>
            <Link
              to={url.name.account_promotion}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_gift.png"}
                  alt=""
                />
              </div>
              <span>Ưu đãi</span>
            </Link>
            <Link
              to={url.name.account_list_appointment_schedule}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_calendar.png"}
                  alt=""
                />
              </div>
              <span>Lịch hẹn</span>
            </Link>
            <Link
              to={url.name.account_list_history_call}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_history.png"}
                  alt=""
                />
              </div>
              <span>Lịch sử cuộc gọi</span>
            </Link>
            <Link
              to={url.name.account_list_invoice}
              onClick={onTouch(false)}
              className="nav-item"
            >
              <div className="round-frame">
                <img
                  className="img-fluid"
                  src={public_url.img + "/icons/icon_history.png"}
                  alt=""
                />
              </div>
              <span>Lịch sử giao dịch</span>
            </Link>
          </ul>
          <hr />
          <ul className="nav nav-sidebar" data-nav-type="accordion">
            <li>
              <a
                href={"tel:" + hotline}
                onClick={onTouch(false)}
                className="nav-item"
              >
                <div className="round-frame">
                  <img
                    className="img-fluid"
                    src={public_url.img + "/icons/icon_cskh.png"}
                    alt=""
                  />
                </div>
                <span>
                  Gọi CSKH
                  <br />({hotline})
                </span>
              </a>
            </li>
          </ul>
        </Drawer>
      </div>
    </Fragment>
  );
};

export default MenuMobile;
