import React, { Fragment, useEffect, useState } from "react";
import { url } from "./constants/Path";
import { withTranslation } from "react-i18next";
import {
  _TokenExpired,
  _FirebaseTokenActive,
  _BrowserNotifyActive,
  _BadgeNotification,
  _BalanceUser,
  _CleanBalanceUser,
} from "./actions";
import { Redirect } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

const Role = (props) => {
  const [windowSize, setWindowSize] = useState({
    width: window.innerWidth,
    height: window.innerHeight,
  });
  const {
    _TokenExpired,
    _BalanceUser,
    _FirebaseTokenActive,
    _BrowserNotifyActive,
    _BadgeNotification,
    layout,
    component,
    path,
    auth,
    user,
    balance,
    language,
    notify,
    ck_firebase,
    browser,
    verify,
    confirm,
    appointment,
    category_service,
    drug_store,
    home_test,
    i18n,
    call_chat,
  } = props;

  // const handleFirebaseToken = useCallback(
  //   (token, name) => {
  //     const reqParamFt = {
  //       firebase_token: {
  //         user: {
  //           id: user.personal._id,
  //         },
  //         os: "webdroh",
  //         token: "webdroh-" + new Date().getTime(),
  //         voip_token: "webdroh-" + new Date().getTime(),
  //         web_token: token,
  //         device_id: getUA,
  //       },
  //     };
  //     ModelFirebaseToken(reqParamFt).then(() => {
  //       _FirebaseTokenActive({ token: true, name: name });
  //     });
  //   },
  //   [_FirebaseTokenActive, user.personal._id]
  // );

  const isNotifySupported = () =>
    "Notification" in window &&
    "serviceWorker" in navigator &&
    "PushManager" in window;

  useEffect(() => {
    window.addEventListener("resize", handleResize.bind(this));
    return () => {
      window.removeEventListener("resize", handleResize.bind(this));
    };
  }, []);

  useEffect(() => {
    window.addEventListener("beforeunload", handleRestoreStorage.bind(this));
    return () => {
      window.removeEventListener(
        "beforeunload",
        handleRestoreStorage.bind(this)
      );
    };
  });

  // useEffect(() => {
  //   if (!user.auth) {
  //     _FirebaseTokenActive({ token: false, name: "" });
  //   }
  //   //*** Handle update balance and check token expired ***//
  //   if (user.personal && user.personal._id) {
  //     if (balance.expire === "007") {
  //       _TokenExpired({ code: balance.expire });
  //     } else {
  //       _BalanceUser();
  //       _BadgeNotification();
  //     }
  //   }
  // }, [
  //   _TokenExpired,
  //   _BalanceUser,
  //   _FirebaseTokenActive,
  //   _BadgeNotification,
  //   path,
  //   balance.expire,
  //   user.personal,
  //   user.auth,
  // ]);

  useEffect(() => {
    if (isNotifySupported()) {
      Promise.resolve(Notification.requestPermission()).then(function (
        permission
      ) {
        _BrowserNotifyActive({ notification: permission });
      });
    } else {
      _BrowserNotifyActive({ notification: "denied" });
    }
  }, [_BrowserNotifyActive, path]);

  // useEffect(() => {
  //   if (user.auth) {
  //     if (browser.notification !== "granted") {
  //       if (ck_firebase.name === "firebase" || !ck_firebase.name) {
  //         const token = "webdroh-" + new Date().getTime();
  //         handleFirebaseToken(token, "server");
  //       }
  //     } else {
  //       if (ck_firebase.name === "server" || !ck_firebase.name) {
  //         try {
  //           const messaging = firebase.messaging();
  //           messaging
  //             .getToken()
  //             .then((token) => {
  //               handleFirebaseToken(token, "firebase");
  //             })
  //             .catch(() => {
  //               if (!ck_firebase.name) {
  //                 const token = "webdroh-" + new Date().getTime();
  //                 handleFirebaseToken(token, "server");
  //               }
  //             });
  //         } catch (error) {
  //           if (!ck_firebase.name) {
  //             const token = "webdroh-" + new Date().getTime();
  //             handleFirebaseToken(token, "server");
  //           }
  //         }
  //       }
  //     }
  //   }
  // }, [browser.notification, user.auth, ck_firebase, handleFirebaseToken]);

  useEffect(() => {
    i18n.changeLanguage(language);
  }, [language, i18n]);

  const handleRestoreStorage = () => {
    localStorage.setItem("language", JSON.stringify(language));
    // localStorage.setItem("user", JSON.stringify(user));
    localStorage.setItem("balance", JSON.stringify(balance));
    localStorage.setItem("notify", JSON.stringify(notify));
    localStorage.setItem("ck_firebase", JSON.stringify(ck_firebase));
    localStorage.setItem("browser", JSON.stringify(browser));
    sessionStorage.setItem("confirm", JSON.stringify(confirm));
    sessionStorage.setItem("verify", JSON.stringify(verify));
    sessionStorage.setItem("appointment", JSON.stringify(appointment));
    sessionStorage.setItem(
      "category_service",
      JSON.stringify(category_service)
    );
    sessionStorage.setItem("drug_store", JSON.stringify(drug_store));
    sessionStorage.setItem("home_test", JSON.stringify(home_test));
    sessionStorage.setItem("call_chat", JSON.stringify(call_chat));
  };

  const handleResize = () => {
    setWindowSize({
      width: window.innerWidth,
      height: window.innerHeight,
    });
  };

  // if (auth && !user.auth) {
  //   return <Redirect to={url.name.index} />;
  // }

  const Layout = layout;
  const Component = component;
  return (
    <Fragment>
      <div className="container custom-mobile-container">
        <Component
          {...props}
          widthScreen={windowSize.width}
          heightScreen={windowSize.height}
        />
      </div>
    </Fragment>
  );
};

const shareStates = (state) => {
  return {
    user: state.user,
    language: state.language,
    verify: state.verify,
    appointment: state.appointment,
    category_service: state.category_service,
    drug_store: state.drug_store,
    home_test: state.home_test,
    call_chat: state.call_chat,
    global: state.global,
    confirm: state.confirm,
    notify: state.notify,
    ck_firebase: state.ck_firebase,
    browser: state.browser,
    balance: state.balance,
    test: state.test,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      _TokenExpired,
      _FirebaseTokenActive,
      _BadgeNotification,
      _BalanceUser,
      _BrowserNotifyActive,
      _CleanBalanceUser,
    },
    dispatch
  );
};

export default connect(shareStates, mapDispatchToProps, null, {
  forwardRef: true,
})(withTranslation()(Role));
