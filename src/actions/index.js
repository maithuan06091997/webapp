import {
  ActionSignIn,
  ActionSignOut,
  ActionCleanStoreUser,
  ActionUpdateUser,
  ActionUpdateAvatarUser,
  ActionRegister,
  ActionLanguageActive,
  ActionVerifyPhone,
  ActionFindProfile,
  ActionRecoverPassword,
  ActionAppointment,
  ActionCategoryService,
  ActionDrugStore,
  ActionUpdatePointPromotion,
  ActionListCity,
  ActionCallChat,
  ActionBrowserNotifyActive,
  ActionChangePassword,
  ActionTokenExpired,
  ActionConfirmation,
  ActionBadgeNotification,
  ActionBrowserCameraActive,
  ActionFirebaseTokenActive,
  ActionBalanceUser,
  ActionCleanBalanceUser,
  ActionTest,
  ActionHomeTest,
} from "./ActionType";

export const _SignIn = (data) => {
  return {
    type: ActionSignIn,
    data: data,
  };
};

export const _SignOut = (data) => {
  return {
    type: ActionSignOut,
    data: data,
  };
};

export const _RecoverPassword = (data) => {
  return {
    type: ActionRecoverPassword,
    data: data,
  };
};

export const _ChangePassword = (data) => {
  return {
    type: ActionChangePassword,
    data: data,
  };
};

export const _UpdateAvatarUser = (data) => {
  return {
    type: ActionUpdateAvatarUser,
    data: data,
  };
};

export const _UpdateUser = (data) => {
  return {
    type: ActionUpdateUser,
    data: data,
  };
};

export const _BalanceUser = (data) => {
  return {
    type: ActionBalanceUser,
    data: data,
  };
};

export const _CleanBalanceUser = () => {
  return {
    type: ActionCleanBalanceUser,
    data: "",
  };
};

export const _UpdatePointPromotion = (data) => {
  return {
    type: ActionUpdatePointPromotion,
    data: data,
  };
};

export const _TokenExpired = (data) => {
  return {
    type: ActionTokenExpired,
    data: data,
  };
};

export const _CleanStoreUser = () => {
  return {
    type: ActionCleanStoreUser,
    data: "",
  };
};

export const _Register = (data) => {
  return {
    type: ActionRegister,
    data: data,
  };
};

export const _LanguageActive = (data) => {
  return {
    type: ActionLanguageActive,
    data: data,
  };
};

export const _FirebaseTokenActive = (data) => {
  return {
    type: ActionFirebaseTokenActive,
    data: data,
  };
};

export const _BrowserNotifyActive = (data) => {
  return {
    type: ActionBrowserNotifyActive,
    data: data,
  };
};

export const _BrowserCameraActive = (data) => {
  return {
    type: ActionBrowserCameraActive,
    data: data,
  };
};

export const _VerifyPhone = (data) => {
  return {
    type: ActionVerifyPhone,
    data: data,
  };
};

export const _Confirmation = (data) => {
  return {
    type: ActionConfirmation,
    data: data,
  };
};

export const _FindProfile = (data) => {
  return {
    type: ActionFindProfile,
    data: data,
  };
};

export const _BadgeNotification = () => {
  return {
    type: ActionBadgeNotification,
    data: "",
  };
};

export const _ListCity = () => {
  return {
    type: ActionListCity,
    data: "",
  };
};

export const _Appointment = (data) => {
  return {
    type: ActionAppointment,
    data: data,
  };
};

export const _CategoryService = (data) => {
  return {
    type: ActionCategoryService,
    data: data,
  };
};

export const _DrugStore = (data) => {
  return {
    type: ActionDrugStore,
    data: data,
  };
};

export const _HomeTest = (data) => {
  return {
    type: ActionHomeTest,
    data: data,
  };
};

export const _CallChat = (data) => {
  return {
    type: ActionCallChat,
    data: data,
  };
};

export const _Test = (data) => {
  return {
    type: ActionTest,
    data: data,
  };
};
