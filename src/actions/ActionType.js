const ActionSignIn = "ActionSignIn";
const ActionSignOut = "ActionSignOut";
const ActionSignInSuccess = "ActionSignInSuccess";
const ActionSignInFailed = "ActionSignInFailed";
const ActionRegister = "ActionRegister";
const ActionRecoverPassword = "ActionRecoverPassword";
const ActionChangePassword = "ActionChangePassword";
const ActionUpdateUser = "ActionUpdateUser";
const ActionUpdateAvatarUser = "ActionUpdateAvatarUser";
const ActionBalanceUser = "ActionBalanceUser";
const ActionYieldBalanceUser = "ActionYieldBalanceUser";
const ActionCleanBalanceUser = "ActionCleanBalanceUser";
const ActionUpdatePointPromotion = "ActionUpdatePointPromotion";
const ActionTokenExpired = "ActionTokenExpired";
const ActionCleanStoreUser = "ActionCleanStoreUser";
const ActionLanguageActive = "ActionLanguageActive";
const ActionFirebaseTokenActive = "ActionFirebaseTokenActive";

const ActionBrowserNotifyActive = "ActionBrowserNotifyActive";
const ActionBrowserCameraActive = "ActionBrowserCameraActive";

const ActionConfirmation = "ActionConfirmation";
const ActionVerifyPhone = "ActionVerifyPhone";
const ActionFindProfile = "ActionFindProfile";

const ActionAppointment = "ActionAppointment";
const ActionCallChat = "ActionCallChat";
const ActionCategoryService = "ActionCategoryService";

const ActionDrugStore = "ActionDrugStore";
const ActionHomeTest = "ActionHomeTest";

const ActionBadgeNotification = "ActionBadgeNotification";
const ActionYieldNotification = "ActionYieldNotification";

const ActionListCity = "ActionListCity";
const ActionYieldListCity = "ActionYieldListCity";
const ActionTest = "ActionTest";

exports.ActionSignIn = ActionSignIn;
exports.ActionSignOut = ActionSignOut;
exports.ActionSignInSuccess = ActionSignInSuccess;
exports.ActionSignInFailed = ActionSignInFailed;
exports.ActionRegister = ActionRegister;
exports.ActionRecoverPassword = ActionRecoverPassword;
exports.ActionChangePassword = ActionChangePassword;
exports.ActionUpdateUser = ActionUpdateUser;
exports.ActionUpdateAvatarUser = ActionUpdateAvatarUser;
exports.ActionBalanceUser = ActionBalanceUser;
exports.ActionYieldBalanceUser = ActionYieldBalanceUser;
exports.ActionCleanBalanceUser = ActionCleanBalanceUser;
exports.ActionUpdatePointPromotion = ActionUpdatePointPromotion;
exports.ActionTokenExpired = ActionTokenExpired;
exports.ActionCleanStoreUser = ActionCleanStoreUser;
exports.ActionLanguageActive = ActionLanguageActive;
exports.ActionFirebaseTokenActive = ActionFirebaseTokenActive;

exports.ActionBrowserNotifyActive = ActionBrowserNotifyActive;
exports.ActionBrowserCameraActive = ActionBrowserCameraActive;

exports.ActionConfirmation = ActionConfirmation;
exports.ActionVerifyPhone = ActionVerifyPhone;
exports.ActionFindProfile = ActionFindProfile;

exports.ActionAppointment = ActionAppointment;
exports.ActionCategoryService = ActionCategoryService;
exports.ActionCallChat = ActionCallChat;

exports.ActionDrugStore = ActionDrugStore;
exports.ActionHomeTest = ActionHomeTest;

exports.ActionListCity = ActionListCity;
exports.ActionYieldListCity = ActionYieldListCity;

exports.ActionBadgeNotification = ActionBadgeNotification;
exports.ActionYieldNotification = ActionYieldNotification;
exports.ActionTest = ActionTest;
