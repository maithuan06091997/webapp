import React, {Fragment, useState, useEffect, useRef} from "react";
import io from "socket.io-client";
import i18next from "i18next";
import {public_url, url} from "../../constants/Path";
import { setting } from "../../api/Config";
import {Input, Modal} from "antd";
import {FiMicOff, FiMic, FiVideo, FiVideoOff} from "react-icons/fi";
import {GiPhone} from "react-icons/gi";
import Rating from "react-rating";
import Loading from "../../components/Loading";
import MediaDevice from "../../helpers/VideoCall/MediaDevice";
import PeerConnection from "../../helpers/VideoCall/PeerConnection";
import {ModelRateVideoCall} from "../../models/ModelCallChat";
import {Redirect} from "react-router-dom";
import * as func from "../../helpers";
import "./styles.scss";

const callConfig = {
    video: {
        width: { ideal: 480, max: 640 },
        height: { ideal: 320, max: 480 }
        // frameRate: { ideal: 10, max: 15 }
    },
    audio: true
};

let socket = io(setting.ohDomainVideoCall, { transports: ['websocket'] });
let peerConnection = null;
let mediaDevice = null;
let timerInterval;

const Video = ({ srcObject, ...props }) => {
    const refVideo = useRef(null);
    useEffect(() => {
        if (refVideo.current && srcObject) refVideo.current.srcObject = srcObject;
    }, [srcObject]);
    return <video ref={refVideo} {...props} />
};

const ScreenVideoCall = (props) => {
    const [loading, setLoading] = useState(false);
    const [localStream, setLocalStream] = useState(null);
    const [peerStream, setPeerStream] = useState(null);
    const [ended, setEnded] = useState(false);
    const [enableVideo, setEnableVideo] = useState(true);
    const [enableAudio, setEnableAudio] = useState(true);
    const [evaluated, setEvaluated] = useState(false);
    const [ratingStar, setRatingStar] = useState(5);
    const [timeCall, setTimeCall] = useState(0);
    const [ratingText, setRatingText] = useState("Xuất sắc");
    const [texComment, setTexComment] = useState("");
    const [countTexComment, setCountTexComment] = useState(0);

    const [modal, contextHolder] = Modal.useModal();

    const exchange = (data) => {
        if (!peerConnection) {
            const fromId = data.from;
            createPeerConnection(fromId);
        }
        if (data.sdp) {
            peerConnection.setRemoteDescription(data.sdp);
            if (data.sdp.type === "offer") {
                peerConnection.createAnswer();
            }
        } else {
            peerConnection.addIceCandidate && peerConnection.addIceCandidate(data.candidate);
        }
    };

    const joinCall = (data = null) => {
        socket.emit("join", data, (isSuccess, socketsInRoom) => {
            if (isSuccess) {
                startTimer();
                for (const i in socketsInRoom) {
                    if (socketsInRoom[i] !== socket.id) {
                        createPeerConnection(socketsInRoom[i]);
                    }
                }
            }
        });
    };

    const createPeerConnection = (socketId) => {
        peerConnection = new PeerConnection(socket, socketId, localStream)
            .on('peerStream', src => {
                setPeerStream(src);
            })
            .on('changeConnectionStatus', () => {})
            .start();
    };

    const startTimer = () => {
        let time = 0;
        timerInterval = setInterval(() => {
            time++;
            setTimeCall(time);
        }, 1000);
    };

    const onEndCall = () => {
        clearInterval(timerInterval);
        if (socket.connected) {
            socket.disconnect();
        }
        setEnded(true);
    };

    const toggleMediaDevice = (deviceType) => {
        if (mediaDevice) {
            if (deviceType === 'video') {
                setEnableVideo(!enableVideo);
                mediaDevice.toggle('Video');
            }
            if (deviceType === 'audio') {
                setEnableAudio(!enableAudio);
                mediaDevice.toggle('Audio');
            }
        }
    };

    const onPressClose = () => {
        window.close();
    };

    useEffect(() => {
        const extra =  window.callData ? window.callData.extra : "";
        if (extra) {
            mediaDevice = new MediaDevice();
            mediaDevice.on('stream', stream => {
                const data = {
                    from: extra.to,
                    to: extra.from,
                    socketid: socket.id,
                    room: extra.roomID
                }
                setLocalStream(stream);
                joinCall(data);
            }).on('error', error => {
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    onOk: onPressClose,
                    centered: true,
                    content: "Vui lòng kiểm tra thiết bị Webcam và Microphone"
                });
            }).start(callConfig);
        }
    }, []);

    useEffect(() => {
        if (localStream) {
            if (!!socket.connected) {
                socket.connect()
            }
            socket.on('send-to-client', data => {
                exchange(data);
            }).on('leave', () => {
                onEndCall();
            }).on("decline-to-client", () => {
                onEndCall();
            }).on("connection", () => {
                console.log("connection")
            }).on("connect", () => {
                console.log('Connected');
            }).on('connect_error', err => {
                console.log('Connection failed', err);
            }).on('disconnect', err => {
                console.log('Connection failed', err);
            });
        }
    }, [localStream]);

    const onSelectRating = value => {
        let text;
        switch(value) {
            case 1:
                text = "Chưa tốt";
                break;
            case 2:
                text = "Tạm được";
                break;
            case 3:
                text = "Tốt";
                break;
            case 4:
                text = "Rất tốt";
                break;
            default:
                text = "Xuất sắc";
        }
        setRatingStar(value);
        setRatingText(text);
    };

    const onHoverRating = value => {
        let text;
        switch(value) {
            case 1:
                text = "Chưa tốt";
                break;
            case 2:
                text = "Tạm được";
                break;
            case 3:
                text = "Tốt";
                break;
            case 4:
                text = "Rất tốt";
                break;
            default:
                text = "Xuất sắc";
        }
        setRatingText(text);
    };

    const onTexComment = e => {
        let lengthContent = e.target.value.length;
        const value = e.target.value.slice(0, 150);
        setTexComment(value);
        setCountTexComment(lengthContent);
    };

    const onPressSendComment = () => {
        if (window.callData) {
            if (!ratingStar) {
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: "Vui lòng xếp hạng cuộc gọi"
                });
                return false;
            }
            const reqParamFt = {
                type: window.callData.type,
                id: window.callData.extra.roomID,
                patient: window.callData.extra.to,
                doctor: window.callData.extra.from,
                rate: ratingStar,
                comment: texComment.trim().replace(/'/g, "")
            }
            setLoading(true);
            ModelRateVideoCall(reqParamFt).then(() => {
                setEvaluated(true);
                setLoading(false);
            }).catch(error => {
                const msg = error.message ? error.message : error.description;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            });
        }
    };

    const onPressCloseWindow = () => {
        window.close();
    };

    let yieldDoctorName = "N/A";
    if (window.callData) {
        yieldDoctorName = window.callData.extra.from_user.last_name + " " + window.callData.extra.from_user.first_name;
    }

    let yieldCallRating;
    if (evaluated) {
        yieldCallRating = (
            <div className="call-rating">
                <h3 className="doctor-name">Bác sĩ <span>{yieldDoctorName}</span></h3>
                <p>Cảm ơn bạn đã gửi đánh giá về cho chúng tôi</p>
                <p style={{marginBottom: "3rem"}}>Cuộc gọi đã kết thúc</p>
                <div onClick={onPressCloseWindow} className="ant-btn btn-primary"><span>Đóng</span></div>
            </div>
        );
    } else {
        yieldCallRating = (
            <div className="call-rating">
                <h3 className="doctor-name">Bác sĩ <span>{yieldDoctorName}</span></h3>
                <h3>Vui lòng xếp hạng chất lượng video của bạn</h3>
                <Rating className="rating"
                        initialRating={ratingStar}
                        onChange={onSelectRating}
                        onHover={onHoverRating}
                        emptySymbol={<img
                            src={public_url.img + "/icons/icon_star_non.png"}
                            alt=""
                            className="icon"/>}
                        fullSymbol={<img
                            src={public_url.img + "/icons/icon_star_rated.png"}
                            alt=""
                            className="icon"/>}/>
                <div className="rating-text">{ratingText}</div>
                <div className="panel-comment">
                    <Input.TextArea className="tex-comment"
                                    placeholder="Nhập nhận xét"
                                    autoSize={false}
                                    onChange={onTexComment}
                                    maxLength={150}
                                    rows={5}/>
                    <div className="tex-count">{countTexComment}/150</div>
                </div>
                <div onClick={onPressSendComment} className="ant-btn btn-primary"><span>Gửi</span></div>
            </div>
        );
    }

    if (!window.callData) {
        if (window.name === "Video Call") {
            window.close();
        } else {
            return <Redirect to={url.name.not_found}/>;
        }
    }

    return ended ?
        (<Fragment>
            {yieldCallRating}
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>) :
        (<Fragment>
            <div className="call-window">
                <Video srcObject={peerStream} autoPlay className="peer-video"/>
                {localStream && <Video srcObject={localStream} autoPlay muted className="local-video"/>}
                <div className="video-control">
                    <div className="text-center mb-3">
                        <span className="call-time">{func.formatTimeCall(timeCall)}</span>
                    </div>
                    <div className="d-flex">
                        <button onClick={() => toggleMediaDevice('video')}>
                            {enableVideo ? <FiVideo/> : <FiVideoOff/>}
                        </button>

                        <button onClick={() => toggleMediaDevice('audio')}>
                            {enableAudio ? <FiMic/> : <FiMicOff/>}
                        </button>

                        <button onClick={onEndCall} className="btn btn-danger">
                            <GiPhone/>
                        </button>
                    </div>
                </div>
                <div className="profile">
                    <p>Bác sĩ <span>{yieldDoctorName}</span></p>
                </div>
            </div>
            {contextHolder}
        </Fragment>);
}

export default ScreenVideoCall;