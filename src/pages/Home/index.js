import React, { Fragment } from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import HomeServices from "./HomeServices";
import "./style.scss";
import ServiceItem from "../../components/ServiceItem";

const HomePage = (props) => {
  return (
    <Fragment>
      <div className="block-home-page">
        <div className="container-fluid wrap-home-application">
          <div className="row">
            <div className="col-12">
              <h2 className="hr-title-vertical">Bạn muốn sử dụng dịch vụ?</h2>
            </div>
          </div>
          <div className="row">
            {HomeServices.map((service, index) => (
              <ServiceItem key={index} {...service} />
            ))}
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HomePage)
);
