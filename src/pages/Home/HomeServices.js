import { public_url, url } from "../../constants/Path";

const HomeServices = [
  {
    url: url.name.examination_1,
    img: public_url.img + "/icons/img_hospital.png",
    title: "Đặt lịch khám/Medifast",
  },
  // {
  //   url: url.name.service_home_test,
  //   img: public_url.img + "/icons/img_examination.png",
  //   title: "Xét nghiệm tại nhà",
  // },
  // {
  //   url: url.name.service_drug_store,
  //   img: public_url.img + "/icons/img_drug.png",
  //   title: "Nhà thuốc online",
  // },
  // {
  //   url: url.name.dr_hera,
  //   img: public_url.img + "/icons/img_hera.png",
  //   title: "Dr.Hera",
  // },
  {
    url: url.name.call_chat_1,
    img: public_url.img + "/icons/img_doctor.png",
    title: "Gọi điện/Chat với bác sĩ",
  },
  // {
  //   url: url.name.list_questions,
  //   img: public_url.img + "/icons/img_question.png",
  //   title: "Hỏi đáp",
  // },
  {
    url: url.name.service_health,
    img: public_url.img + "/icons/img_service.png",
    title: "Dịch vụ sức khỏe",
  },
];

export default HomeServices;
