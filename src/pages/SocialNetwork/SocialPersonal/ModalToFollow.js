import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import BoxSkeleton from "../../../components/BoxSkeleton";
import Loading from "../../../components/Loading";
import {
  getListToFollow,
  postFollow,
} from "../../../services/SocialNetworkService";
import InfiniteScroll from "react-infinite-scroller";
import { Button } from "react-bootstrap";

const ModalToFollow = (props) => {
  const [modalFollow, setModalFollow] = useState(false);
  const [listLike, setListLike] = useState([]);
  const [loading, setLoading] = useState(true);
  const [page, setPage] = useState(2);
  //*** Declare props ***//
  const {
    onOpenModalToFollow,
    openModalToFollow,
    id,
    user,
    count,
    listType,
    title,
  } = props;

  useEffect(() => {
    async function getApi() {
      if (openModalToFollow) {
        setModalFollow(true);
        const listLike = await getListToFollow(listType, id, {
          page: 1,
          limit: 20,
        });
        setListLike(listLike.one_health_msg);
      }
    }
    getApi();
  }, [openModalToFollow, id, listType]);

  const onCloseModalFollow = () => {
    onOpenModalToFollow(false);
    setModalFollow(false);
    setPage(2);
  };
  const loadMore = async () => {
    if (listLike.length >= count) setLoading(false);
    try {
      const listFollow = await getListToFollow(listType, id, {
        page: page,
        limit: 20,
      });
      listLike.push(...listFollow.one_health_msg);
      setPage(page + 1);
    } catch (error) {}
  };

  const handFollow = async (list) => {
    const data = { one_health_msg: {} };
    try {
      await postFollow(list._id, data);
      const position = listLike.indexOf(list);
      if (listLike[position].followed === 0) {
        listLike[position].followed = 1;
      } else {
        listLike[position].followed = 0;
      }
      setListLike([...listLike]);
    } catch (error) {}
  };

  const renderFollow = () => {
    return listLike.map((like, index) => {
      const name = `${like.last_name} ${like.first_name}`;
      return (
        <div key={index} className="py-2 your-contacts">
          <div className="d-flex align-items-center">
            <img
              src={
                like.avatar.url !== ""
                  ? like.avatar.url
                  : "./images/avatar/ic_avatar1.png"
              }
              alt="#"
            />
            <div className="info">
              <p className="mb-0">
                {name.split("").length > 20 ? name.slice(0, 20) + "..." : name}
              </p>
              {like.type === 2 && <div className="sub">Bác sĩ</div>}
            </div>
          </div>
          {user.personal._id !== like._id && (
            <Button
              variant={`${
                like.followed === 0 ? "outline-success" : "outline-secondary"
              }  mr-1" `}
              size="sm"
              className="follow custom-follow"
              onClick={() => handFollow(like)}
            >
              {like.followed === 0 ? "Theo dõi" : "Bỏ theo dõi"}
            </Button>
          )}
        </div>
      );
    });
  };

  function yieldFollow() {
    if (true) {
      return (
        <div className="" style={{ height: "50vh", overflow: "auto" }}>
          {listLike.length < 20 && renderFollow()}
          {listLike.length >= 20 && (
            <InfiniteScroll
              pageStart={0}
              loadMore={loadMore}
              hasMore={true || false}
              loader={
                <BoxSkeleton
                  skeleton={loading}
                  key={0}
                  full={true}
                  length={1}
                  rows={1}
                />
              }
              useWindow={false}
            >
              {renderFollow()}
            </InfiniteScroll>
          )}
        </div>
      );
    }
  }

  if (!listLike) return <Loading open={false} />;
  return (
    <Fragment>
      <Modal
        title={title}
        className=""
        closable={true}
        centered
        width={550}
        footer={null}
        visible={modalFollow}
        onCancel={onCloseModalFollow}
      >
        <div>{yieldFollow()}</div>
      </Modal>
    </Fragment>
  );
};

export default ModalToFollow;
