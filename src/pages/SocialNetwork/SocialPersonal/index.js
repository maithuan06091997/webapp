import React, { useCallback, useEffect, useState } from "react";
import BoxSkeleton from "../../../components/BoxSkeleton";
import { getComment } from "../../../services/SocialNetworkService";
import SocialBlog from "../SocialBlog";
import InfiniteScroll from "react-infinite-scroller";
import "../style.scss";
import PersonalInfo from "./PersonalInfo";

const SocialPersonal = ({ user, computedMatch }) => {
  const limit = 10;
  const { personalId } = computedMatch.params;
  const [postData, setPostData] = useState();
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(2);

  const getBody = useCallback(() => {
    return `{ Posts (limit: ${limit}, creator : "${personalId}", page: ${1} ) { _id created_time creator { _id first_name last_name avatar { url } type} hashtag tagIds { start end text _id } type content link images { url created_time edited deleted type width height } video { url created_time type thumbnail } likes { code } like_count liked comment_count followed comments { content created_time edited } } }`;
  }, [personalId]);

  const getPostData = useCallback(async () => {
    setLoading(true);
    let body = {
      query: getBody(1),
    };
    const postData = await getComment(body);
    setPostData(postData.one_health_msg.Posts);
    setLoading(false);
  }, [getBody]);

  useEffect(() => {
    getPostData();
  }, [getPostData]);

  const loadFunc = async () => {
    if (postData.length < limit) {
      setLoading(true);
      return true;
    }
    let body = {
      query: getBody(page),
    };
    const postDatas = await getComment(body);
    postData.push(...postDatas.one_health_msg.Posts);
    setPage(page + 1);
  };

  if (!postData)
    return (
      <div className="social-network">
        <BoxSkeleton skeleton={true} full={true} length={3} rows={2} />
      </div>
    );
  return (
    <div className="social-network">
      <PersonalInfo personalId={personalId} user={user} />
      <InfiniteScroll
        pageStart={0}
        loadMore={loadFunc}
        hasMore={true || false}
        loader={
          <BoxSkeleton
            skeleton={!loading}
            key={0}
            full={true}
            length={1}
            rows={3}
          />
        }
      >
        <SocialBlog
          listFollow={postData}
          user={user}
          setListFollow={setPostData}
        />
      </InfiniteScroll>
    </div>
  );
};

export default SocialPersonal;
