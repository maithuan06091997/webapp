import React, { useCallback, useEffect, useState } from "react";
import BoxSkeleton from "../../../components/BoxSkeleton";
import { getComment, postFollow } from "../../../services/SocialNetworkService";
import ModalToFollow from "./ModalToFollow";
import { IoMdChatboxes } from "react-icons/io";
import { BsFillPersonPlusFill, BsPersonDashFill } from "react-icons/bs";

const PersonalInfo = ({ personalId, user }) => {
  const [creatorProfile, setCreatorProfile] = useState();
  const [openModalToFollow, setOpenModalToFollow] = useState(false);
  const [openModalFollow, setOpenModalFollow] = useState(false);

  const getCreatorProfile = useCallback(async () => {
    const body = {
      query: `{ Profile (_id : "${personalId}") { first_name last_name avatar { url } type post_count followed followed_count to_follow_count _id } }`,
    };
    const creatorProfile = await getComment(body);
    setCreatorProfile(creatorProfile.one_health_msg.Profile);
  }, [personalId]);
  useEffect(() => {
    getCreatorProfile();
  }, [getCreatorProfile]);

  const handFollow = async () => {
    const data = { one_health_msg: {} };
    try {
      await postFollow(personalId, data);
      if (creatorProfile.followed === 0) {
        creatorProfile.followed = 1;
      } else {
        creatorProfile.followed = 0;
      }
      console.log(creatorProfile);

      setCreatorProfile({ ...creatorProfile });
    } catch (error) {}
  };

  if (!creatorProfile)
    return (
      <div className="social-network">
        <BoxSkeleton skeleton={true} full={true} length={1} rows={3} />
      </div>
    );
  return (
    <div className="personal-info">
      <img
        src={
          creatorProfile.avatar.url !== ""
            ? creatorProfile.avatar.url
            : "./images/avatar/ic_avatar1.png"
        }
        alt="#"
        width="100%"
        className="thumbnail"
      />
      <div className="bg-overlay">
        <div className="info">
          <div className="icon">
            <div>
              <IoMdChatboxes />
            </div>
            Chat
          </div>
          <div>
            <img
              src={
                creatorProfile.avatar.url !== ""
                  ? creatorProfile.avatar.url
                  : "./images/avatar/ic_avatar1.png"
              }
              alt="#"
            />
            <p>
              {creatorProfile.last_name} {creatorProfile.first_name}
            </p>
          </div>
          <div className="icon" onClick={handFollow}>
            <div>
              {creatorProfile.followed === 0 ? (
                <BsFillPersonPlusFill />
              ) : (
                <BsPersonDashFill />
              )}
            </div>
            {creatorProfile.followed === 0 ? "Theo dõi" : "Bỏ theo dõi"}
          </div>
        </div>
        <div className="row count-post">
          <div className="col-4">
            <h5>{creatorProfile.post_count}</h5>
            <p>Bài đăng</p>
          </div>
          <div className="col-4" onClick={() => setOpenModalToFollow(true)}>
            <h5>{creatorProfile.to_follow_count}</h5>
            <p>Người theo dõi</p>
          </div>
          <div className="col-4" onClick={() => setOpenModalFollow(true)}>
            <h5>{creatorProfile.followed_count}</h5>
            <p>Đang theo dõi</p>
          </div>
        </div>
      </div>
      <ModalToFollow
        onOpenModalToFollow={setOpenModalToFollow}
        openModalToFollow={openModalToFollow}
        id={personalId}
        user={user}
        listType="tofollow"
        count={creatorProfile.to_follow_count}
        title="Người theo dõi"
      />
      <ModalToFollow
        onOpenModalToFollow={setOpenModalFollow}
        openModalToFollow={openModalFollow}
        id={personalId}
        user={user}
        listType="following"
        count={creatorProfile.follow_count}
        title="Đang theo dõi"
      />
    </div>
  );
};

export default PersonalInfo;
