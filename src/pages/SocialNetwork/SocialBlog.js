import React, { useState } from "react";
import { ReactTinyLink } from "react-tiny-link";
import { IoIosMore, IoMdChatboxes, IoMdThumbsUp } from "react-icons/io";
import { decryptParam } from "../../helpers";
import SocialBlogImage from "./SocialBlog/SocialBlogImage";
import moment from "moment";
import { Modal } from "antd";
import SocialBlogContactSuggest from "./SocialBlog/SocialBlogContactSuggest";
import {
  putLikePost,
  deletePost,
  postFollow,
} from "../../services/SocialNetworkService";
import { Dropdown, Button } from "react-bootstrap";
import CommentDetails from "./SocialBlog/CommentDetails";
import { NavLink } from "react-router-dom";
import { url } from "../../constants/Path";

const SocialBlog = ({ listFollow, user, setListFollow }) => {
  const token = decryptParam(user.personal.token);
  const [showComment, setShowComment] = useState([]);
  const [modal, contextHolder] = Modal.useModal();
  const likePost = async (id, index) => {
    const data = { one_health_msg: {} };
    try {
      const result = await putLikePost(id, data);
      if (result.one_health_msg.action === "like") {
        listFollow[index].liked = 1;
      } else {
        listFollow[index].liked = 0;
      }
      listFollow[index].like_count = result.one_health_msg.count;
      setListFollow([...listFollow]);
    } catch (error) {}
  };
  const deletePosts = (e, list) => {
    e.preventDefault();
    modal.confirm({
      title: "Xác nhận",
      okText: "Đồng ý",
      cancelText: "Hủy",
      centered: true,
      onOk: () => onOkDeletePost(list),
      content: "Bạn có muốn xóa hoạt động này?",
    });
  };
  const onOkDeletePost = async (list) => {
    const data = { one_health_msg: {} };
    try {
      await deletePost(list._id, data);
      const position = listFollow.indexOf(list);
      listFollow.splice(position, 1);
      setListFollow([...listFollow]);
    } catch (error) {}
  };
  const handFollow = async (list) => {
    const data = { one_health_msg: {} };
    try {
      await postFollow(list.creator._id, data);
      const position = listFollow.indexOf(list);
      listFollow[position].followed = 1;
      setListFollow([...listFollow]);
    } catch (error) {}
  };
  const handShowComment = (id) => {
    if (showComment.includes(id)) {
      showComment.splice(showComment.indexOf(id), 1);
    } else {
      showComment.push(id);
    }
    setShowComment([...showComment]);
  };

  const renderItem = (list) => {
    switch (list.type) {
      case "sharelink":
        if (list.link.includes("http")) {
          return (
            <ReactTinyLink
              cardSize="large"
              showGraphic={true}
              maxLine={2}
              minLine={1}
              url={list.link}
            />
          );
        }
        break;
      case "image":
        return <SocialBlogImage list={list} token={token} />;
      case "video":
        return (
          <div className="content-video">
            <video width="400" controls>
              <source
                src={`${list.video.url}?oh_token=${token}`}
                type="video/mp4"
              />
            </video>
          </div>
        );
      default:
        break;
    }
  };

  return (
    <div>
      {listFollow.map((list, index) => {
        if (list.type === "profile" || list.type === "suggest") return true;
        if (list.type === "contactsuggest")
          return (
            <SocialBlogContactSuggest key={index} suggest={list.suggest} />
          );
        return (
          <div key={index} className="social-blog">
            <div className="social-blog-header">
              <NavLink
                className="social-info"
                to={`${url.name.social_personal}/${list.creator._id}`}
              >
                <img
                  src={
                    list.creator.avatar.url !== ""
                      ? list.creator.avatar.url
                      : "./images/avatar/ic_avatar1.png"
                  }
                  alt="#"
                />
                {list.creator && (
                  <div className="info">
                    <h4>
                      {list.creator.last_name} {list.creator.first_name}
                    </h4>
                    <time>
                      {moment(new Date(list.created_time)).format(
                        "hh:mm DD/MM/YYYY"
                      )}
                    </time>
                  </div>
                )}
              </NavLink>
              <div className="d-flex align-items-center">
                {list.followed === 0 && (
                  <Button
                    variant="outline-success"
                    size="sm"
                    className="follow"
                    onClick={() => handFollow(list)}
                  >
                    Theo dõi
                  </Button>
                )}
                <Dropdown>
                  <Dropdown.Toggle>
                    <div className="more">
                      <IoIosMore />
                    </div>
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item href="/">Báo xấu</Dropdown.Item>
                    {user.personal._id === list.creator._id && (
                      <Dropdown.Item
                        href="/"
                        onClick={(e) => deletePosts(e, list)}
                      >
                        Xóa bài đăng
                      </Dropdown.Item>
                    )}
                  </Dropdown.Menu>
                </Dropdown>
              </div>
            </div>
            {list.content !== "" && <div className="title">{list.content}</div>}
            <div className="content"> {renderItem(list)}</div>
            <div className="comment d-flex align-items-center justify-content-between px-2">
              <div
                className={list.liked === 1 ? "active" : ""}
                onClick={() => likePost(list._id, index)}
              >
                <IoMdThumbsUp /> {list.like_count ?? 0} Thích
              </div>
              <div onClick={() => handShowComment(list._id)}>
                <IoMdChatboxes /> {list.comment_count ?? 0} Bình luận
              </div>
            </div>
            {showComment.includes(list._id) && (
              <CommentDetails list={list} user={user} />
            )}
          </div>
        );
      })}
      {contextHolder}
    </div>
  );
};

export default SocialBlog;
