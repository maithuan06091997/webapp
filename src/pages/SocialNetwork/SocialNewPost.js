import React, { useState } from "react";
import { FaUserCircle } from "react-icons/fa";
import { public_url } from "../../constants/Path";
import ModalSocialNewPost from "./SocialNewPost/ModalSocialNewPost";

const SocialNewPost = ({ setListFollow, user }) => {
  const [openModalNewPosts, setOpenModalNewPosts] = useState(false);
  const onOpenModalNewPosts = (flag) => {
    setOpenModalNewPosts(flag);
  };
  let yieldAvatar;
  if (user.avatar && user.avatar.url) {
    yieldAvatar = <img className="icon" src={user.avatar.url} alt="" />;
  } else {
    yieldAvatar = <FaUserCircle size={`45px`} />;
  }
  return (
    <>
      <div
        className="social-blog new-post"
        onClick={() => setOpenModalNewPosts(true)}
      >
        <div className="d-flex align-items-center ">
          {yieldAvatar}
          <div className="text-share-info">
            Chia sẻ thông tin sức khỏe của bạn
          </div>
        </div>
        <hr />
        <div className="d-flex align-items-center justify-content-between">
          <div className="d-flex align-items-center ">
            <img
              src={public_url.img + "/icons/ic_picture.png"}
              alt="#"
              width="30px"
            />{" "}
            <p className="mb-0 ml-2">Ảnh</p>
          </div>
          <div className="d-flex align-items-center ">
            <img
              src={public_url.img + "/icons/ic_video.png"}
              alt="#"
              width="30px"
            />{" "}
            <p className="mb-0 ml-2">Video</p>
          </div>
          <div className=" d-flex align-items-center ">
            <img
              src={public_url.img + "/icons/ic_link.png"}
              alt="#"
              width="30px"
            />{" "}
            <p className="mb-0 ml-2">Chèn link</p>
          </div>
        </div>
      </div>
      <ModalSocialNewPost
        openModalNewPosts={openModalNewPosts}
        onOpenModalNewPosts={onOpenModalNewPosts}
        setListFollow={setListFollow}
        user={user}
      />
    </>
  );
};

export default SocialNewPost;
