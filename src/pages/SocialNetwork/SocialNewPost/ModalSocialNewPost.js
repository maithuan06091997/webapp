import React, { Fragment, useEffect, useRef, useState } from "react";
import { Modal } from "antd";
import { public_url } from "../../../constants/Path";
import { Button } from "react-bootstrap";
import { ReactTinyLink } from "react-tiny-link";
import {
  getListFollow,
  getNewPost,
} from "../../../services/SocialNetworkService";
import { IoIosClose } from "react-icons/io";

const ModalSocialNewPost = (props) => {
  const [modalNewPosts, setModalNewPosts] = useState(false);
  const [content, setContent] = useState("");
  const [fileImg, setFileImg] = useState([]);
  const [submitDataImg, setSubmitDataImg] = useState();
  const [submitDataVideo, setSubmitDataVideo] = useState();
  const [fileVideo, setFileVideo] = useState();
  const [loadingSubmit, setLoadingSubmit] = useState(false);
  const [modal, contextHolder] = Modal.useModal();
  const [shareLink, setShareLink] = useState("");
  const refShareLink = useRef();
  //*** Declare props ***//
  const { onOpenModalNewPosts, openModalNewPosts, setListFollow, user } = props;

  useEffect(() => {
    async function getApi() {
      if (openModalNewPosts) {
        setModalNewPosts(true);
      }
    }
    getApi();
  }, [openModalNewPosts]);

  const onCloseModalNewPosts = () => {
    onOpenModalNewPosts(false);
    setModalNewPosts(false);
  };

  const closeModalWhenSuccess = () => {
    onOpenModalNewPosts(false);
    setModalNewPosts(false);
    setFileImg([]);
    setSubmitDataImg();
    setContent("");
    setFileVideo();
    setShareLink("");
    setSubmitDataVideo();
  };

  const handPost = async () => {
    setLoadingSubmit(true);
    try {
      let url = `?content=${encodeURIComponent(content)}`;
      let body = {};
      const postData = new FormData();
      if (fileImg.length !== 0) {
        submitDataImg.map((img) => {
          postData.append("data", img);
          return true;
        });
        url = url + "&type=image";
      } else if (fileVideo) {
        postData.append("data", submitDataVideo);
        url = url + "&type=video";
      } else {
        url = url + `&link=${encodeURIComponent(shareLink)}`;
      }
      body = postData;
      await getNewPost(url, body);
      const listFollow = await getListFollow({
        owner: user.personal._id,
        limit: 10,
      });
      setListFollow(listFollow.one_health_msg);
      closeModalWhenSuccess();
    } catch (error) {}
    setLoadingSubmit(false);
  };

  const previewImg = async (e) => {
    const files = e.target.files;
    const arrFiles = Array.prototype.slice.call(files);
    setSubmitDataImg(arrFiles);
    const fileImg = arrFiles.map((file) => getBase64(file));
    setFileImg(await Promise.all(fileImg));
  };
  function getBase64(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    return new Promise((resolve) => {
      reader.onload = (ev) => {
        resolve(ev.target.result);
      };
    });
  }
  const deleteFileImg = (img) => {
    const position = fileImg.indexOf(img);
    fileImg.splice(position, 1);
    submitDataImg.splice(position, 1);
    setSubmitDataImg([...submitDataImg]);
    setFileImg([...fileImg]);
  };

  const previewVideo = (e) => {
    const file = e.target.files[0];
    setSubmitDataVideo(file);
    const reader = new FileReader();
    reader.onload = (ev) => {
      setFileVideo(ev.target.result);
    };
    reader.readAsDataURL(file);
  };

  const deleteFileVideo = () => {
    setFileVideo();
    setSubmitDataVideo();
  };

  const modalShareLink = () => {
    modal.confirm({
      title: "Nhập link bạn muốn chia sẻ!",
      okText: "ĐỒNG Ý",
      cancelText: "HỦY",
      centered: true,
      onOk: handShareLink,
      content: (
        <input
          type="text"
          className="share-link"
          placeholder="VD: https://onehealth.vn"
          ref={refShareLink}
        />
      ),
    });
  };

  const handShareLink = () => {
    if (refShareLink.current.value.includes("http"))
      setShareLink(refShareLink.current.value);
  };
  const renderImg = () => {
    return (
      <div className="row">
        {fileImg.map((img) => {
          if (fileImg.length === 1) {
            return (
              <div className="col-12" key={img}>
                <img src={img} alt="" width="100%" />
                <IoIosClose
                  className="delete"
                  onClick={() => deleteFileImg(img)}
                />
              </div>
            );
          } else {
            return (
              <div className="col-4" key={img}>
                <img src={img} alt="" width="100%" />
                <IoIosClose
                  className="delete"
                  onClick={() => deleteFileImg(img)}
                />
              </div>
            );
          }
        })}
      </div>
    );
  };
  const renderVideo = () => {
    return (
      <div className="position-relative">
        <video width="100%" controls>
          <source src={fileVideo} type="video/mp4"></source>
        </video>
        <IoIosClose className="delete" onClick={deleteFileVideo} />
      </div>
    );
  };

  const renderShareLink = () => {
    return (
      <div className="position-relative">
        <ReactTinyLink
          cardSize="large"
          showGraphic={true}
          maxLine={2}
          minLine={1}
          url={shareLink}
        />
        <IoIosClose className="delete" onClick={() => setShareLink()} />
      </div>
    );
  };

  function yieldNewPosts() {
    return (
      <div className="new-post-popup">
        <textarea
          placeholder="Nhập nội dung bài đăng"
          onChange={(e) => setContent(e.target.value)}
          value={content}
        />
        <div className="preview">
          {fileImg && renderImg()}
          {fileVideo && renderVideo()}
          {shareLink && renderShareLink()}
        </div>
        <div className="row">
          <div className="col-4 new-post-item">
            <label
              className={`bg ${fileVideo || shareLink ? "disabled" : ""} `}
            >
              <img src={public_url.img + "/icons/ic_picture.png"} alt="#" />
              <p>Ảnh</p>
              <input
                type="file"
                hidden
                accept="image/*"
                multiple={true}
                onChange={previewImg}
                disabled={fileVideo || shareLink}
              />
            </label>
          </div>
          <div className="col-4 new-post-item">
            <label
              className={`bg ${
                fileImg.length !== 0 || shareLink ? "disabled" : ""
              } `}
            >
              <img src={public_url.img + "/icons/ic_video.png"} alt="#" />
              <p>Video</p>
              <input
                type="file"
                hidden
                accept="video/*"
                disabled={fileImg.length !== 0 || shareLink}
                onChange={previewVideo}
              />
            </label>
          </div>
          <div className="col-4 new-post-item" onClick={modalShareLink}>
            <div
              className={`bg ${
                fileImg.length !== 0 || fileVideo ? "disabled" : ""
              } `}
            >
              <img src={public_url.img + "/icons/ic_link.png"} alt="#" />
              <p>Chèn link</p>
            </div>
          </div>
        </div>
        <Button
          className="btn-accept mt-2 pt-1 w-100"
          onClick={handPost}
          disabled={loadingSubmit}
        >
          Đăng bài
        </Button>
      </div>
    );
  }

  return (
    <Fragment>
      <Modal
        title="Tạo bài viết"
        className=""
        closable={true}
        centered
        width={550}
        footer={null}
        visible={modalNewPosts}
        onCancel={onCloseModalNewPosts}
      >
        <div>{yieldNewPosts()}</div>
      </Modal>
      {contextHolder}
    </Fragment>
  );
};

export default ModalSocialNewPost;
