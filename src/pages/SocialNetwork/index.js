import React, { useEffect, useState } from "react";
import { getListFollow } from "../../services/SocialNetworkService";
import SocialBlog from "./SocialBlog";
import SocialFollow from "./SocialFollow";
import InfiniteScroll from "react-infinite-scroller";
import "./style.scss";
import BoxSkeleton from "../../components/BoxSkeleton";
import SocialNewPost from "./SocialNewPost";

const SocialNetwork = ({ user }) => {
  const [listFollow, setListFollow] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(2);
  useEffect(() => {
    const getApi = async () => {
      const listFollow = await getListFollow({
        owner: user.personal._id,
        limit: 10,
      });
      setListFollow(listFollow.one_health_msg);
      setLoading(true);
    };
    getApi();
  }, [user.personal._id]);

  const loadFunc = async () => {
    const listFollows = await getListFollow({
      owner: user.personal._id,
      limit: 10,
      page: page,
    });
    listFollow.push(...listFollows.one_health_msg);
    setPage(page + 1);
  };
  if (!loading)
    return (
      <div className="social-network">
        <BoxSkeleton skeleton={!loading} full={true} length={3} rows={3} />
      </div>
    );
  return (
    <div className="social-network">
      <SocialFollow listFollow={listFollow} />
      <SocialNewPost setListFollow={setListFollow} user={user} />
      <InfiniteScroll
        pageStart={0}
        loadMore={loadFunc}
        hasMore={true || false}
        loader={
          <BoxSkeleton
            skeleton={loading}
            key={0}
            full={true}
            length={1}
            rows={3}
          />
        }
      >
        <SocialBlog
          listFollow={listFollow}
          user={user}
          setListFollow={setListFollow}
        />
      </InfiniteScroll>
    </div>
  );
};

export default SocialNetwork;
