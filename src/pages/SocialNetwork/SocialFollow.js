import React from "react";
// import { Modal, Button } from "antd";

const SocialFollow = ({ listFollow }) => {
  // const [isModalVisible, setIsModalVisible] = useState(false);

  // const showModal = () => {
  //   setIsModalVisible(true);
  // };

  // const handleOk = () => {
  //   setIsModalVisible(false);
  // };

  // const handleCancel = () => {
  //   setIsModalVisible(false);
  // };
  return (
    <div className="w-100 follow">
      <div className="d-flex justify-content-between align-items-center mb-2">
        <h5>Gợi ý theo dõi</h5>
        <span className="see-all">Xem tất cả</span>
      </div>
      <div className="row">
        {listFollow[1].suggest.map((follow, index) => {
          if (index > 3) return true;
          return (
            <div className="col-3" key={follow._id}>
              <div className="follow-item">
                <img src={follow.avatar.url} alt="#" className="img-follow" />
                <div className="bg-overay">
                  <img src={follow.avatar.url} alt="#" />
                  <p>
                    {follow.last_name} {follow.first_name}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      {/* <Modal
        title="Gợi ý theo dõi"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        cancelText="Bỏ qua"
        okText="Theo dõi"
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal> */}
    </div>
  );
};

export default SocialFollow;
