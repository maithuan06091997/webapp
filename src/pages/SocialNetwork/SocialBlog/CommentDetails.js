import moment from "moment";
import React, { useEffect, useState } from "react";
import { FaUserCircle } from "react-icons/fa";
import { IoMdThumbsUp } from "react-icons/io";
import {
  getComment,
  putLikeComment,
} from "../../../services/SocialNetworkService";
import ModalLikes from "./ModalLikes";

const CommentDetails = ({ list, user }) => {
  const [openModalLikes, setOpenModalLikes] = useState(false);
  const [comments, setComments] = useState();
  const [commentId, setCommentId] = useState();
  const onOpenModalLikes = (flag) => {
    setOpenModalLikes(flag);
  };
  let page = 1;
  let skip = 0;
  useEffect(() => {
    const getApi = async () => {
      let _body = {
        query:
          '{ Comments( postId :"' +
          list._id +
          '" limit: ' +
          10 +
          " page: " +
          "1" +
          " skip :" +
          (skip + (page - 1) * 10) +
          ' sorts :"created_time_DES" ) { _id creator { _id first_name last_name username avatar { url } active type} tagIds { start end text _id } content image created_time edited comment_count like_count liked} }',
      };
      const comments = await getComment(_body);
      setComments(comments.one_health_msg.Comments.reverse());
    };
    getApi();
  }, [list._id, page, skip]);

  useEffect(() => {
    // let fromUser = user.personal.username;
    // let toUser = currentUC.to.username;
    // let room = currentUC.room.room;
    // let roomID = currentUC.room._id;
    // let roomData = {
    //   from: {
    //     phone: fromUser,
    //   },
    //   to: {
    //     phone: toUser,
    //   },
    //   room: room,
    //   roomId: roomID,
    // };
    // socket.emit("join-room", roomData, function (err, result) {
    //   console.log("join room", result);
    // });
  }, []);

  const likeComment = async (id, index) => {
    const data = { one_health_msg: {} };
    try {
      const result = await putLikeComment(id, data);

      if (result.one_health_msg.action === "like") {
        comments[index].liked = 1;
      } else {
        comments[index].liked = 0;
      }

      comments[index].like_count = result.one_health_msg.count;
      setComments([...comments]);
    } catch (error) {}
  };

  if (!comments) return "";
  return (
    <div className="comment-details">
      {comments.map((comment, index) => (
        <div className="p-2 comment-item" key={comment._id}>
          <div className="d-flex align-items-center justify-content-between">
            <div className="social-info">
              <img
                src={
                  comment.creator.avatar.url !== ""
                    ? comment.creator.avatar.url
                    : "./images/avatar/ic_avatar1.png"
                }
                alt="#"
              />
              <div className="info">
                <h4>
                  {comment.creator.last_name} {comment.creator.first_name}
                </h4>
                {comment.creator.type === 2 && (
                  <div className="sub">Bác sĩ</div>
                )}
              </div>
            </div>
            <div className={comment.liked === 1 ? "active" : ""}>
              <div className="d-flex align-items-center">
                <span
                  className="like"
                  onClick={() => likeComment(comment._id, index)}
                >
                  {comment.liked === 0 ? "Thích" : "Bỏ thích"}
                </span>{" "}
                <span
                  className="d-flex align-items-center cp"
                  onClick={() => {
                    setCommentId(comment._id);
                    onOpenModalLikes(true);
                  }}
                >
                  <IoMdThumbsUp className="mx-1" /> {comment.like_count}
                </span>
              </div>
            </div>
          </div>
          <div className="content mt-2 mb-1">{comment.content}</div>
          <time>
            {moment(new Date(comment.created_time)).format("hh:mm DD/MM/YYYY")}
          </time>
        </div>
      ))}
      <ModalLikes
        onOpenModalLikes={onOpenModalLikes}
        openModalLikes={openModalLikes}
        id={commentId}
        user={user}
      />

      <div className="input-comment p-2">
        {user.avatar && user.avatar.url && (
          <img src={user.avatar.url} alt="#" />
        )}
        {!user.avatar && <FaUserCircle size={`2.2em`} />}
        <input type="text" placeholder="Viết bình luận" />
      </div>
    </div>
  );
};

export default CommentDetails;
