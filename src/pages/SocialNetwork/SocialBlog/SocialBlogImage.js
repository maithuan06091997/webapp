import React, { useState } from "react";
import ImgsViewer from "react-images-viewer";

const SocialBlogImage = ({ list, token }) => {
  const [isPopup, setIsPopup] = useState(false);
  const [imgs, setImgs] = useState([]);
  const [imgIndex, setImgIndex] = useState(0);

  const openPopupImage = (index) => {
    setImgs([]);
    setImgIndex(index);
    setIsPopup(true);
  };
  const closePopupImage = () => {
    setIsPopup(false);
    setImgs([]);
  };
  const nextImg = () => {
    setImgs([]);
    setImgIndex(imgIndex + 1);
  };
  const prevImg = () => {
    setImgs([]);
    setImgIndex(imgIndex - 1);
  };
  const renderListImage = () => {
    return list.images.map((image, index) => {
      imgs.push({ src: `${image.url}?oh_token=${token}` });
      if (index > 4) return true;
      switch (list.images.length) {
        case 1:
          return (
            <div className="col-12" key={image.url}>
              <img
                src={`${image.url}?oh_token=${token}`}
                alt="#"
                className="h-350"
                onClick={() => openPopupImage(index)}
              />
            </div>
          );
        case 2:
          return (
            <div className="col-12 py-1" key={image.url}>
              <img
                src={`${image.url}?oh_token=${token}`}
                alt="#"
                onClick={() => openPopupImage(index)}
              />
            </div>
          );
        case 3:
          return (
            <div className={`col-${index === 0 ? 12 : 6} py-1`} key={image.url}>
              <img
                src={`${image.url}?oh_token=${token}`}
                alt="#"
                onClick={() => openPopupImage(index)}
              />
            </div>
          );
        case 4:
          return (
            <div className="col-6 py-1" key={image.url}>
              <img
                src={`${image.url}?oh_token=${token}`}
                alt="#"
                onClick={() => openPopupImage(index)}
              />
            </div>
          );
        case 5:
          return (
            <div className={`col-${index < 2 ? 6 : 4} py-1`} key={image.url}>
              <img
                src={`${image.url}?oh_token=${token}`}
                alt="#"
                onClick={() => openPopupImage(index)}
              />
            </div>
          );
        default:
          return (
            <div className={`col-${index < 2 ? 6 : 4} py-1`} key={image.url}>
              <div className="position-relative">
                <img
                  src={`${image.url}?oh_token=${token}`}
                  alt="#"
                  onClick={() => openPopupImage(index)}
                />
                {index === 4 && (
                  <div
                    className="bg-overay"
                    onClick={() => openPopupImage(index)}
                  >
                    +{list.images.length - 5}
                  </div>
                )}
              </div>
            </div>
          );
      }
    });
  };
  return (
    <div className="content-img row mx--5">
      {renderListImage()}
      <ImgsViewer
        imgs={imgs}
        currImg={imgIndex}
        isOpen={isPopup}
        onClose={closePopupImage}
        onClickPrev={prevImg}
        onClickNext={nextImg}
      />
    </div>
  );
};

export default SocialBlogImage;
