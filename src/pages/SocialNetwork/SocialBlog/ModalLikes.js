import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import BoxSkeleton from "../../../components/BoxSkeleton";
import Loading from "../../../components/Loading";
import {
  getListLike,
  postFollow,
} from "../../../services/SocialNetworkService";
import InfiniteScroll from "react-infinite-scroller";
import { Button } from "react-bootstrap";

const ModalLikes = (props) => {
  const [modalLikes, setModalLikes] = useState(false);
  const [listLike, setListLike] = useState([]);
  const [page, setPage] = useState(2);
  //*** Declare props ***//
  const { onOpenModalLikes, openModalLikes, id, user } = props;

  useEffect(() => {
    async function getApi() {
      if (openModalLikes) {
        setModalLikes(true);
        const listLike = await getListLike(id, {
          page: 1,
          limit: 20,
        });
        setListLike(listLike.one_health_msg);
      }
    }
    getApi();
  }, [openModalLikes, id]);

  const onCloseModalLikes = () => {
    onOpenModalLikes(false);
    setModalLikes(false);
    setPage(2);
  };
  const loadMore = async () => {
    const listLikes = await getListLike(id, {
      page: page,
      limit: 20,
    });
    listLike.push(...listLikes.one_health_msg);
    setPage(page + 1);
  };

  const handFollow = async (list) => {
    const data = { one_health_msg: {} };
    try {
      await postFollow(list._id, data);
      const position = listLike.indexOf(list);
      if (listLike[position].followed === 0) {
        listLike[position].followed = 1;
      } else {
        listLike[position].followed = 0;
      }
      setListLike([...listLike]);
    } catch (error) {}
  };

  const renderLikes = () => {
    return listLike.map((like, index) => {
      const name = `${like.last_name} ${like.first_name}`;
      return (
        <div key={index} className="py-2 your-contacts">
          <div className="d-flex align-items-center">
            <img
              src={
                like.avatar.url !== ""
                  ? like.avatar.url
                  : "./images/avatar/ic_avatar1.png"
              }
              alt="#"
            />
            <div className="info">
              <p className="mb-0">
                {name.split("").length > 20 ? name.slice(0, 20) + "..." : name}
              </p>
              {like.type === 2 && <div className="sub">Bác sĩ</div>}
            </div>
          </div>
          {user.personal._id !== like._id && (
            <Button
              variant="outline-success mr-1"
              size="sm"
              className="follow"
              onClick={() => handFollow(like)}
            >
              {like.followed === 0 ? "Theo dõi" : "Bỏ theo dõi"}
            </Button>
          )}
        </div>
      );
    });
  };

  function yieldLikes() {
    if (true) {
      return (
        <div className="" style={{ height: "50vh", overflow: "auto" }}>
          {listLike.length < 20 && renderLikes()}
          {listLike.length >= 20 && (
            <InfiniteScroll
              pageStart={0}
              loadMore={loadMore}
              hasMore={true || false}
              loader={
                <BoxSkeleton
                  skeleton={listLike}
                  key={0}
                  full={true}
                  length={1}
                  rows={1}
                />
              }
              useWindow={false}
            >
              {renderLikes()}
            </InfiniteScroll>
          )}
        </div>
      );
    }
  }

  if (!listLike) return <Loading open={false} />;
  return (
    <Fragment>
      <Modal
        title="Lượt thích"
        className=""
        closable={true}
        centered
        width={550}
        footer={null}
        visible={modalLikes}
        onCancel={onCloseModalLikes}
      >
        <div>{yieldLikes()}</div>
      </Modal>
    </Fragment>
  );
};

export default ModalLikes;
