import { Button } from "antd";
import React, { useState } from "react";
import { IoIosArrowForward } from "react-icons/io";
import ModalSeeMore from "./ModalSeeMore";

const SocialBlogContactSuggest = ({ suggest }) => {
  const [openModalSeeMore, setOpenModalSeeMore] = useState(false);
  const onOpenModalModalSeeMore = (flag) => {
    setOpenModalSeeMore(flag);
  };

  return (
    <div className="social-blog contact-suggest">
      <div className="social-blog-header">
        <div>Có thể bạn quen</div>
        <span onClick={() => onOpenModalModalSeeMore(true)}>
          Xem thêm <IoIosArrowForward />
        </span>
      </div>
      <div className="content">
        <div className="row">
          {suggest.map((suggest, index) => {
            if (index > 3) return true;
            return (
              <div className="col-3" key={index}>
                <div className="info">
                  <img
                    src={
                      suggest.avatar.url
                        ? suggest.avatar.url
                        : "./images/avatar/ic_avatar1.png"
                    }
                    alt="#"
                  />
                  <h5>
                    {suggest.last_name} {suggest.first_name}
                  </h5>
                  <p>Bạn từ danh bạ</p>
                  <Button className="btn-accept">Mời tải DR.OH</Button>
                </div>
              </div>
            );
          })}
        </div>
      </div>
      <ModalSeeMore
        onOpenModalSeeMore={onOpenModalModalSeeMore}
        openModalSeeMore={openModalSeeMore}
        suggest={suggest}
      />
    </div>
  );
};

export default SocialBlogContactSuggest;
