import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import BoxSkeleton from "../../../components/BoxSkeleton";
import Loading from "../../../components/Loading";
import { getListSuggest } from "../../../services/SocialNetworkService";
import InfiniteScroll from "react-infinite-scroller";

const ModalSeeMore = (props) => {
  const [modalSeeMore, setModalSeeMore] = useState(false);
  const [listSuggest, setListSuggest] = useState([]);
  const [page, setPage] = useState(2);
  const [keySearch, setKeySearch] = useState("");
  //*** Declare props ***//
  const { onOpenModalSeeMore, openModalSeeMore } = props;

  useEffect(() => {
    async function getApi() {
      if (openModalSeeMore) {
        setModalSeeMore(true);
        const listSuggest = await getListSuggest({
          limit: 20,
          page: 1,
        });
        setListSuggest(listSuggest.one_health_msg);
      }
    }
    getApi();
  }, [openModalSeeMore]);

  const onCloseModalSeeMore = () => {
    onOpenModalSeeMore(false);
    setModalSeeMore(false);
    setPage(2);
  };
  const loadMore = async () => {
    if (listSuggest.length < 20) return true;
    const listSuggests = await getListSuggest({
      limit: 20,
      page: page,
      // keyword: keySearch,
    });
    listSuggest.push(...listSuggests.one_health_msg);
    setPage(page + 1);
  };

  const searchContacts = async (e) => {
    const listSuggest = await getListSuggest({
      limit: 20,
      page: 1,
      keyword: e.target.value,
    });
    setListSuggest(listSuggest.one_health_msg);
    setKeySearch(e.target.value);
    setPage(2);
  };

  function yieldSeeMore() {
    if (true) {
      return (
        <div className="mt-2" style={{ height: "70vh", overflow: "auto" }}>
          <InfiniteScroll
            pageStart={0}
            loadMore={loadMore}
            hasMore={true || false}
            loader={
              <BoxSkeleton
                skeleton={listSuggest}
                key={0}
                full={true}
                length={1}
                rows={1}
              />
            }
            useWindow={false}
          >
            {listSuggest.map((suggest, index) => {
              const name = `${suggest.last_name} ${suggest.first_name}`;
              return (
                <div key={index} className="py-2 your-contacts">
                  <div className="d-flex align-items-center">
                    <img
                      src={
                        suggest.avatar.url !== ""
                          ? suggest.avatar.url
                          : "./images/avatar/ic_avatar1.png"
                      }
                      alt="#"
                    />
                    <p className="mb-0">
                      {name.split("").length > 20
                        ? name.slice(0, 20) + "..."
                        : name}
                    </p>
                  </div>
                  <a
                    href="https://play.google.com/store/apps/details?id=com.onehealth.DoctorOHApp&hl=vi&gl=US"
                    target="__blank"
                    className="bt-invite"
                  >
                    Mời tải DR.OH
                  </a>
                </div>
              );
            })}
          </InfiniteScroll>
        </div>
      );
    }
  }

  if (!listSuggest) return <Loading open={false} />;
  return (
    <Fragment>
      <Modal
        title="Bạn từ danh bạ"
        className=""
        closable={true}
        centered
        width={550}
        footer={null}
        visible={modalSeeMore}
        onCancel={onCloseModalSeeMore}
      >
        <div>
          <input
            type="text"
            placeholder="Tìm kiếm"
            className="search-contact"
            onChange={searchContacts}
          />
          {yieldSeeMore()}
        </div>
      </Modal>
    </Fragment>
  );
};

export default ModalSeeMore;
