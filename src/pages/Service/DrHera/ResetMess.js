import { Modal } from "antd";
import React from "react";
import { FaSyncAlt } from "react-icons/fa";
import { ModelDeleteRoom } from "../../../models/ModelChat";

const ResetMess = ({ roomId, resetMess }) => {
  const [modal, contextHolder] = Modal.useModal();
  const deleteMess = () => {
    const params = {
      data: {
        room: {
          id: roomId,
        },
      },
    };
    ModelDeleteRoom(params).then((response) => {
      resetMess();
    });
  };
  const openModal = () => {
    modal.confirm({
      title: "Xác nhận",
      okText: "Đồng ý",
      cancelText: "Hủy",
      centered: true,
      onOk: deleteMess,
      content:
        "Toàn bộ thông tin chuẩn đoán sẽ bị xóa và khảo sát sẽ thực hiện lại từ đầu. Bạn có chắc chắn muốn tiếp tục xóa?",
    });
  };
  return (
    <div>
      <div>
        <FaSyncAlt size={16} className="icon-sync" onClick={openModal} />
      </div>
      {contextHolder}
    </div>
  );
};

export default ResetMess;
