import React, { useEffect, useState } from "react";
import moment from "moment";
import ReactTooltip from "react-tooltip";
import { public_url } from "../../../constants/Path";
import { Skeleton, Modal } from "antd";
import "./style.scss";
import { FaPaperPlane, FaImage } from "react-icons/fa";
import { Link } from "react-router-dom";
import { url } from "../../../constants/Path";

let listMessagesRef = null;
let messageTextRef = null;
let scrollBottom = false;
let bottomPosition = 0;
let isShowAvatar = false;
let senderId = null;

const SentMessage = ({
  id,
  message,
  type,
  sender,
  created_time,
  sending,
  error,
  resendMessage,
}) => {
  return (
    <div className={`message sent `}>
      {sending ? (
        <span className="left-icon mr-2">
          <i className={`fa fa-spinner fa-pulse`}></i>
        </span>
      ) : null}
      {error ? (
        <span
          className="left-icon mr-2"
          onClick={() => resendMessage(id)}
          style={{ cursor: "pointer" }}
          data-tip={"Thử lại"}
        >
          <i className={`fa fa-repeat`}></i>
          <ReactTooltip effect="solid" />
        </span>
      ) : null}
      <div
        className="content"
        data-tip={moment(created_time).format("hh:mm A DD/MM/YYYY")}
      >
        {type === 5 && <div>{message}</div>}
        {type === 3 && <div>{message}</div>}
        {type === 9 && <div>{message}</div>}

        <ReactTooltip effect="solid" />
      </div>
      {error ? (
        <small className="text-danger w-100 text-right">
          Gửi thất bại
          <Link
            className="text-primary"
            onClick={() => resendMessage(id)}
            to={"#"}
          >
            {" "}
            <u>Thử lại</u>
          </Link>
        </small>
      ) : null}
    </div>
  );
};

const ReceivedMessage = ({
  message,
  type,
  sender,
  explaination,
  created_time,
  item,
  scroll,
}) => {
  const [modalDetailVoucher, setModalDetailVoucher] = useState(false);
  useEffect(() => {
    if (scroll) {
      scrollBottom = true;
    } else {
      scrollBottom = false;
    }
  }, [scroll]);
  return (
    <div className="message received">
      <img
        className="avatar"
        src={
          sender?.avatar && sender.avatar.url
            ? sender.avatar.url
            : `${public_url.img}/icons/icon_avatar.png`
        }
        alt=""
      />
      <div
        className="content"
        data-tip={moment(created_time).format("hh:mm A DD/MM/YYYY")}
      >
        {type === 5 && (
          <div>
            <div>{message}</div>
            {item.url[0] && (
              <div className="mt-3 result">
                <Link to={item.url[0]} target="_blank">
                  XEM KẾT QUẢ CHUẨN ĐOÁN
                </Link>
              </div>
            )}
            {item.suggestion_test_title && (
              <div className="mt-3">{item.suggestion_test_title}</div>
            )}
            <div>
              {Array.isArray(item.service_suggestion) &&
                item.service_suggestion.map((service, index) => (
                  <Link
                    to={url.name.service_health + "?id=" + service._id}
                    key={index}
                    className="service_suggestion"
                    target="_blank"
                  >
                    <img src={public_url.img + "/specialist/droh.png"} alt="" />
                    <div>{service.name}</div>
                  </Link>
                ))}
            </div>
          </div>
        )}
        {type === 3 && <div>{message}</div>}
        {type === 9 && <div>{message}</div>}

        <ReactTooltip effect="solid" />
      </div>
      {Array.isArray(explaination) &&
        explaination[0] !== "" &&
        explaination[0] !== undefined && (
          <>
            <div
              onClick={() => setModalDetailVoucher(true)}
              className="question"
            >
              ?
            </div>{" "}
            <Modal
              title="Giải thích"
              className=""
              closable={true}
              centered
              width={600}
              footer={null}
              visible={modalDetailVoucher}
              onCancel={() => setModalDetailVoucher(false)}
            >
              {explaination}
            </Modal>
          </>
        )}
    </div>
  );
};

const ConversationDetailsHera = ({
  conversation,
  isLoading,
  messages,
  loadMore,
  hasMore,
  onSend,
  currentUser,
  resendMessage,
}) => {
  const [listMessages, setListMessages] = useState(messages);
  const [messageText, setMessageText] = useState("");
  const [partner, setPartner] = useState(null);
  const [ended, setEnded] = useState(false);
  const [messFileImg, setMessFileImg] = useState();
  const [scroll, setScroll] = useState(true);
  useEffect(() => {
    setMessageText("");
    setEnded(conversation ? false : true);
  }, [conversation]);

  useEffect(() => {
    if (conversation) {
      if (currentUser.personal._id) {
        if (
          conversation.receiver &&
          conversation.receiver._id !== currentUser.personal._id
        ) {
          setPartner(conversation.receiver);
        } else {
          setPartner(conversation.sender);
        }
      }
    }
  }, [conversation, currentUser.personal._id]);

  useEffect(() => {
    let newListMessages = [];
    for (let i = messages.length - 1; i >= 0; i--) {
      newListMessages.push(messages[i]);
    }

    setListMessages(newListMessages);
  }, [messages]);

  useEffect(() => {
    if (scrollBottom) {
      scrollBottom = false;
      listMessagesRef.scroll({
        top: listMessagesRef.scrollHeight,
        behavior: "auto",
      });
    } else if (bottomPosition) {
      listMessagesRef.scroll({
        top: listMessagesRef.scrollHeight - bottomPosition,
        behavior: "auto",
      });
    }
  }, [listMessages]);

  const onScrollMessages = (e) => {
    // if (e.target.scrollTop + e.target.offsetHeight >= e.target.scrollHeight && !bottomScrolled) {
    //     this.setState({ bottomScrolled: true })
    // } else if (e.target.scrollTop + e.target.offsetHeight < e.target.scrollHeight && bottomScrolled) {
    //     this.setState({ bottomScrolled: false })
    // }
    // const { getMessagesLoading, Conversation } = this.props;
    if (e.target.scrollTop < 250 && !isLoading && hasMore) {
      loadMore();
      setScroll(false);
    }
    bottomPosition = e.target.scrollHeight - e.target.scrollTop;
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (messageText && messageText.trim()) {
      onSend && onSend(messageText);
      setMessageText("");
    }
  };
  const submitImg = (e) => {
    const file = e.target.files[0];

    const reader = new FileReader();
    reader.onloadend = () => {
      onSend(reader.result, 2);
      setMessFileImg("");
    };
    reader.readAsDataURL(file);
  };

  const onKeyUp = (e) => {
    if (e.key == "Enter") {
      e.preventDefault();
      e.value = "";
      onSubmit(e);
    } else {
      setMessageText(e.target.value);
    }
  };

  const optionAi = (content, id) => {
    onSend(content, id);
    setScroll(true);
  };

  return (
    <div className="chat-box-hera">
      {partner ? (
        <div className="chat-box-header">
          <img
            className="avatar"
            src={`${public_url.img}/icons/img_hera.png`}
            alt="#"
          />
          <div className="flex-fill">
            <p className="fullname mb-1">Dr.Hera</p>
            <span className="doctor-label">Bác sĩ</span>
          </div>
        </div>
      ) : null}
      <div
        className="list-messages"
        onScroll={onScrollMessages}
        ref={(ref) => (listMessagesRef = ref)}
      >
        {isLoading || hasMore ? (
          <div className="mb-2">
            <Skeleton.Avatar
              active
              size={30}
              shape={"circle"}
              className="mr-1"
            />
            <Skeleton.Input
              active
              style={{ width: 200, height: 30, borderRadius: 5 }}
            />
            <div className="text-right">
              <Skeleton.Input
                active
                style={{ width: 200, height: 30, borderRadius: 5 }}
              />
            </div>
          </div>
        ) : null}
        {listMessages && listMessages.length ? (
          listMessages.map((item, index) => {
            if (item.sender && currentUser.personal._id === item.sender._id) {
              senderId = item.sender._id;
              return (
                <SentMessage
                  id={item._id}
                  key={`message_${index}`}
                  message={item.message}
                  type={item.type}
                  sender={item.sender}
                  created_time={item.created_time}
                  sending={item.sending}
                  error={item.error}
                  resendMessage={resendMessage}
                />
              );
            }
            isShowAvatar = item.sender?._id !== senderId;
            senderId = item.sender?._id;
            return (
              <ReceivedMessage
                id={item._id}
                key={`message_${index}`}
                message={item.message}
                type={item.type}
                sender={item.sender}
                created_time={item.created_time}
                explaination={item.explaination}
                isShowAvatar={isShowAvatar}
                item={item}
                scroll={scroll}
              />
            );
          })
        ) : (
          <div />
        )}
      </div>
      {messages[0]?.suggestion?.length === 0 && (
        <div className={`message-box ${ended ? "bg-light" : ""}`}>
          <form onSubmit={onSubmit}>
            <textarea
              className="input-message"
              ref={(ref) => (messageTextRef = ref)}
              disabled={ended}
              onKeyUp={onKeyUp}
              placeholder={ended ? "" : "Nhập vấn đề của bạn"}
              value={messageText}
              onChange={(e) => setMessageText(e.target.value)}
            />

            {!ended ? (
              <>
                <label className="option-file">
                  <FaImage />
                  <input
                    type="file"
                    onChange={submitImg}
                    value={messFileImg}
                    accept="image/*"
                    hidden
                  />
                </label>
                <button className="btn btn-submit" type="submit">
                  <FaPaperPlane />
                </button>
              </>
            ) : null}
          </form>
        </div>
      )}
      {messages[0]?.suggestion?.length > 0 && (
        <div className="option-ai">
          {messages[0].suggestion.map((item, index) => (
            <p key={index} onClick={() => optionAi(item.content, item.id)}>
              {item.content}
            </p>
          ))}
        </div>
      )}
    </div>
  );
};

export default ConversationDetailsHera;
