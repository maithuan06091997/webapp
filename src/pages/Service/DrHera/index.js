import React, { Fragment, useEffect, useState } from "react";
import {
  ModelGetListMessages,
  ModelSendMessage,
} from "../../../models/ModelChat";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";

import { setting } from "../../../api/Config";
import io from "socket.io-client";
import ConversationDetailsHera from "./ConversationDetailsHera";
import ResetMess from "./ResetMess";

let socket = io(setting.ohDomainChat, { transports: ["websocket"] });
let localMessageId = -1;
// type : 1 text, 2 image, 3 choice, 5 send text

const DrHera = (props) => {
  const [activeConversation, setActiveConversation] = useState(null);
  const [listMessages, setListMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [messagePage, setMessagePage] = useState(0);
  const [hasMore, setHasMore] = useState(false);

  //*** Declare props ***//
  const { user } = props;

  useEffect(() => {
    onPressItemChat();
    onReceiveMessage();
  }, []);

  useEffect(() => {
    if (messagePage > 1) {
      getListMessages(messagePage, activeConversation.roomid, listMessages);
    }
  }, [messagePage]);

  // socket -----------------------------------
  const joinRoom = (currentUC) => {
    let fromUser = user.personal.username;
    let roomData = {
      from: { phone: fromUser },
      to: { phone: "chatbot" },
      room: "",
      roomId: "",
      func: "chatbot-hkqkb",
    };
    socket.emit("join-room", roomData, function (err, result) {
      getListMessages(1, result.roomid, []);
      setActiveConversation(result);
    });
  };

  const resetMess = () => {
    setListMessages([]);
    onPressItemChat();
  }

  // end socket -----------------------------------

  const getListMessages = (page, roomID, listMessages) => {
    const params = {
      data: {
        room: {
          id: roomID,
        },
      },
    };
    ModelGetListMessages(page, params)
      .then((response) => {
        setLoading(false);
        let data =
          response.data && response.data.one_health_msg
            ? response.data.one_health_msg
            : [];

        setListMessages(listMessages.concat(data));
        if (data.length >= 25) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      })
      .catch(() => {
        setLoading(false);
        setHasMore(false);
      });
  };

  const onPressItemChat = () => {
    setLoading(true);
    setMessagePage(1);
    setListMessages([]);
    joinRoom();
  };

  const loadMoreMessages = () => {
    setLoading(true);
    setMessagePage(messagePage + 1);
  };

  const onSendMessage = (text, itemId) => {
    if (activeConversation) {
      const { room } = activeConversation;
      const sender = activeConversation.userFrom;
      const receiver = activeConversation.userTo;
      if (room && sender && receiver && user) {
        const _id = localMessageId;
        localMessageId--;

        const params = {
          from: {
            phone: user.personal.username,
          },
          to: {
            phone: "chatbot",
          },
          room: room,
          socketId: socket.id,
          type: 5,
          message: text,
        };
        if (itemId) {
          params.type = 3;
          params.selected_id = itemId;
        }
        let newListMessages = [];
        newListMessages = [
          {
            _id,
            created_time: new Date(),
            deleted: 0,
            disease_suggestion: [],
            images: [],
            message: text,
            modified_time: new Date(),
            receiver,
            room: room._id,
            sender: user.personal,
            service_suggestion: [],
            specialty_suggestion: [],
            status: 1,
            suggestion: [],
            type: 5,
            url: [],
            sending: true,
            paramsData: params,
          },
        ].concat(listMessages);
        setListMessages(newListMessages);
        ModelSendMessage(params).then((response) => { });
      }
    }
  };

  const onReceiveMessage = () => {
    socket.on("new-message", (data) => {
      const sender = activeConversation && activeConversation.userFrom;
      const receiver = activeConversation && activeConversation.userTo;
      if (data && data.from && user) {
        if (data.from.username === user.personal.username) {
        } else {
          let message = {
            _id: data.created_time,
            created_time: data.created_time,
            deleted: 0,
            disease_suggestion: [],
            explaination: data.messages?.explaination,
            images: [],
            message: data.message,
            modified_time: data.created_time,
            receiver:
              receiver && receiver.username === user.username
                ? receiver
                : sender,
            room: data.roomid,
            sender: data.from,
            service_suggestion: data.messages?.service_suggestion,
            specialty_suggestion: data.messages?.specialty_suggestion,
            status: 1,
            suggestion: data.suggestion ?? [],
            suggestion_test_title: data.messages?.suggestion_test_title,
            type: 5,
            url: [],
          };
          setListMessages((listMessages) => {
            return [message].concat(listMessages);
          });
        }
      }
    });
  };

  return (
    <Fragment>
      <div className="block-my-account hera">
        <div className="card card-custom">
          <div className="card-header">
            <h3>Dr.Hera</h3>
            {activeConversation && (
              <ResetMess
                roomId={activeConversation.roomid}
                resetMess={resetMess}
              />
            )}
          </div>
          <div className="card-body">
            <div className="d-flex flex-fill h-100">
              <ConversationDetailsHera
                conversation={activeConversation}
                messages={listMessages}
                hasMore={hasMore}
                loadMore={loadMoreMessages}
                isLoading={loading}
                onSend={onSendMessage}
                resendMessage={() => { }}
                currentUser={user}
              />
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(DrHera)
);
