import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import HealthPayment from "../../../../components/Service/HealthServices/HealthPayment";

const HealthServicesPayment = (props) => {
  return <HealthPayment {...props} />;
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HealthServicesPayment)
);
