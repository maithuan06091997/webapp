import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { _CategoryService } from "../../../../actions";
import CardServices from "../../../../components/CardServices";
import * as func from "../../../../helpers";
import "./style.scss";
import { url } from "../../../../constants/Path";
import { Redirect, withRouter } from "react-router";
import { nameOH } from "../../../../constants/Default";
import { FaRegListAlt } from "react-icons/fa";
import { Button, Modal } from "antd";
import PackageHealthServicesItem from "./PackageHealthServicesItem";
import { createServiceTransaction } from "../../../../services/HealthService";

function PackageHealthServices(props) {
  const { history, category_service, _CategoryService } = props;
  const [hasChecked, setHasChecked] = useState(false);
  const [subPrice, setSubPrice] = useState(0);
  const formRef = useRef();
  const form =
    category_service && category_service.form ? category_service.form : [];
  const formOne = func.findDataOfStep(form, 1);
  const formTwo = func.findDataOfStep(form, 2);
  const [modal, contextHolder] = Modal.useModal();

  // useEffect(() => {
  //   if (formOne !== "") {
  //     if (formOne.services.length === 0) {
  //       const form =
  //         category_service && category_service.form
  //           ? category_service.form
  //           : [];
  //       const formOne = func.findDataOfStep(form, 1);
  //       const params = {
  //         step: 3,
  //         data: {
  //           id: [],
  //           name: [],
  //           price: formOne.price,
  //         },
  //       };
  //       const s3 = form.findIndex((key) => {
  //         return key.step === 3;
  //       });
  //       if (s3 === -1) {
  //         form.push(params);
  //       } else {
  //         form[s3] = params;
  //       }
  //       _CategoryService(form);
  //       history.push(url.name.service_health_payment);
  //     } else {
  //       setSubPrice(formOne.services[0].normal_price);
  //     }
  //   }
  // }, [formOne, history, _CategoryService, category_service]);
  if (form.length < 2) {
    return <Redirect to={url.name.service_health} />;
  }
  const yieldProfileInfo = () => {
    return (
      <form ref={formRef}>
        {formOne.services.map((service, index) => (
          <PackageHealthServicesItem
            service={service}
            index={index}
            key={index}
            hasChecked={hasChecked}
            subPrice={subPrice}
            setSubPrice={setSubPrice}
          />
        ))}
      </form>
    );
  };

  const handCheckedPackage = () => {
    if (!hasChecked) {
      let total = 0;
      formOne.services.map((service) => (total += service.normal_price));
      setSubPrice(total);
    } else {
      setSubPrice(formOne.services[0].normal_price);
    }
    setHasChecked(!hasChecked);
  };

  const onNextPage = async () => {
    const services_reg = formOne.services.map((value) => {
      return value.service_id;
    });
    const postData = {
      one_health_msg: {
        user: formTwo.user,
        service: formOne._id,
        profile: formTwo._id,
        services_reg: services_reg,
        source: "miniapp",
        mini_app_source: "mb_bank",
        payment_type: 2,
      },
    };
    try {
      const response = await createServiceTransaction(postData);
      const { transaction } = response.one_health_msg;
      console.log(transaction);
      window["ReactNativeWebView"].postMessage(
        JSON.stringify({
          type: "PAYMENT_HUB_TRANSACTION",
          data: {
            merchant: transaction.merchant,
            type: transaction.type,
            id: transaction.id,
            amount: transaction.amount,
            description: transaction.description,
            successMessage: transaction.successMessage,
          },
        })
      );
    } catch (error) {
      console.log(error.response);
      modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Dịch vụ này đã đăng ký",
      });
    }
  };

  const renderPackageServices = () => {
    return (
      <>
        <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
          <div className="wrap-panel-item">
            <div className="wrap-item-img">
              {formOne.service_img && (
                <img
                  className="img-fluid"
                  src={formOne.service_img.url}
                  alt=""
                />
              )}
              {!formOne.service_img && <div className="img-fluid1"></div>}
            </div>
            <div className="wrap-item-info">
              <h4>{formOne.title}</h4>
              <h4 className="sub-name">{formOne.price.toLocaleString()} đ</h4>
              <p className="address">{nameOH[formOne.source]}</p>
            </div>
          </div>
        </div>
        <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col2">
          <div className="wrap-panel-item wrap-profile-info">
            <div className="wrap-title">
              <h4>
                <FaRegListAlt /> Chọn dịch vụ
              </h4>
              <div>
                <label className="d-flex align-items-center">
                  <span className="mr-1">Tất cả</span>
                  <input
                    type="checkbox"
                    onChange={handCheckedPackage}
                    checked
                    disabled
                  />
                </label>
              </div>
            </div>
            <div className="wrap-main">{yieldProfileInfo()}</div>
            <div className="wrap-price">
              <div className="price">
                <p className="label">Giá gói dịch vụ</p>
                <p className="value">{formOne.price.toLocaleString()} đ</p>
              </div>
              <Button className="btn-accept w-100" onClick={onNextPage}>
                <span>Tiếp tục</span>
              </Button>
            </div>
          </div>
        </div>
      </>
    );
  };
  return (
    <CardServices
      header="Chọn gói dịch vụ"
      // description="Quý khách vui lòng chọn gói dịch vụ"
      step="Gói Dịch Vụ"
      className="package-service"
    >
      <div className="clearfix" />
      <div className="wrap-content package-health-service">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={formOne ? false : true}
              full={true}
              length={2}
              rows={1}
              data={renderPackageServices()}
            />
          </div>
        </div>
      </div>
      {contextHolder}
    </CardServices>
  );
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    PackageHealthServices
  )
);
