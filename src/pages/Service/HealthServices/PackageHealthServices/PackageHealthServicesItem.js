import { Modal } from "antd";
import React from "react";

const PackageHealthServicesItem = ({
  service,
  index,
  hasChecked,
  subPrice,
  setSubPrice,
}) => {
  const [modal, contextHolder] = Modal.useModal();

  // const handSubPrice = (e) => {
  //   if (e.target.checked) {
  //     setSubPrice(subPrice + service.normal_price);
  //   } else {
  //     const newPrice = subPrice - service.normal_price;
  //     setSubPrice(newPrice);
  //     if (newPrice === 0) {
  //       e.target.checked = true;
  //       setSubPrice(service.normal_price);
  //       modal.warning({
  //         title: "Thông báo",
  //         okText: "Đóng",
  //         centered: true,
  //         content: "Vui lòng chọn tối thiểu một dịch vụ trong danh sách!",
  //       });
  //     }
  //   }
  // };
  return (
    <label className="wrap-line">
      <p className="label">{service.vi_name}</p>
      {index === 0 && hasChecked && (
        <input
          className="value"
          type="checkbox"
          value={service.service_id + "|" + service.vi_name}
          name={`checkbox${index}`}
          defaultChecked={true}
          checked
          disabled
          // onChange={(e) => handSubPrice(e)}
        />
      )}
      {index === 0 && !hasChecked && (
        <input
          className="value"
          type="checkbox"
          value={service.service_id + "|" + service.vi_name}
          name={`checkbox${index}`}
          defaultChecked={true}
          checked
          disabled
          // onChange={(e) => handSubPrice(e)}
        />
      )}
      {index !== 0 && hasChecked && (
        <input
          className="value"
          type="checkbox"
          name={`checkbox${index}`}
          value={service.service_id + "|" + service.vi_name}
          defaultChecked={true}
          checked
          disabled
          // onChange={(e) => handSubPrice(e)}
        />
      )}
      {index !== 0 && !hasChecked && (
        <input
          className="value"
          type="checkbox"
          value={service.service_id + "|" + service.vi_name}
          name={`checkbox${index}`}
          defaultChecked={false}
          checked
          disabled
          // onChange={(e) => handSubPrice(e)}
        />
      )}
      {contextHolder}
    </label>
  );
};

export default PackageHealthServicesItem;
