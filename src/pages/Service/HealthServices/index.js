import { Empty, Button } from "antd";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { withRouter, useLocation } from "react-router";
import BoxSkeleton from "../../../components/BoxSkeleton";
import CardServices from "../../../components/CardServices";
import { public_url } from "../../../constants/Path";
import { getInfoMb } from "../../../services/HealthService";
import HealthServicesByCategory from "./HealthServicesByCategory";
import "./style.scss";

function HealthServices(props) {
  const [listServices, setListServices] = useState();
  const [infoMb, setInfoMb] = useState();
  const [category, setCategory] = useState();
  const params = useLocation();
  const [errors, setErrors] = useState();
  const user = localStorage.getItem("user");
  useEffect(() => {
    async function getApi() {
      const tokenObj = new URLSearchParams(params.search);
      const token = tokenObj.get("logintoken");
      if (token) {
        const loginToken = localStorage.getItem("loginToken");
        localStorage.setItem("loginToken", token);
        if (loginToken !== token) {
          try {
            const infoMb = await getInfoMb(token);
            if (infoMb.one_health_msg) {
              localStorage.setItem(
                "user",
                JSON.stringify(infoMb.one_health_msg)
              );
              setInfoMb(infoMb);
            }
          } catch (error) {
            if (error.response) {
              setErrors(error.response?.data?.status?.message);
            }
          }
        }
      }
      const data = [
        {
          _id: "59c7614285e41690d99d9e68",
          name: "Nổi bật",
          short_name: "noibat",
          icon: {
            _id: "59c8c3a585e41690d99ff33f",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c7614285e41690d99d9e68.png",
          },
        },
        {
          _id: "59c7618785e41690d99d9ef3",
          name: "Độc quyền",
          short_name: "docquyen",
          icon: {
            _id: "59c8c3a585e41690d99ff341",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c7618785e41690d99d9ef3.png",
          },
        },
        {
          _id: "59c853ce85e41690d99ee1ee",
          name: "D.vụ Ytế",
          short_name: "dichvuyte",
          icon: {
            _id: "59c8c29685e41690d99ff124",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c853ce85e41690d99ee1ee.png",
          },
        },
        {
          _id: "59c8542885e41690d99ee294",
          name: "Xét nghiệm",
          short_name: "xetnghiem",
          icon: {
            _id: "59c8c3a585e41690d99ff343",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c8542885e41690d99ee294.png",
          },
        },
        {
          _id: "5cf0865a43edc296163d8eb9",
          name: "Gym & Yoga",
          short_name: "gym_and_yoga",
          icon: {
            _id: "5cf08a2d43edc296163dd7af",
            url: "https://dev-chat.droh.co/images/group_service/icon/ic_cato7.png",
          },
        },
        {
          _id: "5d883f8221ea73b306199e0d",
          name: "Bar Dr.Tea ",
          short_name: "Bar Dr.Tea ",
          icon: {
            _id: "5d883fe121ea73b30619a337",
            url: "https://dev-chat.droh.co/images/group_service/icon/drtea.png",
          },
        },
        {
          _id: "59c8546585e41690d99ee305",
          name: "Thẩm mỹ",
          short_name: "thammy",
          icon: {
            _id: "59c8c3a585e41690d99ff345",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c8546585e41690d99ee305.png",
          },
        },
        {
          _id: "59c8554885e41690d99ee474",
          name: "Các gói khác",
          short_name: "cacgoikhac",
          icon: {
            _id: "59c8c3a585e41690d99ff347",
            url: "https://dev-chat.droh.co/images/group_service/icon/59c8554885e41690d99ee474.png",
          },
        },
      ];
      setListServices(data);
      setCategory(data[0]._id);
    }
    getApi();
  }, [params.search]);

  const onPressNext = (service) => {
    setCategory(service._id);
  };

  const renderListSpecial = () => {
    if (listServices) {
      if (Array.isArray(listServices) && listServices.length > 0) {
        return listServices.map((service, index) => (
          <div
            key={index}
            className={`wrap-panel-col ${
              service._id === category ? "active" : ""
            }`}
          >
            <div
              onClick={() => onPressNext(service)}
              className="wrap-panel-item"
            >
              <div className="wrap-item-img">
                <img className="img-fluid" src={service.icon.url} alt="" />
              </div>
              <div className="wrap-item-info">
                <span>{service.name}</span>
              </div>
            </div>
          </div>
        ));
      } else {
        return (
          <Empty className="ant-empty-custom" description="Dịch vụ trống" />
        );
      }
    }
  };

  const back = () => {
    window["ReactNativeWebView"].postMessage(
      JSON.stringify({
        type: "GO_BACK",
      })
    );
  };

  if (errors)
    return (
      <>
        <div className="wrap-content-error">
          <img
            className="img-fluid"
            src={public_url.img + "/thanks.png"}
            alt=""
          />
          <div className="content">
            <h3>{errors}</h3>
            <Button className="btn-accept custom-btn" onClick={back}>
              <span>Back</span>
            </Button>
          </div>
        </div>
      </>
    );
  return (
    <CardServices
      header="Chọn loại dịch vụ"
      description="Quý khách vui lòng chọn loại dịch vụ"
      step="Loại Dịch Vụ"
      hiddenButtonBack={true}
    >
      <div className="clearfix" />
      <div className="wrap-content block-list-health-service block-list-health-service-custom">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={listServices ? false : true}
              full={false}
              panelCol={3}
              length={21}
              rows={1}
              data={renderListSpecial()}
            />
          </div>
        </div>
      </div>
      {((infoMb && infoMb.one_health_msg) ||
        (user && JSON.parse(user) && JSON.parse(user).token)) && (
        <HealthServicesByCategory
          categoryId={category}
          token={infoMb ? infoMb.one_health_msg.token : JSON.parse(user).token}
          personal={
            infoMb ? infoMb.one_health_msg.personal : JSON.parse(user).personal
          }
        />
      )}
    </CardServices>
  );
}

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HealthServices)
);
