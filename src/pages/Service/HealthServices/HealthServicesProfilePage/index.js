import React, { Fragment } from "react";
import { Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import HealthServicesProfile from "../../../../components/Service/HealthServices/HealthServicesProfile";
import { url } from "../../../../constants/Path";

const StepOnePage = (props) => {
  const { category_service } = props;
  const form =
    category_service && category_service.form ? category_service.form : [];

  if (form.length < 1) {
    return <Redirect to={url.name.service_health} />;
  }

  return (
    <Fragment>
      <HealthServicesProfile {...props} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(StepOnePage)
);
