import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import Loading from "../../../../components/Loading";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { _CategoryService } from "../../../../actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { getDetailsHealthServices } from "../../../../services/HealthService";
import { url } from "../../../../constants/Path";

const ModalDetailHealthServices = (props) => {
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalDetailVoucher, setModalDetailVoucher] = useState(false);
  const [infoDetailHealthServices, setInfoDetailHealthServices] =
    useState(undefined);
  //*** Declare props ***//
  const {
    _TokenExpired,
    _CategoryService,
    onOpenModalDetailVoucher,
    detailHealthServices,
    openModalDetailVoucher,
    token,
  } = props;
  useEffect(() => {
    async function getApi(id) {
      setLoading(true);
      setSkeleton(true);
      setModalDetailVoucher(true);
      const detailsHealthServices = await getDetailsHealthServices(
        id,
        {
          showContent: 1,
        },
        token
      );
      setInfoDetailHealthServices(detailsHealthServices.one_health_msg[0]);
      setLoading(false);
      setSkeleton(false);
    }
    if (
      props.location.search &&
      props.location.search.split("id=")[1] &&
      !openModalDetailVoucher
    ) {
      const id = props.location.search.split("id=")[1];
      getApi(id);
    }
    if (openModalDetailVoucher) {
      const id = detailHealthServices._id;
      getApi(id);
    }
  }, [
    _TokenExpired,
    openModalDetailVoucher,
    detailHealthServices,
    props.location.search,
    token,
  ]);

  const onCloseModalDetailVoucher = (event) => {
    setInfoDetailHealthServices(undefined);
    onOpenModalDetailVoucher(event, false);
    setModalDetailVoucher(false);
    props.location.search = undefined;
  };

  const nextPageHealthProfile = (item) => {
    const params = [
      {
        step: 1,
        data: item,
      },
    ];
    _CategoryService(params);
    props.history.push({ pathname: url.name.service_health_profile });
  };

  function yieldDetailVoucher() {
    if (infoDetailHealthServices) {
      return (
        <Fragment>
          <div className="wrap-top">
            {infoDetailHealthServices.service_img && (
              <img
                className="img-fluid"
                src={infoDetailHealthServices.service_img.url}
                alt=""
              />
            )}
            {!infoDetailHealthServices.service_img && (
              <div className="banner-top"></div>
            )}
            <h4>{infoDetailHealthServices.title}</h4>
          </div>
          <div className="wrap-main">
            <div className="info-short-desc">
              <div className="right">
                <p className="label">
                  Loại dịch vụ:{" "}
                  <span>
                    {infoDetailHealthServices.type === 1
                      ? "Tại nhà"
                      : "Tại phòng khám"}
                  </span>
                </p>
                <p className="label">
                  Đơn vị cung cấp:{" "}
                  <span>{infoDetailHealthServices.provider?.name}</span>
                </p>
                <p className="label">
                  Địa chỉ:{" "}
                  <span>{infoDetailHealthServices.provider?.address}</span>
                </p>
              </div>
            </div>
            <div className="info-desc">
              <p className="label">
                Giá dịch vụ:{" "}
                <span className="point">
                  {infoDetailHealthServices.price?.toLocaleString()} đ
                </span>
              </p>
              <div
                onClick={() => nextPageHealthProfile(infoDetailHealthServices)}
                className="ant-btn btn-accept"
              >
                <span>Đăng ký</span>
              </div>
              {infoDetailHealthServices.content && (
                <>
                  <h4 className="title">Mô tả</h4>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: infoDetailHealthServices.content,
                    }}
                  />
                </>
              )}
            </div>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <Modal
        title="Chi tiết quà"
        className="block-modal-detail-voucher"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modalDetailVoucher}
        onCancel={onCloseModalDetailVoucher}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetailVoucher()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    ModalDetailHealthServices
  )
);
