import { Empty, Modal } from "antd";
import React, { useEffect, useState } from "react";
import Pagination from "react-js-pagination";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import ModalDetailHealthServices from "../ModalDetailHealthServices";
import {
  getDetailsHealthServices,
  getListHealthServicesByCategory,
} from "../../../../services/HealthService";
import "./style.scss";
import * as func from "../../../../helpers";
import { _CategoryService } from "../../../../actions";
import { url } from "../../../../constants/Path";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

function HealthServicesByCategory(props) {
  const limit = 9;
  const { categoryId, token, personal, _CategoryService } = props;
  const [activePage, setActivePage] = useState(1);
  const [total, setTotal] = useState();
  const [openModalDetailVoucher, setOpenModalDetailVoucher] = useState(false);
  const [detailHealthServices, setDetailHealthServices] = useState();
  const [listHealthServicesByCategory, setListHealthServicesByCategory] =
    useState();
  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    setListHealthServicesByCategory();
    async function getApi() {
      const listHealthServicesByCategory =
        await getListHealthServicesByCategory(
          {
            categorytype: categoryId,
            limit: limit,
            page: activePage,
            miniapp: "mb_bank",
          },
          token
        );
      setListHealthServicesByCategory(listHealthServicesByCategory);
      setTotal(listHealthServicesByCategory.totalCount);
    }
    getApi();
  }, [categoryId, activePage, token]);

  useEffect(() => {
    setActivePage(1);
  }, [categoryId]);

  const handlePageChange = (pageNumber) => {
    setActivePage(pageNumber);
  };

  const onOpenModalDetailVoucher = (event, flag, item) => {
    event.stopPropagation();
    setOpenModalDetailVoucher(flag);
    setDetailHealthServices(item);
  };

  const onOpenModalErrorGender = (gender_name) => {
    modal.warning({
      title: "Thông báo",
      okText: "Đóng",
      centered: true,
      content: `Gói này chỉ dành cho ${gender_name}`,
    });
  };

  const nextPageHealthProfile = async (item) => {
    if (item.isRegistered) {
      modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Dịch vụ này đã đăng ký",
      });
    } else {
      const detailsHealthServices = await getDetailsHealthServices(
        item._id,
        {
          showContent: 1,
        },
        token
      );
      const params = [
        {
          step: 1,
          data: detailsHealthServices.one_health_msg[0],
        },
      ];
      _CategoryService(params);
      props.history.push({ pathname: url.name.service_health_profile });
    }
  };

  const renderItemHealthServicesByCategory = () => {
    if (listHealthServicesByCategory) {
      if (
        Array.isArray(listHealthServicesByCategory.one_health_msg) &&
        listHealthServicesByCategory.one_health_msg.length > 0
      ) {
        return listHealthServicesByCategory.one_health_msg.map(
          (item, index) => {
            const gender_value = func.findGender(personal.sex, "value");
            const gender_name = func.findGender(item.only, "text");
            return (
              <div
                key={index}
                onClick={
                  gender_value === item.only || item.only === "all"
                    ? () => nextPageHealthProfile(item)
                    : () => onOpenModalErrorGender(gender_name)
                }
                className="col-xl-4 col-lg-4 col-md-4 wrap-panel-col"
              >
                <div className="wrap-panel-item">
                  <div className="wrap-top">
                    {item.service_img && (
                      <img
                        className="img-fluid"
                        src={item.service_img.url}
                        alt=""
                      />
                    )}
                    {!item.service_img && <div></div>}
                  </div>
                  <div className="wrap-main">
                    <div className="label">{item.title}</div>
                    <div>
                      {item.type === 1 ? (
                        <>Lấy mẫu tại nhà (Nội thành TP.HCM)</>
                      ) : (
                        <>
                          Tại phòng khám (116 Gò Dầu,Phường Tân Qúy, Q.Tân Phú,
                          TP.HCM)
                        </>
                      )}
                    </div>
                    <div className="info">
                      <span>{item.price.toLocaleString()} đ</span>
                      <div>
                        <span
                          className="ant-btn ant-btn-lg read-more"
                          onClick={(event) =>
                            onOpenModalDetailVoucher(event, true, item)
                          }
                        >
                          Xem chi tiết
                        </span>
                        <span
                          className={`ant-btn ant-btn-lg btn-booking booking ${
                            item.isRegistered ? "disable" : ""
                          }`}
                        >
                          Đặt dịch vụ
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        );
      } else {
        return (
          <Empty className="ant-empty-custom" description="Chưa có dịch vụ" />
        );
      }
    }
  };

  const renderListHealthServicesByCategory = () => {
    return (
      <div className="wrap-panel-line">
        <div className="wrap-content-header">
          <div className="wrap-content-body w-100">
            <div className="row">{renderItemHealthServicesByCategory()}</div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="wrap-panel-content health-category">
      <div className="row mx-0">
        <BoxSkeleton
          skeleton={listHealthServicesByCategory ? false : true}
          length={limit}
          rows={3}
          panelCol={3}
          data={renderListHealthServicesByCategory()}
        />
        <ModalDetailHealthServices
          {...props}
          onOpenModalDetailVoucher={onOpenModalDetailVoucher}
          openModalDetailVoucher={openModalDetailVoucher}
          detailHealthServices={detailHealthServices}
          token={token}
        />
      </div>
      {total && (
        <div className={"block-pagination"}>
          <Pagination
            hideDisabled
            innerClass="pagination"
            activePage={activePage}
            itemsCountPerPage={limit}
            totalItemsCount={total}
            pageRangeDisplayed={4}
            onChange={handlePageChange}
            itemClass="page-item"
            linkClass="page-link"
          />
        </div>
      )}
      {contextHolder}
    </div>
  );
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    HealthServicesByCategory
  )
);
