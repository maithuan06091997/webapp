import React, { Fragment, useCallback, useEffect, useState } from "react";
import _ from "lodash";
import queryString from "query-string";
import { public_url, url } from "../../../constants/Path";
import {
  _Confirmation,
  _Appointment,
  _CallChat,
  _CategoryService,
  _DrugStore,
  _HomeTest,
} from "../../../actions";
import { withRouter, Redirect, Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

const SUCCESS = "Thành công";
const FAILED = "Thất bại";
const GATEWAY_ONEPAY = "OnePay";
const GATEWAY_WALLET = "Wallet";

const ServiceConfirmationPage = (props) => {
  const [method, setMethod] = useState(undefined);
  const [status, setStatus] = useState(undefined);
  //*** Declare props ***//
  const {
    _Confirmation,
    _Appointment,
    _CallChat,
    _HomeTest,
    _CategoryService,
    _DrugStore,
    user,
    location,
    confirm,
  } = props;
  const parsed = queryString.parse(location.search);

  const handleCleanConfirmation = useCallback(() => {
    _Confirmation({
      status: false,
      type: "",
      name: "",
      gateway: "",
    });
  }, [_Confirmation]);

  useEffect(() => {
    console.log(confirm);
    if (confirm.name === "APPOINTMENT") {
      _Appointment([]);
    }
    if (confirm.name === "VIDEO_CALL_SCHEDULE") {
      _CallChat([]);
    }
    if (confirm.name === "HEALTH") {
      _CategoryService([]);
      _DrugStore([]);
    }
    if (confirm.name === "RECORD") {
      _HomeTest([]);
    }
  }, [
    _Appointment,
    _CallChat,
    _HomeTest,
    _DrugStore,
    _CategoryService,
    confirm,
  ]);

  useEffect(() => {
    if (confirm.gateway === "Wallet") {
      setMethod(GATEWAY_WALLET);
      if (!_.isEmpty(parsed)) {
        handleCleanConfirmation();
      }
    } else {
      if (!_.isEmpty(parsed)) {
        if (parsed.type && parsed.status) {
          switch (parsed.type) {
            case "onepay":
              setMethod(GATEWAY_ONEPAY);
              break;
            default:
              handleCleanConfirmation();
          }
          if (func.ucFirst(parsed.status) === "Done") {
            setStatus(SUCCESS);
          } else if (func.ucFirst(parsed.status) === "Failed") {
            setStatus(FAILED);
          } else {
            handleCleanConfirmation();
          }
        } else {
          handleCleanConfirmation();
        }
      }
    }
  }, [handleCleanConfirmation, confirm.gateway, parsed]);

  if (!confirm.status || confirm.type !== "service" || !confirm.gateway) {
    return <Redirect to={url.name.home} />;
  }

  function yieldFailed() {
    return (
      <div className="wrap-content-failed">
        <h4>
          Kính gửi {user.personal.last_name + " " + user.personal.first_name},
        </h4>
        <p className="font-weight-bold">
          Cảm ơn quý khách đã sử dụng dịch vụ của Dr.OH
        </p>
        <p>
          <span className="label">Phương thức thanh toán: </span>
          <span className="value method">{method}</span>
        </p>
        <p>
          <span className="label">Tình trạng thanh toán: </span>
          <span className="value badge badge-danger">{status}</span>
        </p>
        <p className="label">
          Chúng tôi rất tiếc, thanh toán của bạn chưa thành công. Nó có thể là
          một số lý do sau:
        </p>
        <p className="label">- Bạn đã hủy thanh toán.</p>
        <p className="label">- Bạn đã nhập sai ngày hết hạn thẻ.</p>
        <p className="label">- Tín dụng của bạn đã đạt đến giới hạn.</p>
        <p className="label">- Lỗi máy tính hoặc mạng.</p>
        <div className="wrap-img">
          <img
            className="img-fluid"
            src={public_url.img + "/thanks.png"}
            alt=""
          />
        </div>
      </div>
    );
  }

  function yieldSuccessAppointment() {
    return (
      <div className="wrap-content">
        <img
          className="img-fluid"
          src={public_url.img + "/thanks.png"}
          alt=""
        />
        <div className="content">
          <h3>Cảm ơn quý khách đã sử dụng dịch vụ của Dr.OH</h3>
          <p>
            Quý khách vui lòng xem thông báo xác nhận đặt khám và quy trình đặt
            khám tại bệnh viện đã gửi qua ứng dụng hoặc truy cập vào phần{" "}
            <Link to={url.name.account_list_history}>"lịch hẹn"</Link> để xem
            chi tiết.
          </p>
        </div>
      </div>
    );
  }

  function yieldSuccessVideoCall() {
    return (
      <div className="wrap-content">
        <img
          className="img-fluid"
          src={public_url.img + "/thanks.png"}
          alt=""
        />
        <div className="content">
          <h3>Cảm ơn quý khách đã sử dụng dịch vụ của Dr.OH</h3>
          <p>
            Quý khách vui lòng xem thông báo xác nhận đặt dịch vụ đã gửi qua ứng
            dụng hoặc truy cập vào phần
            <Link to={url.name.account_list_history}>"lịch hẹn"</Link> để xem
            chi tiết.
          </p>
        </div>
      </div>
    );
  }

  function yieldSuccessHealth() {
    return (
      <div className="wrap-content">
        <img
          className="img-fluid"
          src={public_url.img + "/thanks.png"}
          alt=""
        />
        <div className="content">
          <h3>Cảm ơn quý khách đã sử dụng dịch vụ của Dr.OH</h3>
          <p>
            Quý khách vui lòng xem thông báo xác nhận đặt dịch vụ đã gửi qua ứng
            dụng hoặc truy cập vào phần
            <Link to={url.name.account_list_history}>"lịch hẹn"</Link> để xem
            chi tiết.
          </p>
        </div>
      </div>
    );
  }

  function yieldConfirmation() {
    if (method === GATEWAY_ONEPAY) {
      if (status === SUCCESS) {
        if (confirm.name === "APPOINTMENT") {
          return yieldSuccessAppointment();
        }
        if (confirm.name === "VIDEO_CALL_SCHEDULE") {
          return yieldSuccessVideoCall();
        }
        if (confirm.name === "HEALTH") {
          return yieldSuccessHealth();
        }
        if (confirm.name === "RECORD") {
          return yieldSuccessAppointment();
        }
      } else {
        return yieldFailed();
      }
    }

    if (method === GATEWAY_WALLET) {
      if (confirm.name === "APPOINTMENT") {
        return yieldSuccessAppointment();
      }
      if (confirm.name === "VIDEO_CALL_SCHEDULE") {
        return yieldSuccessVideoCall();
      }
      if (confirm.name === "HEALTH") {
        return yieldSuccessHealth();
      }
      if (confirm.name === "RECORD") {
        return yieldSuccessAppointment();
      }
    }
  }

  return (
    <Fragment>
      <div className="block-service-confirmation">
        <div className="card card-custom">
          <div className="card-header">
            <h3>Xác nhận đặt dịch vụ</h3>
          </div>
          <div className="card-body">{yieldConfirmation()}</div>
        </div>
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      _Confirmation,
      _Appointment,
      _HomeTest,
      _CallChat,
      _CategoryService,
      _DrugStore,
    },
    dispatch
  );
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    ServiceConfirmationPage
  )
);
