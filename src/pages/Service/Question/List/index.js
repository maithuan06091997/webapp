import React, { Fragment, useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import {
  ModelGetQuestionDetails,
  ModelGetQuestions,
  ModelGetSpecialities,
  ModelLikeQuestion,
} from "../../../../models/ModelQuestion";
import * as func from "../../../../helpers";
import { FaUser, FaUserCircle } from "react-icons/fa";
import Rating from "react-rating";
import { public_url, url } from "../../../../constants/Path";
import InfiniteScroll from "react-infinite-scroller";
import { Empty, Modal, Skeleton } from "antd";
import Timeago from "../../../../components/Timeago";
import {
  BsListUl,
  BsChevronDown,
  BsChevronUp,
  BsHeart,
  BsDot,
  BsHeartFill,
} from "react-icons/bs";

const ListQuestions = ({ history, ...props }) => {
  const [loading, setLoading] = useState(false);
  const [listQuestions, setListQuestions] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [activeQuestionId, setActiveQuestionId] = useState(null);
  const [questionDetails, setQuestionDetails] = useState(null);
  const [getDetailsLoading, setGetDetailsLoading] = useState(null);
  const [keyword, setKeyword] = useState("");
  const [specialities, setSpecialities] = useState([]);
  const [showSpecialities, setShowSpecialities] = useState(false);
  const [searchParams, setSearchParams] = useState({
    page: 1,
    keyword: "",
    speciality: null,
  });

  useEffect(() => {
    getSpecialities();
    if (
      props.computedMatch &&
      props.computedMatch.params &&
      props.computedMatch.params.questionId
    ) {
      onShowModalDetail(props.computedMatch.params.questionId);
    }
  }, []);

  useEffect(() => {
    getListQuestions();
  }, [searchParams]);

  const getSpecialities = () => {
    ModelGetSpecialities().then((response) => {
      if (response && response.data && response.data.one_health_msg) {
        setSpecialities(response.data.one_health_msg);
      }
    });
  };

  const getListQuestions = () => {
    const loginData = localStorage.getItem("user");
    const user = JSON.parse(loginData);
    setLoading(true);
    const params = {
      search_option: {
        search: searchParams.keyword.trim(),
        status: 3,
      },
    };
    ModelGetQuestions(
      user.personal._id,
      searchParams.page,
      searchParams.speciality,
      params
    ).then((response) => {
      setLoading(false);
      if (response && response.data && response.data.one_health_msg) {
        const newListQuestions = listQuestions.concat(
          response.data.one_health_msg
        );
        setListQuestions(newListQuestions);
        if (response.data.one_health_msg.length === 25) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    });
  };

  const loadMore = () => {
    setLoading(true);
    const { page } = searchParams;
    setSearchParams({ ...searchParams, page: page + 1 });
  };

  const likeQuestion = (questionId) => {
    if (listQuestions && listQuestions.length) {
      const newListQuestions = listQuestions.map((item) => {
        if (item._id === questionId && !item.liked) {
          return {
            ...item,
            liked: true,
            likes: item.likes ? item.likes + 1 : 1,
          };
        }
        return item;
      });
      setListQuestions(newListQuestions);

      if (questionDetails && questionDetails._id === questionId) {
        const newQuestionDetails = {
          ...questionDetails,
          liked: true,
          likes: questionDetails.likes ? questionDetails.likes + 1 : 1,
        };
        setQuestionDetails(newQuestionDetails);
      }

      const loginData = localStorage.getItem("user");
      const user = JSON.parse(loginData);
      const params = {
        like: {
          patient: {
            id: user.personal._id,
          },
          question: {
            id: questionId,
          },
        },
      };
      ModelLikeQuestion(params);
    }
  };

  const renderListData = () => {
    return listQuestions.map((item, index) => (
      <div className="question-item" key={`question_${index}`}>
        <div className="d-flex">
          <p className="mb-0 text-muted flex-fill">
            {item.sender
              ? `${func.getSexName(item.sender.sex)} - ${func.yieldAge(
                  item.sender.birthday
                )} tuổi`
              : null}
          </p>
          <Timeago className="text-muted" date={item.question_time} />
        </div>
        <p className="question-content">{item.question}</p>
        <div className="answer-item">
          <div className="answer-doctor">
            <div className="avatar-block mr-2">
              {item.replier.avatar && item.replier.avatar.url ? (
                <img src={item.replier.avatar.url} alt="replier-avatar" />
              ) : (
                <FaUserCircle size={`40px`} />
              )}
            </div>
            <div className="doctor-info">
              <p className="mb-0 font-weight-bold">
                {item.replier.last_name} {item.replier.first_name}
              </p>
              {item.replier.rate ? (
                <Fragment>
                  <Rating
                    initialRating={item.replier.rate.rate}
                    className="rating"
                    readonly
                    emptySymbol={
                      <img
                        src={`${public_url.img}/icons/icon_star_non.png`}
                        alt=""
                        className="icon"
                      />
                    }
                    fullSymbol={
                      <img
                        src={`${public_url.img}/icons/icon_star_rated.png`}
                        alt=""
                        className="icon"
                      />
                    }
                  />
                  <span className="text-muted">
                    {" "}
                    ({item.replier.rate.count} <FaUser size=".75rem" />)
                  </span>
                </Fragment>
              ) : null}
            </div>
          </div>
          <p className="answer-content">{item.answer}</p>
        </div>
        <div className="d-flex mt-2">
          <div className="flex-fill">
            <Link
              to={`#`}
              onClick={() => likeQuestion(item._id)}
              className={`btn-like ${item.liked ? "liked" : ""}`}
            >
              <b>{item.likes}</b> {item.liked ? <BsHeartFill /> : <BsHeart />}{" "}
              <BsDot /> Cảm ơn
            </Link>
          </div>
          <Link
            className="text-primary"
            to={`#`}
            onClick={() => onShowModalDetail(item._id)}
          >
            Chi tiết câu hỏi
          </Link>
        </div>
      </div>
    ));
  };

  const renderEmpty = () => {
    return !(listQuestions && listQuestions.length) ? (
      <Empty className="ant-empty-custom" description={false} />
    ) : null;
  };

  const renderLoading = () => {
    return (
      <div className="loading-item">
        <div className="question-item" key={`question_loading_0`}>
          <Skeleton active paragraph={{ rows: 3 }} />
        </div>
        <div className="question-item" key={`question_loading_1`}>
          <Skeleton active paragraph={{ rows: 3 }} />
        </div>
      </div>
    );
  };

  const onCloseModalDetail = () => {
    setModalVisible(false);
    history.push(`${url.name.list_questions}`);
  };

  const onShowModalDetail = (questionId) => {
    setModalVisible(true);
    setActiveQuestionId(questionId);
  };

  useEffect(() => {
    if (activeQuestionId) {
      history.push(`${url.name.list_questions}/${activeQuestionId}`);

      const loginData = localStorage.getItem("user");
      const user = JSON.parse(loginData);
      setGetDetailsLoading(true);
      ModelGetQuestionDetails(user.personal._id, activeQuestionId).then(
        (response) => {
          setGetDetailsLoading(false);
          if (response && response.data && response.data.one_health_msg) {
            setQuestionDetails(response.data.one_health_msg);
          }
        }
      );
    }
  }, [activeQuestionId]);

  const renderModalDetail = () => {
    return (
      <Modal
        title="Chi tiết câu hỏi"
        visible={modalVisible}
        closable={true}
        footer={null}
        onCancel={onCloseModalDetail}
        centered
      >
        {getDetailsLoading ? (
          <Skeleton active paragraph={{ rows: 6 }} />
        ) : questionDetails ? (
          <div className="question-details-modal">
            <div className="question-item">
              <div className="d-flex">
                <p className="mb-0 text-muted flex-fill">
                  {questionDetails.sender
                    ? `${func.getSexName(
                        questionDetails.sender.sex
                      )} - ${func.yieldAge(
                        questionDetails.sender.birthday
                      )} tuổi`
                    : null}
                </p>
                <Timeago
                  className="text-muted"
                  date={questionDetails.question_time}
                />
              </div>
              <p className="question-content">{questionDetails.question}</p>
              <div className="answer-item">
                <div className="answer-doctor">
                  <div className="avatar-block mr-2">
                    {questionDetails.replier.avatar &&
                    questionDetails.replier.avatar.url ? (
                      <img
                        src={questionDetails.replier.avatar.url}
                        alt="replier-avatar"
                      />
                    ) : (
                      <FaUserCircle size={`40px`} />
                    )}
                  </div>
                  <div className="doctor-info">
                    <p className="mb-0 font-weight-bold">
                      {questionDetails.replier.last_name}{" "}
                      {questionDetails.replier.first_name}
                    </p>
                    {questionDetails.replier.rate ? (
                      <Fragment>
                        <Rating
                          initialRating={questionDetails.replier.rate.rate}
                          className="rating"
                          readonly
                          emptySymbol={
                            <img
                              src={`${public_url.img}/icons/icon_star_non.png`}
                              alt=""
                              className="icon"
                            />
                          }
                          fullSymbol={
                            <img
                              src={`${public_url.img}/icons/icon_star_rated.png`}
                              alt=""
                              className="icon"
                            />
                          }
                        />
                        <span className="text-muted">
                          {" "}
                          ({questionDetails.replier.rate.count}{" "}
                          <FaUser size=".75rem" />)
                        </span>
                      </Fragment>
                    ) : null}
                  </div>
                </div>
                <p className="answer-content">{questionDetails.answer}</p>
              </div>
              <div className="mt-2">
                <Link
                  to={`#`}
                  onClick={() => likeQuestion(questionDetails._id)}
                  className={`btn-like ${questionDetails.liked ? "liked" : ""}`}
                >
                  <b>{questionDetails.likes}</b>{" "}
                  {questionDetails.liked ? <BsHeartFill /> : <BsHeart />}{" "}
                  <BsDot /> Cảm ơn
                </Link>
              </div>
            </div>
            {questionDetails.suggest && questionDetails.suggest.length ? (
              <div className="suggest-questions">
                <h3 className="title text-muted">Câu hỏi tương tự</h3>
                {questionDetails.suggest.map((item, index) => (
                  <div
                    className="question-item"
                    key={`suggest_${index}`}
                    onClick={() => onShowModalDetail(item._id)}
                  >
                    <div className="d-flex">
                      <p className="mb-0 text-muted flex-fill">
                        {item.sender
                          ? `${func.getSexName(
                              item.sender.sex
                            )} - ${func.yieldAge(item.sender.birthday)} tuổi`
                          : null}
                      </p>
                      <Timeago
                        className="text-muted"
                        date={item.question_time}
                      />
                    </div>
                    <p className="question-content">{item.question}</p>
                  </div>
                ))}
              </div>
            ) : null}
          </div>
        ) : null}
      </Modal>
    );
  };

  const onKeyUp = (e) => {
    if (e.key === "Enter") {
      setListQuestions([]);
      setSearchParams({ ...searchParams, page: 1, keyword });
    }
  };

  const selectSpeciality = (_id) => {
    if (_id !== searchParams.speciality) {
      setListQuestions([]);
      setSearchParams({ ...searchParams, page: 1, speciality: _id });
    }
  };

  const renderSpecialities = () => {
    if (specialities && specialities.length) {
      return (
        <>
          <p className="mb-0">Xem hỏi đáp theo chuyên khoa</p>
          <div
            className={`list-specialities ${showSpecialities ? "show" : ""}`}
          >
            <div className="row">
              <div className="col-6 col-sm-4 col-md-3">
                <div
                  className={`wrap-panel-item speciality ${
                    !searchParams.speciality ? "active" : ""
                  }`}
                  key={`speciality_all`}
                  onClick={() => selectSpeciality(null)}
                >
                  <img
                    src={
                      public_url.img +
                      "/specialist/" +
                      func.yieldIconSpecialist(0)
                    }
                    alt=""
                  />
                  <div className="flex-fill name">Tất cả</div>
                </div>
              </div>
              {specialities.map((item, index) => {
                const icon_code = item.code ? item.code : 0;
                const icon_special =
                  public_url.img +
                  "/specialist/" +
                  func.yieldIconSpecialist(icon_code);
                return (
                  <div
                    className="col-6 col-sm-4 col-md-3"
                    key={`speciality_${index}`}
                  >
                    <div
                      className={`wrap-panel-item speciality ${
                        searchParams.speciality === item._id ? "active" : ""
                      }`}
                      onClick={() => selectSpeciality(item._id)}
                    >
                      <img src={icon_special} alt={icon_code} />
                      <div className="flex-fill name">{item.name}</div>
                    </div>
                  </div>
                );
              })}
            </div>
            <div
              className={`text-center show-more-specialities ${
                showSpecialities ? "show" : ""
              }`}
            >
              <Link
                to="#"
                onClick={() => setShowSpecialities(!showSpecialities)}
              >
                {showSpecialities ? (
                  <b>
                    Thu gọn <BsChevronUp />
                  </b>
                ) : (
                  <b>
                    Xem thêm <BsChevronDown />
                  </b>
                )}
              </Link>
            </div>
          </div>
        </>
      );
    }
    return null;
  };

  return (
    <Fragment>
      <div className="block-questions">
        <div className="block-questions-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Hỏi đáp</h3>
              <Link to={url.name.my_questions}>
                <BsListUl /> Câu hỏi của bạn
              </Link>
            </div>
            <div className="card-body">
              <div className="wrap-content">
                <div className="input-group mt-3 mb-3">
                  <input
                    className="form-control"
                    placeholder="Tìm kiếm câu hỏi..."
                    value={keyword}
                    onChange={(e) => setKeyword(e.target.value)}
                    onKeyUp={onKeyUp}
                  />
                  <div className="input-group-append">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => onKeyUp({ key: "Enter" })}
                    >
                      Tìm kiếm
                    </button>
                  </div>
                </div>
                {renderSpecialities()}
                <div className="row justify-content-center mb-3">
                  <div className="col-md-6">
                    <Link
                      to={url.name.create_question}
                      className="btn btn-block btn-accept"
                    >
                      Đặt câu hỏi cho bác sĩ
                    </Link>
                  </div>
                </div>
                <InfiniteScroll
                  hasMore={hasMore && !loading}
                  loadMore={loadMore}
                  initialLoad={false}
                >
                  {renderListData()}
                  {loading ? renderLoading() : renderEmpty()}
                </InfiniteScroll>
              </div>
            </div>
          </div>
        </div>
      </div>
      {renderModalDetail()}
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(ListQuestions)
);
