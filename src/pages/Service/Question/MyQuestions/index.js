import React, { Fragment, useEffect, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import {
  ModelGetMyQuestions,
  ModelGetQuestionDetails,
  ModelLikeQuestion,
} from "../../../../models/ModelQuestion";
import { public_url, url } from "../../../../constants/Path";
import { Empty, Modal, Skeleton } from "antd";
import Timeago from "../../../../components/Timeago";
import {
  BsFileCheck,
  BsClock,
  BsFilePlus,
  BsHeartFill,
  BsHeart,
  BsDot,
} from "react-icons/bs";
import InfiniteScroll from "react-infinite-scroller";
import { FaUser, FaUserCircle } from "react-icons/fa";
import { BsInbox } from "react-icons/bs";
import Rating from "react-rating";
import ImgsViewer from "react-images-viewer";

const MyQuestions = ({ history, ...props }) => {
  const [loading, setLoading] = useState(false);
  const [listQuestions, setListQuestions] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(1);
  const [modalVisible, setModalVisible] = useState(false);
  const [activeQuestionId, setActiveQuestionId] = useState(null);
  const [questionDetails, setQuestionDetails] = useState(null);
  const [getDetailsLoading, setGetDetailsLoading] = useState(null);
  const [questionImages, setQuestionsImages] = useState([]);
  const [modalImage, setModalImage] = useState(false);
  const [imgIndex, setImgIndex] = useState(0);

  useEffect(() => {
    getListQuestions();
    if (
      props.computedMatch &&
      props.computedMatch.params &&
      props.computedMatch.params.questionId
    ) {
      onShowModalDetail(props.computedMatch.params.questionId);
    }
  }, [page]);

  const getListQuestions = () => {
    const loginData = localStorage.getItem("user");
    const user = JSON.parse(loginData);
    setLoading(true);
    const params = {
      search_option: {
        patient: {
          id: user.personal._id,
        },
      },
    };
    ModelGetMyQuestions(page, params).then((response) => {
      setLoading(false);
      if (response && response.data && response.data.one_health_msg) {
        const newListQuestions = listQuestions.concat(
          response.data.one_health_msg
        );
        setListQuestions(newListQuestions);
        if (response.data.one_health_msg.length === 20) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      }
    });
  };

  const renderListData = () => {
    return listQuestions.map((item, index) => (
      <div
        className="question-item"
        key={`question_${index}`}
        onClick={() => onShowModalDetail(item._id)}
      >
        <p className="question-content mb-0">{item.question}</p>
        <div className="d-flex">
          <div className="flex-fill">
            {item.status === 1 ? (
              <span className="text-warning">
                <BsClock /> Chờ xác nhận
              </span>
            ) : item.status === 2 ? (
              <span className="text-primary">
                <BsFilePlus /> Đã gửi đến bác sĩ
              </span>
            ) : (
              <span className="text-success">
                <BsFileCheck /> Đã được trả lời
              </span>
            )}
          </div>
          <div>
            <Timeago className="text-muted" date={item.question_time} />
          </div>
        </div>
      </div>
    ));
  };

  const renderLoading = () => {
    return (
      <div className="loading-item">
        <div className="question-item pt-0" key={`question_loading_0`}>
          <Skeleton active paragraph={{ rows: 1 }} />
        </div>
        <div className="question-item pt-0" key={`question_loading_1`}>
          <Skeleton active paragraph={{ rows: 1 }} />
        </div>
      </div>
    );
  };

  const renderEmpty = () => {
    return !(listQuestions && listQuestions.length) ? (
      <Empty className="ant-empty-custom" description={false} />
    ) : null;
  };

  const loadMore = () => {
    setLoading(true);
    setPage(page + 1);
  };

  const onCloseModalDetail = () => {
    setModalVisible(false);
    history.push(`${url.name.my_questions}`);
  };

  const onShowModalDetail = (questionId) => {
    setModalVisible(true);
    setActiveQuestionId(questionId);
  };

  const likeQuestion = (questionId) => {
    if (questionDetails && questionDetails._id === questionId) {
      const newQuestionDetails = {
        ...questionDetails,
        liked: true,
        likes: questionDetails.likes ? questionDetails.likes + 1 : 1,
      };
      setQuestionDetails(newQuestionDetails);
    }

    const loginData = localStorage.getItem("user");
    const user = JSON.parse(loginData);
    const params = {
      like: {
        patient: {
          id: user.personal._id,
        },
        question: {
          id: questionId,
        },
      },
    };
    ModelLikeQuestion(params);
  };

  useEffect(() => {
    if (activeQuestionId) {
      history.push(`${url.name.my_questions}/${activeQuestionId}`);

      const loginData = localStorage.getItem("user");
      const user = JSON.parse(loginData);
      setGetDetailsLoading(true);
      ModelGetQuestionDetails(user.personal._id, activeQuestionId).then(
        (response) => {
          setGetDetailsLoading(false);
          if (response && response.data && response.data.one_health_msg) {
            setQuestionDetails(response.data.one_health_msg);
            if (response.data.one_health_msg.images) {
              let newQuesionImages = response.data.one_health_msg.images.map(
                (item) => ({ src: item.url })
              );
              setQuestionsImages(newQuesionImages);
            }
          }
        }
      );
    }
  }, [activeQuestionId]);

  const renderModalDetail = () => {
    return (
      <Modal
        title="Chi tiết câu hỏi"
        visible={modalVisible}
        closable={true}
        footer={null}
        onCancel={onCloseModalDetail}
        centered
      >
        {getDetailsLoading ? (
          <Skeleton active paragraph={{ rows: 3 }} />
        ) : questionDetails ? (
          <div className="question-details-modal">
            <div className="question-item">
              <div className="d-flex">
                <p className="mb-0 text-muted flex-fill"></p>
                <Timeago
                  className="text-muted"
                  date={questionDetails.question_time}
                />
              </div>
              <p className="question-content">{questionDetails.question}</p>
              {questionImages && questionImages.length ? (
                <div className="question-images">
                  {questionImages.map((item, index) => (
                    <img
                      src={item.src}
                      alt=""
                      key={`question_img_${index}`}
                      onClick={() => {
                        setImgIndex(index);
                        setModalImage(true);
                      }}
                    />
                  ))}
                  <ImgsViewer
                    imgs={questionImages}
                    currImg={imgIndex}
                    isOpen={modalImage}
                    onClickPrev={() => setImgIndex(imgIndex - 1)}
                    onClickNext={() => setImgIndex(imgIndex + 1)}
                    onClose={() => setModalImage(false)}
                  />
                </div>
              ) : null}
              {questionDetails.answer ? (
                <Fragment>
                  <div className="answer-item">
                    <div className="answer-doctor">
                      <div className="avatar-block mr-2">
                        {questionDetails.replier.avatar &&
                        questionDetails.replier.avatar.url ? (
                          <img
                            src={questionDetails.replier.avatar.url}
                            alt="replier-avatar"
                          />
                        ) : (
                          <FaUserCircle size={`40px`} />
                        )}
                      </div>
                      <div className="doctor-info">
                        <p className="mb-0 font-weight-bold">
                          {questionDetails.replier.last_name}{" "}
                          {questionDetails.replier.first_name}
                        </p>
                        {questionDetails.replier.rate ? (
                          <Fragment>
                            <Rating
                              initialRating={questionDetails.replier.rate.rate}
                              className="rating"
                              readonly
                              emptySymbol={
                                <img
                                  src={`${public_url.img}/icons/icon_star_non.png`}
                                  alt=""
                                  className="icon"
                                />
                              }
                              fullSymbol={
                                <img
                                  src={`${public_url.img}/icons/icon_star_rated.png`}
                                  alt=""
                                  className="icon"
                                />
                              }
                            />
                            <span className="text-muted">
                              {" "}
                              ({questionDetails.replier.rate.count}{" "}
                              <FaUser size=".75rem" />)
                            </span>
                          </Fragment>
                        ) : null}
                      </div>
                    </div>
                    <p className="answer-content">{questionDetails.answer}</p>
                  </div>
                  <div className="mt-2">
                    <Link
                      to={`#`}
                      onClick={() => likeQuestion(questionDetails._id)}
                      className={`btn-like ${
                        questionDetails.liked ? "liked" : ""
                      }`}
                    >
                      <b>{questionDetails.likes}</b>{" "}
                      {questionDetails.liked ? <BsHeartFill /> : <BsHeart />}{" "}
                      <BsDot /> Cảm ơn
                    </Link>
                  </div>
                </Fragment>
              ) : (
                <h4 className="text-center text-muted">
                  <BsInbox size={"3rem"} />
                  <br />
                  Câu hỏi của bạn chưa được trả lời.
                </h4>
              )}
            </div>
          </div>
        ) : null}
      </Modal>
    );
  };

  return (
    <Fragment>
      <div className="block-my-questions">
        <div className="card card-custom">
          <div className="card-header">
            <h3>Câu hỏi của tôi</h3>
            <Link
              to={url.name.create_question}
              className="btn btn-sm btn-accept"
            >
              Đặt câu hỏi
            </Link>
          </div>
          <div className="card-body">
            <InfiniteScroll
              hasMore={hasMore && !loading}
              loadMore={loadMore}
              initialLoad={false}
            >
              {renderListData()}
              {loading ? renderLoading() : renderEmpty()}
            </InfiniteScroll>
          </div>
        </div>
        {renderModalDetail()}
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(MyQuestions)
);
