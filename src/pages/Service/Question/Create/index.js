import React, { Fragment, useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { BsPlus, BsTextareaT, BsImages, BsXCircleFill, BsCheckCircle } from "react-icons/bs";
import { hasVietnamese } from "../../../../helpers";
import { ModelCreateQuestion, ModelSaveQuestionImage } from "../../../../models/ModelQuestion";
import Loading from "../../../../components/Loading";
import { public_url, url } from "../../../../constants/Path";

const CreateQuestion = ({ history }) => {
    const [uploadImages, setUploadImages] = useState([]);
    const [questionContent, setQuestionContent] = useState("");
    const [invalidLength, setInvalidLength] = useState(false);
    const [invalidVNCharacters, setInvalidVNCharacters] = useState(false);
    const [loading, setLoading] = useState(false);
    const [success, setSuccess] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    const [imageErrorMessage, setImageErrorMessage] = useState(null);
    let uploadedImages = [];

    const onUploadImages = (e) => {
        e.preventDefault();
        if (e.target.files) {
            let images = [];
            for (let i = 0; i < e.target.files.length; i++) {
                images.push({ file: e.target.files[i], imageUrl: URL.createObjectURL(e.target.files[i]) });
                fileToBase64(e.target.files[i]).then(result => {
                    console.log(result)
                })
            }
            setUploadImages(uploadImages.concat(images));
        }
    };

    const removeImage = (removeIndex) => {
        const newImages = uploadImages.filter((item, index) => index !== removeIndex);
        setUploadImages(newImages);
    };

    const onSubmit = (e) => {
        e.preventDefault();
        let valid = true;
        if (questionContent.trim().length <= 30) {
            valid = false;
            setInvalidLength(true);
        }
        if (!hasVietnamese(questionContent)) {
            valid = false;
            setInvalidVNCharacters(true);
        }
        if (valid) {
            setLoading(true);
            setErrorMessage(null);
            const loginData = localStorage.getItem("user");
            const user = JSON.parse(loginData);
            const params = {
                senderID: user.personal._id,
                question: questionContent
            };
            ModelCreateQuestion(params).then(response => {
                if (response && response.data && response.data.result === "true") {
                    if (uploadImages && uploadImages.length) {
                        uploadImages.forEach((item, index) => {
                            fileToBase64(item.file).then((data) => {
                                saveQuestionImage(response.data.one_health_msg._id, data, index);
                            });
                        });
                    } else {
                        setLoading(false);
                        setSuccess(true);
                    }
                } else {
                    setLoading(false);
                    setErrorMessage("Gửi câu hỏi thất bại, vui lòng thử lại sau.");
                }
            });
        }
    }

    const fileToBase64 = (file) => {
        return new Promise(resolve => {
            var reader = new FileReader();
            reader.onload = function (event) {
                resolve(event.target.result);
            };
            reader.readAsDataURL(file);
        });
    };

    const saveQuestionImage = (questionId, data, index) => {
        var params = {
            image: {
                type: 2, // 2 upload for question
                data, //encode base 64 of image
                question: {
                    id: questionId // id of question
                }
            }
        };
        ModelSaveQuestionImage(params).then(response => {
            if (response && response.data && response.data.result === "true") {
                uploadedImages[index] = true;
            } else {
                uploadedImages[index] = false;
            }
            let totalCompleted = 0;
            let totalUploaded = 0;
            uploadedImages.forEach(result => {
                if (result === true || result === false) {
                    totalCompleted++;
                }
                if (result === true) {
                    totalUploaded++;
                }
            });
            if (totalCompleted === uploadImages.length) {
                setLoading(false);
                if (totalUploaded === totalCompleted) {
                    setSuccess(true);
                } else {
                    setImageErrorMessage(`Gửi thất bại ${totalCompleted - totalUploaded} hình ảnh`);
                }
            }
        });
    };

    const onChangeQuestionContent = (e) => {
        setQuestionContent(e.target.value);
        if (e.target.value.trim().length > 30) {
            setInvalidLength(false);
        }
        if (hasVietnamese(e.target.value)) {
            setInvalidVNCharacters(false);
        }
    }

    const renderSuccess = () => {
        return (
            <div className="row justify-content-center">
                <div className="col-md-8 col-lg-6 mt-3">
                    <img className="img-fluid" src={public_url.img + "/thanks.png"} alt="" />
                    <div className="text-center mt-3">
                        <h3>Câu hỏi đã được gửi thành công.</h3>
                        <p>
                            Câu hỏi của bạn sẽ được gửi đến cho bác sĩ. Kết quả sẽ thông báo cho bạn sớm nhất, truy cập <Link to={url.name.my_questions}><b>Câu hỏi của tôi</b></Link> để xem chi tiết.
                        </p>
                    </div>
                </div>
            </div>
        );
    }

    return (
        <Fragment>
            <div className="block-create-question">
                {success ? renderSuccess() :
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Đặt câu hỏi</h3>
                        </div>
                        <div className="card-body-top">
                            <p className="mb-1">
                                - Câu hỏi sẽ được trả lời trong 24h
                        </p>
                            <p className="mb-0">
                                - Hoàn toàn bảo mật thông tin danh tính của bạn
                        </p>
                        </div>
                        <div className="card-body pt-3">
                            <form onSubmit={onSubmit}>
                                <div className="form-group">
                                    <label className="mb-2"><BsTextareaT /> Câu hỏi:</label>
                                    <textarea
                                        className={`form-control ${invalidLength || invalidVNCharacters ? "is-invalid" : ""}`}
                                        placeholder="Viết câu hỏi của bạn..."
                                        value={questionContent} onChange={onChangeQuestionContent}
                                        disabled={imageErrorMessage}
                                    />
                                    <div className="feedback mt-1">
                                        <p className={`${invalidLength ? "text-danger" : "text-muted"} mb-0`}>
                                            - Câu hỏi phải dài hơn 30 ký tự
                                    </p>
                                        <p className={`${invalidVNCharacters ? "text-danger" : "text-muted"}`}>
                                            - Vui lòng nhập tiếng Việt có dấu để bác sĩ có thể hiểu chính xác vấn đề của bạn
                                    </p>
                                    </div>
                                </div>
                                <label className="mb-2"><BsImages /> Thêm hình ảnh mô tả:</label>
                                <div className="upload-images">
                                    {
                                        uploadImages.map((item, index) => (
                                            <div className="image" key={`upload_img_${index}`}>
                                                <img src={item.imageUrl} alt={`img_${index}`} />
                                                {uploadedImages && uploadedImages[index] ?
                                                    <span className="remove">
                                                        <BsCheckCircle className="text-success" size="1.2rem" />
                                                    </span> :
                                                    <span className="remove" onClick={() => removeImage(index)}>
                                                        <BsXCircleFill className="text-danger" size="1.2rem" />
                                                    </span>}
                                            </div>
                                        ))
                                    }
                                    <label htmlFor="upload-question-images" className="btn-upload">
                                        <BsPlus size="3rem" />
                                    </label>
                                    <input className="d-none" id="upload-question-images" type="file" accept="image/*" multiple onChange={onUploadImages} />
                                </div>
                                {imageErrorMessage ?
                                    <div className="row justify-content-center">
                                        <p className="text-danger text-center w-100">{imageErrorMessage}</p>
                                        <div className="col-md-6">
                                            <Link className="ant-btn ant-btn-block ant-btn-lg btn-link" to={url.name.my_questions}>
                                                Câu hỏi của tôi
                                        </Link>
                                        </div>
                                    </div> :
                                    <div className="row justify-content-center">
                                        <div className="col-md-6">
                                            <button className="ant-btn ant-btn-block ant-btn-lg btn-accept" type="submit">
                                                Gửi câu hỏi
                                        </button>
                                        </div>
                                    </div>}
                                {errorMessage ? <p className="text-danger text-center">{errorMessage}</p> : null}
                            </form>
                        </div>
                    </div>}
            </div>
            <Loading open={loading} />
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, { forwardRef: true })(CreateQuestion));
