import React, { Fragment } from "react";
import { url } from "../../../../constants/Path";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
// import "../style.scss";
import RecordsTestContent from "../../../../components/Service/HomeTest/RecordsTestContent";

const RecordsTest = (props) => {
  const { home_test } = props;
  const form = home_test && home_test.form ? home_test.form : [];

  if (form.length < 2) {
    return <Redirect to={url.name.service_home_test} />;
  }

  return (
    <Fragment>
      <div className="w-100">
        <RecordsTestContent {...props} />
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(RecordsTest)
);
