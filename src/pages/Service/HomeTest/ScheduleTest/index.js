import React, { Fragment } from "react";
import { url } from "../../../../constants/Path";
import { withRouter, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import ScheduleTestContent from "../../../../components/Service/HomeTest/ScheduleTestContent";

const ScheduleTest = (props) => {
  const { home_test } = props;
  const form = home_test && home_test.form ? home_test.form : [];

  if (form.length < 1) {
    return <Redirect to={url.name.service_home_test} />;
  }

  return (
    <Fragment>
      <div className="block-call-chat-page">
        <ScheduleTestContent {...props} />
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(ScheduleTest)
);
