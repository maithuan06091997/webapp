import React, {
  Fragment,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { Modal, Empty } from "antd";
import { _HomeTest } from "../../../actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import "./style.scss";
import BoxSkeleton from "../../../components/BoxSkeleton";
import { url } from "../../../constants/Path";
import CardServices from "../../../components/CardServices";
import {
  getHomeTestDetail,
  getListHomeTest,
} from "../../../services/HomeTestService";
import { withRouter } from "react-router";

const HomeTest = (props) => {
  const isRendered = useRef(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHospital, setListHospital] = useState([]);
  const [detailHospital, setDetailHospital] = useState(undefined);
  const [modalDetailHospital, setModalDetailHospital] = useState(false);
  //*** Declare props ***//
  const { _HomeTest, history } = props;

  useEffect(() => {
    const getApi = async () => {
      setSkeleton(true);
      try {
        const resultFt = await getListHomeTest();
        if (!isRendered.current) {
          const dataListHospital = resultFt.one_health_msg;
          setListHospital(dataListHospital);
          setSkeleton(false);
        }
      } catch (error) {
        if (!isRendered.current) {
          if (error.code !== "007") {
            console.log("Examination - StepOne - ListHospital");
            setSkeleton(false);
          }
        }
      }
    };
    getApi();
    return () => {
      isRendered.current = true;
    };
  }, []);

  const onPressNext = (item) => {
    const params = [
      {
        step: 1,
        data: item,
      },
    ];
    _HomeTest(params);
    history.push(url.name.service_schedule_test);
  };

  const onReadDetail = async (id) => {
    setModalDetailHospital(true);
    const detailHospital = await getHomeTestDetail(id);
    setDetailHospital(detailHospital.one_health_msg);
  };

  const onCloseModalDetailHospital = () => {
    setModalDetailHospital(false);
  };

  function yieldListHospital() {
    if (listHospital && listHospital.length > 0) {
      return _.map(listHospital, (item, index) => {
        return (
          <div
            key={index}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-top">
                <img className="img-fluid" src={item.banner.url} alt="" />
              </div>
              <div className="wrap-main">
                <h4 className="title">{item.title}</h4>
                <p className="address text-muted">
                  Giá đặt khám:{" "}
                  <span className="price">
                    {item.totalCredit.toLocaleString()}đ
                  </span>
                </p>
              </div>
              <div className="wrap-event-action">
                <div className="box-button-event">
                  <div
                    onClick={onReadDetail.bind(this, item._id)}
                    className="ant-btn ant-btn-lg read-more"
                  >
                    <span>Xem chi tiết</span>
                  </div>
                  <div
                    onClick={onPressNext.bind(this, item)}
                    className="ant-btn ant-btn-lg btn-booking booking"
                  >
                    <span>Đặt khám</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Danh sách bệnh viện trống"
          />
        );
      }
    }
  }

  function yieldDetailHospital() {
    if (detailHospital) {
      return (
        <Fragment>
          <div className="wrap-top">
            <img src={detailHospital.banner.url} alt={detailHospital.title} />
          </div>
          <div className="wrap-main">
            <div className="title mb-2">
              <h4>{detailHospital.title}</h4>
              <p className="text-muted mb-0">
                Đơn vị cung cấp: {detailHospital.provider.name}
              </p>
              <p className="text-muted mb-0">
                Địa chỉ cung cấp: {detailHospital.provider.address}
              </p>
              <p className="text-muted">
                Giá đặt khám:{" "}
                <span className="price">
                  {detailHospital.originCredit.toLocaleString()}đ
                </span>
              </p>
              <div
                onClick={onPressNext.bind(this, detailHospital)}
                className="ant-btn btn-booking ant-btn-lg booking w-100"
              >
                <span>Đặt khám</span>
              </div>
            </div>
            <div className="content">
              <div
                dangerouslySetInnerHTML={{ __html: detailHospital.html }}
              ></div>
            </div>
          </div>
        </Fragment>
      );
    } else {
      return null;
    }
  }

  return (
    <CardServices
      header="Xét nghiệm tại nhà"
      description="Quý khách vui lòng chọn gói xét nghiệm"
      step="Xét nghiệm tại nhà"
    >
      <div className="wrap-content block-list-home-test">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={skeleton}
              full={false}
              length={4}
              rows={7}
              data={yieldListHospital()}
            />
          </div>
        </div>
      </div>
      <Modal
        className="block-modal-detail-hospital-examination ant-modal-custom"
        width={600}
        closable={true}
        footer={null}
        visible={modalDetailHospital}
        onCancel={onCloseModalDetailHospital}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">{yieldDetailHospital()}</div>
        </div>
      </Modal>
    </CardServices>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _HomeTest }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(HomeTest)
);
