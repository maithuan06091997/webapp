import React from "react";
import { connect } from "react-redux";
import { Redirect, withRouter } from "react-router";
import RecordsPaymentContent from "../../../../components/Service/HomeTest/RecordsPaymentContent";
import { url } from "../../../../constants/Path";

const RecordsPayment = (props) => {
  const { home_test } = props;
  const form = home_test && home_test.form ? home_test.form : [];

  if (form.length < 3) {
    return <Redirect to={url.name.service_home_test} />;
  }

  return <RecordsPaymentContent {...props} />;
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(RecordsPayment)
);
