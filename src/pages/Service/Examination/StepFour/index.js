import React, {Fragment} from "react";
import StepFour from "../../../../components/Service/Examination/StepFour";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepFourPage = (props) => {
    const {appointment} = props;
    const form  = appointment && appointment.form ? appointment.form : [];

    if (form.length < 3) {
        return <Redirect to={url.name.examination_1}/>;
    }

    return (
        <Fragment>
            <div className="block-examination-page">
                <StepFour {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepFourPage));
