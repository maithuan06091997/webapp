import React, {Fragment} from "react";
import StepFive from "../../../../components/Service/Examination/StepFive";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepFivePage = (props) => {
    const {appointment} = props;
    const form  = appointment && appointment.form ? appointment.form : [];

    if (form.length < 4) {
        return <Redirect to={url.name.examination_1}/>;
    }

    return (
        <Fragment>
            <div className="block-examination-page">
                <StepFive {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepFivePage));
