import React, {Fragment} from "react";
import StepThree from "../../../../components/Service/Examination/StepThree";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepThreePage = (props) => {
    const {appointment} = props;
    const form  = appointment && appointment.form ? appointment.form : [];

    if (form.length < 2) {
        return <Redirect to={url.name.examination_1}/>;
    }

    return (
        <Fragment>
            <div className="block-examination-page">
                <StepThree {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepThreePage));
