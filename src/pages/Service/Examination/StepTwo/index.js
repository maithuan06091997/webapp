import React, {Fragment} from "react";
import StepTwo from "../../../../components/Service/Examination/StepTwo";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepTwoPage = (props) => {
    const {appointment} = props;
    const form  = appointment && appointment.form ? appointment.form : [];

    if (form.length < 1) {
        return <Redirect to={url.name.examination_1}/>;
    }

    return (
        <Fragment>
            <div className="block-examination-page">
                <StepTwo {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepTwoPage));
