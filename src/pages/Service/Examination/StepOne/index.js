import React, { Fragment } from "react";
import StepOne from "../../../../components/Service/Examination/StepOne";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "../style.scss";

const StepOnePage = (props) => {
  return (
    <Fragment>
      <div className="block-examination-page">
        <StepOne {...props} />
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(StepOnePage)
);
