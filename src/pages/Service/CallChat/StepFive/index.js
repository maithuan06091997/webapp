import React, {Fragment} from "react";
import StepFive from "../../../../components/Service/CallChat/StepFive";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepFivePage = (props) => {
    const {call_chat} = props;
    const form  = call_chat && call_chat.form ? call_chat.form : [];

    if (form.length < 4) {
        return <Redirect to={url.name.call_chat_1}/>;
    }

    return (
        <Fragment>
            <div className="block-call-chat-page">
                <StepFive {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepFivePage));
