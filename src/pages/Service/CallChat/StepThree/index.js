import React, {Fragment} from "react";
import StepThree from "../../../../components/Service/CallChat/StepThree";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepThreePage = (props) => {
    const {call_chat} = props;
    const form  = call_chat && call_chat.form ? call_chat.form : [];

    if (form.length < 2) {
        return <Redirect to={url.name.call_chat_1}/>;
    }

    return (
        <Fragment>
            <div className="block-call-chat-page">
                <StepThree {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepThreePage));
