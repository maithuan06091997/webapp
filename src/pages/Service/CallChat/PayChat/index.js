import { Button } from "antd";
import { Modal } from "antd";
import React, { useEffect, useState } from "react";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import { connect } from "react-redux";
import CardServices from "../../../../components/CardServices";
import {
  findDataOfStep,
  findGender,
  formatNumberToMoney,
  yieldAge,
  yieldLocation,
} from "../../../../helpers";
import { getPriceChat, postPayChat } from "../../../../services/ChatService";
import "./style.scss";

function PayChat(props) {
  const { call_chat, user } = props;
  const form = call_chat && call_chat.form ? call_chat.form : [];
  const formOne = findDataOfStep(form, 1);
  const formTwo = findDataOfStep(form, 2);
  const formThree = findDataOfStep(form, 3);
  const [price, setPrice] = useState();
  const [modal, contextHolder] = Modal.useModal();

  useEffect(() => {
    const getApi = async () => {
      const price = await getPriceChat({ doctorid: formThree._id });
      setPrice(price.one_health_msg);
    };
    getApi();
  }, [formThree._id]);

  const confirmChat = async () => {
    try {
      const data = {
        one_health_msg: {
          user: {
            id: user.personal._id,
          },
          type: 2,
        },
      };
      await postPayChat(data);
    } catch (error) {}
  };

  const modalChat = () => {
    modal.confirm({
      title: "Xác nhận",
      okText: "Chat ngay",
      cancelText: "Huỷ",
      onOk: confirmChat,
      centered: true,
      content: "Bạn có chắc chắn muốn chat với bác sĩ!",
    });
  };

  const renderInfo = () => {
    return (
      <>
        <div className="wrap-line">
          <p className="label">Họ và tên</p>
          <p className="value">{formOne.name}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Giới tính</p>
          <p className="value">{findGender(formOne.sex, "text")}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Tuổi</p>
          <p className="value">{yieldAge(formOne.birthday)}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Số điện thoại</p>
          <p className="value">{formOne.phone ?? formOne.phone[0]}</p>
        </div>
        <div className="wrap-line address">
          <p className="label">Địa chỉ</p>
          <p className="value">{yieldLocation(formOne.location)}</p>
        </div>
      </>
    );
  };
  const renderService = () => {
    let totalPrice = 0;
    console.log(price && price.invoiceChatPrePay);

    if (
      price &&
      price.invoiceChatPrePay &&
      price.invoiceChatPrePay.totalCredit
    ) {
      totalPrice =
        formatNumberToMoney(price.invoiceChatPrePay.totalCredit) + "đ";
    } else if (
      price &&
      price.invoiceChatPostPaid &&
      price.invoiceChatPostPaid.totalCredit
    ) {
      totalPrice =
        formatNumberToMoney(price.invoiceChatPostPaid.totalCredit) + "đ";
    }

    return (
      <>
        <div className="wrap-line">
          <p className="label">Dịch vụ</p>
          <p className="value">{"Chat với bác sĩ"}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Bác sĩ</p>
          <p className="value">
            {formThree.last_name + " " + formThree.first_name}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Chuyên khoa</p>
          <p className="value">{formTwo.name}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Giá phiên chat</p>
          <p className="value price">{totalPrice}</p>
        </div>
      </>
    );
  };

  return (
    <CardServices
      header="Xác nhận thông tin"
      description="Quý khách vui lòng kiểm tra lại các thông tin"
      step="Xác nhận thông tin"
    >
      <div className="wrap-content block-pay-chat">
        <div className="row">
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
            <div className="wrap-panel-item">
              <div className="wrap-title">
                <h4>
                  <FaRegUserCircle /> Thông tin bệnh nhân
                </h4>
              </div>
              <div className="wrap-main">{renderInfo()}</div>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
            <div className="wrap-panel-item ">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Thông tin dịch vụ
                </h4>
              </div>
              <div className="wrap-main">{renderService()}</div>
            </div>
          </div>
          <div className="col-12 wrap-panel-col mt-3">
            <Button className="btn-accept w-100" onClick={modalChat}>
              Chat với bác sĩ
            </Button>
          </div>
        </div>
      </div>
      {contextHolder}
    </CardServices>
  );
}

export default connect(null, null, null, { forwardRef: true })(PayChat);
