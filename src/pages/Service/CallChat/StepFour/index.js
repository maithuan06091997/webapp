import React, {Fragment} from "react";
import StepFour from "../../../../components/Service/CallChat/StepFour";
import {url} from "../../../../constants/Path";
import {withRouter, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import "../style.scss";

const StepFourPage = (props) => {
    const {call_chat} = props;
    const form  = call_chat && call_chat.form ? call_chat.form : [];

    if (form.length < 3) {
        return <Redirect to={url.name.call_chat_1}/>;
    }

    return (
        <Fragment>
            <div className="block-call-chat-page">
                <StepFour {...props}/>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(StepFourPage));
