import { Button, Modal } from "antd";
import React, { useState } from "react";
import { FaRegListAlt } from "react-icons/fa";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import { _DrugStore } from "../../../actions";
import BoxSkeleton from "../../../components/BoxSkeleton";
import CardServices from "../../../components/CardServices";
import { public_url, url } from "../../../constants/Path";
import ModalSearchDrug from "./ModalSearchDrug";
import "./style.scss";

const DrugStoreOnline = ({ history, _DrugStore }) => {
  const [valueAmount, setValueAmount] = useState(1);
  const [nameDrug, setNameDrug] = useState("");
  const [listDrug, setListDrung] = useState([]);
  const [imagePreviewUrl, setImagePreviewUrl] = useState();
  const [openModal, setOpenModal] = useState(false);
  const [file, setFile] = useState();
  const yieldTutorial = () => {
    return (
      <ul className="tutorial">
        <li>
          <div>
            <img
              src={public_url.img + "/drug/bg_circle.png"}
              alt="#"
              className="img-cricle"
            />
            <img
              src={public_url.img + "/drug/img_prescription.png"}
              alt="#"
              className="img-content"
            />
          </div>
          <p>Chụp hình ảnh toa thuốc và gửi cho chúng tôi</p>
        </li>
        <li>
          <div>
            <img
              src={public_url.img + "/drug/bg_circle.png"}
              alt="#"
              className="img-cricle"
            />
            <img
              src={public_url.img + "/drug/img_doctor.png"}
              alt="#"
              className="img-content"
            />
          </div>
          <p>Dược sĩ DR.OH sẽ liên hệ xác nhận và đặt mua thuốc trong</p>
        </li>
        <li>
          <div>
            <img
              src={public_url.img + "/drug/bg_circle.png"}
              alt="#"
              className="img-cricle"
            />
            <img
              src={public_url.img + "/drug/img_watch.png"}
              alt="#"
              className="img-content"
            />
          </div>
          <p>Giờ làm việc từ 8:00 đến 20:00 tất cả các ngày trong tuần</p>
        </li>
      </ul>
    );
  };

  const previewImage = (event) => {
    const file = event.target.files[0];
    setFile(file);
    const reader = new FileReader();
    reader.onloadend = () => {
      setImagePreviewUrl(reader.result);
    };
    reader.readAsDataURL(file);
  };

  const yieldUploadImage = () => {
    return (
      <form className="upload-image position-relative">
        <label htmlFor="upload-img" className="mb-0 w-100">
          {imagePreviewUrl && <img src={imagePreviewUrl} alt="#" />}

          <span
            type="button"
            className="btn-accept w-100 mt-2 text-center py-2 rounded custom-btn"
          >
            Tải ảnh toa thuốc
          </span>
        </label>
        {imagePreviewUrl && (
          <Button
            className="btn-danger h-auto py-0 position-absolute top-15 right-5"
            onClick={() => setImagePreviewUrl()}
          >
            Xóa ảnh
          </Button>
        )}
        <input
          type="file"
          id="upload-img"
          hidden={true}
          onChange={previewImage}
        />
      </form>
    );
  };

  const handValueIncrease = () => {
    setValueAmount(valueAmount + 1);
  };

  const handValueReduction = () => {
    valueAmount > 1 && setValueAmount(valueAmount - 1);
  };

  const onOpenModal = (flag) => {
    setOpenModal(flag);
  };

  const addDrug = () => {
    if (nameDrug !== "") {
      const data = { name: nameDrug, quantity: valueAmount };
      listDrug.push(data);
      setListDrung([...listDrug]);
      setNameDrug("");
      setValueAmount(1);
    } else {
      Modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Vui lòng nhập tên thuốc",
      });
    }
  };

  const handDeleteDrug = (drug) => {
    const position = listDrug.indexOf(drug);
    listDrug.splice(position, 1);
    setListDrung([...listDrug]);
  };

  const nextPage = () => {
    if (!imagePreviewUrl && listDrug.length === 0) {
      Modal.warning({
        title: "Thông báo",
        okText: "Đóng",
        centered: true,
        content: "Vui lòng thêm hình ảnh toa thuốc",
      });
      return true;
    }
    const params = [
      {
        step: 1,
        data: {
          imgDrug: imagePreviewUrl,
          info: listDrug,
          fileName: imagePreviewUrl,
          nameImg: file?.name,
        },
      },
    ];
    _DrugStore(params);
    history.push(url.name.service_drug_delivery_address);
  };

  const yieldAdd = () => {
    return (
      <>
        {listDrug.length > 0 && (
          <table className="table">
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên thuốc</th>
                <th>Số lượng</th>
                <th>Xóa</th>
              </tr>
            </thead>
            <tbody>
              {listDrug.map((drug, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{drug.name}</td>
                  <td>{drug.quantity}</td>
                  <td>
                    <Button
                      className="btn-danger h-auto py-0"
                      onClick={() => handDeleteDrug(drug)}
                    >
                      Xóa
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
        <form className="form-drug">
          <div className="row">
            <div className="col-6">
              <input
                type="text"
                name="nameDrug"
                value={nameDrug}
                className="input-name-drug"
                placeholder="Nhập tên thuốc"
                onClick={() => onOpenModal(true)}
                readOnly
              />
            </div>
            <div className="col-3">
              <div className="form-amount">
                <div className="btn-amount pb-1" onClick={handValueReduction}>
                  -
                </div>
                <input
                  type="text"
                  name="quantity"
                  className="input-amount"
                  readOnly
                  value={valueAmount}
                />
                <div className="btn-amount" onClick={handValueIncrease}>
                  +
                </div>
              </div>
            </div>
            <div className=" col-3">
              <Button className="btn-accept custom-btn" onClick={addDrug}>
                <span>Thêm</span>
              </Button>
            </div>
          </div>
        </form>
      </>
    );
  };

  const renderDrugStore = () => {
    return (
      <>
        <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col2">
          <div className="wrap-panel-item wrap-profile-info">
            <div className="wrap-title">
              <h4>
                <FaRegListAlt /> Hướng dẫn mua thuốc
              </h4>
            </div>
            <div className="wrap-main">{yieldTutorial()}</div>
          </div>
        </div>
        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
          <div className="wrap-panel-item wrap-profile-info">
            <div className="wrap-title">
              <h4>
                <FaRegListAlt /> Tải ảnh từ toa thuốc
              </h4>
            </div>
            <div className="wrap-main">{yieldUploadImage()}</div>
          </div>
        </div>
        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
          <div className="wrap-panel-item wrap-profile-info">
            <div className="wrap-title">
              <h4>
                <FaRegListAlt /> Thêm thuốc ngoài toa
              </h4>
            </div>
            <div className="wrap-main">{yieldAdd()}</div>
          </div>
        </div>
        <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col2 mt-2 pt-2 pb-2">
          <Button className="btn-accept w-100 mt-2" onClick={nextPage}>
            <span>Tiếp tục</span>
          </Button>
        </div>
      </>
    );
  };
  return (
    <CardServices
      header="Đặt hàng theo toa thuốc"
      description="Quý khách vui lòng chọn toa thuốc"
      step="Đặt hàng toa thuốc"
    >
      <div className="wrap-content block-list-health-service drugstore">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              full={false}
              panelCol={3}
              length={21}
              rows={1}
              data={renderDrugStore()}
            />
          </div>
        </div>
      </div>
      <ModalSearchDrug
        onOpenModal={onOpenModal}
        openModal={openModal}
        setNameDrug={setNameDrug}
      />
    </CardServices>
  );
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _DrugStore }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(DrugStoreOnline)
);
