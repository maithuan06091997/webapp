import React from "react";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { bindActionCreators } from "redux";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import CardServices from "../../../../components/CardServices";
import Geocode from "react-geocode";
import { _Confirmation, _DrugStore } from "../../../../actions";

import {
  getFullName,
  findDataOfStep,
  findGender,
  yieldAge,
} from "../../../../helpers";
import "./style.scss";
import { url } from "../../../../constants/Path";
import { Button } from "antd";
import { setting } from "../../../../api/Config";
import { postDrugOrder } from "../../../../services/DrugService";

const ConfirmOrder = ({
  drug_store,
  history,
  user,
  _DrugStore,
  _Confirmation,
}) => {
  const form = drug_store && drug_store.form ? drug_store.form : [];
  if (form.length < 2) {
    history.push(url.name.service_drug_store);
  }
  const formOne = findDataOfStep(form, 1);
  const formTwo = findDataOfStep(form, 2);

  //Usage example:

  const dataURLtoFile = (dataurl, filename) => {
    if (dataurl) {
      var arr = dataurl.split(","),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]),
        n = bstr.length,
        u8arr = new Uint8Array(n);

      while (n--) {
        u8arr[n] = bstr.charCodeAt(n);
      }

      return new File([u8arr], filename, { type: mime });
    }
  };

  const nextPage = async () => {
    const location = [
      formTwo.address,
      formTwo.ward.label,
      formTwo.district.label,
      formTwo.provinces.label,
    ];
    Geocode.setApiKey(setting.googleKeyApi);
    const response = await Geocode.fromAddress(location);
    const latlng = {
      lat: response.results[0].geometry.location.lat,
      lng: response.results[0].geometry.location.lng,
    };

    const addressId = {
      wardId: formTwo.ward.value,
      districtId: formTwo.district.value,
      provinceId: formTwo.provinces.value,
    };

    const info = {
      first_name: user.personal.first_name,
      last_name: user.personal.last_name,
      phone: user.personal.username,
      sex: user.personal.sex,
      birthday: user.personal.birthday,
      email: user.personal.email,
      address: location,
      ward: formTwo.ward.value,
      district: formTwo.district.value,
      province: formTwo.provinces.value,
    };
    const urlApi =
      setting.api +
      "/api/prescription?drugList=" +
      encodeURIComponent(JSON.stringify(formOne.info)) +
      "&latlng=" +
      encodeURIComponent(JSON.stringify(latlng)) +
      "&note=" +
      encodeURIComponent(formTwo.note) +
      "&address=" +
      encodeURIComponent(location) +
      "&info=" +
      encodeURIComponent(JSON.stringify(info)) +
      "&addressId=" +
      encodeURIComponent(JSON.stringify(addressId));

    const file = dataURLtoFile(formOne.fileName, formOne.nameImg);
    const postData = new FormData();
    postData.append("data", file);

    await postDrugOrder(urlApi, postData);
    _Confirmation({
      status: true,
      type: "service",
      name: "HEALTH",
      gateway: "Wallet",
    });
    history.push({
      pathname: url.name.service_confirmation,
    });
  };

  const yieldDetailsInfo = () => {
    const location = [
      formTwo.address,
      formTwo.ward?.label,
      formTwo.district?.label,
      formTwo.provinces?.label,
    ];
    return (
      <>
        <div className="wrap-line">
          <p className="label">Họ và tên</p>
          <p className="value">{getFullName(user.personal)}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Giới tính</p>
          <p className="value">{findGender(user.personal.sex, "text")}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Tuổi</p>
          <p className="value">{yieldAge(user.personal.birthday)}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Số điện thoại</p>
          <p className="value">{user.personal.username}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Địa chỉ</p>
          <p className="value">{location.join(", ")}</p>
        </div>
        {formTwo.note && (
          <div className="wrap-line">
            <p className="label">Ghi chú</p>
            <p className="value">{formTwo.note}</p>
          </div>
        )}
      </>
    );
  };
  const yieldImgDrug = (imgDrug) => {
    return <img src={imgDrug} alt="#" />;
  };
  const yieldDrug = (info) => {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>STT</th>
            <th>Tên thuốc</th>
            <th>Số lượng</th>
          </tr>
        </thead>
        <tbody>
          {Array.isArray(info) &&
            info.map((drug, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{drug.name}</td>
                <td>{drug.quantity}</td>
              </tr>
            ))}
        </tbody>
      </table>
    );
  };
  const renderConfirmDrug = () => {
    return (
      <>
        <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col2">
          <div className="wrap-panel-item wrap-profile-info">
            <div className="wrap-title">
              <h4>
                <FaRegUserCircle /> Thông tin cá nhân
              </h4>
            </div>
            <div className="wrap-main">{yieldDetailsInfo()}</div>
          </div>
        </div>
        {formOne.imgDrug && (
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
            <div className="wrap-panel-item wrap-profile-info wrap-img-info">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Hình ảnh toa thuốc
                </h4>
              </div>
              <div className="wrap-main">{yieldImgDrug(formOne.imgDrug)}</div>
            </div>
          </div>
        )}
        {formOne.info?.length !== 0 && (
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Thuốc ngoài toa
                </h4>
              </div>
              <div className="wrap-main">{yieldDrug(formOne.info)}</div>
            </div>
          </div>
        )}
        <div className="col-12 mt-3 pt-1">
          <Button
            className="btn-accept w-100"
            htmlType="submit"
            onClick={nextPage}
          >
            <span>Xác nhận</span>
          </Button>
        </div>
      </>
    );
  };
  return (
    <CardServices
      header="Xác nhận đặt hàng"
      description="Quý khách vui lòng kiểm tra lại các thông tin đặt hàng"
      step="Xác nhận đặt hàng"
    >
      <div className="wrap-content package-health-service_details">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={false}
              full={true}
              panelCol={3}
              length={1}
              rows={5}
              data={renderConfirmDrug()}
            />
          </div>
        </div>
      </div>
    </CardServices>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _DrugStore, _Confirmation }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(ConfirmOrder)
);
