import React, { Fragment, useEffect, useRef, useState } from "react";
import { Modal } from "antd";
import Loading from "../../../../components/Loading";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { _CategoryService } from "../../../../actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { public_url } from "../../../../constants/Path";
import {
  getListNameDrug,
  getListOriginalDrug,
} from "../../../../services/DrugService";

const ModalSearchDrug = ({ onOpenModal, openModal, setNameDrug }) => {
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [listDrug, setListDrug] = useState();
  const [loadingDrug, setLoadingDrug] = useState(false);
  const [activeSearch, setActiveSearch] = useState("proprietary");
  const nameDrugRef = useRef();
  const activeDrugRef = useRef();

  //*** Declare props ***//

  useEffect(() => {
    async function getApi() {
      setLoading(false);
      setSkeleton(false);
      setModal(openModal);
    }
    getApi();
  }, [openModal]);

  const onCloseModalDetailVoucher = () => {
    onOpenModal(false);
    setModal(false);
  };

  const handSearchNameDrug = async () => {
    const { value } = nameDrugRef.current;
    if (value === "") return;
    setActiveSearch("proprietary");
    const data = {
      one_health_msg: {
        proprietary_name: value,
      },
    };
    try {
      setLoadingDrug(false);
      const listNameDrug = await getListNameDrug(data);
      setListDrug(listNameDrug);
      setLoadingDrug(true);
    } catch (error) {}
  };
  const handSearchOriginalDrug = async () => {
    const { value } = activeDrugRef.current;
    if (value === "") return;
    setActiveSearch("original");
    const data = {
      one_health_msg: {
        original_search: value,
      },
    };
    try {
      setLoadingDrug(false);
      const listNameDrug = await getListOriginalDrug(data);
      setListDrug(listNameDrug);
      setLoadingDrug(true);
    } catch (error) {}
  };
  const selectValue = (drug) => {
    setNameDrug(drug.proprietary_name);
    onOpenModal(false);
    setModal(false);
  };

  function yieldDetail() {
    return (
      <div className="popup-search-drug">
        <div className="row search-input">
          <div className="col-12 col-md-6">
            <input
              placeholder="Tìm kiếm theo tên thuốc"
              type="search"
              ref={nameDrugRef}
              onChange={handSearchNameDrug}
            />
            <img
              src={public_url.img + "/homepage/ic_search.png"}
              alt="#"
              onClick={handSearchNameDrug}
            />
          </div>
          <div className="col-12 col-md-6 md-mt-2">
            <input
              placeholder="Tìm kiếm theo hoạt tính thuốc"
              type="search"
              ref={activeDrugRef}
              onChange={handSearchOriginalDrug}
            />
            <img
              src={public_url.img + "/homepage/ic_search.png"}
              alt="#"
              onClick={handSearchOriginalDrug}
            />
          </div>
        </div>
        {!listDrug && (
          <div className="drug-img">
            <img src={public_url.img + "/medicalrecord/medicine.png"} alt="#" />
            <p className="mt-2">Vui lòng nhập tên thuốc</p>
          </div>
        )}
        {listDrug && (
          <div className="drug-list">
            <BoxSkeleton
              skeleton={!loadingDrug}
              full={true}
              length={1}
              rows={25}
              data={yieldListDrug()}
            />
          </div>
        )}
      </div>
    );
  }

  const yieldListDrug = () => {
    return (
      <div className="list-drug-data">
        {listDrug.one_health_msg.map((drug, index) => (
          <div
            className="list-drug-item"
            key={index}
            onClick={() => selectValue(drug)}
          >
            <p className={activeSearch === "proprietary" ? "active" : ""}>
              Tên thuốc: {drug.proprietary_name}
            </p>
            <p className={activeSearch === "original" ? "active" : ""}>
              Hoạt tính: {drug.original_name}
            </p>
          </div>
        ))}
      </div>
    );
  };

  return (
    <Fragment>
      <Modal
        title="Tìm thuốc"
        className="block-modal-detail-voucher"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modal}
        onCancel={onCloseModalDetailVoucher}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetail()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(ModalSearchDrug)
);
