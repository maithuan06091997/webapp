import React, { useCallback, useEffect, useState } from "react";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import CardServices from "../../../../components/CardServices";
import { Button, Form, Input } from "antd";
import "./style.scss";
import {
  getListProvinces,
  getListDistrict,
  getListWard,
} from "../../../../services/DrugService";
import BoxSelect from "../../../../components/BoxSelect";
import BoxSelectV2 from "../../../../components/BoxSelectV2";
import { _DrugStore } from "../../../../actions";
import { withRouter } from "react-router";
import { url } from "../../../../constants/Path";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

const DrugDeliveryAddress = (props) => {
  const [listProvinces, setListProvinces] = useState();
  const [listDistrict, setListDistrict] = useState();
  const [listWard, setListWard] = useState();
  const [resetValueDistrict, setResetValueDistrict] = useState();
  const [resetValueWard, setResetValueWard] = useState();
  const { drug_store, history, _DrugStore } = props;
  const form = drug_store && drug_store.form ? drug_store.form : [];
  if (form.length < 1) {
    history.push(url.name.service_drug_store);
  }
  const getListProvincesApi = useCallback(async () => {
    const listProvinces = await getListProvinces();
    setListProvinces(listProvinces.one_health_msg);
  }, []);

  useEffect(() => {
    getListProvincesApi();
  }, [getListProvincesApi]);

  const onSelectRelationshipDistrict = async (event) => {
    setResetValueDistrict("");
    setResetValueWard("");
    const data = {
      one_health_msg: {
        data: {
          province: {
            id: event.value,
          },
        },
      },
    };
    const listDistrict = await getListDistrict(data);
    setListDistrict(listDistrict.one_health_msg);
  };

  const onSelectRelationshipWard = async (event) => {
    setResetValueDistrict(event);
    setResetValueWard("");
    const data = {
      one_health_msg: {
        data: {
          district: {
            id: event.value,
          },
        },
      },
    };
    const listWard = await getListWard(data);
    setListWard(listWard.one_health_msg);
  };

  const onSelectWard = (event) => {
    setResetValueWard(event);
  };

  const nextPage = (event) => {
    const s2 = form.findIndex((key) => {
      return key.step === 2;
    });
    if (s2 === -1) {
      form.push({ step: 2, data: event });
    } else {
      form[s2] = { step: 2, data: event };
    }
    _DrugStore(form);
    props.history.push({ pathname: url.name.service_drug_confirm });
  };

  const renderDrugAddress = () => {
    if (listProvinces) {
      return (
        <div className="col-12 wrap-panel-col">
          <div className="wrap-panel-item wrap-profile-info">
            <Form
              size="large"
              className="form-drug-address w-100"
              name="form-drug-address"
              onFinish={nextPage}
            >
              <Input.Group compact>
                <Form.Item
                  label="Tỉnh/Thành"
                  name="provinces"
                  rules={[
                    { required: true, message: "Vui lòng chọn tỉnh/thành" },
                  ]}
                >
                  <BoxSelect
                    placeholder="Chọn tỉnh/thành"
                    onChange={onSelectRelationshipDistrict}
                    placement={"auto"}
                    data={listProvinces}
                  />
                </Form.Item>
                <Form.Item
                  label="Quận/Huyện"
                  name="district"
                  rules={[
                    { required: true, message: "Vui lòng chọn quận/huyện" },
                  ]}
                >
                  <BoxSelectV2
                    placeholder="Chọn quận/huyện"
                    onChange={onSelectRelationshipWard}
                    placement={"auto"}
                    resetValue={resetValueDistrict}
                    data={listDistrict}
                  />
                </Form.Item>
              </Input.Group>
              <Input.Group compact>
                <Form.Item
                  label="Phường/Xã"
                  name="ward"
                  rules={[
                    { required: true, message: "Vui lòng chọn phường/xã" },
                  ]}
                >
                  <BoxSelectV2
                    placeholder="Chọn phường/xã"
                    onChange={onSelectWard}
                    placement={"auto"}
                    resetValue={resetValueWard}
                    data={listWard}
                  />
                </Form.Item>
                <Form.Item
                  label="Số nhà, đường, thôn, ấp,..."
                  name="address"
                  rules={[{ required: true, message: "Vui lòng nhập số nhà" }]}
                >
                  <Input placeholder="Nhập số nhà, đường, thôn, ấp,..." />
                </Form.Item>
              </Input.Group>
              <Input.Group compact>
                <Form.Item label="Ghi chú" name="note">
                  <Input.TextArea rows={3} placeholder="Nhập ghi chú" />
                </Form.Item>
              </Input.Group>
              <Button className="btn-accept w-100 mt-2" htmlType="submit">
                <span>Tiếp tục</span>
              </Button>
            </Form>
          </div>
        </div>
      );
    }
  };
  return (
    <CardServices
      header="Địa chỉ giao thuốc"
      description="Quý khách vui lòng chọn địa chỉ giao thuốc"
      step="Địa chỉ giao thuốc"
    >
      <div className="wrap-content drug-address">
        <div className="wrap-panel-content">
          <div className="row">
            <BoxSkeleton
              skeleton={false}
              full={true}
              panelCol={3}
              length={1}
              rows={5}
              data={renderDrugAddress()}
            />
          </div>
        </div>
      </div>
    </CardServices>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _DrugStore }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    DrugDeliveryAddress
  )
);
