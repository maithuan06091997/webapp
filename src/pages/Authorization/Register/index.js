import React, { Fragment, useState, useEffect } from "react";
import i18next from "i18next";
import { Form, Checkbox, Button, Modal } from "antd";
import { url } from "../../../constants/Path";
import { msg_text } from "../../../constants/Message";
import WrapLeft from "../Common/WrapLeft";
import WrapDownApp from "../Common/WrapDownApp";
import Footer from "../Common/Footer";
import Loading from "../../../components/Loading";
import BoxInputPhone from "../../../components/BoxInputPhone";
import {
  ModelCheckUserExist,
  ModelAgreeTerms,
} from "../../../models/ModelUser";
import { _VerifyPhone } from "../../../actions";
import { bindActionCreators } from "redux";
import { withRouter, Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

const RegisterPage = (props) => {
  const [fields, setFields] = useState([]);
  const [loading, setLoading] = useState(false);
  //*** Declare props ***//
  const { _VerifyPhone, location, history, user } = props;

  const [modal, contextHolder] = Modal.useModal();

  const phone =
    location.state && location.state.phone ? location.state.phone : undefined;
  const type =
    location.state && location.state.type ? location.state.type : "Register";

  useEffect(() => {
    setFields([{ name: ["itemPhone"], value: phone }]);
  }, [phone]);

  const onFinish = (values) => {
    const phone = func.isValidPhone(values.itemPhone);
    const reqParamFt = { username: phone };
    setLoading(true);
    ModelCheckUserExist(reqParamFt)
      .then((resultFt) => {
        const dataCheckUserExist = resultFt.data.one_health_msg;
        if (dataCheckUserExist.status === "active") {
          throw new Error(msg_text.phone_exist);
        } else {
          return ModelAgreeTerms(reqParamFt);
        }
      })
      .then(() => {
        _VerifyPhone({
          phone: phone,
          flag: "unverified",
          type: type,
          countdown: 30,
        });
        history.push(url.name.verify);
      })
      .catch((error) => {
        const msg = error.message ? error.message : error.description;
        modal.warning({
          title: "Thông báo",
          okText: "Đóng",
          centered: true,
          content: i18next.t(msg),
        });
        setLoading(false);
      });
  };

  if (user.auth) {
    return <Redirect to={url.name.home} />;
  }

  let yieldText = <strong>Nhập số điện thoại để bắt đầu sử dụng</strong>;
  if (phone && type === "SignIn") {
    yieldText = (
      <strong className="color-red">
        Số điện thoại "{phone}" chưa được tạo tài khoản. Đăng ký ngay số điện
        thoại này
      </strong>
    );
  }

  return (
    <Fragment>
      <div className="block-index">
        <div className="container-fluid">
          <div className="row align-items-center wrap-main">
            <div className="col-xl-6 col-lg-6 wrap-left">
              <WrapLeft />
            </div>
            <div className="col-xl-6 col-lg-6 wrap-right">
              <div className="card">
                <div className="card-body">
                  <Form
                    onFinish={onFinish}
                    fields={fields}
                    size="large"
                    className="form-register"
                    name="form-register"
                  >
                    <p className="text-center">{yieldText}</p>{" "}
                    <BoxInputPhone
                      required={true}
                      autoFocus={true}
                      initialValue={phone}
                      placeholder="Nhập số điện thoại"
                      name="itemPhone"
                    />{" "}
                    <Form.Item
                      initialValue={true}
                      name="itemAgreement"
                      valuePropName="checked"
                      rules={[
                        {
                          validator: (_, value) =>
                            value
                              ? Promise.resolve()
                              : Promise.reject(
                                  "Vui lòng xác nhận đã đọc điều khoản sử dụng"
                                ),
                        },
                      ]}
                    >
                      <Checkbox className="chkAgreement">
                        Tôi đã đọc và đồng ý với{" "}
                        <span className="linkAgreement">
                          điều khoản sử dụng
                        </span>{" "}
                        của Dr.OH
                      </Checkbox>
                    </Form.Item>{" "}
                    <Form.Item>
                      <Button
                        type="primary"
                        loading={loading}
                        className="w-100 mt-2"
                        htmlType="submit"
                      >
                        <span>Đăng ký</span>
                      </Button>
                    </Form.Item>
                  </Form>

                  <div className="text-center">
                    <hr />
                    <p>Bạn đã có tài khoản?</p>
                    <Link
                      className="ant-btn ant-btn-lg btn-accept w-100"
                      to={url.name.index}
                    >
                      <span>Đăng nhập</span>
                    </Link>
                  </div>
                </div>
              </div>
              <WrapDownApp />
            </div>
          </div>
        </div>
      </div>
      <Footer />
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _VerifyPhone }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(RegisterPage)
);
