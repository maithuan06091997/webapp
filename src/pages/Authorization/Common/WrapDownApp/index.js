import React, {Fragment} from "react";
import {public_url} from "../../../../constants/Path";
import {google_play, app_store} from "../../../../constants/Default";
import "./style.scss";

const WrapDownApp = () => {
    return (
        <Fragment>
            <div className="wrap-down-app text-center mt-3">
                <p className="text-muted">Tải ứng dụng trên điện thoại</p>
                <div className="icon-app">
                    <a target="_blank" rel="noopener noreferrer" href={app_store}>
                        <img className="img-fluid" src={public_url.img + "/appstore.png"} alt="Dr.OH - App Store"/>
                    </a>
                    <a target="_blank" rel="noopener noreferrer" href={google_play}>
                        <img className="img-fluid" src={public_url.img + "/googleplay.png"} alt="Dr.OH - App Googleplay"/>
                    </a>
                </div>
            </div>
        </Fragment>
    );
};

export default WrapDownApp;
