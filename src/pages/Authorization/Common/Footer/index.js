import React, {Fragment} from "react";
import {url} from "../../../../constants/Path";
import {Link} from "react-router-dom";
import "./style.scss";

const Footer = () => {
    return (
        <Fragment>
            <div className="block-footer">
                <div className="wrap-footer">
                    <div className="footer-content">
                        <ul className="ul-tag-list">
                            <li><Link to={url.name.register}>Đăng ký</Link></li>
                            <li><Link to={url.name.index}>Đăng nhập</Link></li>
                            <li>Điều khoản</li>
                            <li>Trợ giúp</li>
                        </ul>
                    </div>
                    <hr/>
                    <div className="footer-copyright">
                        <span>Dr.OH © 2020</span>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Footer;
