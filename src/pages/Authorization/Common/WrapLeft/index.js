import React, {Fragment} from "react";
import {Link} from "react-router-dom";
import {url, public_url} from "../../../../constants/Path";

const WrapLeft = () => {
    return (
        <Fragment>
            <Link to={url.name.index}>
                <img className="img-fluid" src={public_url.img + "/logo.png"} alt="Dr.OH"/>
            </Link>
            <div className="slogan">
                <h1 className="text-muted">Ứng dụng thông minh</h1>
                <h3 className="text-muted">Dễ dàng - Tiện lợi - Tin cậy</h3>
            </div>
        </Fragment>
    );
};

export default WrapLeft;
