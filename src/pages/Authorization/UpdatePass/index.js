import React, {Fragment, useEffect, useState} from "react";
import i18next from "i18next";
import {Form, Input, Button, Modal} from "antd";
import {url} from "../../../constants/Path";
import WrapLeft from "../Common/WrapLeft";
import WrapDownApp from "../Common/WrapDownApp";
import Footer from "../Common/Footer";
import Loading from "../../../components/Loading";
import {_CleanStoreUser, _RecoverPassword, _VerifyPhone, _Register} from "../../../actions";
import {Redirect, withRouter} from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

const UpdatePassPage = (props) => {
    const [loading, setLoading] = useState(false);
    //*** Declare props ***//
    const {
        _CleanStoreUser, _VerifyPhone, _RecoverPassword, _Register, user, verify
    } = props;

    const [modal, contextHolder] = Modal.useModal();

    const phone = verify && verify.phone ? verify.phone : undefined;
    const type = verify && verify.type ? verify.type : undefined;
    const code = verify && verify.code ? verify.code : "";

    useEffect(() => {
        if (!user.auth && user.message) {
            setLoading(false);
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(user.message)
            });
            _CleanStoreUser();
        }
    }, [_VerifyPhone, _CleanStoreUser, user.auth, user.message, modal]);

    const onFinish = values => {
        const reqParamFt = {
            phone: phone,
            code: code,
            password: values.itemConfirm
        };
        setLoading(true);
        if (type === "Recover") {
            _RecoverPassword(reqParamFt);
        } else {
            _Register(reqParamFt);
        }
    };

    //*** If the user is logged in, then redirect the homepage ***//
    if (user.auth) {
        return <Redirect to={url.name.home}/>;
    }

    //*** Phone number is required to access the page ***//
    if (!phone) {
        return <Redirect to={url.name.index}/>;
    }

    return (
        <Fragment>
            <div className="block-index">
                <div className="container-fluid">
                    <div className="row align-items-center wrap-main">
                        <div className="col-xl-6 col-lg-6 wrap-left">
                            <WrapLeft/>
                        </div>
                        <div className="col-xl-6 col-lg-6 wrap-right">
                            <div className="card">
                                <div className="card-body">
                                    <Form onFinish={onFinish}
                                          size="large"
                                          className="form-update-pass"
                                          name="form-update-pass">
                                        <p className="text-center">
                                            <strong>Nhập mật khẩu</strong>
                                        </p>
                                        {" "}
                                        <Form.Item name="itemPass"
                                                   rules={[{required: true, message: "Vui lòng nhập mật khẩu"},
                                                       {
                                                           validateTrigger: 'onBlur',
                                                           min: 6,
                                                           message: "Vui lòng nhập trên 5 ký tự"
                                                       }]}>
                                            <Input.Password placeholder="Nhập mật khẩu"/>
                                        </Form.Item>
                                        {" "}
                                        <Form.Item name="itemConfirm"
                                                   dependencies={["itemPass"]}
                                                   rules={[{required: true, message: "Vui lòng xác nhận mật khẩu"},
                                                       ({getFieldValue}) => ({
                                                           validateTrigger: 'onBlur',
                                                           validator(rule, value) {
                                                               if (!value || getFieldValue("itemPass") === value) {
                                                                   return Promise.resolve();
                                                               }
                                                               return Promise.reject("Hai mật khẩu bạn nhập không khớp");
                                                           }
                                                       })
                                                   ]}>
                                            <Input.Password placeholder="Nhập xác nhận mật khẩu"/>
                                        </Form.Item>
                                        {" "}
                                        <Form.Item>
                                            <Button type="primary" loading={loading} className="w-100 mt-2"
                                                    htmlType="submit"><span>Tiếp tục</span>
                                            </Button>
                                        </Form.Item>
                                    </Form>
                                </div>
                            </div>
                            <WrapDownApp/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        _CleanStoreUser, _VerifyPhone,
        _RecoverPassword, _Register
    }, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(UpdatePassPage));
