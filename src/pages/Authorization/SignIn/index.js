import React, {Fragment, useState} from "react";
import i18next from "i18next";
import {Modal, Form, Button} from "antd";
import {url} from "../../../constants/Path";
import WrapLeft from "../Common/WrapLeft";
import WrapDownApp from "../Common/WrapDownApp";
import Footer from "../Common/Footer";
import Loading from "../../../components/Loading";
import BoxInputPhone from "../../../components/BoxInputPhone";
import {ModelCheckUserExist} from "../../../models/ModelUser";
import {withRouter, Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

const SignInPage = (props) => {
    const [loading, setLoading] = useState(false);
    //*** Declare props ***//
    const {history, user} = props;

    const [modal, contextHolder] = Modal.useModal();

    const onFinish = values => {
        const phone = func.isValidPhone(values.itemPhone);
        const pushState = {type: "SignIn", phone: phone};
        const reqParamFt = {username: phone};
        setLoading(true);
        ModelCheckUserExist(reqParamFt).then(resultFt => {
            const dataCheckUserExist = resultFt.data.one_health_msg;
            if (dataCheckUserExist.status === "active") {
                history.push({
                    pathname: url.name.confirm_pass,
                    state: pushState
                });
            } else {
                history.push({
                    pathname: url.name.register,
                    state: pushState
                });
            }
        }).catch(error => {
            const msg = error.message ? error.message : error.description;
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(msg)
            });
            setLoading(false);
        });
    };

    if (user.auth) {
        return <Redirect to={url.name.home}/>;
    }

    return (
        <Fragment>
            <div className="block-index">
                <div className="container-fluid">
                    <div className="row align-items-center wrap-main">
                        <div className="col-xl-6 col-lg-6 wrap-left">
                            <WrapLeft/>
                        </div>
                        <div className="col-xl-6 col-lg-6 wrap-right">
                            <div className="card">
                                <div className="card-body">
                                    <Form onFinish={onFinish}
                                          size="large"
                                          className="form-sign-in"
                                          name="form-sign-in">
                                        <p className="text-center">
                                            <strong>Nhập số điện thoại để bắt đầu sử dụng</strong>
                                        </p>
                                        {" "}
                                        <BoxInputPhone required={true}
                                                       autoFocus={true}
                                                       placeholder="Nhập số điện thoại"
                                                       name="itemPhone"/>
                                        {" "}
                                        <Form.Item>
                                            <Button type="primary" loading={loading} className="w-100 mt-2"
                                                    htmlType="submit"><span>Đăng nhập</span></Button>
                                        </Form.Item>
                                    </Form>

                                    <div className="text-center">
                                        <hr/>
                                        <p>Bạn chưa có tài khoản?</p>
                                        <Link className="ant-btn ant-btn-lg btn-accept w-100"
                                              to={url.name.register}><span>Tạo tài khoản mới</span></Link>
                                    </div>
                                </div>
                            </div>
                            <WrapDownApp/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(SignInPage));
