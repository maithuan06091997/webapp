import React, {Fragment, useState, useEffect} from "react";
import i18next from "i18next";
import {Form, Input, Button, Modal} from "antd";
import {hotline} from "../../../constants/Default";
import {url} from "../../../constants/Path";
import WrapLeft from "../Common/WrapLeft";
import WrapDownApp from "../Common/WrapDownApp";
import Footer from "../Common/Footer";
import Loading from "../../../components/Loading";
import {ModelRequestRecoverPassword} from "../../../models/ModelUser";
import {_CleanStoreUser, _VerifyPhone, _SignIn} from "../../../actions";
import {bindActionCreators} from "redux";
import {Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./style.scss";

const ConfirmPassPage = (props) => {
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    //*** Declare props ***//
    const {
        _VerifyPhone, _SignIn, _CleanStoreUser, location, history, user
    } = props;

    const [modal, contextHolder] = Modal.useModal();

    const phone = location.state && location.state.phone ? location.state.phone : undefined;

    useEffect(() => {
        if (!user.auth && user.message) {
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(user.message)
            });
            _CleanStoreUser();
        }
    }, [_CleanStoreUser, modal, user.auth, user.message]);

    const onFinish = values => {
        const reqSignIn = {
            phone: phone,
            password: values.itemPassword
        };
        _SignIn(reqSignIn);
    };

    const onConfirmRecoverPass = () => {
        setVisible(true);
    };

    const onAbortRecoverPass = () => {
        setVisible(false);
    };

    const onAcceptRecoverPass = () => {
        const reqParamFt = {
            data: {
                user: {
                    phone: phone
                }
            }
        };
        setLoading(true);
        ModelRequestRecoverPassword(reqParamFt).then(() => {
            _VerifyPhone({phone: phone, flag: "unverified", type: "Recover", countdown: 30});
            history.push(url.name.verify);
        }).catch(error => {
            const msg = error.message ? error.message : error.description;
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(msg)
            });
            setLoading(false);
        })
    };

    //*** If the user is logged in, then redirect the homepage ***//
    if (user.auth) {
        return <Redirect to={url.name.home}/>;
    }

    //*** Phone number is required to access the page ***//
    if (!phone) {
        return <Redirect to={url.name.index}/>;
    }

    return (
        <Fragment>
            <div className="block-index">
                <div className="container-fluid">
                    <div className="row align-items-center wrap-main">
                        <div className="col-xl-6 col-lg-6 wrap-left">
                            <WrapLeft/>
                        </div>
                        <div className="col-xl-6 col-lg-6 wrap-right">
                            <div className="card">
                                <div className="card-body">
                                    <Form onFinish={onFinish}
                                          size="large"
                                          className="form-confirm-pass"
                                          name="form-confirm-pass">
                                        <p className="text-center"><strong>Nhập mật khẩu</strong></p>
                                        {" "}
                                        <Form.Item name="itemPassword"
                                                   rules={
                                                       [{
                                                           required: true,
                                                           message: "Vui lòng nhập mật khẩu"
                                                       }]}>
                                            <Input.Password autoFocus={true} placeholder={"Nhập mật khẩu"}/>
                                        </Form.Item>
                                        {" "}
                                        <Form.Item>
                                            <Button type="primary" className="w-100 mt-2"
                                                    htmlType="submit"><span>Tiếp tục</span></Button>
                                        </Form.Item>
                                        {" "}
                                        <p className="text-center">Bạn quên mật khẩu?
                                            <span onClick={onConfirmRecoverPass} className="recover-pass mb-2"> Lấy lại mật khẩu</span>
                                        </p>
                                        <div className="cskh"><a href={"tel:" + hotline}>Gọi CSKH {hotline}</a></div>
                                    </Form>
                                </div>
                            </div>
                            <WrapDownApp/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            <Modal className="block-modal-recover-pass"
                   title="Lấy lại mật khẩu"
                   visible={visible}
                   okText="Đồng ý"
                   onOk={onAcceptRecoverPass}
                   cancelText="Huỷ"
                   onCancel={onAbortRecoverPass}
                   closable={false}
                   confirmLoading={loading}
                   centered>
                <p>Mã OTP sẽ được gửi về số điện thoại {phone}</p>
            </Modal>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_CleanStoreUser, _VerifyPhone, _SignIn}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(ConfirmPassPage));
