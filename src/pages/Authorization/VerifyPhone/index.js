import React, {Fragment, useCallback, useState, useEffect} from "react";
import i18next from "i18next";
import {Button, Modal, message} from "antd";
import {hotline} from "../../../constants/Default";
import {url} from "../../../constants/Path";
import {msg_text} from "../../../constants/Message";
import ReactCodeInput from "react-verification-code-input";
import WrapLeft from "../Common/WrapLeft";
import WrapDownApp from "../Common/WrapDownApp";
import Footer from "../Common/Footer";
import Loading from "../../../components/Loading";
import {
    ModelVerifyOTP, ModelVerifyRecoverOTP,
    ModelRequestOTP, ModelRequestOTPRecoverPassword
} from "../../../models/ModelUser";
import {_VerifyPhone} from "../../../actions";
import {bindActionCreators} from "redux";
import {Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./style.scss";

const VerifyPhonePage = (props) => {
    const [loading, setLoading] = useState(false);
    const [lock, setLock] = useState(false);
    const [correct, setCorrect] = useState(true);
    const [visible, setVisible] = useState(false);
    const [requestOTP, setRequestOTP] = useState(false);
    //*** Declare props ***//
    const {_VerifyPhone, history, verify, user} = props;

    const [modal, contextHolder] = Modal.useModal();

    const phone = verify && verify.phone ? verify.phone : undefined;
    const flag = verify && verify.flag ? verify.flag : undefined;
    const type = verify && verify.type ? verify.type : undefined;
    const countdown = verify && verify.countdown ? verify.countdown : 0;

    const handleRequestOTPRecoverPassword = useCallback(params => {
        ModelRequestOTPRecoverPassword(params).then(() => {
            setLoading(false);
            message.success(msg_text.otp_success + " " + phone);
            _VerifyPhone({...verify, countdown: 30});
        }).catch(error => {
            const msg = error.message ? error.message : error.description;
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(msg)
            });
            setLoading(false);
        });
    }, [_VerifyPhone, verify, phone, modal]);

    const handleRequestOTP = useCallback(params => {
        ModelRequestOTP(params).then(() => {
            setLoading(false);
            message.success(msg_text.otp_success + " " + phone);
            _VerifyPhone({...verify, countdown: 30});
        }).catch(error => {
            const msg = error.message ? error.message : error.description;
            modal.warning({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: i18next.t(msg)
            });
            setLoading(false);
        });
    }, [_VerifyPhone, verify, phone, modal]);

    useEffect(() => {
        const timerCountdown = setInterval(() => {
            const timeLeft = countdown - 1;
            _VerifyPhone({...verify, countdown: timeLeft});
        }, 1000);
        if (countdown === 0) {
            clearInterval(timerCountdown);
            setRequestOTP(true);
        } else {
            setRequestOTP(false);
        }
        return () => {
            clearInterval(timerCountdown);
        };
    }, [_VerifyPhone, verify, countdown]);

    const onFinish = useCallback(values => {
        const reqParamFt = {
            verify_code: {
                phone: phone,
                pin_code: values
            }
        };
        setLock(true);
        if (type === "Recover") {
            ModelVerifyRecoverOTP(reqParamFt).then(resultFt => {
                const dataVerifyRecoverOTP = resultFt.data.one_health_msg;
                const code = dataVerifyRecoverOTP.id;
                _VerifyPhone({phone: phone, flag: "verified", code: code, type: type});
                history.push(url.name.update_pass);
            }).catch(() => {
                console.log("VerifyPhonePage - VerifyRecoverOTP");
                setLock(false);
                setCorrect(false);
            });
        } else {
            ModelVerifyOTP(reqParamFt).then(resultFt => {
                const dataVerifyOTP = resultFt.data.one_health_msg;
                const code = dataVerifyOTP.id;
                _VerifyPhone({phone: phone, flag: "verified", code: code, type: type});
                history.push(url.name.update_pass);
            }).catch(() => {
                console.log("VerifyPhonePage - VerifyOTP");
                setLock(false);
                setCorrect(false);
            });
        }
    },[_VerifyPhone, history, type, phone]);

    const onInputCode = () => { setCorrect(true); };

    const onConfirmRequestOTP = () => {
        setVisible(true);
    };

    const onAbortRequestOTP = () => {
        setVisible(false);
    };

    const onAcceptRequestSMSOTP = useCallback(() => {
        setLoading(true);
        setVisible(false);
        if (type === "Recover") {
            const reqParamFt = {
                data: {
                    user: {
                        phone: phone,
                    }
                }
            };
            handleRequestOTPRecoverPassword(reqParamFt);
        } else {
            const reqParamFt = {
                phone: phone
            };
            handleRequestOTP(reqParamFt);
        }
    }, [type, phone, handleRequestOTPRecoverPassword, handleRequestOTP]);

    const onAcceptRequestVoiceOTP = useCallback(() => {
        setLoading(true);
        setVisible(false);
        if (type === "Recover") {
            const reqParamFt = {
                data: {
                    user: {
                        phone: phone,
                    },
                    voice_otp: "true"
                }
            };
            handleRequestOTPRecoverPassword(reqParamFt);
        } else {
            const reqParamFt = {
                phone: phone,
                voice_otp: "true"
            };
            handleRequestOTP(reqParamFt);
        }
    }, [type, phone, handleRequestOTPRecoverPassword, handleRequestOTP]);

    //*** If the user is logged in, then redirect the homepage ***//
    if (user.auth) {
        return <Redirect to={url.name.home}/>;
    }

    //*** The flag must be unverified to access the page ***//
    if (!flag || flag === "verified") {
        return <Redirect to={url.name.index}/>;
    }

    let yieldMess;
    if (!correct) {
        yieldMess = (<p className="color-red text-center">{msg_text.otp_wrong}</p>);
    }

    return (
        <Fragment>
            <div className="block-index">
                <div className="container-fluid">
                    <div className="row align-items-center wrap-main">
                        <div className="col-xl-6 col-lg-6 wrap-left">
                            <WrapLeft/>
                        </div>
                        <div className="col-xl-6 col-lg-6 wrap-right">
                            <div className="card">
                                <div className="card-body">
                                    <div className="verify-phone">
                                        <p className="text-center">
                                            <strong>
                                                Vui lòng nhập mã xác thực đã được gửi về số điện thoại {phone}
                                            </strong>
                                        </p>
                                        {" "}
                                        <div className="mb-3">
                                            <ReactCodeInput type="number"
                                                            loading={lock}
                                                            disabled={lock}
                                                            onChange={onInputCode}
                                                            onComplete={onFinish}
                                                            className={correct ? "code-input" : "code-input wrong-code"}
                                                            fieldWidth={40}
                                                            fieldHeight={40}
                                                            fields={5}/>
                                            {yieldMess}
                                        </div>
                                        {" "}
                                        <p className="text-center">Bạn không nhận được mã OTP?</p>
                                        <div onClick={requestOTP ? onConfirmRequestOTP : null}
                                             className="send-again mb-3">
                                            Gửi lại mã
                                            OTP {countdown !== 0 ? "(" + countdown.toLocaleString().padStart(2, "0") + ")" : ""}
                                        </div>
                                        <div className="cskh"><a href={"tel:" + hotline}>Gọi CSKH {hotline}</a></div>
                                    </div>
                                </div>
                            </div>
                            <WrapDownApp/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
            <Modal className="block-modal-request-otp"
                   title={"Yêu cầu gửi lại OTP về số điện thoại " + phone}
                   visible={visible}
                   closable={true}
                   footer={null}
                   onCancel={onAbortRequestOTP}
                   centered>
                <div className="wrap-event-action">
                    <Button size="large" onClick={onAcceptRequestVoiceOTP}
                            htmlType="button"><span>Nhận mã qua cuộc gọi</span></Button>
                    <Button size="large" onClick={onAcceptRequestSMSOTP}
                            htmlType="button"><span>Nhận mã qua tin nhắn</span></Button>
                </div>
            </Modal>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_VerifyPhone}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(VerifyPhonePage));
