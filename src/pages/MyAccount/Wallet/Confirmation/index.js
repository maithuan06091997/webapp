import React, {Fragment, useCallback, useEffect, useState} from "react";
import _ from "lodash";
import {public_url, url} from "../../../../constants/Path";
import queryString from "query-string";
import {_Confirmation} from "../../../../actions";
import {bindActionCreators} from "redux";
import {withRouter, Link, Redirect} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const SUCCESS           = "Thành công";
const FAILED            = "Thất bại";
const GATEWAY_ONEPAY    = "OnePay";

const WalletConfirmationPage = (props) => {
    const [method, setMethod] = useState(undefined);
    const [invoiceCode, setInvoiceCode] = useState(undefined);
    const [datetime, setDatetime] = useState(undefined);
    const [status, setStatus] = useState(undefined);
    // const [amount, setAmount] = useState(0);
    //*** Declare props ***//
    const {_Confirmation, user, location, confirm} = props;
    const parsed = queryString.parse(location.search);

    const handleCleanConfirmation = useCallback(() => {
        _Confirmation({
            status: false,
            type: "",
            name: "",
            gateway: "",
        });
    }, [_Confirmation]);

    useEffect(() => {
        if (!_.isEmpty(parsed)) {
            if (parsed.type && parsed.status) {
                switch (parsed.type) {
                    case "onepay":
                        setMethod(GATEWAY_ONEPAY);
                        break;
                    default:
                        handleCleanConfirmation();
                }
                if (func.ucFirst(parsed.status) === "Done") {
                    setInvoiceCode(parsed["invoice-code"]);
                    setDatetime(parsed.date);
                    setStatus(SUCCESS);
                } else if (func.ucFirst(parsed.status) === "Failed") {
                    setStatus(FAILED);
                } else {
                    handleCleanConfirmation();
                }
            } else {
                handleCleanConfirmation();
            }
        } else {
            handleCleanConfirmation();
        }
    }, [handleCleanConfirmation, parsed]);

    if (!confirm.status || confirm.type !== "recharge") {
        return <Redirect to={url.name.home}/>;
    }

    let yieldInfo;
    if (status === SUCCESS) {
        yieldInfo = (
            <Fragment>
                <p>
                    <span className="label">Mã hóa đơn: </span>
                    <span className="value invoice-code">{invoiceCode}</span>
                </p>
                <p>
                    <span className="label">Ngày giao dịch: </span>
                    <span className="value datetime">{func.yieldDatetime(datetime)}</span>
                </p>
                <p className="label">
                    Để xem chi tiết giao dịch của quý khách. Vui lòng truy cập vào phần
                    <Link to={url.name.account_list_invoice}>"lịch sử giao dịch"</Link>
                </p>
            </Fragment>
        );
    } else {
        yieldInfo = (
            <Fragment>
                <p className="label">
                    Chúng tôi rất tiếc, thanh toán của bạn chưa thành công. Nó có thể là một số lý do sau:
                </p>
                <p className="label">- Bạn đã hủy thanh toán.</p>
                <p className="label">- Bạn đã nhập sai ngày hết hạn thẻ.</p>
                <p className="label">- Tín dụng của bạn đã đạt đến giới hạn.</p>
                <p className="label">- Lỗi máy tính hoặc mạng.</p>
            </Fragment>
        );
    }

    return (
        <Fragment>
            <div className="block-confirmation">
                <div className="card card-custom">
                    <div className="card-header">
                        <h3>Xác nhận thông tin giao dịch</h3>
                    </div>
                    <div className="card-body">
                        <div className="wrap-content">
                            <h4>Kính gửi {user.personal.last_name + " " + user.personal.first_name},</h4>
                            <p className="font-weight-bold">Cảm ơn quý khách đã sử dụng dịch vụ của Dr.OH</p>
                            <p>
                                <span className="label">Phương thức thanh toán: </span>
                                <span className="value method">{method}</span>
                            </p>
                            <p>
                                <span className="label">Tình trạng thanh toán: </span>
                                <span
                                    className={status && status === SUCCESS ? "value badge badge-success" : "value badge badge-danger"}>
                                    {status}
                                </span>
                            </p>
                           {/*<p>
                                <span className="label">Tổng số tiền giao dịch: </span>
                                <span className="value amount">
                                    {amount.toLocaleString() + " đ"}
                                </span>
                            </p>*/}
                            {yieldInfo}
                            <div className="wrap-img">
                                <img className="img-fluid" src={public_url.img + "/thanks.png"} alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_Confirmation}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(WalletConfirmationPage));
