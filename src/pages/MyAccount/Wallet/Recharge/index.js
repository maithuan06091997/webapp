import React, { Fragment, useEffect, useRef, useState } from "react";
import _ from "lodash";
// import i18next from "i18next";
// import { AiOutlineRight } from "react-icons/ai";
// import { Button, Form, Input, InputNumber, Modal, Radio } from "antd";
import { public_url } from "../../../../constants/Path";
// import { msg_text } from "../../../../constants/Message";
import Loading from "../../../../components/Loading";
import { ModelGetListCardUsed } from "../../../../models/ModelWallet";
import { _Confirmation } from "../../../../actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
// import * as func from "../../../../helpers";
import "./style.scss";
import { Modal } from "antd";

const dataMoney = [
  { id: 0, text: "30,000", value: 30000 },
  { id: 1, text: "50,000", value: 50000 },
  { id: 2, text: "100,000", value: 100000 },
  { id: 3, text: "200,000", value: 200000 },
  { id: 4, text: "500,000", value: 500000 },
  { id: 5, text: "1,000,000", value: 1000000 },
  { id: 6, text: "2,000,000", value: 2000000 },
  { id: 7, text: "5,000,000", value: 5000000 },
];

//*** Handle component amount default ***//
// const AmountDefault = (props) => {
//   const { label, value, selected, onClick } = props;
//   const onPress = () => {
//     onClick(value, label);
//   }

//   const isActive = selected === value;
//   const className = isActive ? "amount-item active" : "amount-item";

//   return (
//     <Fragment>
//       <span className={className} onClick={onPress}>{label}</span>
//     </Fragment>
//   );
// };

const WalletRechargePage = (props) => {
  const isRendered = useRef(false);
  // const [amount, setAmount] = useState(dataMoney[0]);
  const [loading, setLoading] = useState(false);
  // const [method, setMethod] = useState("card-oh");
  const [listCardATM, setListCardATM] = useState([]);
  const [listCardVisa, setListCardVisa] = useState([]);
  // //*** Declare props ***//
  const { _TokenExpired, _Confirmation, _BalanceUser, user, balance } = props;

  const [modal, contextHolder] = Modal.useModal();
  // const [form] = Form.useForm();

  useEffect(() => {
    const reqParams = null;
    ModelGetListCardUsed(reqParams)
      .then((resultFt) => {
        if (!isRendered.current) {
          const dataListCard = resultFt.data.one_health_msg.data;
          let cardATM = [];
          let cardVisa = [];
          _.map(dataListCard, (item) => {
            if (item.cardType === "atm_card") {
              cardATM.push(item);
            } else {
              cardVisa.push(item);
            }
          });
          setListCardATM(cardATM);
          setListCardVisa(cardVisa);
        }
        return null;
      })
      .catch((error) => {
        if (error.code !== "007") {
          console.log("WalletRechargePage - GetListCardUsed");
        }
      });
    return () => {
      isRendered.current = true;
    };
  }, []);

  // const checkValidOtherAmount = (rule, value) => {
  //   if (amount) {
  //     return Promise.resolve();
  //   }
  //   if (value < 30000) {
  //     return Promise.reject("Số tiền phải từ 30,000 trở lên");
  //   }
  //   return Promise.resolve();
  // }

  // const handleSubmitOH = params => {
  //   ModelSubmitOH(params).then(() => {
  //     _BalanceUser();
  //     modal.success({
  //       title: "Thông báo",
  //       okText: "Đóng",
  //       centered: true,
  //       content: msg_text.submit_oh_success
  //     });
  //     form.resetFields();
  //     setLoading(false);
  //   }).catch(error => {
  //     if (error.code === "007") {
  //       _TokenExpired(error);
  //     } else {
  //       const msg = error.message ? error.message : error.description;
  //       modal.warning({
  //         title: "Thông báo",
  //         okText: "Đóng",
  //         centered: true,
  //         content: i18next.t(msg)
  //       });
  //       setLoading(false);
  //     }
  //   });
  // };

  // const handleSubmitOnePay = params => {
  //   ModelSubmitOnePay(params).then(resultFt => {
  //     _Confirmation({
  //       status: true,
  //       type: "recharge"
  //     });
  //     window.location.href = resultFt.data.one_health_msg;
  //   }).catch(error => {
  //     if (error.code === "007") {
  //       _TokenExpired(error);
  //     } else {
  //       const msg = error.message ? error.message : error.description;
  //       modal.warning({
  //         title: "Thông báo",
  //         okText: "Đóng",
  //         centered: true,
  //         content: i18next.t(msg)
  //       });
  //       setLoading(false);
  //     }
  //   });
  // };

  // const onFinish = values => {
  //   setLoading(true);
  //   if (!values.itemGatewayATM && values.itemMethod === "card-oh") {
  //     const reqParamFt = {
  //       serial: values.itemSeriCode,
  //       pin: values.itemPinCode,
  //       user: user.personal._id
  //     };
  //     handleSubmitOH(reqParamFt);
  //   }
  //   if (values.itemGatewayATM === "OnePay" || values.itemGatewayVisa === "OnePay") {
  //     let payAmount;
  //     if (values.itemOtherAmount && !amount) {
  //       payAmount = values.itemOtherAmount;
  //     } else {
  //       payAmount = amount.value;
  //     }
  //     const reqParamFt = {
  //       amount: payAmount,
  //       return_url: window.location.protocol + "//" + window.location.host + url.name.account_wallet_confirmation
  //     };
  //     if (values.itemCardATMInfo && values.itemCardATMInfo !== "new") {
  //       reqParamFt.verifyToken = values.itemCardATMInfo.verifyToken;
  //     }
  //     if (values.itemCardVisaInfo && values.itemCardVisaInfo !== "new") {
  //       reqParamFt.verifyToken = values.itemCardVisaInfo.verifyToken;
  //     }
  //     handleSubmitOnePay(reqParamFt);
  //   }
  // };

  // const onPressAmountDefault = (value, label) => {
  //   form.resetFields(['itemOtherAmount']);
  //   setAmount({ text: label, value: value });
  // };

  // const onInputOtherAmount = value => {
  //   if (value) {
  //     setAmount(undefined);
  //   }
  // };

  // const onChangeMethod = e => {
  //   const value = e.target.value;
  //   setMethod(value);
  // };

  // function yieldMethodCardPanel() {
  //   if (method === "card-oh") {
  //     return (
  //       <div className="wrap-form-method-card">
  //         <h4>Nhập thông tin thẻ OneHealth</h4>
  //         <Form.Item label="Mã pin"
  //           name="itemPinCode"
  //           rules={[{ required: true, message: "Vui lòng nhập mã pin" },
  //           { whitespace: true, message: "Vui lòng nhập mã pin" }]}>
  //           <Input placeholder="Nhập mã pin" />
  //         </Form.Item>
  //         <Form.Item label="Số seri"
  //           name="itemSeriCode"
  //           rules={[{ required: true, message: "Vui lòng nhập số seri" },
  //           { whitespace: true, message: "Vui lòng nhập số seri" }]}>
  //           <Input placeholder="Nhập số seri" />
  //         </Form.Item>
  //       </div>
  //     );
  //   }
  //   return null;
  // }

  // function yieldMethodATMPanel() {
  //   if (method === "atm") {
  //     let detailCardATM;
  //     let yieldCardUsed;
  //     if (listCardATM.length > 0) {
  //       detailCardATM = _.map(listCardATM, (item, index) => {
  //         const bankName = func.yieldCardUsed(item).bankName;
  //         const logo = func.yieldCardUsed(item).logo;
  //         const cardNumber = item.cardNumber;
  //         return (
  //           <Radio key={index} value={item}>
  //             <div className="info">
  //               <img alt=''
  //                 src={public_url.img + logo} />
  //               <div className="text">
  //                 <p>{bankName}</p>
  //                 <p>{cardNumber}</p>
  //               </div>
  //             </div>
  //           </Radio>
  //         );
  //       });
  //       yieldCardUsed = (
  //         <div className="wrap-card">
  //           <h4>Thẻ bạn đã dùng</h4>
  //           <Form.Item name="itemCardATMInfo" initialValue={"new"}>
  //             <Radio.Group className="card-info">
  //               <Radio value={"new"}>
  //                 <div className="info">
  //                   <img alt='' src={public_url.img + "/wallet/logo_atm.png"} />
  //                   <p>Sử dụng thẻ mới</p>
  //                 </div>
  //               </Radio>
  //               {detailCardATM}
  //             </Radio.Group>
  //           </Form.Item>
  //         </div>
  //       );
  //     }
  //     const listMoneyDefault = _.map(dataMoney, (item, index) => {
  //       return (
  //         <AmountDefault key={index} onClick={onPressAmountDefault}
  //           value={item.value}
  //           selected={amount ? amount.value : undefined}
  //           label={item.text} />
  //       );
  //     });

  //     return (
  //       <div className="wrap-form-method-atm">
  //         <div className="wrap-gateway">
  //           <h4>Chọn cổng thanh toán</h4>
  //           <Form.Item name="itemGatewayATM" initialValue={"OnePay"}>
  //             <Radio.Group className="gateway-info">
  //               <Radio value={"OnePay"}>
  //                 <div className="info">
  //                   <img alt=''
  //                     src={public_url.img + "/wallet/logo_onepay.png"} />
  //                   <p>OnePAY</p>
  //                 </div>
  //               </Radio>
  //             </Radio.Group>
  //           </Form.Item>
  //         </div>
  //         {yieldCardUsed}
  //         <div className="wrap-select-amount">
  //           <h4>Chọn số tiền</h4>
  //           <div className="wrap-amount-item">
  //             {listMoneyDefault}
  //           </div>
  //         </div>
  //         <div className="wrap-input-amount">
  //           <h4>Mệnh giá khác</h4>
  //           <Form.Item name="itemOtherAmount"
  //             rules={[{ validateTrigger: 'onBlur', validator: checkValidOtherAmount }]}>
  //             <InputNumber onChange={onInputOtherAmount}
  //               formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
  //               parser={value => value.replace(/\$\s?|(,*)/g, '')} />
  //           </Form.Item>
  //         </div>
  //       </div>
  //     );
  //   }
  //   return null;
  // }

  // function yieldMethodVisaPanel() {
  //   if (method === "visa") {
  //     let detailCardVisa;
  //     let yieldCardUsed;
  //     if (listCardVisa.length > 0) {
  //       detailCardVisa = _.map(listCardVisa, (item, index) => {
  //         const bankName = func.yieldCardUsed(item).bankName;
  //         const logo = func.yieldCardUsed(item).logo;
  //         const cardNumber = item.cardNumber;
  //         return (
  //           <Radio key={index} value={item}>
  //             <div className="info">
  //               <img alt=''
  //                 src={public_url.img + logo} />
  //               <div className="text">
  //                 <p>{bankName}</p>
  //                 <p>{cardNumber}</p>
  //               </div>
  //             </div>
  //           </Radio>
  //         );
  //       });
  //       yieldCardUsed = (
  //         <div className="wrap-card">
  //           <h4>Thẻ bạn đã dùng</h4>
  //           <Form.Item name="itemCardVisaInfo" initialValue={"new"}>
  //             <Radio.Group className="card-info">
  //               <Radio value={"new"}>
  //                 <div className="info">
  //                   <img alt=''
  //                     src={public_url.img + "/wallet/logo_atm.png"} />
  //                   <p>Sử dụng thẻ mới</p>
  //                 </div>
  //               </Radio>
  //               {detailCardVisa}
  //             </Radio.Group>
  //           </Form.Item>
  //         </div>
  //       );
  //     }
  //     const listMoneyDefault = _.map(dataMoney, (item, index) => {
  //       return (
  //         <AmountDefault key={index} onClick={onPressAmountDefault}
  //           value={item.value}
  //           selected={amount ? amount.value : undefined}
  //           label={item.text} />
  //       );
  //     });

  //     return (
  //       <div className="wrap-form-method-visa">
  //         <div className="wrap-gateway">
  //           <h4>Chọn cổng thanh toán</h4>
  //           <Form.Item name="itemGatewayVisa" initialValue={"OnePay"}>
  //             <Radio.Group className="gateway-info">
  //               <Radio value={"OnePay"}>
  //                 <div className="info">
  //                   <img alt=''
  //                     src={public_url.img + "/wallet/logo_onepay.png"} />
  //                   <p>OnePAY</p>
  //                 </div>
  //               </Radio>
  //             </Radio.Group>
  //           </Form.Item>
  //         </div>
  //         {yieldCardUsed}
  //         <div className="wrap-select-amount">
  //           <h4>Chọn số tiền</h4>
  //           <div className="wrap-amount-item">
  //             {listMoneyDefault}
  //           </div>
  //         </div>
  //         <div className="wrap-input-amount">
  //           <h4>Mệnh giá khác</h4>
  //           <Form.Item name="itemOtherAmount"
  //             rules={[{ validateTrigger: 'onBlur', validator: checkValidOtherAmount }]}>
  //             <InputNumber onChange={onInputOtherAmount}
  //               formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
  //               parser={value => value.replace(/\$\s?|(,*)/g, '')} />
  //           </Form.Item>
  //         </div>
  //       </div>
  //     );
  //   }
  //   return null;
  // }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Số dư DR.OH</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-wallet">
                <div className="wrap-balance">
                  <div className="wrap-panel-item">
                    <div className="wrap-item-img">
                      <img
                        className="img-fluid"
                        src={public_url.img + "/icons/ic_wallet.png"}
                        alt=""
                      />
                    </div>
                    <div className="wrap-item-info">
                      <p className="label">Số dư DR.OH</p>
                      <p className="value">
                        {balance && balance.vnd
                          ? balance.vnd.toLocaleString()
                          : 0}{" "}
                        đ
                      </p>
                    </div>
                  </div>
                </div>
                {/* <Form onFinish={onFinish}
                  form={form}
                  size="large"
                  className="form-payment"
                  name="form-payment">
                  <div className="wrap-method">
                    <Form.Item name="itemMethod"
                      initialValue={method}>
                      <Radio.Group className="wrap-method-item" onChange={onChangeMethod}>
                        <Radio value={"card-oh"}>
                          <div className="method-card">
                            <div className="icon">
                              <img src={public_url.img + "/wallet/ic_oh.png"} alt="" />
                            </div>
                            <div className="label">
                              <p>Thẻ OH</p>
                            </div>
                          </div>
                        </Radio>
                        {" "}
                        <Radio value={"atm"}>
                          <div className="method-atm">
                            <div className="icon">
                              <img src={public_url.img + "/wallet/ic_atm.png"} alt="" />
                            </div>
                            <div className="label">
                              <p>Thẻ ATM</p>
                            </div>
                          </div>
                        </Radio>
                        {" "}
                        <Radio value={"visa"}>
                          <div className="method-visa">
                            <div className="icon">
                              <img src={public_url.img + "/wallet/ic_visa.png"} alt="" />
                            </div>
                            <div className="label">
                              <p>Thẻ quốc tế</p>
                            </div>
                          </div>
                        </Radio>
                      </Radio.Group>
                    </Form.Item>
                  </div>
                  {" "}
                  {yieldMethodCardPanel()}
                  {" "}
                  {yieldMethodATMPanel()}
                  {" "}
                  {yieldMethodVisaPanel()}
                  {" "}
                  <Form.Item className="wrap-form-action">
                    <hr />
                    <div className="box-button-event">
                      <Button loading={loading} className="btn-accept" htmlType="submit">
                        <span>Tiếp tục</span> <AiOutlineRight />
                      </Button>
                    </div>
                  </Form.Item>
                </Form> */}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _Confirmation }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    WalletRechargePage
  )
);
