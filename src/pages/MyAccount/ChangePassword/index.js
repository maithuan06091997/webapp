import React, {Fragment, useState} from "react";
import i18next from "i18next";
import {Button, Form, Input, Modal} from "antd";
import md5 from "md5";
import {ModelChangePass} from "../../../models/ModelUser";
import {_ChangePassword} from "../../../actions";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./style.scss";

const ChangePasswordPage = (props) => {
    const [loading, setLoading] = useState(false);
    //*** Declare props ***//
    const {_TokenExpired, _ChangePassword, user} = props;

    const [modal, contextHolder] = Modal.useModal();

    const onFinish = values => {
        const userID = user.personal._id;
        const reqParamFt = {
            old_password: md5(values.itemOldPass),
            new_password: md5(values.itemConfirm)
        };
        setLoading(true);
        ModelChangePass(userID, reqParamFt).then(() => {
            _ChangePassword(true);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Đổi mật khẩu</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-change-password">
                                <Form onFinish={onFinish}
                                      size="large"
                                      className="form-change-pass"
                                      name="form-change-pass">
                                    <Form.Item label="Nhập mật khẩu cũ"
                                               name="itemOldPass"
                                               rules={[{required: true, message: "Vui lòng nhập mật khẩu cũ"},
                                                   {validateTrigger: 'onBlur', min: 6, message: "Vui lòng nhập trên 5 ký tự"}]}>
                                        <Input.Password placeholder="Nhập mật khẩu cũ"/>
                                    </Form.Item>
                                    {" "}
                                    <Form.Item label="Nhập mật khẩu mới"
                                               name="itemNewPass"
                                               rules={[{required: true, message: "Vui lòng nhập mật khẩu mới"},
                                                   {validateTrigger: 'onBlur', min: 6, message: "Vui lòng nhập trên 5 ký tự"}]}>
                                        <Input.Password placeholder="Nhập mật khẩu mới"/>
                                    </Form.Item>
                                    {" "}
                                    <Form.Item label="Xác nhận mật khẩu"
                                               name="itemConfirm"
                                               dependencies={["itemNewPass"]}
                                               rules={[{required: true, message: "Vui lòng xác nhận mật khẩu"},
                                                   ({getFieldValue}) => ({
                                                       validateTrigger: 'onBlur',
                                                       validator(rule, value) {
                                                           if (!value || getFieldValue("itemNewPass") === value) {
                                                               return Promise.resolve();
                                                           }
                                                           return Promise.reject("Hai mật khẩu bạn nhập không khớp");
                                                       }
                                                   })
                                               ]}>
                                        <Input.Password placeholder="Xác nhận mật khẩu"/>
                                    </Form.Item>
                                    {" "}
                                    <Form.Item className="wrap-form-action">
                                        <hr/>
                                        <div className="box-button-event">
                                            <Button loading={loading} className="btn-accept" htmlType="submit">
                                                <span>Đổi mật khẩu</span>
                                            </Button>
                                        </div>
                                    </Form.Item>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_ChangePassword}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(ChangePasswordPage));
