import React, {Fragment, useEffect, useRef, useState} from "react";
import _ from "lodash";
import {Empty} from "antd";
import {url} from "../../../../constants/Path";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import {ModelListExaminationSchedule} from "../../../../models/ModelExamination";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const AppointmentSchedulePage = (props) => {
    const isRendered = useRef(false);
    const [skeleton, setSkeleton] = useState(false);
    const [listAppointmentSchedule, setListAppointmentSchedule] = useState([]);
    //*** Declare props ***//
    const {history} = props;

    //*** Handle yield list appointment schedule ***//
    useEffect(() => {
        setSkeleton(true);
        ModelListExaminationSchedule().then(resultFt => {
            if (!isRendered.current) {
                const dataListAppointmentSchedule = resultFt.data.one_health_msg;
                setListAppointmentSchedule(dataListAppointmentSchedule);
                setSkeleton(false);
            }
            return null;
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("AppointmentSchedulePage - ListExaminationSchedule");
                    setSkeleton(false);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, []);

    const onDetailAppointmentSchedule = item => {
        history.push({
            pathname: url.name.account_detail_appointment_schedule,
            state: item
        });
    };

    function yieldListAppointmentSchedule() {
        if (listAppointmentSchedule.length > 0) {
            return _.map(listAppointmentSchedule, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div onClick={onDetailAppointmentSchedule.bind(this, item)} className="wrap-panel-item">
                            <div className="wrap-item-img">
                                <img className="img-fluid" src={item.url} alt=""/>
                            </div>
                            <div className="wrap-item-info">
                                <h4>{item.service_name}</h4>
                                <h4 className="sub-name">{func.yieldScheduleInfo(item)["subName"]}</h4>
                                <p className="datetime">{func.yieldScheduleInfo(item)["datetime"]}</p>
                                <p className="address">{func.yieldScheduleInfo(item)["address"]}</p>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Lịch hẹn trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Lịch hẹn</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-list-appointment-schedule">
                                <div className="wrap-panel-content">
                                    <div className="row">
                                        <BoxSkeleton skeleton={skeleton}
                                                     full={false}
                                                     length={10}
                                                     rows={2}
                                                     data={yieldListAppointmentSchedule()}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(AppointmentSchedulePage));
