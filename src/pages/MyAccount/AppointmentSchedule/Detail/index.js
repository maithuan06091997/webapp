import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import { url } from "../../../../constants/Path";
import { Empty } from "antd";
import Loading from "../../../../components/Loading";
import ServiceExaminationContent from "./ServiceExaminationContent";
import ServiceVideoCallContent from "./ServiceVideoCallContent";
import { ModelDetailExaminationSchedule } from "../../../../models/ModelExamination";
import { ModelDetailVideoCallSchedule } from "../../../../models/ModelCallChat";
import { Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { getDetailsTakeBlood } from "../../../../services/HealthService";

const DetailAppointmentSchedulePage = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [detailAppointmentSchedule, setDetailAppointmentSchedule] =
    useState(undefined);
  //*** Declare props ***//
  const { location } = props;
  const handleDetailAppointmentSchedule = useCallback(async () => {
    setLoading(true);
    if (location.state) {
      const id = location.state._id;
      if (location.state.type === "appt_hospital") {
        ModelDetailExaminationSchedule(id)
          .then((resultFt) => {
            if (!isRendered.current) {
              const dataDetailAppointmentSchedule =
                resultFt.data.one_health_msg;
              setDetailAppointmentSchedule(dataDetailAppointmentSchedule);
              setLoading(false);
            }
            return null;
          })
          .catch((error) => {
            if (error.code !== "007") {
              console.log(
                "DetailAppointmentSchedulePage - DetailExaminationSchedule"
              );
              setLoading(false);
            }
          });
      }
      if (location.state.type === "take_blood") {
        const detailAppointmentSchedule = await getDetailsTakeBlood(id, {
          checkinvoice: "true",
        });
        setDetailAppointmentSchedule(
          detailAppointmentSchedule.one_health_msg[0]
        );
        setLoading(false);
      }
      if (location.state.type === "call_video_schedule") {
        ModelDetailVideoCallSchedule(id)
          .then((resultFt) => {
            if (!isRendered.current) {
              const dataDetailVideoCallSchedule = resultFt.data.one_health_msg;
              setDetailAppointmentSchedule(dataDetailVideoCallSchedule);
              setLoading(false);
            }
            return null;
          })
          .catch((error) => {
            if (error.code !== "007") {
              console.log(
                "DetailAppointmentSchedulePage - DetailVideoCallSchedule"
              );
              setLoading(false);
            }
          });
      }
    }
    return () => {
      isRendered.current = true;
    };
  }, [location]);

  //*** Handle yield detail appointment schedule ***//
  useEffect(() => {
    if (!isRendered.current) {
      handleDetailAppointmentSchedule();
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleDetailAppointmentSchedule]);

  const handleUpdateAppointmentSchedule = () => {
    handleDetailAppointmentSchedule();
  };

  if (!location.state) {
    return <Redirect to={url.name.account_list_appointment_schedule} />;
  }

  let yieldContent;
  if (location.state.type === "appt_hospital") {
    yieldContent = (
      <ServiceExaminationContent
        {...props}
        updateSchedule={handleUpdateAppointmentSchedule}
        detailSchedule={detailAppointmentSchedule}
      />
    );
  }
  if (location.state.type === "call_video_schedule") {
    yieldContent = (
      <ServiceVideoCallContent
        {...props}
        updateSchedule={handleUpdateAppointmentSchedule}
        detailSchedule={detailAppointmentSchedule}
      />
    );
  }
  if (location.state.type === "take_blood") {
    yieldContent = (
      <ServiceVideoCallContent
        {...props}
        updateSchedule={handleUpdateAppointmentSchedule}
        detailSchedule={detailAppointmentSchedule}
      />
    );
  }

  if (!loading && !detailAppointmentSchedule) {
    yieldContent = (
      <Empty
        className="ant-empty-custom"
        description="Chi tiết lịch hẹn trống"
      />
    );
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Chi tiết lịch hẹn</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-detail-appointment-schedule">
                {yieldContent}
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(DetailAppointmentSchedulePage)
);
