import React, { Fragment, useState } from "react";
import i18next from "i18next";
import moment from "moment";
import { Modal } from "antd";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import { msg_text } from "../../../../constants/Message";
import Loading from "../../../../components/Loading";
import { ModelCancelVideoCallSchedule } from "../../../../models/ModelCallChat";
import * as func from "../../../../helpers";
import "./style.scss";

const ServiceVideoCallContent = (props) => {
  const [loading, setLoading] = useState(false);
  //*** Declare props ***//
  const {
    _TokenExpired,
    _BalanceUser,
    _BadgeNotification,
    updateSchedule,
    detailSchedule,
  } = props;

  const [modal, contextHolder] = Modal.useModal();

  const onAcceptAbortAppointment = () => {
    const id = detailSchedule._id;
    setLoading(true);
    ModelCancelVideoCallSchedule(id)
      .then(() => {
        modal.success({
          title: "Thành công",
          okText: "Đóng",
          centered: true,
          content: msg_text.cancel_video_call_success,
        });
        _BalanceUser();
        _BadgeNotification();
        updateSchedule();
        setLoading(false);
      })
      .catch((error) => {
        if (error.code === "007") {
          _TokenExpired(error);
        } else {
          const msg = error.message
            ? error.message
            : error.description
            ? error.description
            : msg_text.cancel_video_call_error;
          modal.warning({
            title: "Thông báo",
            okText: "Đóng",
            centered: true,
            content: i18next.t(msg),
          });
          setLoading(false);
        }
      });
  };

  const onConfirmAbortAppointment = () => {
    modal.confirm({
      title: "Thông báo",
      okText: "Đồng ý",
      cancelText: "Huỷ",
      onOk: onAcceptAbortAppointment,
      centered: true,
      content: "Bạn có chắc chắn muốn hủy lịch gọi video?",
    });
  };

  function yieldDetailAppointmentSchedule() {
    console.log(detailSchedule);
    if (detailSchedule) {
      const status = detailSchedule.status;
      const profile_name =
        detailSchedule.profile?.last_name +
        " " +
        detailSchedule.profile?.first_name;
      const profile_age = detailSchedule.profile?.birthday;
      const profile_sex = detailSchedule.profile?.sex;
      const profile_location = func.yieldLocation(
        detailSchedule.profile?.location
      );
      const schedule_created = func.yieldDatetimeSchedule(
        detailSchedule.created_time
      );
      const doctor_name =
        detailSchedule.doctor?.last_name +
        " " +
        detailSchedule.doctor?.first_name;
      const appt_date = moment(detailSchedule.start_time).format("DD/MM/YYYY");
      const appt_hour_start = moment(detailSchedule.start_time).format("HH:mm");
      const appt_hour_end = moment(detailSchedule.end_time).format("HH:mm");
      let yieldAbortAppointment;
      if (status === "new") {
        yieldAbortAppointment = (
          <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
            <div className="wrap-panel-item">
              <div className="wrap-event-action">
                <div className="box-button-event">
                  <div
                    onClick={onConfirmAbortAppointment}
                    className="ant-btn btn-danger"
                  >
                    <span>Hủy lịch hẹn</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
      return (
        <Fragment>
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  <h4>
                    <FaRegUserCircle /> Thông tin bệnh nhân
                  </h4>
                </div>
                <div className="wrap-main">
                  <div className="wrap-line drop-line">
                    <p className="label">Họ và tên</p>
                    <p className="value">{profile_name}</p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Tuổi</p>
                    <p className="value">{func.yieldAge(profile_age)}</p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Giới tính</p>
                    <p className="value">
                      {func.findGender(profile_sex, "text")}
                    </p>
                  </div>
                  <div className="wrap-line drop-line">
                    <p className="label">Địa chỉ</p>
                    <p className="value">{profile_location}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  <h4>
                    <FaRegListAlt /> Thông tin lịch khám
                  </h4>
                </div>
                <div className="wrap-main">
                  <div className="wrap-line">
                    <p className="label">Ngày đặt lịch</p>
                    <p className="value">{schedule_created}</p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Bác sĩ</p>
                    <p className="value">{doctor_name}</p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Ngày hẹn</p>
                    <p className="value datetime">{appt_date}</p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Giờ hẹn</p>
                    <p className="value datetime">
                      {appt_hour_start + " - " + appt_hour_end}
                    </p>
                  </div>
                  <div className="wrap-line">
                    <p className="label">Trạng thái</p>
                    <p className="value">
                      <span
                        className={
                          func.findStatusVideoCall(status, "color") + " badge"
                        }
                      >
                        {func.findStatusVideoCall(status, "text")}
                      </span>
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {yieldAbortAppointment}
          </div>
        </Fragment>
      );
    }
    return null;
  }

  return (
    <Fragment>
      <div className="wrap-panel-content">
        {yieldDetailAppointmentSchedule()}
      </div>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

export default ServiceVideoCallContent;
