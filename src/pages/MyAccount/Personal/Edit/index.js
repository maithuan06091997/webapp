import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import i18next from "i18next";
import {Button, Form, Input, Modal} from "antd";
import {msg_text} from "../../../../constants/Message";
import {public_url} from "../../../../constants/Path";
import {RiCameraFill} from "react-icons/ri";
import BoxSelect from "../../../../components/BoxSelect";
import BoxInputBirthday from "../../../../components/BoxInputBirthday";
import BoxInputFullName from "../../../../components/BoxInputFullName";
import Loading from "../../../../components/Loading";
import {dataGender} from "../../../../constants/Data";
import {ModelListDistrict, ModelListWard} from "../../../../models/ModelCommon";
import {ModelUpdateUserInfo, ModelUpdateAvatar} from "../../../../models/ModelUser";
import {_ListCity, _UpdateUser, _UpdateAvatarUser} from "../../../../actions";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const UpdatePersonalPage = (props) => {
    const refInputFile = React.createRef();
    const isRendered = useRef(false);
    const [loading, setLoading] = useState(false);
    const [fields, setFields] = useState([]);
    const [listDistrict, setListDistrict] = useState([]);
    const [listWard, setListWard] = useState([]);
    const [avatar, setAvatar]   = useState(public_url.img + "/icons/icon_avatar.png");
    //*** Declare props ***//
    const {
        _TokenExpired, _ListCity, _UpdateUser,
        _UpdateAvatarUser, global, user
    } = props;
    const detail = user.personal;

    const [modal, contextHolder] = Modal.useModal();

    const handleListDistrict = useCallback((id) => {
        if (id) {
            const reqParamFt = {
                data: {
                    province: {
                        id: id
                    }
                }
            };
            ModelListDistrict(reqParamFt).then(resultFt => {
                if (!isRendered.current) {
                    const dataListDistrict = resultFt.data.one_health_msg;
                    setListDistrict(dataListDistrict);
                }
                return null;
            }).catch(error => {
                if (error.code !== "007") {
                    console.log("UpdatePersonalPage - ListDistrict");
                }
            });
        }
        return () => {
            isRendered.current = true;
        };
    }, []);

    const handleListWard = useCallback((id) => {
        if (id) {
            const reqParamFt = {
                data: {
                    district: {
                        id: id
                    }
                }
            };
            ModelListWard(reqParamFt).then(resultFt => {
                if (!isRendered.current) {
                    const dataListWard = resultFt.data.one_health_msg;
                    setListWard(dataListWard);
                }
                return null;
            }).catch(error => {
                if (error.code !== "007") {
                    console.log("UpdatePersonalPage - ListWard");
                }
            });
        }
        return () => {
            isRendered.current = true;
        };
    }, []);

    useEffect(() => {
        _ListCity();
    }, [_ListCity]);

    useEffect(() => {
        const fullName = detail.last_name + " " + detail.first_name;
        const birthday = detail.birthday ? func.formatDatetimeToString(detail.birthday) : "";
        const email = detail.email ? detail.email : "";
        const address = detail.address ? detail.address : "";

        const gender_value = func.findGender(detail.sex, "value");
        const gender_name = func.findGender(detail.sex, "text");
        const gender = gender_value && gender_name ? {value: gender_value, label: gender_name} : "";

        const city_id = global.city && detail.province ? detail.province.province_id : "";
        const city_name = global.city && detail.province ? detail.province.vi_name : "";
        const city = city_id && city_name ? {value: city_id, label: city_name} : "";

        const district_id = global.city && detail.district ? detail.district.district_id : "";
        const district_name = global.city && detail.district ? detail.district.vi_name : "";
        const district = district_id && district_name ? {value: district_id, label: district_name} : "";

        const ward_id = global.city && detail.ward ? detail.ward.ward_id : "";
        const ward_name = global.city && detail.ward ? detail.ward.vi_name : "";
        const ward = ward_id && ward_name ? {value: ward_id, label: ward_name} : "";

        setFields([
            {name: ['itemFullName'], value: fullName.trim() ? fullName : ""},
            {name: ['itemGender'], value: gender},
            {name: ['itemEmail'], value: email},
            {name: ['itemBirthday'], value: birthday},
            {name: ['itemAddress'], value: address},
            {name: ['itemCity'], value: city},
            {name: ['itemDistrict'], value: district},
            {name: ['itemWard'], value: ward}
        ]);

        handleListDistrict(city_id);
        handleListWard(district_id);
    }, [global.city, detail, handleListDistrict, handleListWard]);

    useEffect(() => {
        if (user.avatar && user.avatar.url) {
            setAvatar(user.avatar.url);
        }
    }, [user.avatar]);

    const onFinish = values => {
        const userID = detail._id;
        const reqParamFt = {
            user: {
                name: values.itemFullName ? values.itemFullName.trim() : "",
                sex: func.findGender(values.itemGender.value, "id"),
                email: values.itemEmail ? values.itemEmail : "",
                birthday: func.formatISODatetime(values.itemBirthday),
                province: values.itemCity.value,
                district: values.itemDistrict.value,
                ward: values.itemWard.value,
                address: values.itemAddress ? values.itemAddress.trim() : ""
            }
        };
        setLoading(true);
        ModelUpdateUserInfo(userID, reqParamFt).then(resultFt => {
            modal.success({
                title: "Thành công",
                okText: "Đóng",
                centered: true,
                content: msg_text.update_user_success
            });
            const dataUpdateUser = resultFt.data.one_health_msg;
            _UpdateUser(dataUpdateUser);
            setLoading(false);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description ? error.description : msg_text.update_user_error;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    const onInputBirthday = e => {
        const value = func.yieldValueDate(e.target.value);
        setFields([{name: ['itemBirthday'], value: value}]);
    };

    const onSelectCity = values => {
        setFields([
            {name: ['itemDistrict'], value: ""},
            {name: ['itemWard'], value: ""}
        ]);
        setListWard([]);
        handleListDistrict(values.value);
    };

    const onSelectDistrict = values => {
        setFields([{name: ['itemWard'], value: ""}]);
        handleListWard(values.value);
    };

    const onPressUpload = () => {
        refInputFile.current.click();
    };

    const onUploadAvatar = () => {
        if (refInputFile.current.files[0]) {
            const file = refInputFile.current.files[0];
            const exts = file.name.split(".");
            const validExts = ["png", "PNG", "jpg", "JPG", "jpeg", "JPEG"];
            if (validExts.indexOf(exts[1]) > -1) {
                let reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = () => {
                    const reqParamFt = {
                        image: {
                            type: 1,
                            data: reader.result,
                            user: {
                                id: detail._id
                            }
                        }
                    };
                    ModelUpdateAvatar(reqParamFt).then(resultFt => {
                        const dataUpdateUser = resultFt.data.one_health_msg;
                        setAvatar(dataUpdateUser.url);
                        _UpdateAvatarUser(dataUpdateUser);
                    }).catch(error => {
                        if (error.code === "007") {
                            _TokenExpired(error);
                        } else {
                            const msg = error.message ? error.message : error.description ? error.description : msg_text.update_user_error;
                            modal.warning({
                                title: "Thông báo",
                                okText: "Đóng",
                                centered: true,
                                content: i18next.t(msg)
                            });
                        }
                    });
                };
            } else {
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: msg_text.file_wrong
                });
            }
        }
    };

    let yieldFullName;
    if (detail && detail.first_name) {
        yieldFullName = detail.last_name+" "+detail.first_name;
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Cập nhật hồ sơ cá nhân</h3>
                        </div>
                        <div className="card-body scrollbar-1">
                            <div className="wrap-content block-edit-personal">
                                <Form onFinish={onFinish}
                                      fields={fields}
                                      size="large"
                                      className="form-edit-user"
                                      name="form-edit-user">
                                    <div onClick={onPressUpload} className="avatar">
                                        <div className="avatar-left">
                                            <img className="img-fluid" src={avatar} alt=""/>
                                        </div>
                                        <div className="avatar-right">
                                            <p className="label-name">{yieldFullName ? yieldFullName : "-"}</p>
                                            <hr/>
                                            <div className="upload-avatar">
                                                <p>Thay đổi ảnh</p>
                                                <RiCameraFill size={`2em`}/>
                                            </div>
                                        </div>
                                    </div>
                                    <input ref={refInputFile}
                                           type="file"
                                           onChange={onUploadAvatar}
                                           accept=".png, .PNG, .jpg, .JPG, .jpeg, .JPEG"
                                           className="d-none"/>
                                    {" "}
                                    <Input.Group compact>
                                        <BoxInputFullName label="Họ và tên"
                                                          required={true}
                                                          name="itemFullName"
                                                          placeholder="Nhập họ và tên"/>
                                        {" "}
                                        <Form.Item label="Giới tính"
                                                   name="itemGender"
                                                   rules={[{required: true, message: "Vui lòng chọn giới tính"}]}>
                                            <BoxSelect  placeholder="Chọn giới tính"
                                                        data={dataGender(this)}/>
                                        </Form.Item>
                                    </Input.Group>
                                    {" "}
                                    <Input.Group compact>
                                        <Form.Item label="Email"
                                                   name="itemEmail"
                                                   rules={[{validateTrigger: 'onBlur', type: 'email', message: "Email không hợp lệ"}]}>
                                            <Input/>
                                        </Form.Item>
                                        {" "}
                                        <BoxInputBirthday label="Ngày sinh (dd/mm/yyyy)"
                                                          name="itemBirthday"
                                                          onChange={onInputBirthday}
                                                          placeholder="__ / __ / ____"/>
                                    </Input.Group>
                                    {" "}
                                    <Input.Group compact>
                                        <Form.Item label="Số nhà, đường, thôn, ấp,..."
                                                   name="itemAddress"
                                                   rules={[{required: true, message: "Vui lòng nhập số nhà"},
                                                       {whitespace: true, message: "Vui lòng nhập số nhà"}]}>
                                            <Input placeholder="Nhập số nhà, đường, thôn, ấp,..."/>
                                        </Form.Item>
                                        {" "}
                                        <Form.Item label="Tỉnh/Thành"
                                                   name="itemCity"
                                                   rules={[{required: true, message: "Vui lòng chọn tỉnh/thành"}]}>
                                            <BoxSelect  placeholder="Chọn tỉnh/thành"
                                                        onChange={onSelectCity}
                                                        data={global.city}/>
                                        </Form.Item>
                                    </Input.Group>
                                    {" "}
                                    <Input.Group compact>
                                        <Form.Item label="Quận/Huyện"
                                                   name="itemDistrict"
                                                   rules={[{required: true, message: "Vui lòng chọn quận/huyện"}]}>
                                            <BoxSelect  placeholder="Chọn quận/huyện"
                                                        onChange={onSelectDistrict}
                                                        data={listDistrict}/>
                                        </Form.Item>
                                        {" "}
                                        <Form.Item label="Phường/Xã"
                                                   name="itemWard"
                                                   rules={[{required: true, message: "Vui lòng chọn phường/xã"}]}>
                                            <BoxSelect  placeholder="Chọn phường/xã"
                                                        data={listWard}/>
                                        </Form.Item>
                                    </Input.Group>
                                    {" "}
                                    <Form.Item className="wrap-form-action">
                                        <hr/>
                                        <div className="box-button-event">
                                            <Button loading={loading} className="btn-accept" htmlType="submit">
                                                <span>Cập nhật hồ sơ</span>
                                            </Button>
                                        </div>
                                    </Form.Item>
                                </Form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_ListCity, _UpdateUser, _UpdateAvatarUser}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(UpdatePersonalPage));
