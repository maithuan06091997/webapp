import React, {Fragment, useEffect, useState} from "react";
import _ from "lodash";
import {url} from "../../../../constants/Path";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const PersonalPage = (props) => {
    //*** Declare props ***//
    const {user} = props;

    const [items, setItems] = useState([
        {label: 'Họ và tên', value: ""},
        {label: 'Giới tính', value: ""},
        {label: 'Ngày sinh', value: ""},
        {label: 'Email', value: ""},
        {label: 'Địa chỉ', value: ""}
    ]);

    useEffect(() => {
        const detailUser = user.personal;
        const fullName = detailUser.last_name + " " + detailUser.first_name;
        const gender = detailUser.sex ? func.findGender(detailUser.sex, "text") : "";
        const birthday = detailUser.birthday ? func.formatDatetimeToString(detailUser.birthday) : "";
        const email = detailUser.email ? detailUser.email : "";
        const address = detailUser.address ? detailUser.address : "";
        const city = detailUser.province ? detailUser.province.vi_name : "";
        const district = detailUser.district ? detailUser.district.vi_name : "";
        const ward = detailUser.ward ? detailUser.ward.vi_name : "";
        let fullAddress = "";
        if (address && city && district) {
            fullAddress = address + ", " + ward + ", " + district + ", " + city;
        }
        setItems([
            {label: 'Họ và tên', value: fullName.trim() ? fullName : ""},
            {label: 'Giới tính', value: gender},
            {label: 'Ngày sinh', value: birthday},
            {label: 'Email', value: email},
            {label: 'Địa chỉ', value: fullAddress},
        ]);
    }, [user]);

    function yieldDetailPersonal() {
        return _.map(items, (item, index) => {
            return (
                <div key={index} className="wrap-line">
                    <p className="label">{item.label}</p>
                    <p className="value">{item.value ? item.value : "-"}</p>
                </div>
            );
        });
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Hồ sơ cá nhân</h3>
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <Link to={url.name.account_edit_personal}
                                          className="ant-btn btn-accept"><span>Cập nhật hồ sơ</span></Link>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-detail-personal">
                                <div className="wrap-panel-content">
                                    <div className="wrap-panel-item">
                                        <div className="wrap-main">
                                            {yieldDetailPersonal()}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(PersonalPage));
