import React, { Fragment, useEffect, useState } from "react";
import { Modal, Empty, List, Button } from "antd";
import {public_url, url} from "../../../constants/Path";
import i18next from "i18next";
import BoxSkeleton from "../../../components/BoxSkeleton";
import Loading from "../../../components/Loading";
import ModalPointHistory from "../../../components/Promotion/ModalPointHistory";
import { ModelGetListNotification, ModelPutReadAll, ModelPutClicked } from "../../../models/ModelNotification";
import {withRouter} from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../helpers";
import "./style.scss";

let isShowBtnLoadMore = false;
let itemSelected = null;

const NotificationPage = (props) => {

    const textColor1 = '#161616';       // black
    const textColor2 = '#777777';       // grey

    const [modal, contextHolder] = Modal.useModal();

    const [skeleton, setSkeleton] = useState(true);
    const [page, setPage] = useState(1);
    const [listData, setListData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [showDetail, setShowDetail] = useState(false);
    const [openModalPointHistory, setOpenModalPointHistory] = useState(false);
    const [reload, setReload] = useState(1);

    //*** Declare props ***//
    const {_BadgeNotification, history} = props;

    useEffect(() => {
        const reqParams = {
            page: page
        };
        ModelGetListNotification(reqParams).then(resultFt => {
            _BadgeNotification();
            setSkeleton(false);
            setLoading(false);
            const dataFt = resultFt.data.one_health_msg ? resultFt.data.one_health_msg : [];
            let allData = [];
            if (page > 1) {
                allData = listData.concat(dataFt);
            } else {
                allData = dataFt;
            }

            isShowBtnLoadMore = dataFt.length >= 20;
            setListData(allData);
        }).catch((error) => {
            setSkeleton(false);
            setLoading(false);
            modal.error({
                title: "Thông báo",
                okText: "Đóng",
                content: i18next.t(error.message)
            });
        });
    }, [_BadgeNotification, page, reload, modal]);

    const onPressReadAll = () => {
        setLoading(true);

        const reqParams = null;
        ModelPutReadAll(reqParams).then(resultFt => {
            console.log('ModelPutReadAll - SUCCESS:' + JSON.stringify(resultFt));
            setLoading(false);
            setPage(1);
            setReload(reload + 1);
        }).catch((error) => {
            console.log('ModelPutReadAll - FAIL');
            setLoading(false);
            modal.error({
                title: "Thông báo",
                okText: "Đóng",
                content: i18next.t(error.message)
            });
        });
    }

    const onPressDetail = (item) => {
        if (item.clicked) {
            gotoDetail(item);
        } else {
            setLoading(true);

            const reqParams = {
                notification: item._id
            };
            ModelPutClicked(reqParams).then(resultFt => {
                setLoading(false);

                let temp = [...listData];
                temp.forEach(element => {
                    if (element._id === item._id) {
                        element.clicked = true;
                    }
                });
                setListData(temp);

                gotoDetail(item);
            }).catch((error) => {
                console.log('ModelPutClicked - FAIL');
                setLoading(false);
                gotoDetail(item);
            });
        }
    }

    const getIcon = (item) => {
        let icon = null;
        switch (item.type + '') {
            case "1":
                icon = public_url.img + "/notification/ic_info.png";
                break;
            case "705":
                icon = public_url.img + "/notification/icon_lixi.png";
                break;
            case "2":
            case "1006": // book video schedule
                icon = public_url.img + "/myhealth/ic_video_call.png";
                break;
            case "3":
            case "29":
                icon = public_url.img + "/myhealth/ic_chat.png";
                break;
            case "5":
            case "13":
            case "37": // Nhà thuốc Hủy giao thuốc
                icon = public_url.img + "/myhealth/ic_drug.png";
                break;
            case "6":
            case "7":
            case "17":
            case "21":
            case "27":
            case "1003": // collect payment
                icon = public_url.img + "/notification/ic_cashin.png";
                break;
            case "1001": // collect payment
            case "1002":
                if (item.title && item.title.includes('Hoàn tiền')) {
                    icon = public_url.img + "/notification/ic_refund.png";
                } else {
                    icon = public_url.img + "/notification/ic_cashout.png";
                }
                break;
            case "8":
            case "14":
            case "16": // KQ CLS
                icon = public_url.img + "/myhealth/ic_result.png";
                break;
            case "11":
                icon = public_url.img + "/myhealth/ic_question.png";
                break;
            case "20":
                icon = public_url.img + "/notification/ic_recall.png";
                break;
            case "23":
            case "24":
            case "25":
            case "26":
            case "400": // TAKE BLOOD
                icon = public_url.img + "/myhealth/ic_appt_home.png";
                break;
            case "30":
            case "31":
            case "32":
            case "33":
            case "39":
                icon = public_url.img + "/myhealth/ic_service.png";
                break;
            case "36":
                icon = public_url.img + "/notification/forward.png";
                break;
            case "38":
                icon = public_url.img + "/myhealth/ic_appt_hos.png";
                break;

            //SOCIAL NETWORK

            case "100": //Post
            case "101": //Like
            case "102": //Comment
            case "103": //Follow
            case "104":// mention(tag) user
                if (item.extra && item.extra.urlAvatar) {
                    let uriAvatar = func.getSocialAvatar(item.extra.urlAvatar);
                    if (uriAvatar && uriAvatar !== '') {
                        icon = uriAvatar;
                    } else {
                        icon = public_url.img + "/icons/icon_avatar.png";
                    }
                } else {
                    icon = public_url.img + "/icons/icon_avatar.png";
                }
                break;
            case "110": // birthday
                icon = public_url.img + "/notification/ic_birthday.png";
                break;
            case "200": // Apply Promotion
                icon = public_url.img + "/notification/ic_promo.png";
                break;
            case "201": // Point History
                icon = public_url.img + "/notification/ic_gift.png";
                break;

            // Health Tracking
            case "300": // BMI PT
            case "301": // BMI COMMENT
                icon = public_url.img + "/notification/ic_bmi.png";
                break;

            case "700": // DRUG
            case "701": // DRUG
            case "702": // DRUG
            case "703": // DRUG
            case "704": // DRUG
                icon = public_url.img + "/myhealth/ic_drug.png";
                break;

            default:
                icon = public_url.img + "/notification/ic_info.png";
                break;
        }

        return icon;
    }

    const onOpenModalPointHistory = flag => {
        setOpenModalPointHistory(flag);
    };

    const gotoDetail = (item) => {
        switch (item.type + '') {
            case "1":
                if (item.extra && item.extra !== "") {
                    let extra = item.extra;
                    if (extra && extra.url) {
                        // OHNotiWebview
                        window.open(extra.url, "_blank");
                    } else if (extra && extra.data) {
                        // OHNotiDetails
                        itemSelected = extra.data;
                        setShowDetail(true);
                    }
                }
                break;
            case "2":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.url) {
                        // OHAccountPayHisDetails , Chi tiết cuộc gọi
                    }
                }
                break;
            case "5":   // case này duplicate vs case 13
            case "13":  // case này duplicate vs case 5
            case "37":  // Nhà thuốc Hủy giao thuốc
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.prescription && extra.prescription.id) {
                        //title: "Chi tiết toa thuốc",
                        //name: 'OHPrescriptionDetails',
                    }
                }
                break;
            case "6":
            case "17":
            case "21":
            case "22":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.payment_his && extra.payment_his.id) {
                        //title: "Chi tiết giao dịch",
                        //name: 'OHAccountPayHisDetails',
                    }
                }
                break;
            case "8":
            case "14":
            case "16": // KQ CLS
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.paraclinical && extra.paraclinical.id) {
                        //title: "Chỉ định cận lâm sàng",
                        //name: 'OHSubclinicalDetails',
                    }
                }
                break;
            case "11":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.question) {
                        //title: "Câu hỏi",
                        //name: 'OHNewFeedDetails',
                    }
                }
                break;
            case "20": //Notification of SubClinicalResult
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.doctor && extra.doctor.id) {
                        //title: doctorName,
                        //name: 'OHDoctorDetail'
                    }
                }
                break;
            case "23":
            case "24":
            case "25":
            case "26":
            case "30":
            case "31":
            case "32":
            case "33":
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.order) {
                        //title: "Chi tiết đặt dịch vụ",
                        //name: 'OHServicesBagDetails',
                    }
                }
                break;
            case "38":  // Đăng ký khám tại bệnh viện - Đã đăng ký
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.schedule) {
                        //title: "Chi tiết đặt khám",
                        //name: 'OHApptHospitalHistoryDetail',
                        history.push({
                            pathname: url.name.account_detail_history_examination,
                            state: {id: extra.schedule}
                        });
                    }
                }
                break;
            case "39": {    // Regist Service from HIS
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.data && extra.data.hospital) {
                        // OHCollectPaymentRoot
                    }
                }
                break;
            }

            //SOCIAL NETWORK

            case "100": //Post
            case "101": //Like
            case "102": //Comment
            case "104": //mention(tag) user
                if (item.extra) {
                    if (item.extra && item.extra.postId) {
                        // OHNewsFeedDetailsRoot
                    }
                }
                break;
            case "103": //Follow
                {
                    if (item.extra) {
                        if (item.extra && item.extra.profileId) {
                            //title: "Trang cá nhân",
                            //name: 'OHSocialNetworkProfileMain',
                        }
                    }
                    break;
                }
            case "110": //Birthday
                {
                    if (item.extra) {
                        let extra = item.extra;
                        let userType = extra.type + '';
                        if (userType && userType.trim() !== '') {
                            if (userType === "2") { // doctor
                                //title: "Trang cá nhân",
                                //name: 'OHSocialNetworkProfileMain',
                            } else { // patient, nurse, pt, admin, moderator
                                // open chat
                            }
                        }
                    }
                    break;
                }
            case "200": // Apply Promotion
                if (item.extra) {
                    //title: "Ưu đãi",
                    //name: 'OHPromotionMain',
                }
                break;
            case "201": // Point History
                if (item.extra) {
                    //title: "Lịch sử điểm",
                    //name: 'OHAccountPromotionHis',
                    setOpenModalPointHistory(true);
                }
                break;
            case "300": // BMI PT
                if (item.extra) {
                    //OHHealthTrackingUsersMainRoot
                }
                break;
            case "700": // Drug Order
            case "701":
            case "702":
            case "703":
            case "704":
                if (item.extra) {
                    //OHDrugOrderDetailRoot
                }
                break;
            case "705":
                {
                    // let extra = item && item.extra;
                    // detail lucky money
                    break;
                }
            case "1001":
            case "1002":
            case "1003": {
                // Collect Payment
                if (item.extra) {
                    // let extra = item.extra;
                    //title: "Chi tiết giao dịch",
                    //name: 'OHAccountPayHisDetails',
                }
                break;
            }
            case "1006": {
                // book video schedule
                if (item.extra) {
                    let extra = item.extra;
                    if (extra && extra.examId) {
                        //title: "Chi tiết lịch gọi video",
                        //name: 'OHDoctorVideoHistoryDetail',
                        history.push({
                            pathname: url.name.account_detail_history_call,
                            state: {id: extra.examId}
                        });
                    }
                }
                break;
            }
            case "400": {
                // take blood
                if (item.extra) {
                    let extra = item.extra;
                    if (extra) {
                        //title: "Chi tiết lịch hẹn",
                        //name: 'OHApptHomeHistoryDetail',
                    }
                }
                break;
            }
            default:

        }
    }

    const onPressLoadMore = () => {
        setLoading(true);
        setPage(page + 1);
    }

    const onPressCloseModalDetail = () => {
        setShowDetail(false);
    }

    const renderItem = (item) => {
        let title       = item.title ? item.title : '';
        let time        = item.created_time ? func.formatTimeSocial(item.created_time) : '';
        let isChecked   = !!item.clicked;
        let body        = item.body ? item.body : '';
        return (
            <div className='item' style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', paddingTop: 10 }}
                onClick={() => onPressDetail(item)}>
                <div style={{ marginLeft: 10, textAlign: 'center', width: 52, height: 52, borderRadius: 26 }}>
                    <img alt='' style={{ width: 50, height: 50 }} src={getIcon(item)} />
                </div>
                <div style={{ flex: 1, marginLeft: 20, paddingBottom: 10, borderBottom: "1px solid #ccc" }}>
                    <div style={{ marginBottom: 5, fontSize: 16, fontWeight: 'bold', color: (isChecked ? textColor2 : textColor1) }}>{title}</div>
                    {
                        (body) ? (
                            <div style={{ fontSize: 14, color: textColor2 }}>{body}</div>
                        ) : null
                    }
                    <div style={{ fontSize: 14, color: textColor2 }}>{time}</div>
                </div>
            </div>
        );
    }

    const renderLoadMore = () => {
        return (isShowBtnLoadMore && !loading) ? (
            <div style={{ marginTop: 20, textAlign: 'center' }}>
                <Button className="btn-loadmore" onClick={() => onPressLoadMore()}>{'Xem thêm'}</Button>
            </div>
        ) : null;
    }

    const renderListData = () => {
        return (
            <List
                itemLayout="horizontal"
                dataSource={listData}
                renderItem={item => renderItem(item)}
                loadMore={renderLoadMore()}
            />
        );
    }

    const renderEmpty = () => {
        return (
            <Empty className="ant-empty-custom" description={false} />
        );
    }

    const renderModalDetail = () => {
        let title = "";
        let body = "";
        let date = "";

        if (itemSelected) {
            if (itemSelected.title) {
                title = itemSelected.title;
            }
            if (itemSelected.extra) {
                let extra = itemSelected.extra;
                if (typeof extra === "string") {
                    extra = extra.replaceAll("'", "\"");
                    extra = JSON.parse(extra);
                }
                if (extra && extra.data) {
                    body = extra.data.body;
                }
            }
            if (itemSelected.time) {
                date = func.isoDateToString(itemSelected.time, 'HH:mm DD/MM/YYYY');
            }
        }

        return (
            <Modal
                title="Chi tiết thông báo"
                visible={showDetail}
                closable={true}
                footer={null}
                onCancel={onPressCloseModalDetail}
            >
                <div>
                    <div style={{ marginBottom: 20 }}>
                        <span style={{ fontSize: 18, color: textColor1 }}>
                            {title}
                        </span>
                    </div>
                    <div style={{ marginBottom: 5 }}>
                        <span style={{ fontSize: 14, color: textColor2 }}>{'Ngày: '}</span>
                        <span style={{ fontSize: 14, color: textColor1 }}>
                            {date}
                        </span>
                    </div>
                    <div>
                        <span style={{ fontSize: 14, color: textColor2 }}>{'Nội dung: '}</span>
                        <span style={{ fontSize: 14, color: textColor1 }}>
                            {body}
                        </span>
                    </div>
                </div>
            </Modal>
        );
    }

    const renderMain = () => {
        let view;
        if (listData.length > 0) {
            view = renderListData();
        } else {
            view = renderEmpty();
        }
        return view;
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Danh sách thông báo</h3>
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={() => onPressReadAll()}
                                         className="ant-btn btn-readall"><span>Đã xem tất cả</span></div>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-list-notification">
                                <div className="wrap-panel-content">
                                    <BoxSkeleton skeleton={skeleton}
                                                 full={true}
                                                 length={10}
                                                 rows={3}
                                                 data={renderMain()}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {renderModalDetail()}
            <ModalPointHistory {...props}
                               onOpenModalPointHistory={onOpenModalPointHistory}
                               openModalPointHistory={openModalPointHistory}/>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, { forwardRef: true })(NotificationPage));
