import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import {Empty} from "antd";
import { url, public_url } from "../../../../constants/Path";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import {ModelGetListInvoice} from "../../../../models/ModelInvoice"
import Pagination from "react-js-pagination";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers"
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 16;
const textColorFinish = '#25A93C'; // green
const textColorWait = '#0392ce';    // sky
const textColorCancel = '#f96268';  // red
const textColorMoney = '#FF9500';  // orange

const InvoicePage = (props) => {
    const isRendered = useRef(false);
    const [skeleton, setSkeleton] = useState(false);
    const [listInvoice, setListInvoice] = useState([]);
    const [totalRecord, setTotalRecord] = useState(0);
    const [page, setPage] = useState(1);
    //*** Declare props ***//
    const {_TokenExpired, user, history } = props;

    //*** Handle yield list invoice ***//
    const handleListInvoice = useCallback((page, trigger = false) => {
        const id = user.personal._id;
        if (id) {
            const reqParamFt = {
                page: page,
                limit: LIMIT
            }
            setSkeleton(true);
            ModelGetListInvoice(id, reqParamFt).then(resultFt => {
                if (!isRendered.current) {
                    const dataFt = resultFt.data.one_health_msg.list;
                    const count = resultFt.data.one_health_msg.count;
                    setTotalRecord(count > LIMIT ? count : 0);
                    setListInvoice(dataFt);
                    setSkeleton(false);
                }
                return null;
            }).catch(error => {
                if (!isRendered.current) {
                    if (trigger && error.code === "007") {
                        _TokenExpired(error);
                    }
                    if (error.code !== "007") {
                        console.log("InvoicePage - ListInvoice");
                        setSkeleton(false);
                    }
                }
            });
        }
        return () => {
            isRendered.current = true;
        };
    }, [_TokenExpired, user.personal._id]);

    useEffect(() => {
        if (!isRendered.current) {
            handleListInvoice(1);
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleListInvoice]);

    const onPressDetail = (item) => {
        history.push({
            pathname: url.name.account_detail_invoice,
            state: item
        });
    };

    const onNextPage = (page) => {
        setPage(page);
        handleListInvoice(page, true);
    };

    const yieldTitle = (type) => {
        switch (type) {
            case "sale_prescription":
                return "Bán thuốc qua toa";
            case "prescription":
                return "Toa thuốc";
            case "paraclinical":
                return "Cận lâm sàng";
            case "service":
                return "Dịch vụ";
            case "promotion":
                return "Mã khuyến mãi";
            case "call_his":
                return "Cuộc gọi Video";
            case "chat":
                return "Chat với Bác sĩ";
            case "appointment":
                return "Đặt khám tại nhà";
            case "schedule_appointment":
                return "Đặt khám tại bệnh viện";
            case 'refund_sale_prescription':
                return "Hoàn tiền bán thuốc qua toa";
            case 'refund_prescription':
                return "Hoàn tiền toa thuốc";
            case 'refund_schedule_appointment':
            case 'refund_appointment':
                return "Hoàn tiền đặt lịch khám";
            case 'refund_paraclinical':
                return "Hoàn tiền cận lâm sàng";
            case 'refund_service':
                return "Hoàn tiền dịch vụ";
            case 'refund_other':
                return "Hoàn tiền";
            case "napas":
                return "Nạp tiền Napas";
            case "one_pay":
                return "Nạp tiền thẻ cào";
            case "top-up":
                return "Nạp tiền Top-up";
            case "oh_card":
                return "Nạp tiền thẻ OH";
            case "epay_bank":
                return "Nạp tiền thẻ ATM";
            case "oh_foundation":
                return "Quyên góp";
            case "nganluong":
                return "Nạp tiền Visa/Master";
            case "onepay_atm":
                return "Nạp tiền ATM";
            case "onepay_visa":
                return "Nạp tiền thẻ quốc tế";
            case "debit":
                return "Trừ tiền";
            case "lixi":
                return "Lì xì";
            case "his_thuho_ngoaitru":
                return "Thanh toán ngoại trú";
            case "his_tamung_noitru":
                return "Tạm ứng nội trú";
            case "his_thuho_toathuoc":
                return "Thanh toán toa thuốc";
            case "his_thanhtoan_xuatvien":
                return "Thanh toán xuất viện";
            case "refund_his_thuho_ngoaitru":
                return "Hoàn tiền thanh toán ngoại trú";
            case "refund_his_tamung_noitru":
                return "Hoàn tiền tạm ứng nội trú";
            case "refund_his_thuho_toathuoc":
                return "Hoàn tiền thanh toán toa thuốc";
            case "refund_from_his":
                return "Hoàn tiền thanh toán viện phí";
            case "refund_his_thanhtoan_xuatvien":
            case "his_tratien_xuatvien":
                return "Hoàn tiền thanh toán xuất viện";
            case "call_video_schedule":
                return "Thanh toán lịch gọi video";
            case "refund_call_video_schedule":
                return "Hoàn tiền lịch gọi video";
            default:
                return "Không xác định";
        }
    };

    const yieldIcon = (type) => {
        let icon = null;
        switch (type) {
            case "sale_prescription":
            case "prescription":
            case "appointment":
                icon = public_url.img + "/invoice/ic_visit_doctor_seccolor.png";
                break;
            case "paraclinical":
                icon = public_url.img + "/invoice/ic_meste_gray.png";
                break;
            case "service":
                icon = public_url.img + "/invoice/ic_cashout_services.png";
                break;
            case "promotion":
            case "refund_his_thuho_ngoaitru":
            case "refund_his_tamung_noitru":
            case "refund_his_thuho_toathuoc":
            case "refund_his_thanhtoan_xuatvien":
            case "his_tratien_xuatvien":
            case "refund_from_his":
                icon = public_url.img + "/invoice/ic_cashin_gray.png";
                break;
            case "oh_foundation":
                icon = public_url.img + "/invoice/ic_heart.png";
                break;
            case "call_his":
            case "call_video_schedule":
                icon = public_url.img + "/invoice/ic_video_call_gray.png";
                break;
            case "chat":
                icon = public_url.img + "/invoice/ic_chat_gray.png";
                break;
            case "schedule_appointment":
                icon = public_url.img + "/invoice/ic_hospital.png";
                break;
            case 'refund_sale_prescription':
            case 'refund_prescription':
            case 'refund_schedule_appointment':
            case 'refund_appointment':
            case 'refund_paraclinical':
            case 'refund_service':
            case 'refund_other':
            case "refund_call_video_schedule":
                icon = public_url.img + "/invoice/ic_prepay.png";
                break;
            case "napas":
            case "one_pay":
            case "top-up":
            case "oh_card":
            case "epay_bank":
            case "nganluong":
            case "onepay":
            case "debit":
            case "his_thuho_ngoaitru":
            case "his_tamung_noitru":
            case "his_thuho_toathuoc":
            case "his_thanhtoan_xuatvien":
                icon = public_url.img + "/invoice/ic_cashout_gray.png";
                break;
            case "lixi":
                icon = public_url.img + "/invoice/ic_lixi.png";
                break;
            default:
                icon = public_url.img + "/invoice/ic_cashin_gray.png";
                break;
        }

        return icon;
    };

    function yieldListInvoice() {
        if (listInvoice.length > 0) {
            return _.map(listInvoice, (item, index) => {
                const icon = yieldIcon(item.invoiceType);
                const code = item.code;
                const title = yieldTitle(item.invoiceType);
                const price = func.numberToMoney(item.totalCredit) + 'đ';
                const created = func.isoDateToString(item.created_time, 'HH:mm - DD/MM/YYYY');

                let sttColor = textColorWait;
                let sttText = '';
                const stt = item.status;
                if (stt === 'new') {
                    sttColor = textColorWait;
                    sttText = 'Chưa thanh toán';
                } else if (stt === 'paid') {
                    sttColor = textColorFinish;
                    sttText = 'Đã thanh toán';
                } else if (stt === 'cancel') {
                    sttColor = textColorCancel;
                    sttText = 'Đã hủy';
                }

                return (
                    <div key={index}
                         onClick={onPressDetail.bind(this, item)}
                         className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-item-img round-frame">
                                <img className="img-fluid" src={icon} alt=""/>
                            </div>
                            <div className="wrap-item-info">
                                <h4>{title}</h4>
                                <div className="wrap-line">
                                    <p className="label">Số hóa đơn:</p>
                                    <p className="value">{code}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Trạng thái:</p>
                                    <p style={{color: sttColor}} className="value status">{sttText}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Tổng tiền:</p>
                                    <p style={{color: textColorMoney}} className="value amount">
                                        {price}
                                        {(item.promotion) ? (<img alt=''
                                                                  style={{width: 20, height: 20, marginLeft: 10}}
                                                                  src={public_url.img + "/invoice/ic_percent.png"}/>) : null}
                                    </p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Ngày thanh toán:</p>
                                    <p className="value datetime">{created}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Lịch sử giao dịch trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Lịch sử giao dịch</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-list-invoice">
                                <div className="wrap-panel-content">
                                    <div className="row">
                                        <BoxSkeleton skeleton={skeleton}
                                                     full={false}
                                                     length={10}
                                                     rows={3}
                                                     data={yieldListInvoice()}/>
                                    </div>
                                </div>
                                <div className={totalRecord ? "block-pagination" : "block-pagination d-none"}>
                                    <Pagination hideDisabled
                                                innerClass="pagination"
                                                activePage={page}
                                                itemsCountPerPage={LIMIT}
                                                totalItemsCount={totalRecord}
                                                pageRangeDisplayed={4}
                                                onChange={onNextPage}
                                                itemClass="page-item"
                                                linkClass="page-link"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(InvoicePage));
