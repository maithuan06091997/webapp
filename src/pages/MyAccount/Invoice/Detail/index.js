import React, { Fragment, useEffect, useState } from "react";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { ModelGetDetailInvoice } from "../../../../models/ModelInvoice";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const textColorOH = "#02C8A7"; // green OH
const textColor1 = "#161616"; // black
const textColor2 = "#777777"; // grey
const textColor3 = "#cccccc"; // grey 2
const textColorCreate = "#25A93C"; // green
const textColorWait = "#0392ce"; // sky
const textColorCancel = "#f96268"; // red
const textColorMoney = "#FF9500"; // orange

const DetailInvoicePage = (props) => {
  const [skeleton, setSkeleton] = useState(true);
  const [paymentData, setPaymentData] = useState({});
  //*** Declare props ***//
  const { location } = props;

  useEffect(() => {
    setSkeleton(true);
    if (location.state) {
      const reqParamFt = {
        invoice: location.state._id,
      };
      ModelGetDetailInvoice(reqParamFt)
        .then((resultFt) => {
          const dataFt = resultFt.data.one_health_msg;
          setSkeleton(false);
          setPaymentData(dataFt);
        })
        .catch((error) => {
          if (error.code !== "007") {
            console.log(
              "ModelGetDetailInvoice - ERROR : " + JSON.stringify(error)
            );
            setSkeleton(false);
          }
        });
    }
  }, [location]);

  const getTitle = (type) => {
    switch (type) {
      case "sale_prescription":
        return "Bán thuốc qua toa";
      case "prescription":
        return "Toa thuốc";
      case "paraclinical":
        return "Cận lâm sàng";
      case "service":
        return "Dịch vụ";
      case "promotion":
        return "Mã khuyến mãi";
      case "call_his":
        return "Cuộc gọi Video";
      case "chat":
        return "Chat với Bác sĩ";
      case "appointment":
        return "Đặt khám tại nhà";
      case "schedule_appointment":
        return "Đặt khám tại bệnh viện";
      case "refund_sale_prescription":
        return "Hoàn tiền bán thuốc qua toa";
      case "refund_prescription":
        return "Hoàn tiền toa thuốc";
      case "refund_schedule_appointment":
      case "refund_appointment":
        return "Hoàn tiền đặt lịch khám";
      case "refund_paraclinical":
        return "Hoàn tiền cận lâm sàng";
      case "refund_service":
        return "Hoàn tiền dịch vụ";
      case "refund_other":
        return "Hoàn tiền";
      case "napas":
        return "Nạp tiền Napas";
      case "one_pay":
        return "Nạp tiền thẻ cào";
      case "top-up":
        return "Nạp tiền Top-up";
      case "oh_card":
        return "Nạp tiền thẻ OH";
      case "epay_bank":
        return "Nạp tiền thẻ ATM";
      case "oh_foundation":
        return "Quyên góp";
      case "nganluong":
        return "Nạp tiền Visa/Master";
      case "onepay_atm":
        return "Nạp tiền ATM";
      case "onepay_visa":
        return "Nạp tiền thẻ quốc tế";
      case "debit":
        return "Trừ tiền";
      case "lixi":
        return "Lì xì";
      case "his_thuho_ngoaitru":
        return "Thanh toán ngoại trú";
      case "his_tamung_noitru":
        return "Tạm ứng nội trú";
      case "his_thuho_toathuoc":
        return "Thanh toán toa thuốc";
      case "his_thanhtoan_xuatvien":
        return "Thanh toán xuất viện";
      case "refund_his_thuho_ngoaitru":
        return "Hoàn tiền thanh toán ngoại trú";
      case "refund_his_tamung_noitru":
        return "Hoàn tiền tạm ứng nội trú";
      case "refund_his_thuho_toathuoc":
        return "Hoàn tiền thanh toán toa thuốc";
      case "refund_from_his":
        return "Hoàn tiền thanh toán viện phí";
      case "refund_his_thanhtoan_xuatvien":
      case "his_tratien_xuatvien":
        return "Hoàn tiền thanh toán xuất viện";
      case "call_video_schedule":
        return "Thanh toán lịch gọi video";
      case "refund_call_video_schedule":
        return "Hoàn tiền lịch gọi video";
      default:
        return "Không xác định";
    }
  };

  const getDetails = (item) => {
    switch (item.invoiceType) {
      case "call_his":
        return getDetailsCall(item);
      case "chat":
        return getDetailsChat(item);
      case "sale_prescription":
        return getDetailsPrescription(item, true);
      case "prescription":
        return getDetailsPrescription(item, true);
      case "paraclinical":
        return getDetailsParaclinical(item);
      case "appointment":
        return getDetailsAppointment(item);
      case "refund_schedule_appointment":
      case "schedule_appointment":
        return getDetailsApptHospital(item);
      case "service":
        return getDetailsService(item);
      case "refund_sale_prescription":
        return getDetailsPrescription(item, false);
      case "refund_prescription":
        return getDetailsPrescription(item, false);
      case "refund_paraclinical":
        return getDetailsParaclinical(item);
      case "refund_appointment":
        return getDetailsAppointment(item);
      case "refund_service":
        return getDetailsService(item);
      case "refund_other":
        return null;
      case "napas":
        return getDetailsNapas(item);
      case "one_pay":
        return getDetailsOnePay(item);
      case "promotion":
        return getDetailsPromotion(item);
      case "oh_card":
        return getDetailsOhCard(item);
      case "top-up":
        return getDetailsTopUp(item);
      case "epay_bank":
        return getDetailsEpay(item);
      case "oh_foundation":
        return getDetailsFoundation(item);
      case "nganluong":
        return getDetailsNganLuong(item);
      case "onepay_atm":
        return getDetailsOnePayATM(item);
      case "onepay_visa":
        return getDetailsOnePayCredit(item);
      case "debit":
        return null;
      case "lixi":
        return getDetailsLixi(item);
      case "his_thuho_ngoaitru":
      case "his_tamung_noitru":
      case "his_thuho_toathuoc":
      case "his_thanhtoan_xuatvien":
        return getDetailsCollectPayment(item);
      case "refund_his_thuho_ngoaitru":
      case "refund_his_tamung_noitru":
      case "refund_his_thuho_toathuoc":
      case "refund_from_his":
      case "refund_his_thanhtoan_xuatvien":
      case "his_tratien_xuatvien":
        return getDetailsRefundCollectPayment(item);
      case "call_video_schedule":
        return getDetailVideoSchedule(item);
      default:
        return null;
    }
  };

  // collect payment -----------------------------------------------------------------------
  const getPatientInfo = (patientInfo) => {
    if (patientInfo === null || patientInfo.patient_code === null) {
      return null;
    }

    let code =
      patientInfo && patientInfo.patient_code ? patientInfo.patient_code : "";
    let name = patientInfo && patientInfo.name ? patientInfo.name : "";
    let sex = patientInfo && patientInfo.gender ? patientInfo.gender : "male";
    let phone =
      patientInfo && patientInfo.phone_number
        ? func.yieldCoverPhone(patientInfo.phone_number)
        : "";
    let address =
      patientInfo && patientInfo.full_address ? patientInfo.full_address : "";
    let strBD =
      patientInfo && patientInfo.date_of_birth ? patientInfo.date_of_birth : "";
    let newBD = "";
    if (strBD && strBD.length === 10) {
      let arrTemp = strBD.split(/[-/]+/); // split '-' or '/'
      if (arrTemp.length === 3) {
        // hong_duc, drkhoa : 1955-05-22
        if (arrTemp[2] === "00") {
          newBD = "01";
        } else {
          newBD = arrTemp[2];
        }
        if (arrTemp[1] === "00") {
          newBD += "/01";
        } else {
          newBD += "/" + arrTemp[1];
        }
        newBD += "/" + arrTemp[0];
      } else {
        // nhi_dong : 28/06/2018
        newBD = strBD;
      }
    } else if (strBD && strBD.length === 4) {
      // ung buou: 1979
      newBD = strBD;
    }

    return (
      <div className="containerItem">
        <span className="titleItem">{"Thông tin bệnh nhân"}</span>
        <div>
          <span style={{ fontSize: 14, color: textColor2 }}>
            {"Mã bệnh nhân:"}
          </span>
          <span
            style={{
              flex: 1,
              color: textColorCancel,
              fontSize: 14,
              textAlign: "right",
            }}
          >
            {code}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ fontSize: 14, color: textColor2 }}>{"Họ tên:"}</span>
          <span
            style={{
              flex: 1,
              color: textColor1,
              fontSize: 14,
              textAlign: "right",
            }}
          >
            {name}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ fontSize: 14, color: textColor2 }}>
            {"Giới tính:"}
          </span>
          <span
            style={{
              flex: 1,
              color: textColor1,
              fontSize: 14,
              textAlign: "right",
            }}
          >
            {func.getSexName(sex)}
          </span>
        </div>
        {newBD.trim() !== "" ? (
          <div style={{ marginTop: 5 }}>
            <span style={{ fontSize: 14, color: textColor2 }}>
              {"Ngày sinh:"}
            </span>
            <span
              style={{
                flex: 1,
                color: textColor1,
                fontSize: 14,
                textAlign: "right",
              }}
            >
              {newBD}
            </span>
          </div>
        ) : null}
        {phone !== "" ? (
          <div style={{ marginTop: 5 }}>
            <span style={{ fontSize: 14, color: textColor2 }}>
              {"Số điện thoại:"}
            </span>
            <span
              style={{
                flex: 1,
                color: textColor1,
                fontSize: 14,
                textAlign: "right",
              }}
            >
              {phone}
            </span>
          </div>
        ) : null}
        <div style={{ marginTop: 5 }}>
          <span style={{ fontSize: 14, color: textColor2, marginRight: 10 }}>
            {"Điạ chỉ:"}
          </span>
          <span
            style={{
              flex: 1,
              color: textColor1,
              fontSize: 14,
              textAlign: "right",
            }}
            numberOfLines={2}
          >
            {address}
          </span>
        </div>
      </div>
    );
  };

  const getDetailVideoSchedule = (item) => {
    if (item.book_online_exam) {
      let data = item.book_online_exam;
      let appointTime = data.start_time
        ? func.isoDateToString(data.start_time, "HH:mm")
        : "";
      let appointDate = data.start_time
        ? func.isoDateToString(data.start_time, "DD/MM/YYYY")
        : "";
      let doctorName = data.doctor ? func.getFullName(data.doctor) : "";

      return (
        <div>
          {getUserInfo(data.profile, "Thông tin bệnh nhân")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin lịch hẹn"}</span>
            <div className="itemDetail">
              <span className="name">{"Bác sĩ:"}</span>
              <span className="value">{doctorName}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Ngày hẹn gọi:"}</span>
              <span className="value">{appointDate}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Giờ hẹn gọi:"}</span>
              <span className="value">{appointTime}</span>
            </div>
          </div>
        </div>
      );
    }
    return null;
  };

  const getOutHos = (summary) => {
    if (summary === null || summary.ticket_id === null) {
      return null;
    }

    let ticketView = [];
    let temp1 = summary;
    let totalPrice = temp1.total_price
      ? func.numberToMoney(temp1.total_price) + "đ"
      : "";
    let invoice =
      temp1.invoice && temp1.invoice.his_invoice
        ? temp1.invoice.his_invoice
        : "";
    let group = temp1.group_detail ? temp1.group_detail : null;
    let ticketItemView = [];

    ticketItemView.push(
      <div style={{ marginHorizontal: 10, marginVertical: 10 }}>
        <span style={{ fontSize: 14, fontWeight: "bold", color: textColor1 }}>
          {"Mã hóa đơn:"}
        </span>
        <span style={{ marginLeft: 10, fontSize: 14, color: textColorCancel }}>
          {invoice}
        </span>
      </div>
    );

    if (group.length > 0) {
      ticketItemView.push(
        <div
          style={{
            marginHorizontal: 10,
            borderColor: textColor3,
            borderBottomWidth: 1,
          }}
        >
          <span
            style={{
              flex: 1,
              fontSize: 12,
              color: textColor2,
              textAlign: "center",
            }}
          >
            {"Nội dung"}
          </span>
          <span
            style={{
              width: 20,
              fontSize: 12,
              color: textColor2,
              textAlign: "center",
            }}
          >
            {"SL"}
          </span>
          <span
            style={{
              width: 80,
              fontSize: 12,
              color: textColor2,
              textAlign: "right",
            }}
          >
            {"Đơn giá"}
          </span>
          <span
            style={{
              width: 80,
              fontSize: 12,
              color: textColor2,
              textAlign: "right",
            }}
          >
            {"Thành tiền"}
          </span>
        </div>
      );
    }
    for (let i = 0; i < group.length; i++) {
      let temp2 = group[i];
      let items = temp2.items ? temp2.items : [];
      ticketItemView.push(
        <div style={{ marginHorizontal: 10, marginTop: 10 }}>
          <span
            style={{
              flex: 1,
              fontSize: 14,
              fontWeight: "bold",
              color: textColor1,
            }}
          >
            {temp2.name ? temp2.name : ""}
          </span>
          <span
            style={{
              width: 80,
              fontSize: 12,
              fontWeight: "bold",
              color: textColor1,
              textAlign: "right",
            }}
          >
            {temp2.total_amount
              ? func.numberToMoney(temp2.total_amount) + "đ"
              : ""}
          </span>
        </div>
      );
      for (let j = 0; j < items.length; j++) {
        let temp3 = items[j];
        ticketItemView.push(
          <div style={{ marginHorizontal: 10, marginTop: 5 }}>
            <div>
              <span
                style={{ paddingRight: 5, fontSize: 12, color: textColor1 }}
              >
                {j + 1 + "."}
              </span>
              <span
                style={{ flex: 1, fontSize: 12, color: textColor1 }}
                numberOfLines={2}
              >
                {temp3.vi_name ? temp3.vi_name : ""}
              </span>
              <span
                style={{
                  width: 20,
                  fontSize: 12,
                  color: textColor1,
                  textAlign: "center",
                }}
              >
                {temp3.quantity ? temp3.quantity : ""}
              </span>
              <span
                style={{
                  width: 80,
                  fontSize: 12,
                  color: textColor1,
                  textAlign: "right",
                }}
              >
                {temp3.normal_price
                  ? func.numberToMoney(temp3.normal_price) + "đ"
                  : ""}
              </span>
              <span
                style={{
                  width: 80,
                  fontSize: 12,
                  color: textColor1,
                  textAlign: "right",
                }}
              >
                {temp3.total_price
                  ? func.numberToMoney(temp3.total_price) + "đ"
                  : ""}
              </span>
            </div>
            {temp3.doctor_name ? (
              <div style={{ alignItems: "center", marginTop: 5 }}>
                <span
                  style={{ marginLeft: 15, fontSize: 12, color: textColor2 }}
                >
                  {"Bác sĩ:"}
                </span>
                <span
                  style={{
                    flex: 1,
                    marginLeft: 5,
                    fontSize: 12,
                    color: textColor1,
                  }}
                >
                  {temp3.doctor_name}
                </span>
              </div>
            ) : null}
            <div style={{ paddingHorizontal: 15 }}>
              {temp3.seq_no ? (
                <div style={{ alignItems: "center" }}>
                  <span style={{ fontSize: 16, color: textColor2 }}>
                    {"STT:"}
                  </span>
                  <span
                    style={{
                      marginLeft: 5,
                      fontSize: 16,
                      fontWeight: "bold",
                      color: textColorOH,
                    }}
                  >
                    {temp3.seq_no}
                  </span>
                </div>
              ) : null}
              {temp3.room_name ? (
                <div style={{ alignItems: "center", marginLeft: 20 }}>
                  <span style={{ fontSize: 16, color: textColor2 }}>
                    {"Phòng:"}
                  </span>
                  <span
                    style={{
                      marginLeft: 5,
                      fontSize: 16,
                      fontWeight: "bold",
                      color: textColorOH,
                    }}
                  >
                    {temp3.room_name}
                  </span>
                </div>
              ) : null}
            </div>
          </div>
        );
      }
      ticketItemView.push(
        <div
          style={{
            flex: 1,
            height: 5,
            marginHorizontal: 10,
            borderColor: textColor3,
            borderBottomWidth: 1,
          }}
        />
      );
    }
    ticketView.push(
      <div style={{ borderColor: textColor3, borderWidth: 1 }}>
        <div style={{ height: 10 }} />
        {ticketItemView}
        <div style={{ height: 20 }} />
        <div style={{ alignItems: "center", marginHorizontal: 10 }}>
          <span
            style={{
              flex: 1,
              textAlign: "right",
              fontSize: 14,
              fontWeight: "bold",
              color: textColor1,
            }}
          >
            {"Tổng cộng:"}
          </span>
          <span
            style={{
              marginLeft: 10,
              fontSize: 14,
              fontWeight: "bold",
              color: textColorMoney,
            }}
          >
            {totalPrice}
          </span>
        </div>
        <div style={{ height: 10 }} />
      </div>
    );

    return (
      <div style={{ flex: 1 }}>
        <div className="containerItem">
          <span className="titleItem">{"Danh sách gói khám"}</span>
          {ticketView}
        </div>
      </div>
    );
  };

  const getInHos = (summary) => {
    if (summary === null || summary.treatment_item === null) {
      return null;
    }

    let invoice =
      summary.invoice && summary.invoice.his_invoice
        ? summary.invoice.his_invoice
        : "";
    let treatment = summary.treatment_item;

    let treatmentView = [];
    let checkin = treatment.check_in_date
      ? func.isoDateToString(treatment.check_in_date, "HH:mm DD/MM/YYYY")
      : "";
    let description = treatment.introduction_text
      ? treatment.introduction_text
      : "";

    treatmentView.push(
      <div>
        <div style={{ height: 10 }} />
        <div>
          <span style={{ width: 100, fontSize: 14, color: textColor2 }}>
            {"Mã hóa đơn:"}
          </span>
          <span style={{ fontSize: 14, color: textColorCancel }}>
            {invoice}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 100, fontSize: 14, color: textColor2 }}>
            {"Ngày vào:"}
          </span>
          <span style={{ fontSize: 14, color: textColor2 }}>{checkin}</span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 100, fontSize: 14, color: textColor2 }}>
            {"Ghi chú:"}
          </span>
          <span style={{ fontSize: 14, color: textColor2 }}>{description}</span>
        </div>
        <div style={{ height: 10 }} />
      </div>
    );

    return (
      <div style={{ flex: 1 }}>
        <div className="containerItem">
          <span className="titleItem">{"Thông tin gói khám"}</span>
          {treatmentView}
        </div>
      </div>
    );
  };

  const getLeaveHos = (summary) => {
    if (summary === null || summary.invoice === null) {
      return null;
    }
    let invoice = summary.invoice.his_invoice
      ? summary.invoice.his_invoice
      : "";
    return (
      <div style={{ flex: 1 }}>
        <div className="containerItem">
          <span className="titleItem">{"Thông tin gói khám"}</span>
          <div>
            <span style={{ width: 100, fontSize: 14, color: textColor2 }}>
              {"Mã hóa đơn:"}
            </span>
            <span style={{ fontSize: 14, color: textColorCancel }}>
              {invoice}
            </span>
          </div>
        </div>
      </div>
    );
  };

  const getDrugList = (presDetail, drugList, invoice) => {
    if (presDetail === null) {
      return null;
    }

    // pres
    let presView = [];

    let temp = presDetail;
    let dateCreate = temp.input_date
      ? func.isoDateToString(temp.input_date, "HH:mm DD/MM/YYYY")
      : "";
    let diagnosis = temp.diagnosis ? temp.diagnosis : "";
    let note = temp.note ? temp.note : "";
    let re_examine = temp.re_examine ? temp.re_examine : "";
    let doctor = temp.doctor_name ? temp.doctor_name : "";
    let totalPrice =
      func.numberToMoney(Math.round(paymentData.totalCredit)) + "đ";

    let icd10PrimaryText =
      temp.primary_icd10 && temp.primary_icd10.vi_name
        ? temp.primary_icd10.vi_name
        : "";
    let icd10PrimaryCode =
      temp.primary_icd10 && temp.primary_icd10.code
        ? temp.primary_icd10.code
        : "";

    let icd10 = temp.icd10 && temp.icd10 ? temp.icd10 : [];
    let icd10View = [];
    for (let j = 0; j < icd10.length; j++) {
      let temp2 = icd10[j];
      if (temp2.code !== "" && temp2.code !== icd10PrimaryCode) {
        icd10View.push(
          <span style={{ fontSize: 12, color: textColor1 }}>
            {temp2.vi_name ? temp2.vi_name : ""}
          </span>
        );
      }
    }

    let drugView = [];
    for (let j = 0; j < drugList.length; j++) {
      let temp3 = drugList[j];
      let name1 = temp3.proprietary_name ? temp3.proprietary_name : "";
      let name2 = temp3.original_names ? temp3.original_names : "";
      let name = name1 + " (" + name2 + ")";

      let quantityNum = temp3.quantity_num ? temp3.quantity_num + "" : "0";
      let unit = temp3.enum_unit_import_sell_value
        ? temp3.enum_unit_import_sell_value
        : "";
      let quantity = quantityNum + " " + unit;
      let price = temp3.price
        ? func.numberToMoney(Math.round(temp3.price)) + "đ/" + unit
        : "";

      drugView.push(
        <div style={{ marginHorizontal: 5, marginTop: 10 }}>
          <div>
            <span style={{ minWidth: 20, fontSize: 12, color: textColor1 }}>
              {j + 1 + ")"}
            </span>
            <div
              style={{
                flex: 1,
                paddingBottom: 2,
                borderColor: textColor3,
                borderBottomWidth: 1,
              }}
            >
              <span
                style={{
                  flex: 1,
                  fontSize: 12,
                  fontWeight: "bold",
                  color: textColor1,
                }}
              >
                {name}
              </span>
              <span
                style={{ fontSize: 12, fontWeight: "bold", color: textColor1 }}
              >
                {quantity}
              </span>
            </div>
          </div>
          <div style={{ marginLeft: 20, marginTop: 2 }}>
            <div style={{ flex: 1, flexWrap: "wrap" }}>
              <span style={{ marginRight: 5, color: textColor1, fontSize: 12 }}>
                {temp3.usage_title_value ? temp3.usage_title_value : ""}
              </span>
              {temp3.morning ? (
                <span style={{ color: textColor1, fontSize: 12 }}>
                  {"Sáng: "}
                  <span
                    style={{
                      color: textColorWait,
                      fontSize: 12,
                      fontWeight: "bold",
                    }}
                  >
                    {temp3.morning + " "}
                  </span>
                </span>
              ) : null}
              {temp3.noon ? (
                <span style={{ color: textColor1, fontSize: 12 }}>
                  {"Trưa: "}
                  <span
                    style={{
                      color: textColorWait,
                      fontSize: 12,
                      fontWeight: "bold",
                    }}
                  >
                    {temp3.noon + " "}
                  </span>
                </span>
              ) : null}
              {temp3.afternoon ? (
                <span style={{ color: textColor1, fontSize: 12 }}>
                  {"Chiều: "}
                  <span
                    style={{
                      color: textColorWait,
                      fontSize: 12,
                      fontWeight: "bold",
                    }}
                  >
                    {temp3.afternoon + " "}
                  </span>
                </span>
              ) : null}
              {temp3.evening ? (
                <span style={{ color: textColor1, fontSize: 12 }}>
                  {"Tối: "}
                  <span
                    style={{
                      color: textColorWait,
                      fontSize: 12,
                      fontWeight: "bold",
                    }}
                  >
                    {temp3.evening + " "}
                  </span>
                </span>
              ) : null}
              <span style={{ color: textColor1, fontSize: 12 }}>
                {(temp3.time ? "(" + temp3.time : "(") + " ngày)"}
              </span>
            </div>
            <span style={{ color: textColorCancel, fontSize: 12 }}>
              {price}
            </span>
          </div>
          <span
            style={{
              flex: 1,
              marginLeft: 20,
              color: textColorWait,
              fontSize: 12,
            }}
          >
            {temp3.note ? temp3.note : ""}
          </span>
        </div>
      );
    }

    presView.push(
      <div style={{ backgroundColor: "white" }}>
        <div style={{ height: 10 }} />
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Mã toa thuốc:"}
          </span>
          <span style={{ fontSize: 12, color: textColorCancel }}>
            {invoice}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Ngày tạo:"}
          </span>
          <span style={{ fontSize: 12, color: textColor1 }}>{dateCreate}</span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"ICD chính:"}
          </span>
          <span style={{ flex: 1, fontSize: 12, color: textColor1 }}>
            {icd10PrimaryText}
          </span>
        </div>
        {icd10View.length > 0 ? (
          <div style={{ marginTop: 5 }}>
            <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
              {"ICD kèm theo:"}
            </span>
            <div style={{ flex: 1 }}>{icd10View}</div>
          </div>
        ) : null}
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Chẩn đoán:"}
          </span>
          <span
            style={{
              flex: 1,
              fontSize: 12,
              fontWeight: "bold",
              color: textColor1,
            }}
          >
            {diagnosis}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Dặn dò:"}
          </span>
          <span
            style={{
              flex: 1,
              fontSize: 12,
              fontWeight: "bold",
              color: textColorWait,
            }}
          >
            {note}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Tái khám:"}
          </span>
          <span
            style={{ fontSize: 12, fontWeight: "bold", color: textColorCancel }}
          >
            {re_examine}
          </span>
        </div>
        <div style={{ marginTop: 5 }}>
          <span style={{ width: 90, fontSize: 12, color: textColor2 }}>
            {"Bác sĩ:"}
          </span>
          <span style={{ fontSize: 12, fontWeight: "bold", color: textColor1 }}>
            {doctor}
          </span>
        </div>
        {drugView.length > 0 ? (
          <div
            style={{
              marginTop: 10,
              paddingBottom: 10,
              borderColor: textColor3,
              borderWidth: 1,
            }}
          >
            {drugView}
          </div>
        ) : null}
        <div
          style={{
            marginTop: 5,
            alignItems: "flex-end",
            justifyContent: "flex-end",
          }}
        >
          <span style={{ width: 80, fontSize: 14, color: textColor2 }}>
            {"Tổng cộng:"}
          </span>
          <span
            style={{ fontSize: 14, fontWeight: "bold", color: textColorMoney }}
          >
            {totalPrice}
          </span>
        </div>
        <div style={{ height: 10 }} />
      </div>
    );

    return (
      <div style={{ flex: 1 }}>
        <div className="containerItem">
          <span className="titleItem">{"Thông tin gói khám"}</span>
          {presView}
        </div>
      </div>
    );
  };

  const getDetailsCollectPayment = (item) => {
    let result = null;
    if (item.invoiceType === "his_thuho_toathuoc") {
      if (
        item &&
        item.his_meta &&
        item.his_meta.his_prescription &&
        item.his_meta.his_prescription.pres_detail
      ) {
        let prescription = item.his_meta.his_prescription;
        let patientInfo = prescription.patient ? prescription.patient : null;
        let invoice = prescription.invoice_code
          ? prescription.invoice_code
          : "";
        let presDetail = prescription.pres_detail
          ? prescription.pres_detail
          : null;
        let drugList = prescription.drug_list ? prescription.drug_list : [];

        result = (
          <div>
            {getPatientInfo(patientInfo)}
            {getDrugList(presDetail, drugList, invoice)}
          </div>
        );
      }
    } else {
      if (item && item.his_meta && item.his_meta.summary) {
        let patientInfo = item.his_meta.patient_info
          ? item.his_meta.patient_info
          : null;
        let summary = item.his_meta.summary;

        result = (
          <div>
            {getPatientInfo(patientInfo)}
            {summary && item.invoiceType === "his_thuho_ngoaitru"
              ? getOutHos(summary)
              : null}
            {summary && item.invoiceType === "his_tamung_noitru"
              ? getInHos(summary)
              : null}
            {summary && item.invoiceType === "his_thanhtoan_xuatvien"
              ? getLeaveHos(summary)
              : null}
          </div>
        );
      }
    }
    return result;
  };

  const getDetailsRefundCollectPayment = (item) => {
    let result = null;
    if (item && item.his_meta && item.user) {
      let patientInfo = item.his_meta.patient_info
        ? item.his_meta.patient_info
        : null;
      let summary = item.his_meta.summary ? item.his_meta.summary : null;
      let invoice =
        summary && summary.invoice && summary.invoice.his_invoice
          ? summary.invoice.his_invoice
          : "";
      if (invoice !== "") {
        result = (
          <div style={{ flex: 1 }}>
            {getUserInfo(item.user, "Thông tin người dùng")}
            {getPatientInfo(patientInfo)}
            <div className="containerItem">
              <span className="titleItem">{"Thông tin gói khám"}</span>
              <div>
                <span style={{ width: 90, fontSize: 14, color: textColor2 }}>
                  {"Mã hóa đơn:"}
                </span>
                <span style={{ fontSize: 14, color: textColorCancel }}>
                  {invoice}
                </span>
              </div>
            </div>
          </div>
        );
      }
    } else if (item && item.his_prescription && item.user) {
      let patientInfo = item.his_prescription.patient
        ? item.his_prescription.patient
        : null;
      let invoice = item.his_prescription.invoice_code
        ? item.his_prescription.invoice_code
        : "";
      if (invoice !== "") {
        result = (
          <div style={{ flex: 1 }}>
            {getUserInfo(item.user, "Thông tin người dùng")}
            {getPatientInfo(patientInfo)}
            <div className="containerItem">
              <span className="titleItem">{"Thông tin toa thuốc"}</span>
              <div>
                <span style={{ width: 90, fontSize: 14, color: textColor2 }}>
                  {"Mã hóa đơn:"}
                </span>
                <span style={{ fontSize: 14, color: textColorCancel }}>
                  {invoice}
                </span>
              </div>
            </div>
          </div>
        );
      }
    }
    return result;
  };
  // end collect payment -----------------------------------------------------------------

  const getDetailsLixi = (item) => {
    let result = null;
    if (item && item.lixi_information && item.user) {
      let lixiFullname = func.getFullName(item.lixi_information.toUser);
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người dùng")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin lì xì"}</span>
            <div className="itemDetail">
              <span className="name">{"Người nhận:"}</span>
              <span className="value">{lixiFullname}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsFoundation = (item) => {
    let result = null;
    if (item && item.oh_foundation_hist && item.user) {
      let foundData = item.oh_foundation_hist;
      let foundID = foundData.code ? foundData.code : "";
      let foundEvent =
        foundData.event && foundData.event.name ? foundData.event.name : "";
      let foundNickname =
        foundData.info && foundData.info.name ? foundData.info.name : "";
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người dùng")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin quyên góp"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã quyên góp:"}</span>
              <span className="value">{foundID}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Chương trình:"}</span>
              <span className="value">{foundEvent}</span>
            </div>
            {foundNickname !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Nhân danh:"}</span>
                <span className="value">{foundNickname}</span>
              </div>
            ) : null}
            {/* <div className="itemDetail">
                            <span className="name">{'Gửi xác nhận cho cá nhân \nhay tổ chức muốn quyên góp:'}</span>
                            <span className="value">{foundConfirm}</span>
                        </div> */}
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsEpay = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"THẺ ATM"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsNapas = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"NAPAS"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsNganLuong = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"VISA/MASTER"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsOnePayATM = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"ATM"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsOnePayCredit = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"Thẻ quốc tế"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsOnePay = (item) => {
    let result = null;
    if (item && item.onePay && item.user) {
      let textName = item.onePay.type.toUpperCase();
      let textPrice = func.numberToMoney(item.onePay.amount) + "đ";
      let textPin = item.onePay.pin;
      let textSeri = item.onePay.serial;
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Thẻ cào:"}</span>
              <span className="value">{textName}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Mệnh giá:"}</span>
              <span className="value">{textPrice}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Mã PIN:"}</span>
              <span className="value">{textPin}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Mã serial:"}</span>
              <span className="value">{textSeri}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsPromotion = (item) => {
    let result = null;
    if (item && item.user) {
      let promotionCode = "";
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp mã khuyến mãi")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin mã khuyến mãi"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã khuyến mãi:"}</span>
              <span className="value">{promotionCode}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsOhCard = (item) => {
    let result = null;
    if (item && item.user) {
      let textSeri = item.ohSerial;
      let textPrice = func.numberToMoney(item.totalCredit) + "đ";
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"THẺ ONEHEALTH"}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Mệnh giá:"}</span>
              <span className="value">{textPrice}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Mã serial:"}</span>
              <span className="value">{textSeri}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsTopUp = (item) => {
    let result = null;
    if (item && item.user) {
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người nạp tiền")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin nạp tiền"}</span>
            <div className="itemDetail">
              <span className="name">{"Nguồn tiền:"}</span>
              <span className="value">{"TOPUP ONEHEALTH"}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsCall = (item) => {
    let result = null;
    if (item.callHistory && item.callHistory.to) {
      let doctor = func.getFullName(item.callHistory.to);
      let start_time = func.isoDateToString(
        item.callHistory.start_time,
        "HH:mm DD/MM/YYYY"
      );
      let duration = func.secondToMinute(item.callHistory.duration);
      result = (
        <Fragment>
          {getUserInfo(item.callHistory.from, "Thông tin người gọi")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin cuộc gọi"}</span>
            <div className="itemDetail">
              <span className="name">{"Bác sĩ:"}</span>
              <span className="value">{doctor}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Gọi lúc:"}</span>
              <span className="value">{start_time}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Thời gian gọi:"}</span>
              <span className="value">{duration}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsChat = (item) => {
    let result = null;
    if (item.chat && item.chat.to) {
      let doctor = func.getFullName(item.chat.to);
      let createTime = func.isoDateToString(
        item.chat.created_time,
        "HH:mm DD/MM/YYYY"
      );
      let lastMess = func.isoDateToString(
        item.chat.last_message_time,
        "HH:mm DD/MM/YYYY"
      );
      result = (
        <Fragment>
          {getUserInfo(item.chat.from, "Thông tin người chat")}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin phiên chat"}</span>
            <div className="itemDetail">
              <span className="name">{"Bác sĩ:"}</span>
              <span className="value">{doctor}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Bắt đầu phiên chat:"}</span>
              <span className="value">{createTime}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Tin nhắn sau cùng:"}</span>
              <span className="value">{lastMess}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDrug = (
    number,
    name,
    count,
    isNormal,
    usageNormal,
    usage,
    usageOrther,
    note
  ) => {
    return (
      <div style={{ paddingVertical: 5 }}>
        <span style={{ fontSize: 14 }}>{number}</span>
        <div
          style={{ flex: 1, justifyContent: "space-between", marginLeft: 5 }}
        >
          <div
            style={{ flex: 1, justifyContent: "space-between", marginLeft: 5 }}
          >
            <span
              style={{
                flex: 1,
                color: textColor1,
                fontSize: 14,
                fontWeight: "500",
              }}
            >
              {name}
            </span>
            <span style={{ color: textColorWait, fontSize: 14 }}>{count}</span>
          </div>
          {isNormal ? (
            <div
              style={{
                flex: 1,
                justifyContent: "space-between",
                marginLeft: 5,
              }}
            >
              <span style={{ color: textColor1, fontSize: 14 }}>
                {"Sáng: " + (usageNormal.morning ? usageNormal.morning : 0)}
              </span>
              <span style={{ color: textColor1, fontSize: 14 }}>
                {"Trưa: " + (usageNormal.afternoon ? usageNormal.afternoon : 0)}
              </span>
              <span style={{ color: textColor1, fontSize: 14 }}>
                {"Chiều: " + (usageNormal.evening ? usageNormal.evening : 0)}
              </span>
              <span style={{ color: textColor1, fontSize: 14 }}>
                {"Tối: " + (usageNormal.night ? usageNormal.night : 0)}
              </span>
              <span style={{ color: textColorWait, fontSize: 14 }}>
                {usage}
              </span>
            </div>
          ) : (
            <div style={{ flex: 1, justifyContent: "space-between" }}>
              <span
                style={{
                  flex: 1,
                  color: textColorWait,
                  marginLeft: 5,
                  fontSize: 14,
                }}
                numberOfLines={0}
              >
                {usageOrther}
              </span>
              <span style={{ color: textColorWait, fontSize: 14 }}>
                {usage}
              </span>
            </div>
          )}
          {note && note !== "" ? (
            <div style={{ flex: 1, marginLeft: 5 }}>
              <span style={{ color: textColorCancel, fontSize: 14 }}>
                {"Chú ý: "}
              </span>
              <span style={{ color: textColor2, fontSize: 14 }}>{note}</span>
            </div>
          ) : null}
          <div
            style={{ marginTop: 5, height: 1, backgroundColor: textColor3 }}
          />
        </div>
      </div>
    );
  };

  const getAddress = (data) => {
    let dataAddress = data.location ? data.location : data;

    let result = "";
    result = dataAddress.address ? dataAddress.address + " " : "";
    result +=
      dataAddress.ward && dataAddress.ward.vi_name
        ? dataAddress.ward.vi_name + " "
        : "";
    result +=
      dataAddress.district && dataAddress.district.vi_name
        ? dataAddress.district.vi_name + " "
        : "";
    result +=
      dataAddress.province && dataAddress.province.vi_name
        ? dataAddress.province.vi_name + " "
        : "";
    return result;
  };

  const getUserInfo = (patient, title) => {
    let fullname = "";
    let old = "";
    let sex = "";
    let address = "";
    fullname = func.getFullName(patient);
    old = func.yieldAge(patient.birthday);
    sex = func.getSexName(patient.sex);
    address = getAddress(patient);
    return (
      <div className="containerItem">
        <span className="titleItem">{title}</span>
        <div className="itemDetail">
          <span className="name">{"Họ tên:"}</span>
          <span className="value">{fullname}</span>
        </div>
        <div className="itemDetail">
          <span className="name">{"Tuổi:"}</span>
          <span className="value">{old}</span>
        </div>
        <div className="itemDetail">
          <span className="name">{"Giới tính:"}</span>
          <span className="value">{sex}</span>
        </div>
        <div className="itemDetail">
          <span className="name">{"Địa chỉ:"}</span>
          <span className="value" style={{ marginLeft: 10 }}>
            {address}
          </span>
        </div>
      </div>
    );
  };

  const renderItemExtraDrug = (item, index) => {
    return (
      <div
        style={{
          alignItems: "center",
          marginHorizontal: 10,
          paddingVertical: 5,
          borderBottomColor: textColor3,
          borderBottomWidth: 1,
        }}
      >
        <span
          style={{
            width: 50,
            fontSize: 14,
            color: textColor2,
            textAlign: "center",
          }}
        >
          {index + 1}
        </span>
        <span style={{ flex: 1, fontSize: 14, color: textColor1 }}>
          {item.name}
        </span>
        <span
          style={{
            width: 80,
            fontSize: 14,
            color: textColor1,
            textAlign: "center",
          }}
        >
          {item.quantity}
        </span>
      </div>
    );
  };

  const renderListExtraDrug = (textDrug) => {
    let view = [];
    let index = 0;
    let data = textDrug;
    if (data && data.length === 0) {
      return null;
    }

    data.forEach((element) => {
      view.push(renderItemExtraDrug(element, index));
      index++;
    });

    return (
      <div>
        <span style={{ marginTop: 20, fontSize: 14, color: textColor2 }}>
          {"Thuốc ngoài toa:"}
        </span>
        <div
          style={{
            marginTop: 5,
            paddingVertical: 10,
            borderColor: textColor3,
            borderWidth: 1,
          }}
        >
          <div
            style={{
              marginHorizontal: 5,
              paddingVertical: 5,
              borderColor: textColor3,
              borderBottomWidth: 1,
            }}
          >
            <span
              style={{
                width: 50,
                fontSize: 14,
                color: textColor2,
                textAlign: "center",
              }}
            >
              {"STT"}
            </span>
            <span style={{ flex: 1, fontSize: 14, color: textColor2 }}>
              {"Tên thuốc"}
            </span>
            <span
              style={{
                width: 80,
                fontSize: 14,
                color: textColor2,
                textAlign: "center",
              }}
            >
              {"Số lượng"}
            </span>
          </div>
          {view}
        </div>
      </div>
    );
  };

  const getDetailsPrescription = (item, isDelivery) => {
    let result = null;
    if (item && item.prescription && item.prescription._id && item.user) {
      let orderCode = item.prescription.order_code
        ? item.prescription.order_code
        : "";
      let pay =
        item.payment === "prepay"
          ? "Thanh toán trả trước"
          : "Thanh toán khi nhận hàng";
      let reciever = "";
      let phone = "";
      let deliAddress = "";
      if (item.prescription.deliveryInfo) {
        let deli = item.prescription.deliveryInfo;
        reciever = func.getFullName(deli);
        phone = deli.mobile;
        deliAddress = getAddress(deli);
      }

      let code = item.prescription.code ? item.prescription.code : "";
      let doctor = func.getFullName(item.prescription.doctor);
      let diagnose = item.prescription.diagnose;
      let note = item.prescription.summary_note;
      let icd10 = "";
      if (item.prescription.icd10 && item.prescription.icd10.length > 0) {
        item.prescription.icd10.forEach((element) => {
          icd10 += element.description + "\n";
        });
      }

      let listDrug = [];
      for (let i = 0; i < item.prescription.prescription.length; i++) {
        let drugItem = item.prescription.prescription[i];
        let number = i + 1;
        let name = drugItem.drug ? drugItem.drug.proprietary_name : "";
        let count = drugItem.total_quantity + " " + drugItem.unit;
        let isNormal = !drugItem.other_use;
        let usageNormal = drugItem.other_use
          ? null
          : {
              morning: drugItem.how_use.morning
                ? drugItem.how_use.morning.quantity
                : 0,
              afternoon: drugItem.how_use.afternoon
                ? drugItem.how_use.afternoon.quantity
                : 0,
              evening: drugItem.how_use.evening
                ? drugItem.how_use.evening.quantity
                : 0,
              night: drugItem.how_use.night
                ? drugItem.how_use.night.quantity
                : 0,
            };
        let usage = drugItem.usage;
        let usageOrther = drugItem.other_use;
        let note = drugItem.other_use ? null : drugItem.note;
        listDrug.push(
          getDrug(
            number,
            name,
            count,
            isNormal,
            usageNormal,
            usage,
            usageOrther,
            note
          )
        );
      }

      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người dùng")}
          {isDelivery ? (
            <Fragment>
              {orderCode !== "" ? (
                <div className="containerItem">
                  <span className="titleItem">{"Thông tin giao hàng"}</span>
                  <div className="itemDetail">
                    <span className="name">{"Mã đơn hàng:"}</span>
                    <span className="value">{orderCode}</span>
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Hình thức thanh toán:"}</span>
                    <span className="value" style={{ color: textColorCancel }}>
                      {pay}
                    </span>
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Người nhận:"}</span>
                    <span className="value">{reciever}</span>
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Số điện thoại:"}</span>
                    <span className="value">{phone}</span>
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Địa chỉ:"}</span>
                    <span className="value">{deliAddress}</span>
                  </div>
                </div>
              ) : (
                <div className="containerItem">
                  <span className="titleItem">{"Thông tin giao hàng"}</span>
                  <span className="name">
                    {"Đang chờ người dùng xác nhận giao hàng."}
                  </span>
                </div>
              )}
            </Fragment>
          ) : null}
          {orderCode !== "" ? (
            <Fragment>
              {item.payment === "prepay" ? (
                <div className="containerItem">
                  <span className="titleItem">{"Thông tin toa thuốc"}</span>
                  <div className="itemDetail">
                    <span className="name">{"Mã toa thuốc:"}</span>
                    <span className="value">{code}</span>
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Bác sĩ kê toa:"}</span>
                    <span className="value">{doctor}</span>
                  </div>
                  {icd10 !== "" ? (
                    <div className="itemDetail">
                      <span className="name">{"Chẩn đoán ICD 10:"}</span>
                      <span className="value">{icd10}</span>
                    </div>
                  ) : null}
                  {diagnose !== "" ? (
                    <div className="itemDetail">
                      <span className="name">{"Chẩn đoán:"}</span>
                      <span className="value">{diagnose}</span>
                    </div>
                  ) : null}
                  <div className="itemDetail">
                    <span className="name">{"Đơn thuốc:"}</span>
                  </div>
                  <div
                    style={{
                      flex: 1,
                      borderColor: textColor3,
                      borderWidth: 1,
                      padding: 10,
                    }}
                  >
                    <div
                      style={{
                        paddingVertical: 5,
                        borderBottomWidth: 1,
                        borderColor: textColor3,
                      }}
                    >
                      <span style={{ color: textColor2, fontSize: 14 }}>
                        {"STT"}
                      </span>
                      <div
                        style={{
                          flex: 1,
                          justifyContent: "space-between",
                          marginLeft: 20,
                        }}
                      >
                        <span
                          style={{ flex: 1, color: textColor2, fontSize: 14 }}
                        >
                          {"Tên thuốc\nLiều dùng"}
                        </span>
                        <span style={{ color: textColor2, fontSize: 14 }}>
                          {"Số lượng\nCách dùng"}
                        </span>
                      </div>
                    </div>
                    {listDrug}
                  </div>
                  <div className="itemDetail">
                    <span className="name">{"Lời dặn:"}</span>
                    <span className="value">{note}</span>
                  </div>
                </div>
              ) : (
                <div className="containerItem">
                  <span className="titleItem">{"Thông tin toa thuốc"}</span>
                  <span className="name">
                    {
                      'Do hình thức thanh toán là "Thanh toán khi nhận hàng" nên toa thuốc của bạn sẽ được gửi kèm theo khi giao thuốc.'
                    }
                  </span>
                </div>
              )}
            </Fragment>
          ) : null}
        </Fragment>
      );
    } else if (
      item &&
      item.sale_prescription &&
      item.sale_prescription._id &&
      item.user
    ) {
      let pay =
        item.payment === "prepay"
          ? "Thanh toán trả trước"
          : "Thanh toán khi nhận hàng";
      let reciever = "";
      let deliAddress = item.sale_prescription.subAddress
        ? item.sale_prescription.subAddress
        : "";
      if (item.sale_prescription.info) {
        let deli = item.sale_prescription.info;
        reciever = deli.name ? deli.name : "";
      }

      let textDrugs = item.sale_prescription.textDrugs
        ? item.sale_prescription.textDrugs
        : [];
      let listDrug = [];
      for (let i = 0; i < item.sale_prescription.prescription.length; i++) {
        let drugItem = item.sale_prescription.prescription[i];
        let number = i + 1;
        let name =
          drugItem.drug && drugItem.drug.original_name
            ? drugItem.drug.original_name
            : "";
        let count = drugItem.total_quantity + " " + drugItem.unit;
        let isNormal = !drugItem.other_use;
        let usageNormal = drugItem.other_use
          ? null
          : {
              morning: drugItem.how_use.morning
                ? drugItem.how_use.morning.quantity
                : 0,
              afternoon: drugItem.how_use.afternoon
                ? drugItem.how_use.afternoon.quantity
                : 0,
              evening: drugItem.how_use.evening
                ? drugItem.how_use.evening.quantity
                : 0,
              night: drugItem.how_use.night
                ? drugItem.how_use.night.quantity
                : 0,
            };
        let usage = drugItem.usage;
        let usageOrther = drugItem.other_use;
        let note = drugItem.other_use ? null : drugItem.note;
        listDrug.push(
          getDrug(
            number,
            name,
            count,
            isNormal,
            usageNormal,
            usage,
            usageOrther,
            note
          )
        );
      }

      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người dùng")}
          {isDelivery ? (
            <div className="containerItem">
              <span className="titleItem">{"Thông tin giao hàng"}</span>
              <div className="itemDetail">
                <span className="name">{"Hình thức thanh toán:"}</span>
                <span className="value" style={{ color: textColorCancel }}>
                  {pay}
                </span>
              </div>
              <div className="itemDetail">
                <span className="name">{"Người nhận:"}</span>
                <span className="value">{reciever}</span>
              </div>
              <div className="itemDetail">
                <span className="name">{"Địa chỉ:"}</span>
                <span className="value">{deliAddress}</span>
              </div>
            </div>
          ) : null}
          <div className="containerItem">
            <span className="titleItem">{"Thông tin toa thuốc"}</span>
            {listDrug.length > 0 ? (
              <div>
                <div className="itemDetail">
                  <span className="name">{"Đơn thuốc:"}</span>
                </div>
                <div
                  style={{
                    flex: 1,
                    borderColor: textColor3,
                    borderWidth: 1,
                    padding: 10,
                  }}
                >
                  <div
                    style={{
                      paddingVertical: 5,
                      borderBottomWidth: 1,
                      borderColor: textColor3,
                    }}
                  >
                    <span style={{ color: textColor2, fontSize: 14 }}>
                      {"STT"}
                    </span>
                    <div
                      style={{
                        flex: 1,
                        justifyContent: "space-between",
                        marginLeft: 20,
                      }}
                    >
                      <span
                        style={{ flex: 1, color: textColor2, fontSize: 14 }}
                      >
                        {"Tên thuốc\nLiều dùng"}
                      </span>
                      <span style={{ color: textColor2, fontSize: 14 }}>
                        {"Số lượng\nCách dùng"}
                      </span>
                    </div>
                  </div>
                  {listDrug}
                </div>
              </div>
            ) : null}
            {textDrugs.length > 0 ? renderListExtraDrug(textDrugs) : null}
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsParaclinical = (item) => {
    let result = null;
    if (item && item.paraclinical && item.user) {
      let paraId = item.paraclinical.simple_code
        ? item.paraclinical.simple_code
        : "";
      let paraNote = item.paraclinical.note ? item.paraclinical.note : "";
      let paraType = "";
      let paraName = "";
      if (item.paraclinical.services && item.paraclinical.services.length > 0) {
        paraType = item.paraclinical.services[0].vi_group_name;
        paraName = item.paraclinical.services[0].vi_name;
      }
      result = (
        <Fragment>
          {getUserInfo(item.user, "Thông tin người dùng")}
          <div className="containerItem">
            <span className="titleItem">{"Chỉ định cận lâm sàng"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã cận lâm sàng:"}</span>
              <span className="value">{paraId}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Loại:"}</span>
              <span className="value">{paraType}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Tên:"}</span>
              <span className="value">{paraName}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Lời dặn:"}</span>
              <span className="value">{paraNote}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsService = (item) => {
    let result = null;
    if (item && item.order && item.order.code) {
      let address = "";
      let fullname = func.getFullName(item.order);
      let phone = item.order.phone;
      if (item.order.location) {
        let loc = item.order.location;
        address = loc.address ? loc.address + " " : "";
        address += loc.ward && loc.ward.vi_name ? loc.ward.vi_name + " " : "";
        address +=
          loc.district && loc.district.vi_name
            ? loc.district.vi_name + " "
            : "";
        address +=
          loc.province && loc.province.vi_name
            ? loc.province.vi_name + " "
            : "";
      }
      let serID = item.order.code;
      let serName = item.order.service.title;
      let serProvider =
        item.order.service.provider.name +
        "\n" +
        item.order.service.provider.address;
      let serArea = item.order.service.area;
      let serType = "";
      if (item.order.service.type === 1) {
        serType = "Tại nhà";
      } else if (item.order.service.type === 2) {
        serType = "Tại phòng khám";
      }
      result = (
        <Fragment>
          <div className="containerItem">
            <span className="titleItem">{"Thông tin người đặt"}</span>
            <div className="itemDetail">
              <span className="name">{"Họ tên:"}</span>
              <span className="value">{fullname}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Số điện thoại:"}</span>
              <span className="value">{phone}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Địa chỉ:"}</span>
              <span className="value">{address}</span>
            </div>
          </div>

          <div className="containerItem">
            <span className="titleItem">{"Thông tin dịch vụ"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã đặt dịch vụ:"}</span>
              <span className="value">{serID}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Tên dịch vụ:"}</span>
              <span className="value">{serName}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Đơn vị cung cấp:"}</span>
              <span className="value">{serProvider}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Loại dịch vụ:"}</span>
              <span className="value">{serType}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Khu vực:"}</span>
              <span className="value">{serArea}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsAppointment = (item) => {
    let result = null;
    if (item && item.appointment && item.appointment.code) {
      let address = "";
      let fullname = func.getFullName(item.appointment);
      let phone = item.appointment.phone;
      if (item.appointment.location) {
        let loc = item.appointment.location;
        address = getAddress(loc);
      }
      let appoID = item.appointment.code;
      let appoDoctor = func.getFullName(item.appointment.doctor);
      let appoTime = func.isoDateToString(
        item.appointment.appointment_time,
        "HH:mm DD/MM/YYYY"
      );
      let appoReason = item.appointment.reason;

      result = (
        <Fragment>
          <div className="containerItem">
            <span className="titleItem">{"Thông tin người dùng"}</span>
            <div className="itemDetail">
              <span className="name">{"Họ tên:"}</span>
              <span className="value">{fullname}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Số điện thoại:"}</span>
              <span className="value">{phone}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Địa chỉ khám:"}</span>
              <span className="value">{address}</span>
            </div>
          </div>

          <div className="containerItem">
            <span className="titleItem">{"Thông tin lịch khám"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã khám bệnh:"}</span>
              <span className="value">{appoID}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Bác sĩ:"}</span>
              <span className="value">{appoDoctor}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Thời gian khám:"}</span>
              <span className="value">{appoTime}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Lý do khám:"}</span>
              <span className="value">{appoReason}</span>
            </div>
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const getDetailsApptHospital = (item) => {
    let result = [];
    if (item && item.user) {
      result.push(getUserInfo(item.user, "Thông tin người dùng"));
    }
    if (item && item.schedule_appointment_info && item.user) {
      let apptData = item.schedule_appointment_info;

      let apptId = apptData.code ? apptData.code : "";
      let apptHospname =
        apptData.source && apptData.source.name ? apptData.source.name : "";
      let apptDate = apptData.appoint_date
        ? func.isoDateToString(apptData.appoint_date, "DD/MM/YYYY")
        : "";
      let apptReason = apptData.reason ? apptData.reason : "";

      let clinic = apptData.clinic ? apptData.clinic : "";
      let numberOrder = apptData.num_order ? apptData.num_order : "";
      let doctor =
        apptData.doctor && apptData.doctor.name ? apptData.doctor.name : "";
      let suggestTime = apptData.suggest_time
        ? func.isoDateToString(apptData.suggest_time, "HH:mm")
        : "";
      let numberInsur = apptData.num_insurance ? apptData.num_insurance : "";
      let fullname = apptData.name ? apptData.name : "";
      let phone = apptData.phone ? apptData.phone : "";
      let sex = apptData.sex && apptData.sex === "female" ? "Nữ" : "Nam";
      let birthday = apptData.profile_birthday
        ? func.yieldAge(apptData.profile_birthday)
        : "";
      let address = "";
      if (apptData.location) {
        address = getAddress(apptData.location);
      }
      result.push(
        <Fragment>
          <div className="containerItem">
            <span className="titleItem">{"Thông tin bệnh nhân"}</span>
            <div className="itemDetail">
              <span className="name">{"Họ tên:"}</span>
              <span className="value">{fullname}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Tuổi:"}</span>
              <span className="value">{birthday}</span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Giới tính:"}</span>
              <span className="value">{sex}</span>
            </div>
            {phone !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Số điện thoại:"}</span>
                <span className="value">{phone}</span>
              </div>
            ) : null}
            <div className="itemDetail">
              <span className="name">{"Địa chỉ:"}</span>
              <span className="value">{address}</span>
            </div>
          </div>
          <div className="containerItem">
            <span className="titleItem">{"Thông tin đặt khám"}</span>
            <div className="itemDetail">
              <span className="name">{"Mã đặt khám:"}</span>
              <span className="value" style={{ color: textColorCancel }}>
                {apptId}
              </span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Bệnh viện:"}</span>
              <span className="value" style={{ color: textColorOH }}>
                {apptHospname}
              </span>
            </div>
            <div className="itemDetail">
              <span className="name">{"Ngày khám:"}</span>
              <span className="value" style={{ color: textColorCreate }}>
                {apptDate}
              </span>
            </div>
            {suggestTime !== "" && suggestTime !== "00:00" ? (
              <div className="itemDetail">
                <span className="name">{"Giờ khám dự kiến:"}</span>
                <span className="value" style={{ color: textColorCreate }}>
                  {suggestTime}
                </span>
              </div>
            ) : null}
            {doctor !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Bác sĩ:"}</span>
                <span className="value" style={{ color: textColorOH }}>
                  {doctor}
                </span>
              </div>
            ) : null}
            {clinic !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Phòng:"}</span>
                <span className="value" style={{ color: textColorWait }}>
                  {clinic}
                </span>
              </div>
            ) : null}
            {numberOrder !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Số thứ tự:"}</span>
                <span className="value" style={{ color: textColorWait }}>
                  {numberOrder}
                </span>
              </div>
            ) : null}
            {numberInsur !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Bảo hiểm y tế:"}</span>
                <span className="value" style={{ color: textColorMoney }}>
                  {numberInsur}
                </span>
              </div>
            ) : null}
            {apptReason !== "" ? (
              <div className="itemDetail">
                <span className="name">{"Lý do khám:"}</span>
                <span className="value">{apptReason}</span>
              </div>
            ) : null}
          </div>
        </Fragment>
      );
    }
    return result;
  };

  const renderDetails = () => {
    if (paymentData) {
      return <Fragment>{getDetails(paymentData)}</Fragment>;
    } else {
      return null;
    }
  };

  const renderPromotion = () => {
    if (
      paymentData &&
      paymentData.promotion &&
      paymentData.promotion.discount
    ) {
      let procode = paymentData.promotion;
      let discount = "";
      if (procode.discountType === "amount") {
        discount = func.numberToMoney(procode.discount) + "đ";
      } else {
        discount = procode.discount + "%";
      }
      return (
        <div className="containerItem">
          <span className="titleItem">{"Khuyến mãi"}</span>
          <div className="itemDetail">
            <span className="name">{"Mã khuyễn mãi:"}</span>
            <span className="value">{procode.promotionCode}</span>
          </div>
          <div className="itemDetail">
            <span className="name">{"Giảm giá:"}</span>
            <span className="value" style={{ color: textColorCancel }}>
              {discount}
            </span>
          </div>
        </div>
      );
    } else {
      return null;
    }
  };

  const renderHead = () => {
    if (paymentData) {
      let sttColor = textColor1;
      let sttText = "";
      let stt = paymentData.status;
      if (stt === "new") {
        sttColor = textColorWait;
        sttText = "Chưa thanh toán";
      } else if (stt === "paid") {
        sttColor = textColorCreate;
        sttText = "Đã thanh toán";
      } else if (stt === "cancel") {
        sttColor = textColorCancel;
        sttText = "Đã hủy";
      }

      return (
        <div className="containerItem">
          <span className="titleItem">{getTitle(paymentData.invoiceType)}</span>
          <div className="itemDetail">
            <div className="name">{"Mã hóa đơn:"}</div>
            <div className="value" style={{ color: textColorWait }}>
              {paymentData.code}
            </div>
          </div>
          <div className="itemDetail">
            <span className="name">{"Trạng thái:"}</span>
            <span className="value" style={{ color: sttColor }}>
              {sttText}
            </span>
          </div>
          <div className="itemDetail">
            <span className="name">{"Thời gian:"}</span>
            <span className="value">
              {func.isoDateToString(
                paymentData.created_time,
                "HH:mm DD/MM/YYYY"
              )}
            </span>
          </div>
          {paymentData.promotion ? (
            <div>
              <div className="itemDetail">
                <span className="name">{"Giá tiền:"}</span>
                <span className="value">
                  {func.numberToMoney(paymentData.originCredit) + "đ"}
                </span>
              </div>
              <div className="itemDetail">
                <span className="name">{"Khuyến mãi:"}</span>
                <span className="value">
                  {func.numberToMoney(paymentData.discountCredit) + "đ"}
                </span>
              </div>
              <div className="itemDetail">
                <span className="name">{"Thành tiền:"}</span>
                <span className="value" style={{ color: textColorMoney }}>
                  {func.numberToMoney(Math.round(paymentData.totalCredit)) +
                    "đ"}
                </span>
              </div>
            </div>
          ) : (
            <div className="itemDetail">
              <span className="name">{"Thành tiền:"}</span>
              <span className="value" style={{ color: textColorMoney }}>
                {func.numberToMoney(Math.round(paymentData.totalCredit)) + "đ"}
              </span>
            </div>
          )}
        </div>
      );
    } else {
      return null;
    }
  };

  const renderMain = () => {
    return (
      <Fragment>
        {renderHead()}
        {renderPromotion()}
        {renderDetails()}
      </Fragment>
    );
  };

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Chi tiết giao dịch</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-detail-invoice">
                <BoxSkeleton
                  skeleton={skeleton}
                  full={true}
                  length={2}
                  rows={4}
                  data={renderMain()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(DetailInvoicePage)
);
