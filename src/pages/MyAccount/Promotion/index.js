import React, {Fragment} from "react";
import TabPanePromotion from "../../../components/Promotion/TabPanePromotion";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import "./style.scss";

const PromotionPage = (props) => {
    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Ưu đãi</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-promotion">
                                <TabPanePromotion {...props}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(PromotionPage));
