import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { Empty } from "antd";
import { url } from "../../../../constants/Path";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import Loading from "../../../../components/Loading";
import Pagination from "react-js-pagination";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";
import { getListHistoryDrug } from "../../../../services/DrugService";

//*** Declare constant ***//
const LIMIT = 14;

const HistoryDrug = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHistoryCall, setListHistoryCall] = useState([]);
  const [totalRecord, setTotalRecord] = useState(0);
  const [page, setPage] = useState(1);
  //*** Declare props ***//
  const { _TokenExpired, user, history } = props;

  const handleListHistoryCall = useCallback(
    async (page, trigger = false) => {
      const id = user.personal._id;
      if (id) {
        const reqParamFt = {
          page: page,
          user: id,
          limit: LIMIT,
        };
        setSkeleton(true);
        const resultFt = await getListHistoryDrug(reqParamFt);
        try {
          if (!isRendered.current) {
            const dataListHistoryCall = resultFt.one_health_msg.list;
            const count = resultFt.one_health_msg.count;
            setTotalRecord(count > LIMIT ? count : 0);
            setListHistoryCall(dataListHistoryCall);
            setSkeleton(false);
          }
        } catch (error) {
          if (!isRendered.current) {
            if (trigger && error.code === "007") {
              _TokenExpired(error);
            }
            if (error.code !== "007") {
              console.log("HistoryCallPage - ListHistoryCall");
              setSkeleton(false);
            }
          }
        }
      }
      return () => {
        isRendered.current = true;
      };
    },
    [_TokenExpired, user.personal._id]
  );

  useEffect(() => {
    if (!isRendered.current) {
      handleListHistoryCall(1);
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListHistoryCall]);

  const onNextPage = (page) => {
    setPage(page);
    handleListHistoryCall(page, true);
  };

  const onPressDetail = (item) => {
    const id = item._id;
    setLoading(true);
    history.push({
      pathname: `${url.name.service_drug_history_details}/${id}`,
    });
  };
  function yieldListHistoryCall() {
    if (listHistoryCall.length > 0) {
      return _.map(listHistoryCall, (item, index) => {
        let status = "Chờ liên hệ";
        let colorStatus = "colorCreate"; // lime
        if (item && item.pres_status_new) {
          if (item.pres_status_new === "draft") {
            status = "Chờ nhập thuốc";
            colorStatus = "colorCreate"; // lime
          } else if (item.pres_status_new === "wait_confirm") {
            status = "Chờ xác nhận";
            colorStatus = "colorConfirm"; // Green
          } else if (item.pres_status_new === "confirmed") {
            status = "Đã xác nhận";
            colorStatus = "colorWait"; // Blue Sky
          } else if (item.pres_status_new === "delivery") {
            status = "Chờ giao hàng";
            colorStatus = "colorDelivery"; // Purpil
          } else if (item.pres_status_new === "done") {
            status = "Hoàn thành";
            colorStatus = "colorFinish"; // Blue
          } else if (item.pres_status_new === "cancel") {
            status = "Hủy";
            colorStatus = "colorCancel"; // Red
          } else if (item.pres_status_new === "refund") {
            status = "Đã hoàn tiền";
            colorStatus = "colorRefund"; // orange
          }
        }
        return (
          <div
            key={index}
            onClick={onPressDetail.bind(this, item)}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-info">
                <div className="wrap-line">
                  <p className="label w-100px">Mã đơn hàng:</p>
                  <p className="value color-f96268">{item.code}</p>
                </div>
                <div className="wrap-line">
                  <p className="label w-100px">Ngày đặt:</p>
                  <p className="value">
                    {item.created_time
                      ? func.convertTime(new Date(item.created_time).getTime())
                      : ""}
                  </p>
                </div>
                <div className="wrap-line">
                  <p className="label w-100px">Trạng thái:</p>
                  <p className={colorStatus + " value"}>{status}</p>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Lịch sử đặt hàng trống"
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Lịch sử đặt hàng toa thuốc</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-list-history-drug">
                <div className="wrap-panel-content">
                  <div className="row">
                    <BoxSkeleton
                      skeleton={skeleton}
                      full={false}
                      length={16}
                      rows={1}
                      data={yieldListHistoryCall()}
                    />
                  </div>
                </div>
                <div
                  className={
                    totalRecord ? "block-pagination" : "block-pagination d-none"
                  }
                >
                  <Pagination
                    hideDisabled
                    innerClass="pagination"
                    activePage={page}
                    itemsCountPerPage={LIMIT}
                    totalItemsCount={totalRecord}
                    pageRangeDisplayed={4}
                    onChange={onNextPage}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HistoryDrug)
);
