import React, { Fragment, useEffect, useRef, useState } from "react";
import { Button, Modal } from "antd";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import Loading from "../../../../components/Loading";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import { paymentServices } from "../../../../constants/Data";
import "./style.scss";
import {
  getDetailsHistoryDrug,
  patchCancelDrug,
} from "../../../../services/DrugService";

const DetailHistoryDrugPage = (props) => {
  const isRendered = useRef(false);
  const id = props.computedMatch.params.drugId;
  const [loading, setLoading] = useState(false);
  const [detailHistoryDrug, setDetailHistoryDrug] = useState(undefined);
  //*** Declare props ***//
  const storageUser = JSON.parse(localStorage.getItem("user"));
  const token =
    storageUser.personal && storageUser.personal.token
      ? func.decryptParam(storageUser.personal.token)
      : null;

  const [modal, contextHolder] = Modal.useModal();
  useEffect(() => {
    async function getApi() {
      const detailHistoryDrug = await getDetailsHistoryDrug(id);
      setDetailHistoryDrug(detailHistoryDrug.one_health_msg);
    }
    getApi();
    return () => {
      isRendered.current = true;
    };
  }, [id]);

  const handConfirm = async () => {
    try {
      const data = {
        one_health_msg: {
          byUser: true,
          paymentType: "prepay",
        },
      };
      patchCancelDrug(id, data);
      modal.success({
        title: "Thành công",
        okText: "Đồng ý",
        centered: true,
        content: "Bạn đã đặt đơn hàng thành công",
      });
      detailHistoryDrug.pres_status_new = "confirmed";
      detailHistoryDrug.paymentType = "prepay";
      setDetailHistoryDrug({ ...detailHistoryDrug });
    } catch (error) {}
  };

  const handCancel = async () => {
    try {
      const data = {
        one_health_msg: {
          pres_status_new: "cancel",
        },
      };
      patchCancelDrug(id, data);
      modal.success({
        title: "Thành công",
        okText: "Đồng ý",
        centered: true,
        content: "Bạn đã hủy đơn hàng thành công",
      });
      detailHistoryDrug.pres_status_new = "cancel";
      detailHistoryDrug.paymentType = "postpaid";
      setDetailHistoryDrug({ ...detailHistoryDrug });
    } catch (error) {}
  };

  const handPopupConfirm = () => {
    modal.confirm({
      title: "Xác nhận",
      okText: "Đồng ý",
      cancelText: "Thoát",
      onOk: handConfirm,
      centered: true,
      content: "Bạn có chắc muốn đặt giao hàng?",
    });
  };

  const handPopupCancel = () => {
    modal.confirm({
      title: "Thông báo",
      okText: "Đồng ý",
      cancelText: "Thoát",
      onOk: handCancel,
      centered: true,
      content: "Bạn có chắc muốn hủy đơn hàng?",
    });
  };

  const infoOrder = () => {
    let status = "Chờ liên hệ";
    let colorStatus = "colorCreate"; // lime
    if (detailHistoryDrug && detailHistoryDrug.pres_status_new) {
      if (detailHistoryDrug.pres_status_new === "draft") {
        status = "Chờ nhập thuốc";
        colorStatus = "colorCreate"; // lime
      } else if (detailHistoryDrug.pres_status_new === "wait_confirm") {
        status = "Chờ xác nhận";
        colorStatus = "colorConfirm"; // Green
      } else if (detailHistoryDrug.pres_status_new === "confirmed") {
        status = "Đã xác nhận";
        colorStatus = "colorWait"; // Blue Sky
      } else if (detailHistoryDrug.pres_status_new === "delivery") {
        status = "Chờ giao hàng";
        colorStatus = "colorDelivery"; // Purpil
      } else if (detailHistoryDrug.pres_status_new === "done") {
        status = "Hoàn thành";
        colorStatus = "colorFinish"; // Blue
      } else if (detailHistoryDrug.pres_status_new === "cancel") {
        status = "Hủy";
        colorStatus = "colorCancel"; // Red
      } else if (detailHistoryDrug.pres_status_new === "refund") {
        status = "Đã hoàn tiền";
        colorStatus = "colorRefund"; // orange
      }
    }
    return (
      <div className="wrap-main">
        <div className="wrap-line ">
          <p className="label">Mã đơn hàng</p>
          <p className="value">{detailHistoryDrug.code}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Ngày đặt</p>
          <p className="value">
            {func.convertTime(
              new Date(detailHistoryDrug.created_time).getTime()
            )}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Người đặt</p>
          <p className="value">{detailHistoryDrug.info.name}</p>
        </div>
        <div className="wrap-line ">
          <p className="label">Trạng thái</p>
          <p className={colorStatus + " value"}>{status}</p>
        </div>
        <div className="wrap-line drop-line">
          <p className="label">Địa chỉ</p>
          <p className="value">{detailHistoryDrug.address}</p>
        </div>
      </div>
    );
  };

  function yielddetailHistoryDrug() {
    if (detailHistoryDrug) {
      return (
        <Fragment>
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  <h4>
                    <FaRegUserCircle /> Thông tin đơn hàng
                  </h4>
                </div>
                {infoOrder()}
              </div>
            </div>
            {!detailHistoryDrug.note && detailHistoryDrug.note !== "" && (
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item">
                  <div className="wrap-top">
                    <h4>
                      <FaRegListAlt /> Ghi chú
                    </h4>
                  </div>
                  <div className="wrap-main">
                    <div className="wrap-line">
                      <p>{detailHistoryDrug.note}</p>
                    </div>
                  </div>
                </div>
              </div>
            )}
            {detailHistoryDrug.textDrugs.length > 0 && (
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item">
                  <div className="wrap-top">
                    <h4>
                      <FaRegListAlt /> Thuốc ngoài toa
                    </h4>
                  </div>
                  <div className="wrap-main">
                    <table className="table">
                      <thead>
                        <tr>
                          <th>STT</th>
                          <th>Tên thuốc</th>
                          <th>Số lượng</th>
                        </tr>
                      </thead>
                      <tbody>
                        {detailHistoryDrug.textDrugs.map((drug, index) => (
                          <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{drug.name}</td>
                            <td>{drug.quantity}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            )}
            {detailHistoryDrug.images.length > 0 && (
              <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                <div className="wrap-panel-item">
                  <div className="wrap-top">
                    <h4>
                      <FaRegListAlt /> Hình ảnh toa thuốc
                    </h4>
                  </div>
                  <div className="wrap-main">
                    <div className="wrap-line">
                      <img
                        src={`${detailHistoryDrug.images[0].url}?oh_token=${token}`}
                        alt="#"
                        width="100%"
                        className="img-drug"
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}
            {detailHistoryDrug.prescription.length > 0 && (
              <>
                <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                  <div className="wrap-panel-item">
                    <div className="wrap-top">
                      <h4>
                        <FaRegListAlt /> Toa thuốc đã được bác sĩ chỉ định
                      </h4>
                    </div>
                    <div className="wrap-main">
                      <div className="wrap-line">
                        <table className="table">
                          <thead>
                            <tr>
                              <th>STT</th>
                              <th>
                                Tên thuốc
                                <br />
                                Liều dùng
                              </th>
                              <th>
                                Số lượng <br /> Cách dùng
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {detailHistoryDrug.prescription.map(
                              (drug, index) => (
                                <tr key={index}>
                                  {console.log(drug)}
                                  <td>{index + 1}</td>
                                  <td className="w-65">
                                    <div>{drug.drug.original_name}</div>
                                    <div className="row px-2">
                                      <div className="col-3">
                                        Sáng:{" "}
                                        {drug.how_use.morning.quantity ?? 0}
                                      </div>
                                      <div className="col-3">
                                        Trưa:{" "}
                                        {drug.how_use.morning.afternoon ?? 0}
                                      </div>
                                      <div className="col-3">
                                        Chiều:{" "}
                                        {drug.how_use.morning.evening ?? 0}
                                      </div>
                                      <div className="col-3">
                                        Tối: {drug.how_use.morning.night ?? 0}
                                      </div>
                                    </div>
                                  </td>
                                  <td className="colorConfirm">
                                    <div>
                                      {drug.total_quantity} {drug.unit}
                                    </div>
                                    <div>{drug.usage}</div>
                                  </td>
                                </tr>
                              )
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                  <div className="wrap-panel-item">
                    <div className="wrap-top">
                      <h4>
                        <FaRegListAlt /> Hình thức thanh toán
                      </h4>
                    </div>
                    <div className="wrap-main">
                      <div className="wrap-line">
                        {detailHistoryDrug.pres_status_new ===
                          "wait_confirm" && (
                          <div className="payment-select">
                            <p>Thanh Toán</p>
                            <select>
                              {paymentServices().map((payment, index) => (
                                <option key={index} value={payment.value}>
                                  {payment.text}
                                </option>
                              ))}
                            </select>
                          </div>
                        )}
                        {detailHistoryDrug.pres_status_new !==
                          "wait_confirm" && (
                          <p className="font-bold">
                            {detailHistoryDrug.paymentType
                              ? detailHistoryDrug.paymentType === "postpaid"
                                ? "Thanh toán trả sau"
                                : "Thanh toán trả trước"
                              : ""}
                          </p>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col payment">
                  <div className="wrap-panel-item">
                    <div className="wrap-top">
                      <h4>
                        <FaRegListAlt /> Thanh toán
                      </h4>
                    </div>
                    <div className="wrap-main">
                      {detailHistoryDrug.originAmount && (
                        <div className="wrap-line">
                          <p className="label">Giá tiền:</p>
                          <p className="value">
                            {func.formatNumberToMoney(
                              detailHistoryDrug.originAmount
                            ) + "đ"}
                          </p>
                        </div>
                      )}
                      {detailHistoryDrug.discountAmount && (
                        <div className="wrap-line">
                          <p className="label">Khuyến mãi:</p>
                          <p className="value">
                            {func.formatNumberToMoney(
                              detailHistoryDrug.discountAmount
                            ) + "đ"}
                          </p>
                        </div>
                      )}
                      {detailHistoryDrug.shipPrice && (
                        <div className="wrap-line">
                          <p className="label">Phí vận chuyển:</p>
                          <p className="value">
                            {func.formatNumberToMoney(
                              detailHistoryDrug.shipPrice
                            ) + "đ"}
                          </p>
                        </div>
                      )}
                      <div className="wrap-line">
                        <p className="label">Thành tiền:</p>
                        <p className="value colorCancel">
                          {detailHistoryDrug.totalAmount &&
                            func.formatNumberToMoney(
                              detailHistoryDrug.totalAmount
                            ) + "đ"}
                          {!detailHistoryDrug.totalAmount && "0đ"}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xl-12 wrap-panel-col">
                  {detailHistoryDrug.pres_status_new === "wait_confirm" && (
                    <Button
                      className="btn-accept w-100 mt-1"
                      htmlType="submit"
                      onClick={handPopupConfirm}
                    >
                      <span>Xác nhận giao hàng</span>
                    </Button>
                  )}
                  {(detailHistoryDrug.pres_status_new === "draft" ||
                    detailHistoryDrug.pres_status_new === "wait_confirm") && (
                    <Button
                      className="btn-danger w-100 mt-2"
                      htmlType="submit"
                      onClick={handPopupCancel}
                    >
                      <span>Hủy đơn hàng</span>
                    </Button>
                  )}
                </div>
              </>
            )}
          </div>
        </Fragment>
      );
    }
    return null;
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Chi tiết đơn hàng</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content history-drug-details">
                <div className="wrap-panel-content">
                  {yielddetailHistoryDrug()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(DetailHistoryDrugPage)
);
