import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import i18next from "i18next";
import QRCode from "qrcode.react";
import {nhi_dong} from "../../../../constants/Default";
import {Modal} from "antd";
import {msg_text} from "../../../../constants/Message";
import {url} from "../../../../constants/Path";
import {FaRegListAlt, FaRegUserCircle} from "react-icons/fa";
import Loading from "../../../../components/Loading";
import {ModelCancelExaminationSchedule, ModelDetailExaminationSchedule} from "../../../../models/ModelExamination";
import {Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const Barcode = require('react-barcode');

const DetailHistoryExaminationPage = (props) => {
    const isRendered = useRef(false);
    const [loading, setLoading] = useState(false);
    const [modalGuide, setModalGuide] = useState(false);
    const [iframeGuide, setIframeGuide] = useState(undefined);
    const [typeCode, setTypeCode] = useState("barcode");
    const [detailHistoryExamination, setDetailHistoryExamination] = useState(undefined);
    //*** Declare props ***//
    const {_TokenExpired, _BalanceUser, _BadgeNotification, location, history} = props;

    const [modal, contextHolder] = Modal.useModal();

    const handleDetailHistoryExamination = useCallback((id, type = "") => {
        setLoading(true);
        ModelDetailExaminationSchedule(id).then(resultFt => {
            const dataDetailAppointmentSchedule = resultFt.data.one_health_msg;
            if (type !== "notify") {
                modal.success({
                    title: "Thành công",
                    okText: "Đóng",
                    centered: true,
                    content: msg_text.cancel_appointment_success
                });
                history.push({
                    pathname: url.name.account_detail_history_examination,
                    state: dataDetailAppointmentSchedule
                });
            }
            setDetailHistoryExamination(dataDetailAppointmentSchedule);
            setLoading(false);
        }).catch(error => {
            if (error.code !== "007") {
                console.log("DetailHistoryExaminationPage - DetailExaminationSchedule");
                setLoading(false);
            } else {
                _TokenExpired(error);
            }
        });
    }, [_TokenExpired, history, modal]);

    //*** Handle yield detail history examination ***//
    useEffect(() => {
        if (!isRendered.current) {
            if (location.state) {
                const detailID = location.state.id ? location.state.id : "";
                if (detailID) {
                    handleDetailHistoryExamination(detailID, "notify");
                } else {
                    const detailItem = location.state ? location.state : "";
                    setDetailHistoryExamination(detailItem);
                }
            }
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleDetailHistoryExamination, location]);

    const onOpenGuide = () => {
        setIframeGuide(detailHistoryExamination.source.guide);
        setModalGuide(true);
    };

    const onCloseModalGuide = () => {
        setModalGuide(false);
    };

    const onAcceptAbortAppointment = () => {
        const id = detailHistoryExamination._id;
        setLoading(true);
        ModelCancelExaminationSchedule(id).then(() => {
            _BalanceUser();
            _BadgeNotification();
            handleDetailHistoryExamination(id);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description ? error.description : msg_text.cancel_appointment_error;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    const onConfirmAbortAppointment = () => {
        modal.confirm({
            title: "Thông báo",
            okText: "Đồng ý",
            cancelText: "Huỷ",
            onOk: onAcceptAbortAppointment,
            centered: true,
            content: detailHistoryExamination.source.msg_cancel
        });
    };

    const onPressTypeCode = type => {
        setTypeCode(type);
    };

    function yieldDetailHistoryExamination() {
        if (detailHistoryExamination) {
            const status            = detailHistoryExamination.status;
            const allow_cancel      = detailHistoryExamination.source.allow_cancel;
            const profile_name      = detailHistoryExamination.profile.name ? detailHistoryExamination.profile.name : detailHistoryExamination.profile.names[0];
            const profile_age       = detailHistoryExamination.profile.birthday;
            const profile_insurance = detailHistoryExamination.profile.num_insurance ? detailHistoryExamination.profile.num_insurance : "-";
            const profile_sex       = detailHistoryExamination.profile.sex;
            let yieldAbortAppointment;
            if (status === "registed" && allow_cancel) {
                yieldAbortAppointment = (
                    <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={onConfirmAbortAppointment} className="ant-btn btn-danger"><span>Hủy đặt khám</span>
                                    </div>
                                    <div onClick={onOpenGuide} className="ant-btn btn-accept"><span>Hướng dẫn</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }

            let yieldNoteText;
            if (detailHistoryExamination.source.note) {
                yieldNoteText = (
                    <Fragment>
                        <p className="note-label"><strong>Ghi chú</strong></p>
                        <p className="note-text">{detailHistoryExamination.source.note}</p>
                    </Fragment>
                );
            }

            let yieldSpecExam, yieldAreaExam;
            if (detailHistoryExamination.source.key === nhi_dong.key) {
                yieldSpecExam = (<div className="wrap-line">
                    <p className="label">Phân khu</p>
                    <p className="value">{detailHistoryExamination.exam_area ? detailHistoryExamination.exam_area.name : "-"}</p>
                </div>);
                yieldAreaExam = (<div className="wrap-line">
                    <p className="label">Chi tiết khám</p>
                    <p className="value">{detailHistoryExamination.extra ? detailHistoryExamination.extra.speciality : "-"}</p>
                </div>);
            } else {
                yieldSpecExam = (<div className="wrap-line">
                    <p className="label">Chuyên khoa</p>
                    <p className="value">{detailHistoryExamination.exam_area ? detailHistoryExamination.exam_area.name : "-"}</p>
                </div>);
            }

            let yieldTypeCode, yieldTypeCodeButton;
            if (typeCode === "barcode") {
                yieldTypeCode = (<Barcode value={detailHistoryExamination.code}/>);
                yieldTypeCodeButton = (<span onClick={onPressTypeCode.bind(this, "qr")} className="ant-btn ant-btn-primary">QR</span>);
            } else {
                yieldTypeCode = (<Fragment><QRCode value={detailHistoryExamination.code}/>
                    <div className="text-center mt-1" style={{font: '20px monospace', fontWeight: 500}}>{detailHistoryExamination.code}</div></Fragment>);
                yieldTypeCodeButton = (<span onClick={onPressTypeCode.bind(this, "barcode")} className="ant-btn ant-btn-primary">Barcode</span>);
            }

            return (
                <Fragment>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                            <div className="wrap-panel-item">
                                <div className="wrap-top">
                                    <h4><FaRegUserCircle/> Thông tin bệnh nhân</h4>
                                </div>
                                <div className="wrap-main">
                                    <div className="wrap-line drop-line">
                                        <p className="label">Họ và tên</p>
                                        <p className="value">{profile_name}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Tuổi</p>
                                        <p className="value">{func.yieldAge(profile_age)}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Giới tính</p>
                                        <p className="value">{func.findGender(profile_sex, "text")}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Mã BHYT</p>
                                        <p className="value">{profile_insurance}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                            <div className="wrap-panel-item">
                                <div className="wrap-top">
                                    <h4><FaRegListAlt/> Thông tin lịch khám</h4>
                                </div>
                                <div className="wrap-main">
                                    <div className="wrap-line">
                                        <p className="label">Mã bệnh nhân</p>
                                        <p className="value patient-code">{detailHistoryExamination.patient_code}</p>
                                    </div>
                                    <div className="wrap-line barcode">
                                        <p className="label">
                                            <span>Mã đặt khám</span>
                                            {yieldTypeCodeButton}
                                        </p>
                                        {yieldTypeCode}
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Bệnh viện</p>
                                        <p className="value">{detailHistoryExamination.source.name}</p>
                                    </div>
                                    {yieldSpecExam}
                                    {yieldAreaExam}
                                    <div className="wrap-line">
                                        <p className="label">Loại khám</p>
                                        <p className="value">{detailHistoryExamination.exam_type.name}</p>
                                    </div>
                                    <div className="wrap-line drop-line">
                                        <p className="label">Ngày giờ khám</p>
                                        <p className="value datetime">{func.yieldDatetimeSchedule(detailHistoryExamination.appoint_date)}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Trạng thái</p>
                                        <p className="value">
                                    <span className={func.findStatusAppointment(status, "color") + " badge"}>
                                        {func.findStatusAppointment(status, "text")}
                                    </span>
                                        </p>
                                    </div>
                                </div>
                                <div className="wrap-bot">
                                    {yieldNoteText}
                                </div>
                            </div>
                        </div>
                        {yieldAbortAppointment}
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    if (!location.state) {
        return <Redirect to={url.name.account_list_history_examination}/>;
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Chi tiết đặt khám</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-detail-history-examination">
                                <div className="wrap-panel-content">
                                    {yieldDetailHistoryExamination()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Modal title="Hướng dẫn đặt khám tại bệnh viện"
                   className="block-modal-guide-appointment ant-modal-custom"
                   closable={true}
                   width={600}
                   footer={null}
                   visible={modalGuide}
                   onCancel={onCloseModalGuide}>
                <iframe src={iframeGuide}
                        width="100%" height="100%"
                        title="Hướng dẫn các bước thực hiện khám chữa bệnh"/>
            </Modal>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(DetailHistoryExaminationPage));
