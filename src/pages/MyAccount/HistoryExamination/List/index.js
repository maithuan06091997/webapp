import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react"; // , useCallback, useEffect, useRef, useState
import _ from "lodash";
import { Empty } from "antd";
import { url } from "../../../../constants/Path";
import Loading from "../../../../components/Loading";
import {
  dataSortHospital,
  dataStatusAppointment,
} from "../../../../constants/Data";
import Select from "react-select";
import Pagination from "react-js-pagination";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import {
  ModelDetailExaminationSchedule,
  ModelListHistoryExamination,
} from "../../../../models/ModelExamination";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 16;

const HistoryExaminationPage = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHistoryExamination, setListHistoryExamination] = useState([]);
  const [selectSortHospital, setSelectSortHospital] = useState(
    dataSortHospital(this)[0]
  );
  const [selectSortStatus, setSelectSortStatus] = useState(
    dataStatusAppointment(this)[0]
  );
  const [totalRecord, setTotalRecord] = useState(0);
  const [page, setPage] = useState(1);
  //*** Declare props ***//
  const { _TokenExpired, user, history } = props;

  const handleListHistoryExamination = useCallback(
    (page, source = "", status = "", trigger = false) => {
      const id = user.personal._id;
      if (id) {
        const reqParamFt = {
          page: page,
          user: id,
          limit: LIMIT,
        };
        if (source) {
          reqParamFt.source = source;
        }
        if (status) {
          reqParamFt.status = status;
        }
        setSkeleton(true);
        ModelListHistoryExamination(reqParamFt)
          .then((resultFt) => {
            if (!isRendered.current) {
              const dataListHistoryExamination =
                resultFt.data.one_health_msg.list;
              const count = resultFt.data.one_health_msg.count;
              setTotalRecord(count > LIMIT ? count : 0);
              setListHistoryExamination(dataListHistoryExamination);
              setSkeleton(false);
            }
            return null;
          })
          .catch((error) => {
            if (!isRendered.current) {
              if (trigger && error.code === "007") {
                _TokenExpired(error);
              }
              if (error.code !== "007") {
                console.log("HistoryExaminationPage - ListHistoryExamination");
                setSkeleton(false);
              }
            }
          });
      }
      return () => {
        isRendered.current = true;
      };
    },
    [_TokenExpired, user.personal._id]
  );

  useEffect(() => {
    if (!isRendered.current) {
      handleListHistoryExamination(1);
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListHistoryExamination]);

  const onNextPage = (page) => {
    setPage(page);
    let valueSortHospital, valueSortStatus;
    if (selectSortHospital.value !== "all") {
      valueSortHospital = selectSortHospital.value;
    }
    if (selectSortStatus.value !== "all") {
      valueSortStatus = selectSortStatus.value;
    }
    handleListHistoryExamination(
      page,
      valueSortHospital,
      valueSortStatus,
      true
    );
  };

  const onSelectSortStatus = (values) => {
    setSelectSortStatus(values);
    setPage(1);
    let valueSortHospital, valueSortStatus;
    if (values.value !== "all") {
      valueSortStatus = values.value;
    }
    if (selectSortHospital.value !== "all") {
      valueSortHospital = selectSortHospital.value;
    }
    handleListHistoryExamination(1, valueSortHospital, valueSortStatus, true);
  };

  const onSelectSortHospital = (values) => {
    setSelectSortHospital(values);
    setPage(1);
    let valueSortHospital, valueSortStatus;
    if (values.value !== "all") {
      valueSortHospital = values.value;
    }
    if (selectSortStatus.value !== "all") {
      valueSortStatus = selectSortStatus.value;
    }
    handleListHistoryExamination(1, valueSortHospital, valueSortStatus, true);
  };

  const onPressDetail = (item) => {
    const id = item._id;
    setLoading(true);
    ModelDetailExaminationSchedule(id)
      .then((resultFt) => {
        const dataDetailExaminationSchedule = resultFt.data.one_health_msg;
        history.push({
          pathname: url.name.account_detail_history_examination,
          state: dataDetailExaminationSchedule,
        });
      })
      .catch((error) => {
        if (error.code !== "007") {
          console.log("HistoryExaminationPage - DetailExaminationSchedule");
          setLoading(false);
        } else {
          _TokenExpired(error);
        }
      });
  };

  function yieldListHistoryExamination() {
    if (listHistoryExamination.length > 0) {
      return _.map(listHistoryExamination, (item, index) => {
        const hospitalName = item.source.name;
        const statusText = func.findStatusAppointment(item.status, "text");
        const statusColor = func.findStatusAppointment(item.status, "color");
        const appointDate = func.isoDateToString(
          item.appoint_date,
          "DD/MM/YYYY"
        );
        const appointTime = item.time;
        const time = func.formatTimePeriod(item.appoint_date);
        return (
          <div
            key={index}
            onClick={onPressDetail.bind(this, item)}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-info">
                <div className="wrap-line">
                  <h4>{hospitalName}</h4>
                </div>
                <div className="wrap-line">
                  <p className="label">Trạng thái:</p>
                  <p className={statusColor + " badge"}>{statusText}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Ngày khám:</p>
                  <p className="value">
                    {appointTime + " - " + appointDate} ({time})
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Lịch sử đặt khám trống"
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Lịch sử đặt khám</h3>
            </div>
            <div className="card-body">
              <div className="wrap-filter-history-examination">
                <div className="wrap-hospital">
                  <div className="title">
                    <span>Bệnh viện</span>
                  </div>
                  <Select
                    value={selectSortHospital}
                    onChange={onSelectSortHospital}
                    options={dataSortHospital(this)}
                  />
                </div>
                <div className="wrap-status">
                  <div className="title">
                    <span>Trạng thái</span>
                  </div>
                  <Select
                    value={selectSortStatus}
                    onChange={onSelectSortStatus}
                    options={dataStatusAppointment(this)}
                  />
                </div>
              </div>
              <div className="wrap-content block-list-history-examination">
                <div className="wrap-panel-content">
                  <div className="row">
                    <BoxSkeleton
                      skeleton={skeleton}
                      full={false}
                      length={16}
                      rows={1}
                      data={yieldListHistoryExamination()}
                    />
                  </div>
                </div>
                <div
                  className={
                    totalRecord ? "block-pagination" : "block-pagination d-none"
                  }
                >
                  <Pagination
                    hideDisabled
                    innerClass="pagination"
                    activePage={page}
                    itemsCountPerPage={LIMIT}
                    totalItemsCount={totalRecord}
                    pageRangeDisplayed={4}
                    onChange={onNextPage}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HistoryExaminationPage)
);
