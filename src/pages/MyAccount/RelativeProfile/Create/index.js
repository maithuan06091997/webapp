import React, { Fragment } from "react";
import FormCreateProfile from "../../../../components/RelativeProfile/FormCreateProfile";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { AiOutlineArrowLeft } from "react-icons/ai";

const CreateRelativeProfilePage = (props) => {
  const { history } = props;
  const back = () => {
    history.goBack();
  };

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>
                <span className="back-button" onClick={back}>
                  <AiOutlineArrowLeft />
                </span>{" "}
                Tạo mới hồ sơ người thân
              </h3>
            </div>
            <div className="card-body scrollbar-1">
              <div className="wrap-content block-create-relative-profile">
                <FormCreateProfile {...props} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(CreateRelativeProfilePage)
);
