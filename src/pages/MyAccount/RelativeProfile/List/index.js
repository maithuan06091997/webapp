import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import _ from "lodash";
import i18next from "i18next";
import {Empty, Modal} from "antd";
import {url} from "../../../../constants/Path";
import {msg_text} from "../../../../constants/Message";
import {FaChevronRight, FaTrashAlt} from "react-icons/fa";
import {AiFillPlusCircle} from "react-icons/ai";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import Loading from "../../../../components/Loading";
import {ModelListProfile, ModelDeleteProfile} from "../../../../models/ModelProfile";
import {_FindProfile} from "../../../../actions";
import {bindActionCreators} from "redux";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const RelativeProfilePage = (props) => {
    const isRendered = useRef(false);
    const [loading, setLoading] = useState(false);
    const [skeleton, setSkeleton] = useState(false);
    const [listProfile, setListProfile] = useState([]);
    //*** Declare props ***//
    const {_TokenExpired, _FindProfile, history} = props;

    const [modal, contextHolder] = Modal.useModal();

    const handleListProfile = useCallback(() => {
        setSkeleton(true);
        ModelListProfile().then(resultFt => {
            if (!isRendered.current) {
                const dataListProfile = resultFt.data.one_health_msg.list;
                setListProfile(dataListProfile);
                setSkeleton(false);
            }
            return null;
        }).catch(error => {
            if (!isRendered.current) {
                if (error.code !== "007") {
                    console.log("RelativeProfilePage - ListProfile");
                    setSkeleton(false);
                }
            }
        });
        return () => {
            isRendered.current = true;
        };
    }, []);

    useEffect(() => {
        if (!isRendered.current) {
            handleListProfile();
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleListProfile]);

    const onAcceptRemoveProfile = (item) => {
        const id = item._id;
        const fullName = item.last_name+ " " + item.first_name;
        setLoading(true);
        ModelDeleteProfile(id).then(() => {
            modal.success({
                title: "Thành công",
                okText: "Đóng",
                centered: true,
                content: "'"+fullName+"'. "+msg_text.delete_profile_success
            });
            handleListProfile();
            setLoading(false);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description ? error.description : msg_text.delete_profile_error;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    const onConfirmRemoveProfile = (item) => {
        const fullName = item.last_name+ " " + item.first_name;
        if (item.relationship === "owner") {
            modal.info({
                title: "Thông báo",
                okText: "Đóng",
                centered: true,
                content: "Hồ sơ '"+fullName+"' là thông tin cá nhân của tài khoản. Bạn không thể xoá hồ sơ này!"
            });
        } else {
            modal.confirm({
                title: "Thông báo",
                okText: "Đồng ý",
                cancelText: "Huỷ",
                centered: true,
                onOk: onAcceptRemoveProfile.bind(this, item),
                content: "Bạn có chắc muốn xoá hồ sơ '"+fullName+"'?"
            });
        }
    };

    const onPressCreateNew = () => {
        _FindProfile();
        const pushState = {
            return_url: url.name.account_list_relative_profile
        }
        history.push({
            pathname: url.name.account_create_relative_profile,
            state: pushState
        });
    };

    const onEditProfile = item => {
        _FindProfile();
        const pushState = {
            return_url: url.name.account_list_relative_profile,
            detail_profile: item
        }
        history.push({
            pathname: url.name.account_edit_relative_profile,
            state: pushState
        });
    };

    function yieldListProfile() {
        if (listProfile.length > 0) {
            return _.map(listProfile, (item, index) => {
                return (
                    <div key={index} className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-top">
                                <h4>
                                    {item.name ? item.name : item.names[0]}
                                    <span className="ml-1">({func.findRelationship(item.relationship, "text")})</span>
                                </h4>
                            </div>
                            <div className="wrap-main">
                                <div className="wrap-line">
                                    <p className="label">Ngày sinh</p>
                                    <p className="value">{func.formatDatetimeToString(item.birthday)}</p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Số điện thoại</p>
                                    <p className="value">
                                        {item.phone ? item.phone : item.phones.length > 0 ? item.phones[0] : "-"}
                                    </p>
                                </div>
                                <div className="wrap-line">
                                    <p className="label">Địa chỉ</p>
                                    <p className="value">{func.yieldLocation(item.location)}</p>
                                </div>
                                {item.his_profile.patient_code ? <div className="wrap-line">
                                    <p className="label"><span className="hospital-name">Hồng Đức</span></p>
                                    <p className="value"><span className="code">{item.his_profile.patient_code}</span>
                                    </p>
                                </div> : ""}
                                {item.nhi_dong.patient_code ? <div className="wrap-line">
                                    <p className="label"><span className="hospital-name">Nhi Đồng 2</span></p>
                                    <p className="value"><span className="code">{item.nhi_dong.patient_code}</span></p>
                                </div> : ""}
                                {item.ung_buou.patient_code ? <div className="wrap-line">
                                    <p className="label"><span className="hospital-name">Ung Bướu</span></p>
                                    <p className="value"><span className="code">{item.ung_buou.patient_code}</span></p>
                                </div> : ""}
                                {item.drkhoa.patient_code ? <div className="wrap-line">
                                    <p className="label"><span className="hospital-name">DrKhoa</span></p>
                                    <p className="value"><span className="code">{item.drkhoa.patient_code}</span></p>
                                </div> : ""}
                            </div>
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={onConfirmRemoveProfile.bind(this, item)}
                                         className="ant-btn btn-danger">
                                        <FaTrashAlt className="mr-2"/><span>Xoá</span>
                                    </div>
                                    <div onClick={onEditProfile.bind(this, item)}
                                         className="ant-btn btn-booking">
                                        <span>Cập nhật</span><FaChevronRight className="ml-1"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            });
        } else {
            if (!skeleton) {
                return (<Empty className="ant-empty-custom"
                               description="Hồ sơ người thân trống"/>);
            }
        }
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Hồ sơ người thân</h3>
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={onPressCreateNew}
                                         className="ant-btn btn-accept">
                                        <AiFillPlusCircle/> <span>Tạo mới hồ sơ</span></div>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content block-list-relative-profile">
                                <div className="wrap-panel-content">
                                    <div className="row">
                                        <BoxSkeleton skeleton={skeleton}
                                                     full={false}
                                                     length={8}
                                                     rows={4}
                                                     data={yieldListProfile()}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({_FindProfile}, dispatch);
};

export default withRouter(connect(null, mapDispatchToProps, null, {forwardRef: true})(RelativeProfilePage));
