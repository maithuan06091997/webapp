import React, { Fragment } from "react";
import FormEditProfile from "../../../../components/RelativeProfile/FormEditProfile";
import { url } from "../../../../constants/Path";
import { withRouter, Redirect } from "react-router-dom";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { connect } from "react-redux";
import "./style.scss";
import { Alert } from "antd";

const EditRelativeProfilePage = (props) => {
  //*** Declare props ***//
  const { location, history } = props;

  if (!location.state) {
    return <Redirect to={url.name.account_list_relative_profile} />;
  }

  const back = () => {
    history.goBack();
  };

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>
                <span className="back-button" onClick={back}>
                  <AiOutlineArrowLeft />
                </span>{" "}
                Cập nhật hồ sơ người thân
              </h3>
            </div>
            <Alert
              message="Yêu cầu cập nhật hồ sơ"
              description="Cập nhật thông tin địa chỉ để tiếp tục đặt dịch vụ"
              type="error"
              showIcon
              closable
              className="error-profile mt-2"
            />
            <div className="card-body scrollbar-1">
              <div className="wrap-content block-edit-relative-profile">
                <FormEditProfile {...props} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(EditRelativeProfilePage)
);
