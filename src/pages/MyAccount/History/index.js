import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router";
import ServiceItem from "../../../components/ServiceItem";
import HistoryServices from "./HistoryServices";
import "./style.scss";

function History(props) {
  return (
    <div className="block-home-page">
      <div className="container-fluid wrap-home-application">
        <div className="row">
          <div className="col-12">
            <h2 className="hr-title-vertical">Sức khỏe của tôi</h2>
          </div>
        </div>
        <div className="row">
          {HistoryServices.map((service, index) => (
            <ServiceItem key={index} {...service} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default withRouter(
  connect(null, null, null, { forwardRef: true })(History)
);
