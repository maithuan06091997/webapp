import { url, public_url } from "../../../constants/Path";

const HistoryServices = [
  {
    url: url.name.account_list_history_examination,
    img: public_url.img + "/myhealth/ic_appt_hos.png",
    title: "Lịch Sử Đặt Khám",
  },
  {
    url: url.name.account_list_home_test,
    img: public_url.img + "/myhealth/ic_appt_home.png",
    title: "Danh sách xét nghiệm",
  },
  {
    url: url.name.service_drug_history,
    img: public_url.img + "/myhealth/ic_drug.png",
    title: "Danh sách đặt thuốc",
  },
  {
    url: url.name.account_list_history_call,
    img: public_url.img + "/myhealth/ic_video_call.png",
    title: "Lịch Sử Cuộc Gọi",
  },
  {
    url: url.name.my_questions,
    img: public_url.img + "/myhealth/ic_question.png",
    title: "Thông tin hỏi đáp",
  },
  {
    url: url.name.service_health_history,
    img: public_url.img + "/myhealth/ic_service.png",
    title: "Danh sách dịch vụ",
  },
];

export default HistoryServices;
