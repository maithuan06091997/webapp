import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { Empty } from "antd";
import { url } from "../../../../constants/Path";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import Loading from "../../../../components/Loading";
import {
  ModelDetailVideoCallSchedule,
  ModelListHistoryCall,
} from "../../../../models/ModelCallChat";
import Pagination from "react-js-pagination";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

//*** Declare constant ***//
const LIMIT = 16;

const HistoryCallPage = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHistoryCall, setListHistoryCall] = useState([]);
  const [totalRecord, setTotalRecord] = useState(0);
  const [page, setPage] = useState(1);
  //*** Declare props ***//
  const { _TokenExpired, user, history } = props;

  const handleListHistoryCall = useCallback(
    (page, trigger = false) => {
      const id = user.personal._id;
      if (id) {
        const reqParamFt = {
          page: page,
          user: id,
          limit: LIMIT,
        };
        setSkeleton(true);
        ModelListHistoryCall(reqParamFt)
          .then((resultFt) => {
            if (!isRendered.current) {
              console.log(resultFt);
              const dataListHistoryCall = resultFt.data.one_health_msg.list;
              const count = resultFt.data.one_health_msg.count;
              setTotalRecord(count > LIMIT ? count : 0);
              setListHistoryCall(dataListHistoryCall);
              setSkeleton(false);
            }
            return null;
          })
          .catch((error) => {
            if (!isRendered.current) {
              if (trigger && error.code === "007") {
                _TokenExpired(error);
              }
              if (error.code !== "007") {
                console.log("HistoryCallPage - ListHistoryCall");
                setSkeleton(false);
              }
            }
          });
      }
      return () => {
        isRendered.current = true;
      };
    },
    [_TokenExpired, user.personal._id]
  );

  useEffect(() => {
    if (!isRendered.current) {
      handleListHistoryCall(1);
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListHistoryCall]);

  const onNextPage = (page) => {
    setPage(page);
    handleListHistoryCall(page, true);
  };

  const onPressDetail = (item) => {
    const id = item._id;
    setLoading(true);
    ModelDetailVideoCallSchedule(id)
      .then((resultFt) => {
        const dataDetailVideoCallSchedule = resultFt.data.one_health_msg;
        history.push({
          pathname: url.name.account_detail_history_call,
          state: dataDetailVideoCallSchedule,
        });
      })
      .catch((error) => {
        if (error.code !== "007") {
          console.log("HistoryCallPage - DetailVideoCallSchedule");
          setLoading(false);
        } else {
          _TokenExpired(error);
        }
      });
  };

  function yieldListHistoryCall() {
    if (listHistoryCall.length > 0) {
      return _.map(listHistoryCall, (item, index) => {
        const doctorName = item.doctor.last_name + " " + item.doctor.first_name;
        const callStatusText = func.findStatusVideoCall(item.status, "text");
        const callStatusColor = func.findStatusVideoCall(item.status, "color");
        const startTime = func.isoDateToString(
          item.start_time,
          "HH:mm - DD/MM/YYYY"
        );
        const time = func.formatTimePeriod(item.start_time);
        return (
          <div
            key={index}
            onClick={onPressDetail.bind(this, item)}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-info">
                <div className="wrap-line">
                  <p className="label">Bác sĩ:</p>
                  <p className="value">{doctorName}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Trạng thái:</p>
                  <p className={callStatusColor + " badge"}>{callStatusText}</p>
                </div>
                <div className="wrap-line">
                  <p className="label">Lịch hẹn:</p>
                  <p className="value">
                    {startTime} ({time})
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Lịch sử cuộc gọi trống"
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Lịch sử cuộc gọi</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-list-history-call">
                <div className="wrap-panel-content">
                  <div className="row">
                    <BoxSkeleton
                      skeleton={skeleton}
                      full={false}
                      length={16}
                      rows={1}
                      data={yieldListHistoryCall()}
                    />
                  </div>
                </div>
                <div
                  className={
                    totalRecord ? "block-pagination" : "block-pagination d-none"
                  }
                >
                  <Pagination
                    hideDisabled
                    innerClass="pagination"
                    activePage={page}
                    itemsCountPerPage={LIMIT}
                    totalItemsCount={totalRecord}
                    pageRangeDisplayed={4}
                    onChange={onNextPage}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HistoryCallPage)
);
