import React, {Fragment, useCallback, useEffect, useRef, useState} from "react";
import moment from "moment";
import i18next from "i18next";
import {Modal} from "antd";
import {url} from "../../../../constants/Path";
import {FaRegListAlt, FaRegUserCircle} from "react-icons/fa";
import {msg_text} from "../../../../constants/Message";
import {ModelCancelVideoCallSchedule, ModelDetailVideoCallSchedule} from "../../../../models/ModelCallChat";
import Loading from "../../../../components/Loading";
import {Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";

const DetailHistoryCallPage = (props) => {
    const isRendered = useRef(false);
    const [loading, setLoading] = useState(false);
    const [detailHistoryCall, setDetailHistoryCall] = useState(undefined);
    //*** Declare props ***//
    const {_TokenExpired, _BalanceUser, _BadgeNotification, location, history} = props;

    const [modal, contextHolder] = Modal.useModal();

    const handleDetailHistoryCall = useCallback((id, type = "") => {
        setLoading(true);
        ModelDetailVideoCallSchedule(id).then(resultFt => {
            const dataDetailVideoCallSchedule = resultFt.data.one_health_msg;
            if (type !== "notify") {
                modal.success({
                    title: "Thành công",
                    okText: "Đóng",
                    centered: true,
                    content: msg_text.cancel_video_call_success
                });
                history.push({
                    pathname: url.name.account_detail_history_call,
                    state: dataDetailVideoCallSchedule
                });
            }
            setDetailHistoryCall(dataDetailVideoCallSchedule);
            setLoading(false);
        }).catch(error => {
            if (error.code !== "007") {
                console.log("DetailHistoryCallPage - DetailVideoCallSchedule");
                setLoading(false);
            } else {
                _TokenExpired(error);
            }
        });
    }, [_TokenExpired, history, modal]);

    useEffect(() => {
        if (!isRendered.current) {
            if (location.state) {
                const detailID = location.state.id ? location.state.id : "";
                if (detailID) {
                    handleDetailHistoryCall(detailID, "notify");
                } else {
                    const detailItem = location.state ? location.state : "";
                    setDetailHistoryCall(detailItem);
                }
            }
        }
        return () => {
            isRendered.current = true;
        };
    }, [handleDetailHistoryCall, location]);

    const onAcceptAbortAppointment = () => {
        const id = detailHistoryCall._id;
        setLoading(true);
        ModelCancelVideoCallSchedule(id).then(() => {
            _BalanceUser();
            _BadgeNotification();
            handleDetailHistoryCall(id);
        }).catch(error => {
            if (error.code === "007") {
                _TokenExpired(error);
            } else {
                const msg = error.message ? error.message : error.description ? error.description : msg_text.cancel_video_call_error;
                modal.warning({
                    title: "Thông báo",
                    okText: "Đóng",
                    centered: true,
                    content: i18next.t(msg)
                });
                setLoading(false);
            }
        });
    };

    const onConfirmAbortAppointment = () => {
        modal.confirm({
            title: "Thông báo",
            okText: "Đồng ý",
            cancelText: "Huỷ",
            onOk: onAcceptAbortAppointment,
            centered: true,
            content: "Bạn có chắc chắn muốn hủy lịch gọi video?"
        });
    };

    function yieldDetailHistoryCall() {
        if (detailHistoryCall) {
            const status            = detailHistoryCall.status;
            const profile_name      = detailHistoryCall.profile.last_name + " " + detailHistoryCall.profile.first_name;
            const profile_age       = detailHistoryCall.profile.birthday;
            const profile_sex       = detailHistoryCall.profile.sex;
            const profile_location  = func.yieldLocation(detailHistoryCall.profile.location);
            const schedule_created  = func.yieldDatetimeSchedule(detailHistoryCall.created_time);
            const doctor_name       = detailHistoryCall.doctor.last_name + " " + detailHistoryCall.doctor.first_name;
            const appt_date         = moment(detailHistoryCall.start_time).format("DD/MM/YYYY");
            const appt_hour_start   = moment(detailHistoryCall.start_time).format("HH:mm");
            const appt_hour_end     = moment(detailHistoryCall.end_time).format("HH:mm");
            const invoiceDiscount   = detailHistoryCall.invoice.discountMoney;
            const invoiceAmount     = detailHistoryCall.invoice.totalMoney;

            let yieldAbortAppointment;
            if (status === "new") {
                yieldAbortAppointment = (
                    <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
                        <div className="wrap-panel-item">
                            <div className="wrap-event-action">
                                <div className="box-button-event">
                                    <div onClick={onConfirmAbortAppointment} className="ant-btn btn-danger">
                                        <span>Hủy lịch hẹn</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            return (
                <Fragment>
                    <div className="row">
                        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                            <div className="wrap-panel-item">
                                <div className="wrap-top">
                                    <h4><FaRegUserCircle/> Thông tin bệnh nhân</h4>
                                </div>
                                <div className="wrap-main">
                                    <div className="wrap-line drop-line">
                                        <p className="label">Họ và tên</p>
                                        <p className="value">{profile_name}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Tuổi</p>
                                        <p className="value">{func.yieldAge(profile_age)}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Giới tính</p>
                                        <p className="value">{func.findGender(profile_sex, "text")}</p>
                                    </div>
                                    <div className="wrap-line drop-line">
                                        <p className="label">Địa chỉ</p>
                                        <p className="value">{profile_location}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
                            <div className="wrap-panel-item">
                                <div className="wrap-top">
                                    <h4><FaRegListAlt/> Thông tin lịch khám</h4>
                                </div>
                                <div className="wrap-main">
                                    <div className="wrap-line">
                                        <p className="label">Ngày đặt lịch</p>
                                        <p className="value">{schedule_created}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Bác sĩ</p>
                                        <p className="value">{doctor_name}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Ngày hẹn</p>
                                        <p className="value datetime">{appt_date}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Giờ hẹn</p>
                                        <p className="value datetime">{appt_hour_start + " - " + appt_hour_end}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Trạng thái</p>
                                        <p className="value">
                                            <span
                                                className={func.findStatusVideoCall(status, "color") + " badge"}>
                                                {func.findStatusVideoCall(status, "text")}
                                            </span>
                                        </p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Khuyến mãi</p>
                                        <p className="value">{invoiceDiscount.toLocaleString() + " đ"}</p>
                                    </div>
                                    <div className="wrap-line">
                                        <p className="label">Thành tiền</p>
                                        <p className="value">{invoiceAmount.toLocaleString() + " đ"}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {yieldAbortAppointment}
                    </div>
                </Fragment>
            );
        }
        return null;
    }

    if (!location.state) {
        return <Redirect to={url.name.account_list_history_call}/>;
    }

    return (
        <Fragment>
            <div className="block-my-account">
                <div className="block-my-account-panel">
                    <div className="card card-custom">
                        <div className="card-header">
                            <h3>Chi tiết cuộc gọi</h3>
                        </div>
                        <div className="card-body">
                            <div className="wrap-content  block-detail-history-call">
                                <div className="wrap-panel-content">
                                    {yieldDetailHistoryCall()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Loading open={loading}/>
            {contextHolder}
        </Fragment>
    );
};

export default withRouter(connect(null, null, null, {forwardRef: true})(DetailHistoryCallPage));
