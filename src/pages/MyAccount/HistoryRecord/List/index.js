import React, {
  Fragment,
  useCallback,
  useEffect,
  useRef,
  useState,
} from "react";
import _ from "lodash";
import { Empty } from "antd";
import { url } from "../../../../constants/Path";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import Loading from "../../../../components/Loading";
import Pagination from "react-js-pagination";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { getListHistoryRecord } from "../../../../services/HomeTestService";
import moment from "moment";

//*** Declare constant ***//
const LIMIT = 14;

const HistoryRecord = (props) => {
  const isRendered = useRef(false);
  const [loading, setLoading] = useState(false);
  const [skeleton, setSkeleton] = useState(false);
  const [listHistoryRecord, setListHistoryRecord] = useState([]);
  const [totalRecord, setTotalRecord] = useState(0);
  const [page, setPage] = useState(1);
  //*** Declare props ***//
  const { _TokenExpired, user, history } = props;

  const handleListHistoryRecord = useCallback(
    async (page, trigger = false) => {
      const id = user.personal._id;
      if (id) {
        const reqParamFt = {
          page: page,
          user: id,
          limit: LIMIT,
          category_type: "take_blood",
        };
        setSkeleton(true);
        const resultFt = await getListHistoryRecord(reqParamFt);
        try {
          if (!isRendered.current) {
            const dataListHistoryRecord = resultFt.one_health_msg;
            const count = resultFt.one_health_msg.total;
            setTotalRecord(count > LIMIT ? count : 0);
            setListHistoryRecord(dataListHistoryRecord);
            setSkeleton(false);
          }
        } catch (error) {
          if (!isRendered.current) {
            if (trigger && error.code === "007") {
              _TokenExpired(error);
            }
            if (error.code !== "007") {
              console.log("HistoryRecordPage - ListHistoryRecord");
              setSkeleton(false);
            }
          }
        }
      }
      return () => {
        isRendered.current = true;
      };
    },
    [_TokenExpired, user.personal._id]
  );

  useEffect(() => {
    if (!isRendered.current) {
      handleListHistoryRecord(1);
    }
    return () => {
      isRendered.current = true;
    };
  }, [handleListHistoryRecord]);

  const onNextPage = (page) => {
    setPage(page);
    handleListHistoryRecord(page, true);
  };

  const onPressDetail = (item) => {
    const id = item._id;
    setLoading(true);
    history.push({
      pathname: `${url.name.account_details_home_test}/${id}`,
    });
  };
  function yieldListHistoryRecord() {
    if (listHistoryRecord.length > 0) {
      return _.map(listHistoryRecord, (item, index) => {
        let status = "Chờ liên hệ";
        let colorStatus = "colorCreate"; // lime
        if (item && item.status) {
          if (item.status === 1) {
            status = "Mới tạo";
            colorStatus = "colorCreate"; // green
          } else if (item.status === 2) {
            status = "Đã đăng ký";
            colorStatus = "colorConfirm"; // sky
          } else if (
            item.status === 3 ||
            item.status === 4 ||
            item.status === 6
          ) {
            status = "Đã hủy";
            colorStatus = "colorCancel"; // red
          } else if (item.status === 5) {
            status = "Đã hoàn thành";
            colorStatus = "colorFinish"; // purpil
          }
        }

        return (
          <div
            key={index}
            onClick={onPressDetail.bind(this, item)}
            className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col"
          >
            <div className="wrap-panel-item">
              <div className="wrap-item-info">
                <div className="wrap-line">
                  <p className="label w-100px">Mã đăng ký:</p>
                  <p className="value color-f96268">{item.code}</p>
                </div>
                <div className="wrap-line">
                  <p className="label w-100px">Dịch vụ:</p>
                  <p className="value">{item.service.title}</p>
                </div>
                <div className="wrap-line">
                  <p className="label w-100px">Trạng thái:</p>
                  <p className={colorStatus + " value"}>{status}</p>
                </div>
                <div className="wrap-line">
                  <p className="label w-100px">Lịch hẹn:</p>
                  <p className="value">
                    {`${item.takeblood_info.time_appointment}, ${moment(
                      new Date(item.takeblood_info.date_appointment)
                    ).format("DD/MM/YYYY")}`}
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else {
      if (!skeleton) {
        return (
          <Empty
            className="ant-empty-custom"
            description="Lịch sử đặt hàng trống"
          />
        );
      }
    }
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Lịch sử lấy máu</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content block-list-history-drug">
                <div className="wrap-panel-content">
                  <div className="row">
                    <BoxSkeleton
                      skeleton={skeleton}
                      full={false}
                      length={16}
                      rows={1}
                      data={yieldListHistoryRecord()}
                    />
                  </div>
                </div>
                <div
                  className={
                    totalRecord ? "block-pagination" : "block-pagination d-none"
                  }
                >
                  <Pagination
                    hideDisabled
                    innerClass="pagination"
                    activePage={page}
                    itemsCountPerPage={LIMIT}
                    totalItemsCount={totalRecord}
                    pageRangeDisplayed={4}
                    onChange={onNextPage}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(HistoryRecord)
);
