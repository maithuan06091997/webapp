import React, { Fragment, useEffect, useState } from "react";
import { Button, Modal } from "antd";
import { FaRegUserCircle } from "react-icons/fa";
import Loading from "../../../../components/Loading";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../../../helpers";
import "./style.scss";
import {
  getDetailsHistoryRecord,
  patchCancelRecord,
} from "../../../../services/HomeTestService";
import moment from "moment";

const DetailHistoryRecordPage = (props) => {
  const id = props.computedMatch.params.recordId;
  const [loading, setLoading] = useState(false);
  const [detailHistoryRecord, setDetailHistoryRecord] = useState(undefined);
  //*** Declare props ***//

  const [modal, contextHolder] = Modal.useModal();
  useEffect(() => {
    async function getApi() {
      const detailHistoryRecord = await getDetailsHistoryRecord(id, {
        checkinvoice: true,
      });
      setDetailHistoryRecord(detailHistoryRecord.one_health_msg[0]);
    }
    getApi();
  }, [id]);

  const handCancel = async () => {
    setLoading(true);
    try {
      await patchCancelRecord(id);
      modal.success({
        title: "Thành công",
        okText: "Đồng ý",
        centered: true,
        content: "Bạn đã hủy lịch hẹn thành công",
      });
      detailHistoryRecord.status = "3";
      setDetailHistoryRecord({ ...detailHistoryRecord });
      setLoading(false);
    } catch (error) {}
  };

  const handPopupCancel = () => {
    modal.confirm({
      title: "Thông báo",
      okText: "Đồng ý",
      cancelText: "Thoát",
      onOk: handCancel,
      centered: true,
      content: "Bạn có chắc muốn hủy lịch hẹn?",
    });
  };

  const infoOrder = () => {
    return (
      <div className="wrap-main">
        <div className="wrap-line ">
          <p className="label">Họ và tên</p>
          <p className="value">{detailHistoryRecord.profile.name}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Tuổi</p>
          <p className="value">
            {func.yieldAge(detailHistoryRecord.profile.birthday)} tuổi
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Giới tính</p>
          <p className="value">
            {func.findGender(detailHistoryRecord.profile.sex, "text")}
          </p>
        </div>

        <div className="wrap-line ">
          <p className="label">Số điện thoại</p>
          <p className="value">{detailHistoryRecord.profile.phone}</p>
        </div>
      </div>
    );
  };

  const infoRecord = () => {
    let status = "Chờ liên hệ";
    let colorStatus = "colorCreate"; // lime
    if (detailHistoryRecord.profile && detailHistoryRecord.profile.status) {
      if (detailHistoryRecord.profile.status === 1) {
        status = "Mới tạo";
        colorStatus = "colorCreate"; // green
      } else if (detailHistoryRecord.profile.status === 2) {
        status = "Đã đăng ký";
        colorStatus = "colorConfirm"; // sky
      } else if (
        detailHistoryRecord.profile.status === 3 ||
        detailHistoryRecord.profile.status === 4 ||
        detailHistoryRecord.profile.status === 6
      ) {
        status = "Đã hủy";
        colorStatus = "colorCancel"; // red
      } else if (detailHistoryRecord.profile.status === 5) {
        status = "Đã hoàn thành";
        colorStatus = "colorFinish"; // purpil
      }
    }
    return (
      <div className="wrap-main">
        <div className="wrap-line">
          <p className="label w-100px">Mã đăng ký:</p>
          <p className="value color-f96268">{detailHistoryRecord.code}</p>
        </div>
        <div className="wrap-line">
          <p className="label w-100px">Dịch vụ:</p>
          <p className="value">{detailHistoryRecord.service.title}</p>
        </div>
        <div className="wrap-line">
          <p className="label w-100px">Trạng thái:</p>
          <p className={colorStatus + " value"}>{status}</p>
        </div>
        <div className="wrap-line">
          <p className="label w-100px">Lịch hẹn:</p>
          <p className="value">
            {`${detailHistoryRecord.takeblood_info.time_appointment}, ${moment(
              new Date(detailHistoryRecord.takeblood_info.date_appointment)
            ).format("DD/MM/YYYY")}`}
          </p>
        </div>
        <div className="wrap-line drop-line">
          <p className="label">Địa chỉ</p>
          <p className="value">{detailHistoryRecord.takeblood_info.address}</p>
        </div>
      </div>
    );
  };

  function yielddetailHistoryRecord() {
    if (detailHistoryRecord) {
      return (
        <Fragment>
          <div className="row">
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  <h4>
                    <FaRegUserCircle /> Thông tin đơn hàng
                  </h4>
                </div>
                {infoOrder()}
              </div>
            </div>
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col">
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  <h4>
                    <FaRegUserCircle /> Thông tin lịch hẹn
                  </h4>
                </div>
                {infoRecord()}
              </div>
            </div>
            <div className="col-xl-12 wrap-panel-col">
              {detailHistoryRecord.status === 1 && (
                <Button
                  className="btn-danger w-100 mt-2"
                  htmlType="submit"
                  onClick={handPopupCancel}
                >
                  <span>Hủy lịch hẹn</span>
                </Button>
              )}
            </div>
          </div>
        </Fragment>
      );
    }
    return null;
  }

  return (
    <Fragment>
      <div className="block-my-account">
        <div className="block-my-account-panel">
          <div className="card card-custom">
            <div className="card-header">
              <h3>Chi tiết đơn hàng</h3>
            </div>
            <div className="card-body">
              <div className="wrap-content history-drug-details">
                <div className="wrap-panel-content">
                  {yielddetailHistoryRecord()}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading open={loading} />
      {contextHolder}
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(DetailHistoryRecordPage)
);
