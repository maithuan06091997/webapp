import { Empty } from "antd";
import React, { useEffect, useState } from "react";
import { AiOutlineArrowLeft } from "react-icons/ai";
import Pagination from "react-js-pagination";
import { withRouter } from "react-router";
import BoxSkeleton from "../../../components/BoxSkeleton";
import { url } from "../../../constants/Path";
import { renderStatus } from "../../../helpers";
import { getListHealthServicesOrder } from "../../../services/HealthService";
import "./style.scss";

function HistoryServices(props) {
  const user = JSON.parse(localStorage.getItem("user"));
  const userId = user.personal._id;
  const limit = 12;
  const [activePage, setActivePage] = useState(1);

  const [listHealthServicesByCategory, setListHealthServicesByCategory] =
    useState();

  useEffect(() => {
    async function getApi() {
      const listHealthServicesByCategory = await getListHealthServicesOrder({
        user: userId,
        page: activePage,
        limit: limit,
      });
      setListHealthServicesByCategory(listHealthServicesByCategory);
    }
    getApi();
  }, [activePage, userId]);

  const handlePageChange = (pageNumber) => {
    setListHealthServicesByCategory();
    setActivePage(pageNumber);
  };

  const onOpenModalDetailVoucher = (item) => {
    props.history.push(
      `${url.name.service_health_history_details}/${item._id}`
    );
  };

  const back = () => {
    props.history.goBack();
  };

  const renderItemHealthServicesByCategory = () => {
    if (listHealthServicesByCategory) {
      if (
        Array.isArray(listHealthServicesByCategory.one_health_msg) &&
        listHealthServicesByCategory.one_health_msg.length > 0
      ) {
        return listHealthServicesByCategory.one_health_msg.map(
          (item, index) => (
            <div
              key={index}
              onClick={() => onOpenModalDetailVoucher(item)}
              className="col-xl-4 col-lg-4 col-md-4 wrap-panel-col"
            >
              <div className="wrap-panel-item">
                <div className="wrap-top">
                  {item.service && item.service.service_img && (
                    <img
                      className="img-fluid"
                      src={item.service.service_img.url}
                      alt=""
                    />
                  )}
                  {!item.service_img && !item.service.service_img && (
                    <div></div>
                  )}
                </div>
                <div className="wrap-main">
                  <div className="label">
                    {item.service && item.service.title
                      ? item.service.title
                      : ""}
                  </div>
                  <div className="info">
                    <div>
                      Giá: <span>{item.total_money.toLocaleString()} đ</span>
                    </div>
                    <div className="">
                      Trạng thái: {renderStatus(item.status)}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        );
      } else {
        return (
          <Empty className="ant-empty-custom" description="Chưa có dịch vụ" />
        );
      }
    }
  };

  const renderListHealthServicesByCategory = () => {
    return (
      <div className="wrap-panel-line">
        <div className="wrap-content-header">
          <div className="wrap-content-body w-100">
            <div className="row">{renderItemHealthServicesByCategory()}</div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className="block-my-account">
      <div className="block-my-account-panel">
        <div className="card card-custom">
          <div className="card-header">
            <h3>
              <span className="back-button" onClick={back}>
                <AiOutlineArrowLeft />
              </span>{" "}
              Lịch sử dịch vụ
            </h3>
          </div>
          <div className="card-body">
            <div className="wrap-panel-content health-category-order">
              <div className="row ">
                <BoxSkeleton
                  skeleton={listHealthServicesByCategory ? false : true}
                  length={limit}
                  rows={3}
                  panelCol={3}
                  data={renderListHealthServicesByCategory()}
                />
              </div>
              {listHealthServicesByCategory && (
                <div className={"block-pagination"}>
                  <Pagination
                    hideDisabled
                    innerClass="pagination"
                    activePage={activePage}
                    itemsCountPerPage={limit}
                    totalItemsCount={
                      listHealthServicesByCategory.one_health_msg.total
                    }
                    pageRangeDisplayed={4}
                    onChange={handlePageChange}
                    itemClass="page-item"
                    linkClass="page-link"
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(HistoryServices);
