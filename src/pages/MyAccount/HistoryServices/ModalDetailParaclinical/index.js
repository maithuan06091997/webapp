import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import Loading from "../../../../components/Loading";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { _CategoryService } from "../../../../actions";
import { bindActionCreators } from "redux";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";
import { getListParaclinical } from "../../../../services/HealthService";
import { getCountDateToNow, getFullName } from "../../../../helpers";
import { url } from "../../../../constants/Path";

const ModalDetailParaclinical = (props) => {
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalDetailVoucher, setModalDetailVoucher] = useState(false);
  const [infoDetailHealthServices, setInfoDetailHealthServices] =
    useState(undefined);
  //*** Declare props ***//
  const {
    onOpenModalDetailParaclinical,
    openModalDetailParaclinical,
    orderId,
    userId,
  } = props;

  useEffect(() => {
    async function getApi() {
      if (openModalDetailParaclinical) {
        setLoading(true);
        setSkeleton(true);
        setModalDetailVoucher(openModalDetailParaclinical);
        const postData = {
          one_health_msg: {
            search: {
              patient: {
                id: userId,
              },
            },
          },
        };
        const listParaclinical = await getListParaclinical(orderId, postData);
        setInfoDetailHealthServices(listParaclinical.one_health_msg);
        setLoading(false);
        setSkeleton(false);
      }
    }
    getApi();
  }, [openModalDetailParaclinical, userId, orderId]);

  const onCloseModalDetailVoucher = () => {
    setInfoDetailHealthServices(undefined);
    onOpenModalDetailParaclinical(false);
    setModalDetailVoucher(false);
  };

  function yieldDetailVoucher() {
    if (infoDetailHealthServices) {
      return infoDetailHealthServices.map((item, index) => {
        let headerTime = item.created_time
          ? getCountDateToNow(new Date(item.created_time).getTime())
          : "";
        let nameSubclinical = "";
        if (item.services && item.services[0]) {
          if (item.services[0].vi_name) {
            nameSubclinical = item.services[0].vi_name.trim();
          } else if (item.services[0].en_name) {
            nameSubclinical = item.services[0].en_name.trim();
          } else if (item.services[0].insurance_name) {
            nameSubclinical = item.services[0].insurance_name.trim();
          }
        }
        let nameDoctor = getFullName(item && item.doctor ? item.doctor : "");
        let status = "";
        let colorStatus = "colorCreate";
        if (item && item.status) {
          if (item.status === 1) {
            status = "Chờ xác nhận";
            colorStatus = "colorCreate";
          } else if (item.status === 2) {
            status = "Thanh toán trả trước";
            colorStatus = "colorCancel";
          } else if (item.status === 3) {
            status = "Thanh toán tại bệnh viện";
            colorStatus = "colorCancel";
          } else if (item.status === 4) {
            status = "Đã đăng ký tại bệnh viện";
            colorStatus = "colorConfirm";
          } else if (item.status === 5 && item.send_patient === false) {
            status = "Chờ trả kết quả";
            colorStatus = "colorFinish";
          } else if (item.status === 5 && item.send_patient === true) {
            status = "Đã có kết quả";
            colorStatus = "colorFinish";
          } else if (item.status === 6) {
            status = "Hủy";
            colorStatus = "colorCancel";
          } else if (item.status === 7) {
            status = "Đã hoàn tiền";
            colorStatus = "colorRefund";
          }
        }
        return (
          <Link to="#" className="list-paraclinical" key={index}>
            <h4>{nameSubclinical}</h4>
            <p>
              Trạng thái: <span className={colorStatus}>{status}</span>
            </p>
            <div className="time">
              <p>
                {nameDoctor.trim() !== ""
                  ? "Bác sĩ: " + nameDoctor
                  : "Được đăng ký từ dịch vụ"}
              </p>
              <p>{headerTime}</p>
            </div>
          </Link>
        );
      });
    } else {
      return null;
    }
  }

  return (
    <Fragment>
      <Modal
        title="Danh sách cận lâm sàng"
        className="block-modal-detail-voucher"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modalDetailVoucher}
        onCancel={onCloseModalDetailVoucher}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetailVoucher()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    ModalDetailParaclinical
  )
);
