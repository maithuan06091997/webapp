import React, { Fragment, useEffect, useState } from "react";
import { Modal } from "antd";
import Loading from "../../../../components/Loading";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { _CategoryService } from "../../../../actions";
import { bindActionCreators } from "redux";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "./style.scss";

const ModalStepParaclinical = (props) => {
  const [skeleton, setSkeleton] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalDetailVoucher, setModalDetailVoucher] = useState(false);

  //*** Declare props ***//
  const {
    onOpenModalDetailParaclinical,
    openModalDetailParaclinical,
    code,
  } = props;

  useEffect(() => {
    async function getApi() {
      setLoading(false);
      setSkeleton(false);
      setModalDetailVoucher(openModalDetailParaclinical);
    }
    getApi();
  }, [openModalDetailParaclinical]);

  const onCloseModalDetailVoucher = () => {
    onOpenModalDetailParaclinical(false);
    setModalDetailVoucher(false);
  };

  function yieldDetailVoucher() {
    return (
      <div>
        <ul className="progressbar">
          <li>
            <span>
              Bạn hãy đem theo mã {code} đến Bệnh viện Đa khoa Hồng Đức (32/2
              Thống Nhất, P.11, Q.Gò Vấp, Tp.HCM).
            </span>
          </li>
          <li>
            <span>
              Gặp nhân viên lễ tân để được hướng dẫn thực hiện cận lâm sàng.
            </span>
          </li>
          <li>
            <span>
              Kết quả sẽ được trả cho bạn ở bệnh viện hoặc trên ứng dụng DR.OH.
            </span>
          </li>
        </ul>
      </div>
    );
  }

  return (
    <Fragment>
      <Modal
        title="Danh sách cận lâm sàng"
        className="block-modal-detail-voucher"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={modalDetailVoucher}
        onCancel={onCloseModalDetailVoucher}
      >
        <div className="wrap-panel-content">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={skeleton}
              full={true}
              length={1}
              rows={25}
              data={yieldDetailVoucher()}
            />
          </div>
        </div>
      </Modal>
      <Loading open={loading} />
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ _CategoryService }, dispatch);
};

export default withRouter(
  connect(null, mapDispatchToProps, null, { forwardRef: true })(
    ModalStepParaclinical
  )
);
