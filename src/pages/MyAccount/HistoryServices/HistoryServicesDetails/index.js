import { Button, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import {
  cancelHealthServices,
  getDetailsOrderHealthServices,
} from "../../../../services/HealthService";
import { convertTime, getFullName, renderStatus } from "../../../../helpers";
import "./style.scss";
import ModalDetailHealthServices from "../../../Service/HealthServices/ModalDetailHealthServices";
import ModalDetailParaclinical from "../ModalDetailParaclinical";
import { AiOutlineArrowLeft } from "react-icons/ai";
import { withRouter } from "react-router-dom";

const HistoryServicesDetails = (props) => {
  const orderId = props.computedMatch.params.orderId;
  const [openModalDetailVoucher, setOpenModalDetailVoucher] = useState(false);
  const [openModalDetailParaclinical, setOpenModalDetailParaclinical] =
    useState(false);
  const [healthServiceDetails, setHealthServiceDetails] = useState();
  const [modal, contextHolder] = Modal.useModal();
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    const getApi = async () => {
      const healthServiceDetails = await getDetailsOrderHealthServices(
        orderId,
        {
          checkinvoice: true,
        }
      );
      setHealthServiceDetails(healthServiceDetails.one_health_msg[0]);
    };
    getApi();
  }, [orderId]);

  const yieldDetailsInfo = () => {
    return (
      <>
        <div className="wrap-line">
          <p className="label">Mã đặt dịch vụ</p>
          <p className="value">{healthServiceDetails.code}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Thời gian</p>
          <p className="value">
            {convertTime(new Date(healthServiceDetails.created_time).getTime())}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Hình thức thanh toán</p>
          <p className="value">
            {healthServiceDetails.payment_type === 1 ? "Trả sau" : "Trả trước"}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Trạng thái</p>
          <p className="value">{renderStatus(healthServiceDetails.status)}</p>
        </div>
      </>
    );
  };

  const yieldProfileInfo = () => {
    let location =
      healthServiceDetails && healthServiceDetails.location
        ? healthServiceDetails.location
        : null;
    let address = location && location.address ? location.address : "";
    let textWard =
      location && location.ward && location.ward.vi_name
        ? location.ward.vi_name
        : "";
    let textDistrict =
      location && location.district && location.district.vi_name
        ? location.district.vi_name
        : "";
    let textProvince =
      location && location.province && location.province.vi_name
        ? location.province.vi_name
        : "";

    let addressAll =
      address + ", " + textWard + ", " + textDistrict + ", " + textProvince;
    return (
      <>
        <div className="wrap-line">
          <p className="label">Họ và tên</p>
          <p className="value">{getFullName(healthServiceDetails)}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Số điện thoại</p>
          <p className="value">{healthServiceDetails.phone}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Địa chỉ</p>
          <p className="value">{addressAll}</p>
        </div>
      </>
    );
  };

  const onOpenModalDetailVoucher = (event, flag) => {
    event.stopPropagation();
    setOpenModalDetailVoucher(flag);
  };

  const onOpenModalDetailParaclinical = (flag) => {
    setOpenModalDetailParaclinical(flag);
  };

  const onPressCancel = async () => {
    const userId = healthServiceDetails.user._id;
    const msg = {
      one_health_msg: {
        order: orderId,
        status: 3,
      },
    };
    try {
      await cancelHealthServices(userId, msg);
      modal.success({
        title: "Thành công",
        okText: "Đồng ý",
        centered: true,
        content: "Bạn đã hủy thành công",
      });
      const newHealthServicesDetails = healthServiceDetails;
      newHealthServicesDetails.status = 3;
      setHealthServiceDetails({ ...newHealthServicesDetails });
    } catch (error) {}
  };

  // const showModalConfirm = () => {
  //   modal.confirm({
  //     title: "Thông báo",
  //     okText: "Đồng ý",
  //     cancelText: "Huỷ",
  //     onOk: onPressCancel,
  //     centered: true,
  //     content: "Bạn muốn hủy dịch vụ đã đặt",
  //   });
  // };

  const back = () => {
    props.history.goBack();
  };

  const renderServicesDetails = () => {
    if (healthServiceDetails) {
      return (
        <>
          <div className="col-xl-12 col-lg-12 col-md-12 wrap-panel-col">
            <div className="wrap-panel-item">
              <div className="wrap-item-img">
                {healthServiceDetails.service &&
                  healthServiceDetails.service.service_img && (
                    <img
                      className="img-fluid"
                      src={healthServiceDetails.service.service_img.url}
                      alt=""
                    />
                  )}
                {healthServiceDetails.service &&
                  !healthServiceDetails.service.service_img && (
                    <div className="img-fluid1"></div>
                  )}
              </div>
              <div className="wrap-item-info">
                <h4>{healthServiceDetails.service.title}</h4>
                <h4 className="sub-name">
                  {healthServiceDetails.total_money.toLocaleString()} đ
                </h4>
              </div>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Mô tả chi tiết
                </h4>
              </div>
              <div className="wrap-main">{yieldDetailsInfo()}</div>
              <div className="wrap-price">
                <Button
                  className="btn-accept w-100 mt-6"
                  onClick={(event) => onOpenModalDetailVoucher(event, true)}
                >
                  <span>Xem chi tiết</span>
                </Button>
              </div>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegUserCircle /> Thông tin người đặt
                </h4>
              </div>
              <div className="wrap-main">{yieldProfileInfo()}</div>
              <div className="wrap-price">
                <Button
                  className="btn-accept w-100"
                  onClick={() => onOpenModalDetailParaclinical(true)}
                >
                  <span>Danh sách cận lâm sàng</span>
                </Button>
                {/* {healthServiceDetails.status === 1 && (
                  <Button
                    className="btn-warning text-white w-100 mt-2"
                    onClick={showModalConfirm}
                  >
                    <span>Hủy</span>
                  </Button>
                )} */}
              </div>
            </div>
          </div>
        </>
      );
    }
  };
  return (
    <div className="block-my-account">
      <div className="block-my-account-panel">
        <div className="card card-custom">
          <div className="card-header">
            <h3>
              <span className="back-button" onClick={back}>
                <AiOutlineArrowLeft />
              </span>{" "}
              Chi tiết đặt dịch vụ
            </h3>
          </div>
          <div className="card-body">
            <div className="wrap-content package-health-service_details">
              <div className="wrap-panel-content">
                <div className="row">
                  <BoxSkeleton
                    skeleton={healthServiceDetails ? false : true}
                    full={true}
                    length={2}
                    rows={1}
                    data={renderServicesDetails()}
                  />
                </div>
              </div>
            </div>
            {healthServiceDetails && (
              <>
                <ModalDetailHealthServices
                  {...props}
                  onOpenModalDetailVoucher={onOpenModalDetailVoucher}
                  openModalDetailVoucher={openModalDetailVoucher}
                  detailHealthServices={healthServiceDetails.service}
                  token={user.token}
                />
                <ModalDetailParaclinical
                  {...props}
                  onOpenModalDetailParaclinical={onOpenModalDetailParaclinical}
                  openModalDetailParaclinical={openModalDetailParaclinical}
                  orderId={orderId}
                  userId={healthServiceDetails.user._id}
                />
              </>
            )}

            {contextHolder}
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(HistoryServicesDetails);
