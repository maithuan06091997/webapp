import { Button } from "antd";
import React, { useEffect, useState } from "react";
import { FaRegListAlt, FaRegUserCircle } from "react-icons/fa";
import BoxSkeleton from "../../../../components/BoxSkeleton";
import { getDetailsParaclinical } from "../../../../services/HealthService";
import { getFullName, findGender, yieldAge } from "../../../../helpers";
import "./style.scss";
import ModalStepParaclinical from "../ModalStepParaclinical";

const DetailParaclinical = (props) => {
  const serviceId = props.computedMatch.params.serviceId;
  const [healthServiceDetails, setHealthServiceDetails] = useState();
  const [
    openModalDetailParaclinical,
    setOpenModalDetailParaclinical,
  ] = useState(false);

  useEffect(() => {
    const getApi = async () => {
      const healthServiceDetails = await getDetailsParaclinical(serviceId, {
        checkinvoice: true,
      });
      setHealthServiceDetails(healthServiceDetails.one_health_msg[0]);
    };
    getApi();
  }, [serviceId]);
  console.log(healthServiceDetails);
  const yieldDetailsInfo = () => {
    let nameSubclinicalGroup = "";
    if (
      healthServiceDetails &&
      healthServiceDetails.services &&
      healthServiceDetails.services[0].vi_group_name
    ) {
      nameSubclinicalGroup = healthServiceDetails.services[0].vi_group_name;
    }
    let nameSubclinical = "";
    if (
      healthServiceDetails &&
      healthServiceDetails.services &&
      healthServiceDetails.services[0]
    ) {
      if (healthServiceDetails.services[0].vi_name) {
        nameSubclinical = healthServiceDetails.services[0].vi_name.trim();
      } else if (healthServiceDetails.services[0].en_name) {
        nameSubclinical = healthServiceDetails.services[0].en_name.trim();
      } else if (healthServiceDetails.services[0].insurance_name) {
        nameSubclinical = healthServiceDetails.services[0].insurance_name.trim();
      }
    }
    return (
      <>
        <div className="wrap-line">
          <p className="label">Loại</p>
          <p className="value">{nameSubclinicalGroup}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Tên</p>
          <p className="value">{nameSubclinical}</p>
        </div>
      </>
    );
  };

  const yieldProfileInfo = () => {
    let location =
      healthServiceDetails && healthServiceDetails.patient.location
        ? healthServiceDetails.patient.location
        : null;
    let address = location && location.address ? location.address : "";
    let textWard =
      location && location.ward && location.ward.vi_name
        ? location.ward.vi_name
        : "";
    let textDistrict =
      location && location.district && location.district.vi_name
        ? location.district.vi_name
        : "";
    let textProvince =
      location && location.province && location.province.vi_name
        ? location.province.vi_name
        : "";

    let addressAll =
      address + ", " + textWard + ", " + textDistrict + ", " + textProvince;
    return (
      <>
        <div className="wrap-line">
          <p className="label">Họ và tên</p>
          <p className="value">{getFullName(healthServiceDetails.patient)}</p>
        </div>
        <div className="wrap-line">
          <p className="label">Tuổi</p>
          <p className="value">
            {yieldAge(healthServiceDetails.patient.birthday)}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Giới Tính</p>
          <p className="value">
            {findGender(healthServiceDetails.patient.sex, "text")}
          </p>
        </div>
        <div className="wrap-line">
          <p className="label">Địa chỉ</p>
          <p className="value">{addressAll}</p>
        </div>
      </>
    );
  };

  const yieldNote = (note) => {
    return (
      <div className="wrap-line">
        <p className="label">{note}</p>
      </div>
    );
  };

  const yieldParaclinical = (code) => {
    return (
      <div className="wrap-line">
        <p className="label">{code}</p>
      </div>
    );
  };

  const yieldStatus = (code) => {
    let numberSTT =
      healthServiceDetails && healthServiceDetails.status
        ? healthServiceDetails.status
        : -1;
    let sentPatient =
      healthServiceDetails && healthServiceDetails.send_patient
        ? healthServiceDetails.send_patient
        : false;
    var status = "";
    var colorStatus = "colorCreate";
    if (numberSTT === 1) {
      status = "Chờ xác nhận";
      colorStatus = "colorCreate";
    } else if (numberSTT === 2) {
      status = "Thanh toán trả trước";
      colorStatus = "colorCancel";
    } else if (numberSTT === 3) {
      status = "Thanh toán tại bệnh viện";
      colorStatus = "colorCancel";
    } else if (numberSTT === 4) {
      status = "Đã đăng ký tại bệnh viện";
      colorStatus = "colorConfirm";
    } else if (numberSTT === 5 && sentPatient === false) {
      status = "Chờ trả kết quả";
      colorStatus = "colorFinish";
    } else if (numberSTT === 5 && sentPatient === true) {
      status = "Đã có kết quả";
      colorStatus = "colorFinish";
    } else if (numberSTT === 6) {
      status = "Hủy";
      colorStatus = "colorCancel";
    } else if (numberSTT === 7) {
      status = "Đã hoàn tiền";
      colorStatus = "colorRefund";
    }
    return (
      <div className="wrap-line">
        <p className={`${colorStatus} label`}>{status}</p>
      </div>
    );
  };
  const onOpenModalDetailParaclinical = (flag) => {
    setOpenModalDetailParaclinical(flag);
  };

  const renderServicesDetails = () => {
    if (healthServiceDetails) {
      let note = "";
      if (healthServiceDetails && healthServiceDetails.note) {
        note = healthServiceDetails.note;
      }
      let code = "";
      if (healthServiceDetails && healthServiceDetails.simple_code) {
        code = healthServiceDetails.simple_code;
      }
      return (
        <>
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegUserCircle /> Thông tin người đặt
                </h4>
              </div>
              <div className="wrap-main">{yieldProfileInfo()}</div>
            </div>
          </div>
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Chỉ định cận lâm sàng
                </h4>
              </div>
              <div className="wrap-main">{yieldDetailsInfo()}</div>
            </div>
          </div>
          {note && (
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
              <div className="wrap-panel-item wrap-profile-info">
                <div className="wrap-title">
                  <h4>
                    <FaRegListAlt /> Dặn dò
                  </h4>
                </div>
                <div className="wrap-main">{yieldNote(note)}</div>
              </div>
            </div>
          )}
          {code !== "" && (
            <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
              <div className="wrap-panel-item wrap-profile-info">
                <div className="wrap-title">
                  <h4>
                    <FaRegListAlt /> Mã cận lâm sàng
                  </h4>
                </div>
                <div className="wrap-main">{yieldParaclinical(code)}</div>
              </div>
            </div>
          )}
          <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col2 mt-2 pt-1">
            <div className="wrap-panel-item wrap-profile-info">
              <div className="wrap-title">
                <h4>
                  <FaRegListAlt /> Mã cận lâm sàng
                </h4>
              </div>
              <div className="wrap-main">{yieldStatus()}</div>
            </div>
          </div>
          {healthServiceDetails && healthServiceDetails.status === 2 && (
            <div className="col-12 text-center mt-4">
              <Button
                className="btn-accept  w-100"
                onClick={() => onOpenModalDetailParaclinical(true)}
              >
                Hướng dẫn CLS
              </Button>
            </div>
          )}
          {healthServiceDetails && healthServiceDetails.status === 3 && (
            <div className="col-12 text-center mt-4">
              <Button
                className="btn-accept w-100"
                onClick={() => onOpenModalDetailParaclinical(true)}
              >
                Hướng dẫn CLS
              </Button>
            </div>
          )}
        </>
      );
    }
  };
  return (
    <div className="block-my-account">
      <div className="block-my-account-panel">
        <div className="card card-custom">
          <div className="card-header">
            <h3>Chi tiết cận lâm sàng</h3>
          </div>
          <div className="card-body">
            <div className="wrap-content package-health-service_details2">
              <div className="wrap-panel-content">
                <div className="row">
                  <BoxSkeleton
                    skeleton={healthServiceDetails ? false : true}
                    length={4}
                    rows={1}
                    data={renderServicesDetails()}
                  />
                </div>
              </div>
            </div>
            {healthServiceDetails && (
              <ModalStepParaclinical
                onOpenModalDetailParaclinical={onOpenModalDetailParaclinical}
                openModalDetailParaclinical={openModalDetailParaclinical}
                code={healthServiceDetails.order.code}
                {...props}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailParaclinical;
