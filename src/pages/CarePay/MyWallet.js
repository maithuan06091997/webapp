import React, { Fragment } from "react";
import { Modal } from "antd";
import BoxSkeleton from "../../components/BoxSkeleton";

const MyWallet = (props) => {
  const { openModalWallet, setOpenModalWallet, sourceInfo } = props;

  function yieldWallet() {
    return (
      <div>
        <div className="mb-3 card-bank">
          <img src="./images/wallet/bg_card.png" alt="#" className="bg_card" />
          <div className="info">
            <h5>{sourceInfo.sourceName}</h5>
            <div className="my-3">
              <p className="mb-0">SỐ THẺ CARE PAY CỦA BẠN</p>
              <b>{sourceInfo.sourceNumber}</b>
            </div>
            <div>
              <p className="mb-0">Ngân hàng thụ hưởng</p>
              <b className="d-flex align-items-center">
                <img
                  src="./images/wallet/ic_mb.png"
                  alt="#"
                  className="img-mb"
                />
                Quân đội
              </b>
            </div>
          </div>
        </div>
        <div className="tutorial">
          <h5>Bạn có thể nạp tiền vào CarePay bằng những cách sau:</h5>
          <ul className="list-unstyled">
            <li>
              <b>
                1. Chuyển tiền theo số tài khoản từ dịch vụ internet
                banking/mobile banking của các ngân hàng
              </b>
              <p>+ Ngân hàng thụ hưởng: Ngân hàng Quân đội MB</p>
            </li>
            <li>
              <b>2. Nạp tiền mặt vào CarePay</b>
              <p>+ Tại quầy giao dịch của các ngân hàng</p>
            </li>
            <li>
              <b>3. Chuyển tiền từ tài khoản CarePay khác</b>
              <p>+ Theo hình thức chuyển tiền thông qua số điện thoại</p>
            </li>
          </ul>
        </div>
      </div>
    );
  }

  return (
    <Fragment>
      <Modal
        title="CarePay của tôi"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={openModalWallet}
        onCancel={() => setOpenModalWallet(false)}
      >
        <div className="wrap-panel-content my-wallet">
          <div className="wrap-panel-item">
            <BoxSkeleton
              skeleton={false}
              full={true}
              length={1}
              rows={25}
              data={yieldWallet()}
            />
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};

export default MyWallet;
