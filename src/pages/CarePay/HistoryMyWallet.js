import React, { Fragment, useEffect, useState } from "react";
import { Empty, Modal } from "antd";
import BoxSkeleton from "../../components/BoxSkeleton";
import { getHistoryPayment } from "../../services/CarePayService";
import Loading from "../../components/Loading";
import "./style.scss";
import moment from "moment";
import Pagination from "react-js-pagination";

const HistoryMyWallet = (props) => {
  const { openModalHistoryWallet, setOpenModalHistoryWallet } = props;
  const [histories, setHistories] = useState();
  const [page, setPage] = useState();
  useEffect(() => {
    const getApi = async () => {
      const histories = await getHistoryPayment({ page: page, limit: 10 });
      setHistories(histories.one_health_msg);
    };
    getApi();
  }, [page]);
  if (!histories) return <Loading open={false} />;
  const onNextPage = (page) => {
    setPage(page);
  };
  function yieldWallet() {
    if (Array.isArray(histories.listData) && histories.listData.length > 0) {
      return histories.listData.map((history, index) => (
        <div className="col-xl-6 col-lg-6 col-md-6 wrap-panel-col" key={index}>
          <div className="wrap-panel-item">
            <div className="wrap-item-info">
              <div className="wrap-line">
                <p className="label w-100px">{history.typeName}:</p>
                <p
                  className={`value ${
                    history.type === "cashin" ? "colorCreate" : "color-f96268"
                  } `}
                >
                  {(+history.amount).toLocaleString()}đ
                </p>
              </div>
              {history.orderName && (
                <div className="wrap-line">
                  <p className="label w-100px">Loại dịch vụ:</p>
                  <p className="value">{history.orderName}</p>
                </div>
              )}
              {history.hospitalName && (
                <div className="wrap-line">
                  <p className="label w-100px">Bệnh viện:</p>
                  <p className="value">{history.hospitalName}</p>
                </div>
              )}
              {history.sourceName && (
                <div className="wrap-line">
                  <p className="label w-100px">Chủ thẻ:</p>
                  <p className="value">{history.sourceName}</p>
                </div>
              )}
              {history.sourceNumber && (
                <div className="wrap-line">
                  <p className="label w-100px">Nạp từ thẻ:</p>
                  <p className="value">
                    {history.sourceNumber.substring(0, 4) +
                      " **** **** " +
                      history.sourceNumber.substring(
                        history.sourceNumber.length - 4,
                        history.sourceNumber.length
                      )}
                  </p>
                </div>
              )}
              <div className="wrap-line">
                <p className="label w-100px">Thời gian:</p>
                <p className="value">
                  {moment(new Date(history.createdAt)).format(
                    "HH:mm DD/MM/YYYY"
                  )}
                </p>
              </div>
            </div>
          </div>
        </div>
      ));
    } else {
      return (
        <Empty
          className="ant-empty-custom"
          description="Lịch sử hoạt động trống"
        />
      );
    }
  }

  return (
    <Fragment>
      <Modal
        title="Lịch sử hoạt động"
        closable={true}
        centered
        width={600}
        footer={null}
        visible={openModalHistoryWallet}
        onCancel={() => setOpenModalHistoryWallet(false)}
      >
        <div className="wrap-panel-content block-list-history-walet">
          <div className="wrap-panel-item row">
            <BoxSkeleton
              skeleton={false}
              full={true}
              length={1}
              rows={25}
              data={yieldWallet()}
            />
          </div>
          <div className={"block-pagination"}>
            <Pagination
              hideDisabled
              innerClass="pagination"
              activePage={page}
              itemsCountPerPage={10}
              totalItemsCount={histories.total}
              pageRangeDisplayed={4}
              onChange={onNextPage}
              itemClass="page-item"
              linkClass="page-link"
            />
          </div>
        </div>
      </Modal>
    </Fragment>
  );
};

export default HistoryMyWallet;
