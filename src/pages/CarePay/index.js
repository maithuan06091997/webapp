import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import Loading from "../../components/Loading";
import { url } from "../../constants/Path";
import { getMbBank } from "../../services/CarePayService";
import { IoIosArrowForward } from "react-icons/io";
import "./index.scss";
import MyWallet from "./MyWallet";
import { Button } from "antd";
import HistoryMyWallet from "./HistoryMyWallet";

const CarePay = ({ balance }) => {
  const [listCarePay, setListCarePay] = useState();
  const [openModalWallet, setOpenModalWallet] = useState();
  const [openModalHistoryWallet, setOpenModalHistoryWallet] = useState();

  useEffect(() => {
    const getApi = async () => {
      const listCarePay = await getMbBank();
      setListCarePay(listCarePay);
    };
    getApi();
  }, []);
  if (!listCarePay) return <Loading open={true} />;
  if (!listCarePay.one_health_msg.walletInfo)
    return (
      <div className="mt-3 care-pay">
        Bạn chưa đăng ký CarePay,tải app Dr.OH để tạo CarePay ngay{" "}
        <a
          href="https://play.google.com/store/apps/details?id=com.onehealth.DoctorOHApp&hl=vi&gl=US"
          target="__blank"
          className="color-00c7a7"
        >
          tại đây
        </a>
      </div>
    );
  return (
    <div className="care-pay w-100">
      <img src="./images/wallet/bg_main.png" alt="" className="bg-care-pay" />
      <div className="content">
        <div className="surplus">
          <p>Số dư (VND)</p>
          <h5>
            {(+listCarePay.one_health_msg.walletInfo
              .balanceWallet).toLocaleString()}
          </h5>
          <Link
            to={url.name.account_promotion}
            className="d-flex align-items-center justify-content-center"
          >
            <img src="./images/icons/icon_reward.png" alt="" />
            <span className="mx-2">{balance.point}</span>
            <IoIosArrowForward />
          </Link>
        </div>
        <div className="row">
          {/* <div className="col-6">
            <div className="pay">
              <div className="row">
                <div className="col-4 cp">
                  <Link to="#">
                    <img src="./images/wallet/ic_pay.png" alt="" />
                    <p>Thanh toán</p>
                  </Link>
                </div>
                <div className="col-4 cp">
                  <Link to="#">
                    <img src="./images/wallet/ic_transfer.png" alt="" />
                    <p>Chuyển khoản</p>
                  </Link>
                </div>
                <div className="col-4 cp">
                  <Link to="#">
                    <img src="./images/wallet/ic_wallet.png" alt="" />
                    <p>Nạp tiền</p>
                  </Link>
                </div>
              </div>
            </div>
          </div> */}
          <div className="col-12">
            <div className="info">
              <div>
                <img src="./images/wallet/ic_card1.png" alt="" />
                <b className="ml-2"> CarePay đã được tạo!</b>
              </div>
              <p>
                Tiếp tục sử dụng CarePay để thanh toán mà không cần sử dụng tiền
                mặt và tích lũy điểm thưởng.
              </p>
              <div className="row">
                <div className="col-6">
                  <Button
                    className="btn-booking w-100"
                    onClick={() => setOpenModalWallet(true)}
                  >
                    Thông tin CarePay của bạn
                  </Button>
                </div>
                <div className="col-6">
                  <Button
                    className="btn-booking w-100"
                    onClick={() => setOpenModalHistoryWallet(true)}
                  >
                    Lịch sử giao dịch
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <MyWallet
        openModalWallet={openModalWallet}
        setOpenModalWallet={setOpenModalWallet}
        sourceInfo={listCarePay.one_health_msg.walletInfo.sourceInfo}
      />
      <HistoryMyWallet
        openModalHistoryWallet={openModalHistoryWallet}
        setOpenModalHistoryWallet={setOpenModalHistoryWallet}
      />
    </div>
  );
};

export default withRouter(CarePay);
