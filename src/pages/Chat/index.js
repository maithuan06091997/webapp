import React, { Fragment, useEffect, useState } from "react";
import { public_url } from "../../constants/Path";
import {
  ModelGetListConversations,
  ModelGetListMessages,
  ModelSendMessage,
  ModelSearchUser,
} from "../../models/ModelChat";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import * as func from "../../helpers";
import "./style.scss";

import { setting } from "../../api/Config";
import io from "socket.io-client";
import { BsArrowLeftShort, BsSearch } from "react-icons/bs";
import Timeago from "../../components/Timeago";
import ConversationDetails from "../../components/Chat/ConversationDetails";
import { Skeleton } from "antd";

let socket = io(setting.ohDomainChat, { transports: ["websocket"] });
let localMessageId = -1;
let searchTimeout = null;
// type : 1 text, 2 image, 3 choice, 5 send text

const ChatPage = (props) => {
  const [listConversations, setListConversations] = useState([]);
  const [activeConversation, setActiveConversation] = useState(null);
  const [listMessages, setListMessages] = useState([]);
  const [loading, setLoading] = useState(false);
  const [listSearch, setListSearch] = useState([]);
  const [messagePage, setMessagePage] = useState(0);
  const [hasMore, setHasMore] = useState(false);
  const [activeSearch, setActiveSearch] = useState(false);
  const [searchedUsers, setSearchedUsers] = useState([]);
  const [searchLoading, setSearchLoading] = useState(false);

  //*** Declare props ***//
  const { user } = props;

  useEffect(() => {
    const id = user.personal._id;
    if (id) {
      let reqParamFt = {
        data: {
          user: {
            id: id,
          },
        },
      };
      ModelGetListConversations(reqParamFt)
        .then((resultFt) => {
          let resData =
            resultFt.data && resultFt.data.one_health_msg
              ? resultFt.data.one_health_msg
              : [];
          setListConversations(resData);
        })
        .catch(() => {});
    }
    onReceiveMessage();
  }, []);

  useEffect(() => {
    if (messagePage > 1) {
      getListMessages(messagePage, activeConversation._id, listMessages);
    }
  }, [messagePage]);

  // socket -----------------------------------
  const joinRoom = (currentUC) => {
    let fromUser = user.personal.username;
    let toUser = currentUC.to.username;
    let room = currentUC.room.room;
    let roomID = currentUC.room._id;

    let roomData = {
      from: {
        phone: fromUser,
      },
      to: {
        phone: toUser,
      },
      room: room,
      roomId: roomID,
    };
    socket.emit("join-room", roomData, function (err, result) {
      console.log("join room", result);
    });
  };
  // end socket -----------------------------------

  const getListMessages = (page, roomID, listMessages) => {
    const params = {
      data: {
        room: {
          id: roomID,
        },
      },
    };
    ModelGetListMessages(page, params)
      .then((response) => {
        setLoading(false);
        let data =
          response.data && response.data.one_health_msg
            ? response.data.one_health_msg
            : [];
        setListMessages(listMessages.concat(data));
        if (data.length >= 25) {
          setHasMore(true);
        } else {
          setHasMore(false);
        }
      })
      .catch(() => {
        setLoading(false);
        setHasMore(false);
      });
  };

  const onPressItemChat = (item) => {
    if (!activeConversation || activeConversation._id !== item._id) {
      setLoading(true);
      setMessagePage(1);
      setActiveConversation(item);
      setListMessages([]);
      joinRoom(item);

      setTimeout(() => {
        getListMessages(1, item._id, []);
      }, 500);
    }
  };

  const onPressItemSearchChat = (item) => {
    item.to = {
      avatar: item.avatar,
      username: item.username,
    };
    if (item.room && item.room._id) {
      // nothing
    } else {
      item.room = {
        _id: "",
        room: "",
      };
    }

    // setLoading(true);
    // setActiveConversation(item);
    // joinRoom(item);
  };

  const onChangeSearchKeyword = (e) => {
    let value = e.target.value;
    clearTimeout(searchTimeout);
    if (value === "") {
      setSearchedUsers([]);
    } else {
      searchTimeout = setTimeout(() => {
        setSearchLoading(true);
        let reqParamFt = {
          keyword: value,
          limit: 50,
          page: 1,
          userType: 3,
        };
        ModelSearchUser(reqParamFt)
          .then((resultFt) => {
            setSearchLoading(false);
            let resData =
              resultFt.data && resultFt.data.one_health_msg
                ? resultFt.data.one_health_msg
                : [];
            setSearchedUsers(resData);
          })
          .catch(() => {
            setSearchLoading(false);
          });
      }, 1000);
    }
  };

  const onFocusSearch = () => {
    setActiveSearch(true);
  };

  const loadMoreMessages = () => {
    setLoading(true);
    setMessagePage(messagePage + 1);
  };

  const onSendMessage = (text, type = 1) => {
    if (activeConversation) {
      const { room, sender, receiver } = activeConversation;
      if (room && sender && receiver && user) {
        const _id = localMessageId;
        localMessageId--;
        const messageReceiver =
          user.personal._id === sender._id ? receiver : sender;
        const params = {
          from: {
            phone: user.personal.username,
          },
          to: {
            phone: messageReceiver.username,
          },
          room: room.room,
          socketId: socket.id,
          type: 1,
          message: text,
        };
        if (type === 2) params.type = 2;
        let newListMessages = [];
        if (type === 2) {
          newListMessages = [
            {
              _id,
              created_time: new Date(),
              deleted: 0,
              disease_suggestion: [],
              images: [],
              message: text,
              modified_time: new Date(),
              receiver,
              room: room._id,
              sender: user.personal,
              service_suggestion: [],
              specialty_suggestion: [],
              status: 1,
              suggestion: [],
              type: 2,
              url: [],
              sending: true,
              paramsData: params,
            },
          ].concat(listMessages);
        } else {
          newListMessages = [
            {
              _id,
              created_time: new Date(),
              deleted: 0,
              disease_suggestion: [],
              images: [],
              message: text,
              modified_time: new Date(),
              receiver,
              room: room._id,
              sender: user.personal,
              service_suggestion: [],
              specialty_suggestion: [],
              status: 1,
              suggestion: [],
              type: 1,
              url: [],
              sending: true,
              paramsData: params,
            },
          ].concat(listMessages);
        }
        setListMessages(newListMessages);
        ModelSendMessage(params).then((response) => {
          let listResponseMessages = newListMessages.map((item) => {
            if (item._id === _id) {
              if (response.data && response.data.result == "true") {
                if (
                  response.data.one_health_msg &&
                  response.data.one_health_msg._id
                ) {
                  return response.data.one_health_msg;
                } else {
                  return {
                    ...item,
                    sending: false,
                  };
                }
              } else {
                return {
                  ...item,
                  error: true,
                  sending: false,
                };
              }
            }
            return item;
          });
          setListMessages(listResponseMessages);
        });
      }
    }
  };

  const onReceiveMessage = () => {
    socket.on("new-message", (data) => {
      const { sender, receiver } = activeConversation ? activeConversation : {};
      if (data && data.from && user) {
        if (data.from.username === user.personal.username) {
          setListMessages((listMessages) => {
            const existedIndex = listMessages
              ? listMessages.findIndex(
                  (obj) =>
                    (obj._id < 0 && obj.message === data.message) ||
                    data.type === 2
                )
              : -1;
            if (existedIndex != -1) {
              let newListMessages = listMessages.concat([]);
              newListMessages[existedIndex] = {
                ...newListMessages[existedIndex],
                _id: data.messageId,
                sending: false,
              };
              return newListMessages;
            } else {
              let message = {
                _id: data.messageId,
                created_time: data.created_time,
                deleted: 0,
                disease_suggestion: [],
                explaination: [],
                images: [],
                message: data.message,
                modified_time: data.created_time,
                receiver:
                  receiver && receiver.username == user.username
                    ? sender
                    : receiver,
                room: data.roomid,
                sender: data.from,
                service_suggestion: [],
                specialty_suggestion: [],
                status: 1,
                suggestion: [],
                type: data.type,
                url: [],
              };
              return [message].concat(listMessages);
            }
          });
        } else if (data.type === 2) {
          let message = {
            _id: data.messageId,
            created_time: data.created_time,
            deleted: 0,
            disease_suggestion: [],
            explaination: [],
            images: [],
            message: data.message,
            modified_time: data.created_time,
            receiver:
              receiver && receiver.username == user.username
                ? receiver
                : sender,
            room: data.roomid,
            sender: data.from,
            service_suggestion: [],
            specialty_suggestion: [],
            status: 1,
            suggestion: [],
            type: 2,
            url: [],
          };
          setListMessages((listMessages) => [message].concat(listMessages));
        } else {
          let message = {
            _id: data.messageId,
            created_time: data.created_time,
            deleted: 0,
            disease_suggestion: [],
            explaination: [],
            images: [],
            message: data.message,
            modified_time: data.created_time,
            receiver:
              receiver && receiver.username == user.username
                ? receiver
                : sender,
            room: data.roomid,
            sender: data.from,
            service_suggestion: [],
            specialty_suggestion: [],
            status: 1,
            suggestion: [],
            type: 1,
            url: [],
          };
          setListMessages((listMessages) => [message].concat(listMessages));
        }
      }
    });
  };

  const renderChatList = () => {
    return (
      <div className="list-conversations">
        <div className="search d-flex">
          {activeSearch ? (
            <button className="btn p-0" onClick={() => setActiveSearch(false)}>
              <BsArrowLeftShort fontSize="2rem" />
            </button>
          ) : null}
          <div className="input-group flex-fill">
            <input
              className="form-control"
              placeholder="Tìm kiếm ..."
              onChange={onChangeSearchKeyword}
              onFocus={onFocusSearch}
            />
            <BsSearch className="search-icon" />
          </div>
        </div>
        {activeSearch ? renderListSearched() : renderListConversations()}
      </div>
    );
  };

  const renderListSearched = () => {
    return searchLoading ? (
      <div className="p-3">
        <Skeleton.Avatar active size={40} shape={"circle"} className="mr-1" />
        <Skeleton.Input
          active
          style={{ width: 200, height: 20, borderRadius: 5 }}
        />
      </div>
    ) : searchedUsers && searchedUsers.length ? (
      searchedUsers.map((item) => {
        let name = func.getFullName(item);
        let url =
          item && item.avatar && item.avatar.url
            ? item.avatar.url
            : public_url.img + "/icons/icon_avatar.png";
        let selected = false;
        if (activeConversation && activeConversation._id === item._id) {
          selected = true;
        }
        if (item.username !== "chatbot") {
          return (
            <div
              className={`conversation-item ${selected ? "active" : ""}`}
              key={`searched_${item._id}`}
            >
              <img className="avatar" src={url} alt="" />
              <div
                className="flex-fill"
                onClick={() => onPressItemSearchChat(item)}
              >
                <div className="d-flex">
                  <p className="flex-fill name">{name}</p>
                </div>
                {item.type === 2 ? (
                  <span className="doctor-label">Bác sĩ</span>
                ) : null}
                {item.message ? (
                  <p className="message">{item.message}</p>
                ) : null}
              </div>
            </div>
          );
        }
        return null;
      })
    ) : null;
  };

  const renderListConversations = () => {
    return listConversations && listConversations.length
      ? listConversations.map((item) => {
          let userChat = item.to;
          let name = func.getFullName(userChat);
          let url =
            userChat && userChat.avatar && userChat.avatar.url
              ? userChat.avatar.url
              : public_url.img + "/icons/icon_avatar.png";
          let selected = false;
          if (activeConversation && activeConversation._id === item._id) {
            selected = true;
          }
          if (userChat.username !== "chatbot") {
            return (
              <div
                className={`conversation-item ${selected ? "active" : ""}`}
                key={`conversation_${item._id}`}
              >
                <img className="avatar" src={url} alt="" />
                <div
                  className="flex-fill"
                  onClick={() => onPressItemChat(item)}
                >
                  <div className="d-flex">
                    <p className="flex-fill name">{name}</p>
                    <Timeago
                      date={item.created_time}
                      format="short"
                      className="time text-muted"
                    />
                  </div>
                  {userChat.type === 2 ? (
                    <span className="doctor-label">Bác sĩ</span>
                  ) : null}
                  {item.message ? (
                    <p className="message">{item.message}</p>
                  ) : null}
                </div>
              </div>
            );
          }
          return null;
        })
      : null;
  };
  return (
    <Fragment>
      <div className="block-my-account">
        <div className="card card-custom">
          <div className="card-header">
            <h3>Danh sách trò chuyện</h3>
          </div>
          <div className="card-body">
            <div className="d-flex flex-fill h-100">
              {renderChatList()}
              {activeConversation ? (
                <ConversationDetails
                  conversation={activeConversation}
                  messages={listMessages}
                  hasMore={hasMore}
                  loadMore={loadMoreMessages}
                  isLoading={loading}
                  onSend={onSendMessage}
                  resendMessage={() => {}}
                  currentUser={user}
                />
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default withRouter(
  connect(null, null, null, { forwardRef: true })(ChatPage)
);
