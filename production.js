const express = require("express");
const path = require("path");
const morgan = require("morgan");
const cors = require("cors");
require('dotenv').config({path:__dirname+'/.env.production'});

const app       = express();
const pathName  = "build";
const rootPath  = process.env.REACT_APP_HOME_PAGE

app.use(cors());
app.use(rootPath, express.static(__dirname + "/" + pathName));
app.use(morgan("dev"));

app.get("*", function(req, res) {
    res.sendFile(path.resolve(__dirname, pathName, "index.html"));
});

var port = process.env.port || 6969;

app.listen(port, () => console.log(`Server is starting on port ${port}`));